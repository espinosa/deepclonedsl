package my.home.dsl.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import my.home.dsl.services.DeepCloneGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
@SuppressWarnings("all")
public class InternalDeepCloneParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_HEX", "RULE_INT", "RULE_DECIMAL", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'deepClone'", "'package'", "'.'", "'{'", "'}'", "'-'", "'&'", "'='", "'+='", "'||'", "'&&'", "'=='", "'!='", "'instanceof'", "'>='", "'<='", "'>'", "'<'", "'->'", "'..'", "'=>'", "'<>'", "'?:'", "'<=>'", "'+'", "'*'", "'**'", "'/'", "'%'", "'!'", "'as'", "'?.'", "'*.'", "','", "'('", "')'", "'['", "'|'", "']'", "';'", "'if'", "'else'", "'switch'", "':'", "'default'", "'case'", "'for'", "'while'", "'do'", "'var'", "'val'", "'super'", "'::'", "'new'", "'false'", "'true'", "'null'", "'typeof'", "'throw'", "'return'", "'try'", "'finally'", "'catch'", "'?'", "'extends'"
    };
    public static final int T__68=68;
    public static final int T__69=69;
    public static final int RULE_ID=4;
    public static final int T__66=66;
    public static final int T__67=67;
    public static final int T__64=64;
    public static final int T__29=29;
    public static final int T__65=65;
    public static final int T__28=28;
    public static final int T__62=62;
    public static final int T__27=27;
    public static final int T__63=63;
    public static final int T__26=26;
    public static final int T__25=25;
    public static final int T__24=24;
    public static final int T__23=23;
    public static final int T__22=22;
    public static final int RULE_ANY_OTHER=12;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int T__61=61;
    public static final int T__60=60;
    public static final int EOF=-1;
    public static final int T__55=55;
    public static final int T__56=56;
    public static final int T__19=19;
    public static final int T__57=57;
    public static final int RULE_HEX=6;
    public static final int T__58=58;
    public static final int T__16=16;
    public static final int T__51=51;
    public static final int T__15=15;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__18=18;
    public static final int T__54=54;
    public static final int T__17=17;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int T__59=59;
    public static final int RULE_INT=7;
    public static final int RULE_DECIMAL=8;
    public static final int T__50=50;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int RULE_SL_COMMENT=10;
    public static final int RULE_ML_COMMENT=9;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int RULE_STRING=5;
    public static final int T__32=32;
    public static final int T__71=71;
    public static final int T__33=33;
    public static final int T__72=72;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__70=70;
    public static final int T__36=36;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int RULE_WS=11;
    public static final int T__76=76;
    public static final int T__75=75;
    public static final int T__74=74;
    public static final int T__73=73;
    public static final int T__77=77;

    // delegates
    // delegators


        public InternalDeepCloneParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalDeepCloneParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalDeepCloneParser.tokenNames; }
    public String getGrammarFileName() { return "../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g"; }



     	private DeepCloneGrammarAccess grammarAccess;
     	
        public InternalDeepCloneParser(TokenStream input, DeepCloneGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "Model";	
       	}
       	
       	@Override
       	protected DeepCloneGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleModel"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:67:1: entryRuleModel returns [EObject current=null] : iv_ruleModel= ruleModel EOF ;
    public final EObject entryRuleModel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleModel = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:68:2: (iv_ruleModel= ruleModel EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:69:2: iv_ruleModel= ruleModel EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getModelRule()); 
            }
            pushFollow(FOLLOW_ruleModel_in_entryRuleModel75);
            iv_ruleModel=ruleModel();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleModel; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleModel85); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleModel"


    // $ANTLR start "ruleModel"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:76:1: ruleModel returns [EObject current=null] : ( ruleHeader this_Body_1= ruleBody ) ;
    public final EObject ruleModel() throws RecognitionException {
        EObject current = null;

        EObject this_Body_1 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:79:28: ( ( ruleHeader this_Body_1= ruleBody ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:80:1: ( ruleHeader this_Body_1= ruleBody )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:80:1: ( ruleHeader this_Body_1= ruleBody )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:81:5: ruleHeader this_Body_1= ruleBody
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getModelAccess().getHeaderParserRuleCall_0()); 
                  
            }
            pushFollow(FOLLOW_ruleHeader_in_ruleModel126);
            ruleHeader();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      afterParserOrEnumRuleCall();
                  
            }
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getModelAccess().getBodyParserRuleCall_1()); 
                  
            }
            pushFollow(FOLLOW_ruleBody_in_ruleModel147);
            this_Body_1=ruleBody();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_Body_1; 
                      afterParserOrEnumRuleCall();
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleModel"


    // $ANTLR start "entryRuleHeader"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:105:1: entryRuleHeader returns [String current=null] : iv_ruleHeader= ruleHeader EOF ;
    public final String entryRuleHeader() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleHeader = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:106:2: (iv_ruleHeader= ruleHeader EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:107:2: iv_ruleHeader= ruleHeader EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getHeaderRule()); 
            }
            pushFollow(FOLLOW_ruleHeader_in_entryRuleHeader183);
            iv_ruleHeader=ruleHeader();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleHeader.getText(); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleHeader194); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleHeader"


    // $ANTLR start "ruleHeader"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:114:1: ruleHeader returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : kw= 'deepClone' ;
    public final AntlrDatatypeRuleToken ruleHeader() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;

         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:117:28: (kw= 'deepClone' )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:119:2: kw= 'deepClone'
            {
            kw=(Token)match(input,13,FOLLOW_13_in_ruleHeader231); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                      current.merge(kw);
                      newLeafNode(kw, grammarAccess.getHeaderAccess().getDeepCloneKeyword()); 
                  
            }

            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleHeader"


    // $ANTLR start "entryRuleBody"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:132:1: entryRuleBody returns [EObject current=null] : iv_ruleBody= ruleBody EOF ;
    public final EObject entryRuleBody() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBody = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:133:2: (iv_ruleBody= ruleBody EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:134:2: iv_ruleBody= ruleBody EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBodyRule()); 
            }
            pushFollow(FOLLOW_ruleBody_in_entryRuleBody270);
            iv_ruleBody=ruleBody();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBody; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleBody280); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBody"


    // $ANTLR start "ruleBody"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:141:1: ruleBody returns [EObject current=null] : ( () ( (lv_packageConfig_1_0= rulePackageConfig ) )? ( (lv_cloners_2_0= ruleClassCloner ) )* ) ;
    public final EObject ruleBody() throws RecognitionException {
        EObject current = null;

        EObject lv_packageConfig_1_0 = null;

        EObject lv_cloners_2_0 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:144:28: ( ( () ( (lv_packageConfig_1_0= rulePackageConfig ) )? ( (lv_cloners_2_0= ruleClassCloner ) )* ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:145:1: ( () ( (lv_packageConfig_1_0= rulePackageConfig ) )? ( (lv_cloners_2_0= ruleClassCloner ) )* )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:145:1: ( () ( (lv_packageConfig_1_0= rulePackageConfig ) )? ( (lv_cloners_2_0= ruleClassCloner ) )* )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:145:2: () ( (lv_packageConfig_1_0= rulePackageConfig ) )? ( (lv_cloners_2_0= ruleClassCloner ) )*
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:145:2: ()
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:146:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getBodyAccess().getBodyAction_0(),
                          current);
                  
            }

            }

            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:151:2: ( (lv_packageConfig_1_0= rulePackageConfig ) )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==14) ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:152:1: (lv_packageConfig_1_0= rulePackageConfig )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:152:1: (lv_packageConfig_1_0= rulePackageConfig )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:153:3: lv_packageConfig_1_0= rulePackageConfig
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getBodyAccess().getPackageConfigPackageConfigParserRuleCall_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_rulePackageConfig_in_ruleBody335);
                    lv_packageConfig_1_0=rulePackageConfig();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getBodyRule());
                      	        }
                             		set(
                             			current, 
                             			"packageConfig",
                              		lv_packageConfig_1_0, 
                              		"PackageConfig");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }

            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:169:3: ( (lv_cloners_2_0= ruleClassCloner ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==RULE_ID||LA2_0==33||LA2_0==47) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:170:1: (lv_cloners_2_0= ruleClassCloner )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:170:1: (lv_cloners_2_0= ruleClassCloner )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:171:3: lv_cloners_2_0= ruleClassCloner
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getBodyAccess().getClonersClassClonerParserRuleCall_2_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleClassCloner_in_ruleBody357);
            	    lv_cloners_2_0=ruleClassCloner();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getBodyRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"cloners",
            	              		lv_cloners_2_0, 
            	              		"ClassCloner");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBody"


    // $ANTLR start "entryRulePackageConfig"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:195:1: entryRulePackageConfig returns [EObject current=null] : iv_rulePackageConfig= rulePackageConfig EOF ;
    public final EObject entryRulePackageConfig() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePackageConfig = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:196:2: (iv_rulePackageConfig= rulePackageConfig EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:197:2: iv_rulePackageConfig= rulePackageConfig EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getPackageConfigRule()); 
            }
            pushFollow(FOLLOW_rulePackageConfig_in_entryRulePackageConfig394);
            iv_rulePackageConfig=rulePackageConfig();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_rulePackageConfig; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRulePackageConfig404); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePackageConfig"


    // $ANTLR start "rulePackageConfig"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:204:1: rulePackageConfig returns [EObject current=null] : (otherlv_0= 'package' ( (lv_name_1_0= rulePackageNamespace ) ) ) ;
    public final EObject rulePackageConfig() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:207:28: ( (otherlv_0= 'package' ( (lv_name_1_0= rulePackageNamespace ) ) ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:208:1: (otherlv_0= 'package' ( (lv_name_1_0= rulePackageNamespace ) ) )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:208:1: (otherlv_0= 'package' ( (lv_name_1_0= rulePackageNamespace ) ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:208:3: otherlv_0= 'package' ( (lv_name_1_0= rulePackageNamespace ) )
            {
            otherlv_0=(Token)match(input,14,FOLLOW_14_in_rulePackageConfig441); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getPackageConfigAccess().getPackageKeyword_0());
                  
            }
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:212:1: ( (lv_name_1_0= rulePackageNamespace ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:213:1: (lv_name_1_0= rulePackageNamespace )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:213:1: (lv_name_1_0= rulePackageNamespace )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:214:3: lv_name_1_0= rulePackageNamespace
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getPackageConfigAccess().getNamePackageNamespaceParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_rulePackageNamespace_in_rulePackageConfig462);
            lv_name_1_0=rulePackageNamespace();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getPackageConfigRule());
              	        }
                     		set(
                     			current, 
                     			"name",
                      		lv_name_1_0, 
                      		"PackageNamespace");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePackageConfig"


    // $ANTLR start "entryRulePackageNamespace"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:238:1: entryRulePackageNamespace returns [String current=null] : iv_rulePackageNamespace= rulePackageNamespace EOF ;
    public final String entryRulePackageNamespace() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_rulePackageNamespace = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:239:2: (iv_rulePackageNamespace= rulePackageNamespace EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:240:2: iv_rulePackageNamespace= rulePackageNamespace EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getPackageNamespaceRule()); 
            }
            pushFollow(FOLLOW_rulePackageNamespace_in_entryRulePackageNamespace499);
            iv_rulePackageNamespace=rulePackageNamespace();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_rulePackageNamespace.getText(); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRulePackageNamespace510); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePackageNamespace"


    // $ANTLR start "rulePackageNamespace"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:247:1: rulePackageNamespace returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) ;
    public final AntlrDatatypeRuleToken rulePackageNamespace() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;

         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:250:28: ( (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:251:1: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:251:1: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:251:6: this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )*
            {
            this_ID_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_rulePackageNamespace550); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		current.merge(this_ID_0);
                  
            }
            if ( state.backtracking==0 ) {
               
                  newLeafNode(this_ID_0, grammarAccess.getPackageNamespaceAccess().getIDTerminalRuleCall_0()); 
                  
            }
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:258:1: (kw= '.' this_ID_2= RULE_ID )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==15) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:259:2: kw= '.' this_ID_2= RULE_ID
            	    {
            	    kw=(Token)match(input,15,FOLLOW_15_in_rulePackageNamespace569); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	              current.merge(kw);
            	              newLeafNode(kw, grammarAccess.getPackageNamespaceAccess().getFullStopKeyword_1_0()); 
            	          
            	    }
            	    this_ID_2=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_rulePackageNamespace584); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      		current.merge(this_ID_2);
            	          
            	    }
            	    if ( state.backtracking==0 ) {
            	       
            	          newLeafNode(this_ID_2, grammarAccess.getPackageNamespaceAccess().getIDTerminalRuleCall_1_1()); 
            	          
            	    }

            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePackageNamespace"


    // $ANTLR start "entryRuleClassCloner"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:279:1: entryRuleClassCloner returns [EObject current=null] : iv_ruleClassCloner= ruleClassCloner EOF ;
    public final EObject entryRuleClassCloner() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleClassCloner = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:280:2: (iv_ruleClassCloner= ruleClassCloner EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:281:2: iv_ruleClassCloner= ruleClassCloner EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getClassClonerRule()); 
            }
            pushFollow(FOLLOW_ruleClassCloner_in_entryRuleClassCloner631);
            iv_ruleClassCloner=ruleClassCloner();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleClassCloner; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleClassCloner641); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleClassCloner"


    // $ANTLR start "ruleClassCloner"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:288:1: ruleClassCloner returns [EObject current=null] : ( ( (lv_classToClone_0_0= ruleJvmTypeReference ) ) ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '{' ( (lv_fields_3_0= ruleFieldCloner ) )* otherlv_4= '}' ) ;
    public final EObject ruleClassCloner() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_classToClone_0_0 = null;

        EObject lv_fields_3_0 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:291:28: ( ( ( (lv_classToClone_0_0= ruleJvmTypeReference ) ) ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '{' ( (lv_fields_3_0= ruleFieldCloner ) )* otherlv_4= '}' ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:292:1: ( ( (lv_classToClone_0_0= ruleJvmTypeReference ) ) ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '{' ( (lv_fields_3_0= ruleFieldCloner ) )* otherlv_4= '}' )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:292:1: ( ( (lv_classToClone_0_0= ruleJvmTypeReference ) ) ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '{' ( (lv_fields_3_0= ruleFieldCloner ) )* otherlv_4= '}' )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:292:2: ( (lv_classToClone_0_0= ruleJvmTypeReference ) ) ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '{' ( (lv_fields_3_0= ruleFieldCloner ) )* otherlv_4= '}'
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:292:2: ( (lv_classToClone_0_0= ruleJvmTypeReference ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:293:1: (lv_classToClone_0_0= ruleJvmTypeReference )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:293:1: (lv_classToClone_0_0= ruleJvmTypeReference )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:294:3: lv_classToClone_0_0= ruleJvmTypeReference
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getClassClonerAccess().getClassToCloneJvmTypeReferenceParserRuleCall_0_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleJvmTypeReference_in_ruleClassCloner687);
            lv_classToClone_0_0=ruleJvmTypeReference();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getClassClonerRule());
              	        }
                     		set(
                     			current, 
                     			"classToClone",
                      		lv_classToClone_0_0, 
                      		"JvmTypeReference");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:310:2: ( (lv_name_1_0= RULE_ID ) )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==RULE_ID) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:311:1: (lv_name_1_0= RULE_ID )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:311:1: (lv_name_1_0= RULE_ID )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:312:3: lv_name_1_0= RULE_ID
                    {
                    lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleClassCloner704); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			newLeafNode(lv_name_1_0, grammarAccess.getClassClonerAccess().getNameIDTerminalRuleCall_1_0()); 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getClassClonerRule());
                      	        }
                             		setWithLastConsumed(
                             			current, 
                             			"name",
                              		lv_name_1_0, 
                              		"ID");
                      	    
                    }

                    }


                    }
                    break;

            }

            otherlv_2=(Token)match(input,16,FOLLOW_16_in_ruleClassCloner722); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getClassClonerAccess().getLeftCurlyBracketKeyword_2());
                  
            }
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:332:1: ( (lv_fields_3_0= ruleFieldCloner ) )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==RULE_ID||(LA5_0>=18 && LA5_0<=19)) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:333:1: (lv_fields_3_0= ruleFieldCloner )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:333:1: (lv_fields_3_0= ruleFieldCloner )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:334:3: lv_fields_3_0= ruleFieldCloner
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getClassClonerAccess().getFieldsFieldClonerParserRuleCall_3_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleFieldCloner_in_ruleClassCloner743);
            	    lv_fields_3_0=ruleFieldCloner();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getClassClonerRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"fields",
            	              		lv_fields_3_0, 
            	              		"FieldCloner");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

            otherlv_4=(Token)match(input,17,FOLLOW_17_in_ruleClassCloner756); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getClassClonerAccess().getRightCurlyBracketKeyword_4());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleClassCloner"


    // $ANTLR start "entryRuleFieldCloner"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:362:1: entryRuleFieldCloner returns [EObject current=null] : iv_ruleFieldCloner= ruleFieldCloner EOF ;
    public final EObject entryRuleFieldCloner() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFieldCloner = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:363:2: (iv_ruleFieldCloner= ruleFieldCloner EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:364:2: iv_ruleFieldCloner= ruleFieldCloner EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFieldClonerRule()); 
            }
            pushFollow(FOLLOW_ruleFieldCloner_in_entryRuleFieldCloner792);
            iv_ruleFieldCloner=ruleFieldCloner();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFieldCloner; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleFieldCloner802); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFieldCloner"


    // $ANTLR start "ruleFieldCloner"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:371:1: ruleFieldCloner returns [EObject current=null] : (this_SimpleField_0= ruleSimpleField | this_SimpleExcludedField_1= ruleSimpleExcludedField | this_ComplexField_2= ruleComplexField | this_ReferenceField_3= ruleReferenceField ) ;
    public final EObject ruleFieldCloner() throws RecognitionException {
        EObject current = null;

        EObject this_SimpleField_0 = null;

        EObject this_SimpleExcludedField_1 = null;

        EObject this_ComplexField_2 = null;

        EObject this_ReferenceField_3 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:374:28: ( (this_SimpleField_0= ruleSimpleField | this_SimpleExcludedField_1= ruleSimpleExcludedField | this_ComplexField_2= ruleComplexField | this_ReferenceField_3= ruleReferenceField ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:375:1: (this_SimpleField_0= ruleSimpleField | this_SimpleExcludedField_1= ruleSimpleExcludedField | this_ComplexField_2= ruleComplexField | this_ReferenceField_3= ruleReferenceField )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:375:1: (this_SimpleField_0= ruleSimpleField | this_SimpleExcludedField_1= ruleSimpleExcludedField | this_ComplexField_2= ruleComplexField | this_ReferenceField_3= ruleReferenceField )
            int alt6=4;
            switch ( input.LA(1) ) {
            case RULE_ID:
                {
                int LA6_1 = input.LA(2);

                if ( (LA6_1==EOF||LA6_1==RULE_ID||(LA6_1>=17 && LA6_1<=19)) ) {
                    alt6=1;
                }
                else if ( (LA6_1==16) ) {
                    alt6=3;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 6, 1, input);

                    throw nvae;
                }
                }
                break;
            case 18:
                {
                alt6=2;
                }
                break;
            case 19:
                {
                alt6=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }

            switch (alt6) {
                case 1 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:376:5: this_SimpleField_0= ruleSimpleField
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getFieldClonerAccess().getSimpleFieldParserRuleCall_0()); 
                          
                    }
                    pushFollow(FOLLOW_ruleSimpleField_in_ruleFieldCloner849);
                    this_SimpleField_0=ruleSimpleField();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_SimpleField_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:386:5: this_SimpleExcludedField_1= ruleSimpleExcludedField
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getFieldClonerAccess().getSimpleExcludedFieldParserRuleCall_1()); 
                          
                    }
                    pushFollow(FOLLOW_ruleSimpleExcludedField_in_ruleFieldCloner876);
                    this_SimpleExcludedField_1=ruleSimpleExcludedField();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_SimpleExcludedField_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:396:5: this_ComplexField_2= ruleComplexField
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getFieldClonerAccess().getComplexFieldParserRuleCall_2()); 
                          
                    }
                    pushFollow(FOLLOW_ruleComplexField_in_ruleFieldCloner903);
                    this_ComplexField_2=ruleComplexField();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ComplexField_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 4 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:406:5: this_ReferenceField_3= ruleReferenceField
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getFieldClonerAccess().getReferenceFieldParserRuleCall_3()); 
                          
                    }
                    pushFollow(FOLLOW_ruleReferenceField_in_ruleFieldCloner930);
                    this_ReferenceField_3=ruleReferenceField();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ReferenceField_3; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFieldCloner"


    // $ANTLR start "entryRuleSimpleField"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:422:1: entryRuleSimpleField returns [EObject current=null] : iv_ruleSimpleField= ruleSimpleField EOF ;
    public final EObject entryRuleSimpleField() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSimpleField = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:423:2: (iv_ruleSimpleField= ruleSimpleField EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:424:2: iv_ruleSimpleField= ruleSimpleField EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getSimpleFieldRule()); 
            }
            pushFollow(FOLLOW_ruleSimpleField_in_entryRuleSimpleField965);
            iv_ruleSimpleField=ruleSimpleField();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleSimpleField; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleSimpleField975); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSimpleField"


    // $ANTLR start "ruleSimpleField"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:431:1: ruleSimpleField returns [EObject current=null] : ( (lv_fieldName_0_0= RULE_ID ) ) ;
    public final EObject ruleSimpleField() throws RecognitionException {
        EObject current = null;

        Token lv_fieldName_0_0=null;

         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:434:28: ( ( (lv_fieldName_0_0= RULE_ID ) ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:435:1: ( (lv_fieldName_0_0= RULE_ID ) )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:435:1: ( (lv_fieldName_0_0= RULE_ID ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:436:1: (lv_fieldName_0_0= RULE_ID )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:436:1: (lv_fieldName_0_0= RULE_ID )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:437:3: lv_fieldName_0_0= RULE_ID
            {
            lv_fieldName_0_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleSimpleField1016); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_fieldName_0_0, grammarAccess.getSimpleFieldAccess().getFieldNameIDTerminalRuleCall_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getSimpleFieldRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"fieldName",
                      		lv_fieldName_0_0, 
                      		"ID");
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSimpleField"


    // $ANTLR start "entryRuleSimpleExcludedField"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:461:1: entryRuleSimpleExcludedField returns [EObject current=null] : iv_ruleSimpleExcludedField= ruleSimpleExcludedField EOF ;
    public final EObject entryRuleSimpleExcludedField() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSimpleExcludedField = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:462:2: (iv_ruleSimpleExcludedField= ruleSimpleExcludedField EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:463:2: iv_ruleSimpleExcludedField= ruleSimpleExcludedField EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getSimpleExcludedFieldRule()); 
            }
            pushFollow(FOLLOW_ruleSimpleExcludedField_in_entryRuleSimpleExcludedField1056);
            iv_ruleSimpleExcludedField=ruleSimpleExcludedField();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleSimpleExcludedField; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleSimpleExcludedField1066); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSimpleExcludedField"


    // $ANTLR start "ruleSimpleExcludedField"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:470:1: ruleSimpleExcludedField returns [EObject current=null] : (otherlv_0= '-' ( (lv_fieldName_1_0= RULE_ID ) ) ) ;
    public final EObject ruleSimpleExcludedField() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_fieldName_1_0=null;

         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:473:28: ( (otherlv_0= '-' ( (lv_fieldName_1_0= RULE_ID ) ) ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:474:1: (otherlv_0= '-' ( (lv_fieldName_1_0= RULE_ID ) ) )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:474:1: (otherlv_0= '-' ( (lv_fieldName_1_0= RULE_ID ) ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:474:3: otherlv_0= '-' ( (lv_fieldName_1_0= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,18,FOLLOW_18_in_ruleSimpleExcludedField1103); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getSimpleExcludedFieldAccess().getHyphenMinusKeyword_0());
                  
            }
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:478:1: ( (lv_fieldName_1_0= RULE_ID ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:479:1: (lv_fieldName_1_0= RULE_ID )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:479:1: (lv_fieldName_1_0= RULE_ID )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:480:3: lv_fieldName_1_0= RULE_ID
            {
            lv_fieldName_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleSimpleExcludedField1120); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_fieldName_1_0, grammarAccess.getSimpleExcludedFieldAccess().getFieldNameIDTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getSimpleExcludedFieldRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"fieldName",
                      		lv_fieldName_1_0, 
                      		"ID");
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSimpleExcludedField"


    // $ANTLR start "entryRuleComplexField"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:504:1: entryRuleComplexField returns [EObject current=null] : iv_ruleComplexField= ruleComplexField EOF ;
    public final EObject entryRuleComplexField() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleComplexField = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:505:2: (iv_ruleComplexField= ruleComplexField EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:506:2: iv_ruleComplexField= ruleComplexField EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getComplexFieldRule()); 
            }
            pushFollow(FOLLOW_ruleComplexField_in_entryRuleComplexField1161);
            iv_ruleComplexField=ruleComplexField();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleComplexField; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleComplexField1171); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleComplexField"


    // $ANTLR start "ruleComplexField"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:513:1: ruleComplexField returns [EObject current=null] : ( ( (lv_fieldName_0_0= RULE_ID ) ) otherlv_1= '{' ( (lv_fields_2_0= ruleFieldCloner ) )* otherlv_3= '}' ) ;
    public final EObject ruleComplexField() throws RecognitionException {
        EObject current = null;

        Token lv_fieldName_0_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_fields_2_0 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:516:28: ( ( ( (lv_fieldName_0_0= RULE_ID ) ) otherlv_1= '{' ( (lv_fields_2_0= ruleFieldCloner ) )* otherlv_3= '}' ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:517:1: ( ( (lv_fieldName_0_0= RULE_ID ) ) otherlv_1= '{' ( (lv_fields_2_0= ruleFieldCloner ) )* otherlv_3= '}' )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:517:1: ( ( (lv_fieldName_0_0= RULE_ID ) ) otherlv_1= '{' ( (lv_fields_2_0= ruleFieldCloner ) )* otherlv_3= '}' )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:517:2: ( (lv_fieldName_0_0= RULE_ID ) ) otherlv_1= '{' ( (lv_fields_2_0= ruleFieldCloner ) )* otherlv_3= '}'
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:517:2: ( (lv_fieldName_0_0= RULE_ID ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:518:1: (lv_fieldName_0_0= RULE_ID )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:518:1: (lv_fieldName_0_0= RULE_ID )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:519:3: lv_fieldName_0_0= RULE_ID
            {
            lv_fieldName_0_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleComplexField1213); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_fieldName_0_0, grammarAccess.getComplexFieldAccess().getFieldNameIDTerminalRuleCall_0_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getComplexFieldRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"fieldName",
                      		lv_fieldName_0_0, 
                      		"ID");
              	    
            }

            }


            }

            otherlv_1=(Token)match(input,16,FOLLOW_16_in_ruleComplexField1230); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getComplexFieldAccess().getLeftCurlyBracketKeyword_1());
                  
            }
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:539:1: ( (lv_fields_2_0= ruleFieldCloner ) )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==RULE_ID||(LA7_0>=18 && LA7_0<=19)) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:540:1: (lv_fields_2_0= ruleFieldCloner )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:540:1: (lv_fields_2_0= ruleFieldCloner )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:541:3: lv_fields_2_0= ruleFieldCloner
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getComplexFieldAccess().getFieldsFieldClonerParserRuleCall_2_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleFieldCloner_in_ruleComplexField1251);
            	    lv_fields_2_0=ruleFieldCloner();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getComplexFieldRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"fields",
            	              		lv_fields_2_0, 
            	              		"FieldCloner");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

            otherlv_3=(Token)match(input,17,FOLLOW_17_in_ruleComplexField1264); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getComplexFieldAccess().getRightCurlyBracketKeyword_3());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleComplexField"


    // $ANTLR start "entryRuleReferenceField"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:569:1: entryRuleReferenceField returns [EObject current=null] : iv_ruleReferenceField= ruleReferenceField EOF ;
    public final EObject entryRuleReferenceField() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleReferenceField = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:570:2: (iv_ruleReferenceField= ruleReferenceField EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:571:2: iv_ruleReferenceField= ruleReferenceField EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getReferenceFieldRule()); 
            }
            pushFollow(FOLLOW_ruleReferenceField_in_entryRuleReferenceField1300);
            iv_ruleReferenceField=ruleReferenceField();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleReferenceField; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleReferenceField1310); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleReferenceField"


    // $ANTLR start "ruleReferenceField"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:578:1: ruleReferenceField returns [EObject current=null] : (otherlv_0= '&' ( (lv_fieldName_1_0= RULE_ID ) ) ( (otherlv_2= RULE_ID ) ) ) ;
    public final EObject ruleReferenceField() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_fieldName_1_0=null;
        Token otherlv_2=null;

         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:581:28: ( (otherlv_0= '&' ( (lv_fieldName_1_0= RULE_ID ) ) ( (otherlv_2= RULE_ID ) ) ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:582:1: (otherlv_0= '&' ( (lv_fieldName_1_0= RULE_ID ) ) ( (otherlv_2= RULE_ID ) ) )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:582:1: (otherlv_0= '&' ( (lv_fieldName_1_0= RULE_ID ) ) ( (otherlv_2= RULE_ID ) ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:582:3: otherlv_0= '&' ( (lv_fieldName_1_0= RULE_ID ) ) ( (otherlv_2= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,19,FOLLOW_19_in_ruleReferenceField1347); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getReferenceFieldAccess().getAmpersandKeyword_0());
                  
            }
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:586:1: ( (lv_fieldName_1_0= RULE_ID ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:587:1: (lv_fieldName_1_0= RULE_ID )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:587:1: (lv_fieldName_1_0= RULE_ID )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:588:3: lv_fieldName_1_0= RULE_ID
            {
            lv_fieldName_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleReferenceField1364); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_fieldName_1_0, grammarAccess.getReferenceFieldAccess().getFieldNameIDTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getReferenceFieldRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"fieldName",
                      		lv_fieldName_1_0, 
                      		"ID");
              	    
            }

            }


            }

            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:604:2: ( (otherlv_2= RULE_ID ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:605:1: (otherlv_2= RULE_ID )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:605:1: (otherlv_2= RULE_ID )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:606:3: otherlv_2= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getReferenceFieldRule());
              	        }
                      
            }
            otherlv_2=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleReferenceField1389); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_2, grammarAccess.getReferenceFieldAccess().getClonerReferenceClassClonerCrossReference_2_0()); 
              	
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleReferenceField"


    // $ANTLR start "entryRuleContainerType"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:629:1: entryRuleContainerType returns [EObject current=null] : iv_ruleContainerType= ruleContainerType EOF ;
    public final EObject entryRuleContainerType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleContainerType = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:630:2: (iv_ruleContainerType= ruleContainerType EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:631:2: iv_ruleContainerType= ruleContainerType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getContainerTypeRule()); 
            }
            pushFollow(FOLLOW_ruleContainerType_in_entryRuleContainerType1429);
            iv_ruleContainerType=ruleContainerType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleContainerType; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleContainerType1439); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleContainerType"


    // $ANTLR start "ruleContainerType"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:638:1: ruleContainerType returns [EObject current=null] : ( () ( (lv_fields_1_0= ruleFieldCloner ) )* ) ;
    public final EObject ruleContainerType() throws RecognitionException {
        EObject current = null;

        EObject lv_fields_1_0 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:641:28: ( ( () ( (lv_fields_1_0= ruleFieldCloner ) )* ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:642:1: ( () ( (lv_fields_1_0= ruleFieldCloner ) )* )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:642:1: ( () ( (lv_fields_1_0= ruleFieldCloner ) )* )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:642:2: () ( (lv_fields_1_0= ruleFieldCloner ) )*
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:642:2: ()
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:643:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getContainerTypeAccess().getContainerTypeAction_0(),
                          current);
                  
            }

            }

            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:648:2: ( (lv_fields_1_0= ruleFieldCloner ) )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==RULE_ID||(LA8_0>=18 && LA8_0<=19)) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:649:1: (lv_fields_1_0= ruleFieldCloner )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:649:1: (lv_fields_1_0= ruleFieldCloner )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:650:3: lv_fields_1_0= ruleFieldCloner
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getContainerTypeAccess().getFieldsFieldClonerParserRuleCall_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleFieldCloner_in_ruleContainerType1494);
            	    lv_fields_1_0=ruleFieldCloner();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getContainerTypeRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"fields",
            	              		lv_fields_1_0, 
            	              		"FieldCloner");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleContainerType"


    // $ANTLR start "entryRuleFieldClonerType"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:676:1: entryRuleFieldClonerType returns [EObject current=null] : iv_ruleFieldClonerType= ruleFieldClonerType EOF ;
    public final EObject entryRuleFieldClonerType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFieldClonerType = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:677:2: (iv_ruleFieldClonerType= ruleFieldClonerType EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:678:2: iv_ruleFieldClonerType= ruleFieldClonerType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFieldClonerTypeRule()); 
            }
            pushFollow(FOLLOW_ruleFieldClonerType_in_entryRuleFieldClonerType1533);
            iv_ruleFieldClonerType=ruleFieldClonerType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFieldClonerType; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleFieldClonerType1543); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFieldClonerType"


    // $ANTLR start "ruleFieldClonerType"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:685:1: ruleFieldClonerType returns [EObject current=null] : ( () ( (lv_fieldName_1_0= RULE_ID ) ) ) ;
    public final EObject ruleFieldClonerType() throws RecognitionException {
        EObject current = null;

        Token lv_fieldName_1_0=null;

         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:688:28: ( ( () ( (lv_fieldName_1_0= RULE_ID ) ) ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:689:1: ( () ( (lv_fieldName_1_0= RULE_ID ) ) )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:689:1: ( () ( (lv_fieldName_1_0= RULE_ID ) ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:689:2: () ( (lv_fieldName_1_0= RULE_ID ) )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:689:2: ()
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:690:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getFieldClonerTypeAccess().getFieldClonerTypeAction_0(),
                          current);
                  
            }

            }

            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:695:2: ( (lv_fieldName_1_0= RULE_ID ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:696:1: (lv_fieldName_1_0= RULE_ID )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:696:1: (lv_fieldName_1_0= RULE_ID )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:697:3: lv_fieldName_1_0= RULE_ID
            {
            lv_fieldName_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleFieldClonerType1594); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_fieldName_1_0, grammarAccess.getFieldClonerTypeAccess().getFieldNameIDTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getFieldClonerTypeRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"fieldName",
                      		lv_fieldName_1_0, 
                      		"ID");
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFieldClonerType"


    // $ANTLR start "entryRuleXExpression"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:723:1: entryRuleXExpression returns [EObject current=null] : iv_ruleXExpression= ruleXExpression EOF ;
    public final EObject entryRuleXExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXExpression = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:724:2: (iv_ruleXExpression= ruleXExpression EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:725:2: iv_ruleXExpression= ruleXExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getXExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleXExpression_in_entryRuleXExpression1637);
            iv_ruleXExpression=ruleXExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleXExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleXExpression1647); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXExpression"


    // $ANTLR start "ruleXExpression"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:732:1: ruleXExpression returns [EObject current=null] : this_XAssignment_0= ruleXAssignment ;
    public final EObject ruleXExpression() throws RecognitionException {
        EObject current = null;

        EObject this_XAssignment_0 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:735:28: (this_XAssignment_0= ruleXAssignment )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:737:5: this_XAssignment_0= ruleXAssignment
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getXExpressionAccess().getXAssignmentParserRuleCall()); 
                  
            }
            pushFollow(FOLLOW_ruleXAssignment_in_ruleXExpression1693);
            this_XAssignment_0=ruleXAssignment();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_XAssignment_0; 
                      afterParserOrEnumRuleCall();
                  
            }

            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXExpression"


    // $ANTLR start "entryRuleXAssignment"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:753:1: entryRuleXAssignment returns [EObject current=null] : iv_ruleXAssignment= ruleXAssignment EOF ;
    public final EObject entryRuleXAssignment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXAssignment = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:754:2: (iv_ruleXAssignment= ruleXAssignment EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:755:2: iv_ruleXAssignment= ruleXAssignment EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getXAssignmentRule()); 
            }
            pushFollow(FOLLOW_ruleXAssignment_in_entryRuleXAssignment1727);
            iv_ruleXAssignment=ruleXAssignment();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleXAssignment; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleXAssignment1737); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXAssignment"


    // $ANTLR start "ruleXAssignment"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:762:1: ruleXAssignment returns [EObject current=null] : ( ( () ( ( ruleValidID ) ) ruleOpSingleAssign ( (lv_value_3_0= ruleXAssignment ) ) ) | (this_XOrExpression_4= ruleXOrExpression ( ( ( ( () ( ( ruleOpMultiAssign ) ) ) )=> ( () ( ( ruleOpMultiAssign ) ) ) ) ( (lv_rightOperand_7_0= ruleXAssignment ) ) )? ) ) ;
    public final EObject ruleXAssignment() throws RecognitionException {
        EObject current = null;

        EObject lv_value_3_0 = null;

        EObject this_XOrExpression_4 = null;

        EObject lv_rightOperand_7_0 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:765:28: ( ( ( () ( ( ruleValidID ) ) ruleOpSingleAssign ( (lv_value_3_0= ruleXAssignment ) ) ) | (this_XOrExpression_4= ruleXOrExpression ( ( ( ( () ( ( ruleOpMultiAssign ) ) ) )=> ( () ( ( ruleOpMultiAssign ) ) ) ) ( (lv_rightOperand_7_0= ruleXAssignment ) ) )? ) ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:766:1: ( ( () ( ( ruleValidID ) ) ruleOpSingleAssign ( (lv_value_3_0= ruleXAssignment ) ) ) | (this_XOrExpression_4= ruleXOrExpression ( ( ( ( () ( ( ruleOpMultiAssign ) ) ) )=> ( () ( ( ruleOpMultiAssign ) ) ) ) ( (lv_rightOperand_7_0= ruleXAssignment ) ) )? ) )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:766:1: ( ( () ( ( ruleValidID ) ) ruleOpSingleAssign ( (lv_value_3_0= ruleXAssignment ) ) ) | (this_XOrExpression_4= ruleXOrExpression ( ( ( ( () ( ( ruleOpMultiAssign ) ) ) )=> ( () ( ( ruleOpMultiAssign ) ) ) ) ( (lv_rightOperand_7_0= ruleXAssignment ) ) )? ) )
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==RULE_ID) ) {
                int LA10_1 = input.LA(2);

                if ( (LA10_1==EOF||(LA10_1>=RULE_ID && LA10_1<=RULE_DECIMAL)||(LA10_1>=15 && LA10_1<=18)||(LA10_1>=21 && LA10_1<=49)||(LA10_1>=51 && LA10_1<=75)) ) {
                    alt10=2;
                }
                else if ( (LA10_1==20) ) {
                    alt10=1;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 10, 1, input);

                    throw nvae;
                }
            }
            else if ( ((LA10_0>=RULE_STRING && LA10_0<=RULE_DECIMAL)||LA10_0==16||LA10_0==18||LA10_0==30||LA10_0==37||LA10_0==42||LA10_0==47||LA10_0==49||LA10_0==53||LA10_0==55||(LA10_0>=59 && LA10_0<=61)||LA10_0==64||(LA10_0>=66 && LA10_0<=73)) ) {
                alt10=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }
            switch (alt10) {
                case 1 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:766:2: ( () ( ( ruleValidID ) ) ruleOpSingleAssign ( (lv_value_3_0= ruleXAssignment ) ) )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:766:2: ( () ( ( ruleValidID ) ) ruleOpSingleAssign ( (lv_value_3_0= ruleXAssignment ) ) )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:766:3: () ( ( ruleValidID ) ) ruleOpSingleAssign ( (lv_value_3_0= ruleXAssignment ) )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:766:3: ()
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:767:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getXAssignmentAccess().getXAssignmentAction_0_0(),
                                  current);
                          
                    }

                    }

                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:772:2: ( ( ruleValidID ) )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:773:1: ( ruleValidID )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:773:1: ( ruleValidID )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:774:3: ruleValidID
                    {
                    if ( state.backtracking==0 ) {

                      			if (current==null) {
                      	            current = createModelElement(grammarAccess.getXAssignmentRule());
                      	        }
                              
                    }
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getXAssignmentAccess().getFeatureJvmIdentifiableElementCrossReference_0_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleValidID_in_ruleXAssignment1795);
                    ruleValidID();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getXAssignmentAccess().getOpSingleAssignParserRuleCall_0_2()); 
                          
                    }
                    pushFollow(FOLLOW_ruleOpSingleAssign_in_ruleXAssignment1811);
                    ruleOpSingleAssign();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              afterParserOrEnumRuleCall();
                          
                    }
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:795:1: ( (lv_value_3_0= ruleXAssignment ) )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:796:1: (lv_value_3_0= ruleXAssignment )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:796:1: (lv_value_3_0= ruleXAssignment )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:797:3: lv_value_3_0= ruleXAssignment
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getXAssignmentAccess().getValueXAssignmentParserRuleCall_0_3_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleXAssignment_in_ruleXAssignment1831);
                    lv_value_3_0=ruleXAssignment();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getXAssignmentRule());
                      	        }
                             		set(
                             			current, 
                             			"value",
                              		lv_value_3_0, 
                              		"XAssignment");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:814:6: (this_XOrExpression_4= ruleXOrExpression ( ( ( ( () ( ( ruleOpMultiAssign ) ) ) )=> ( () ( ( ruleOpMultiAssign ) ) ) ) ( (lv_rightOperand_7_0= ruleXAssignment ) ) )? )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:814:6: (this_XOrExpression_4= ruleXOrExpression ( ( ( ( () ( ( ruleOpMultiAssign ) ) ) )=> ( () ( ( ruleOpMultiAssign ) ) ) ) ( (lv_rightOperand_7_0= ruleXAssignment ) ) )? )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:815:5: this_XOrExpression_4= ruleXOrExpression ( ( ( ( () ( ( ruleOpMultiAssign ) ) ) )=> ( () ( ( ruleOpMultiAssign ) ) ) ) ( (lv_rightOperand_7_0= ruleXAssignment ) ) )?
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getXAssignmentAccess().getXOrExpressionParserRuleCall_1_0()); 
                          
                    }
                    pushFollow(FOLLOW_ruleXOrExpression_in_ruleXAssignment1861);
                    this_XOrExpression_4=ruleXOrExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_XOrExpression_4; 
                              afterParserOrEnumRuleCall();
                          
                    }
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:823:1: ( ( ( ( () ( ( ruleOpMultiAssign ) ) ) )=> ( () ( ( ruleOpMultiAssign ) ) ) ) ( (lv_rightOperand_7_0= ruleXAssignment ) ) )?
                    int alt9=2;
                    int LA9_0 = input.LA(1);

                    if ( (LA9_0==21) ) {
                        int LA9_1 = input.LA(2);

                        if ( (synpred1_InternalDeepClone()) ) {
                            alt9=1;
                        }
                    }
                    switch (alt9) {
                        case 1 :
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:823:2: ( ( ( () ( ( ruleOpMultiAssign ) ) ) )=> ( () ( ( ruleOpMultiAssign ) ) ) ) ( (lv_rightOperand_7_0= ruleXAssignment ) )
                            {
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:823:2: ( ( ( () ( ( ruleOpMultiAssign ) ) ) )=> ( () ( ( ruleOpMultiAssign ) ) ) )
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:823:3: ( ( () ( ( ruleOpMultiAssign ) ) ) )=> ( () ( ( ruleOpMultiAssign ) ) )
                            {
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:828:6: ( () ( ( ruleOpMultiAssign ) ) )
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:828:7: () ( ( ruleOpMultiAssign ) )
                            {
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:828:7: ()
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:829:5: 
                            {
                            if ( state.backtracking==0 ) {

                                      current = forceCreateModelElementAndSet(
                                          grammarAccess.getXAssignmentAccess().getXBinaryOperationLeftOperandAction_1_1_0_0_0(),
                                          current);
                                  
                            }

                            }

                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:834:2: ( ( ruleOpMultiAssign ) )
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:835:1: ( ruleOpMultiAssign )
                            {
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:835:1: ( ruleOpMultiAssign )
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:836:3: ruleOpMultiAssign
                            {
                            if ( state.backtracking==0 ) {

                              			if (current==null) {
                              	            current = createModelElement(grammarAccess.getXAssignmentRule());
                              	        }
                                      
                            }
                            if ( state.backtracking==0 ) {
                               
                              	        newCompositeNode(grammarAccess.getXAssignmentAccess().getFeatureJvmIdentifiableElementCrossReference_1_1_0_0_1_0()); 
                              	    
                            }
                            pushFollow(FOLLOW_ruleOpMultiAssign_in_ruleXAssignment1914);
                            ruleOpMultiAssign();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {
                               
                              	        afterParserOrEnumRuleCall();
                              	    
                            }

                            }


                            }


                            }


                            }

                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:849:4: ( (lv_rightOperand_7_0= ruleXAssignment ) )
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:850:1: (lv_rightOperand_7_0= ruleXAssignment )
                            {
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:850:1: (lv_rightOperand_7_0= ruleXAssignment )
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:851:3: lv_rightOperand_7_0= ruleXAssignment
                            {
                            if ( state.backtracking==0 ) {
                               
                              	        newCompositeNode(grammarAccess.getXAssignmentAccess().getRightOperandXAssignmentParserRuleCall_1_1_1_0()); 
                              	    
                            }
                            pushFollow(FOLLOW_ruleXAssignment_in_ruleXAssignment1937);
                            lv_rightOperand_7_0=ruleXAssignment();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElementForParent(grammarAccess.getXAssignmentRule());
                              	        }
                                     		set(
                                     			current, 
                                     			"rightOperand",
                                      		lv_rightOperand_7_0, 
                                      		"XAssignment");
                              	        afterParserOrEnumRuleCall();
                              	    
                            }

                            }


                            }


                            }
                            break;

                    }


                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXAssignment"


    // $ANTLR start "entryRuleOpSingleAssign"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:875:1: entryRuleOpSingleAssign returns [String current=null] : iv_ruleOpSingleAssign= ruleOpSingleAssign EOF ;
    public final String entryRuleOpSingleAssign() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleOpSingleAssign = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:876:2: (iv_ruleOpSingleAssign= ruleOpSingleAssign EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:877:2: iv_ruleOpSingleAssign= ruleOpSingleAssign EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getOpSingleAssignRule()); 
            }
            pushFollow(FOLLOW_ruleOpSingleAssign_in_entryRuleOpSingleAssign1977);
            iv_ruleOpSingleAssign=ruleOpSingleAssign();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleOpSingleAssign.getText(); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleOpSingleAssign1988); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOpSingleAssign"


    // $ANTLR start "ruleOpSingleAssign"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:884:1: ruleOpSingleAssign returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : kw= '=' ;
    public final AntlrDatatypeRuleToken ruleOpSingleAssign() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;

         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:887:28: (kw= '=' )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:889:2: kw= '='
            {
            kw=(Token)match(input,20,FOLLOW_20_in_ruleOpSingleAssign2025); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                      current.merge(kw);
                      newLeafNode(kw, grammarAccess.getOpSingleAssignAccess().getEqualsSignKeyword()); 
                  
            }

            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOpSingleAssign"


    // $ANTLR start "entryRuleOpMultiAssign"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:902:1: entryRuleOpMultiAssign returns [String current=null] : iv_ruleOpMultiAssign= ruleOpMultiAssign EOF ;
    public final String entryRuleOpMultiAssign() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleOpMultiAssign = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:903:2: (iv_ruleOpMultiAssign= ruleOpMultiAssign EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:904:2: iv_ruleOpMultiAssign= ruleOpMultiAssign EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getOpMultiAssignRule()); 
            }
            pushFollow(FOLLOW_ruleOpMultiAssign_in_entryRuleOpMultiAssign2065);
            iv_ruleOpMultiAssign=ruleOpMultiAssign();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleOpMultiAssign.getText(); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleOpMultiAssign2076); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOpMultiAssign"


    // $ANTLR start "ruleOpMultiAssign"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:911:1: ruleOpMultiAssign returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : kw= '+=' ;
    public final AntlrDatatypeRuleToken ruleOpMultiAssign() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;

         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:914:28: (kw= '+=' )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:916:2: kw= '+='
            {
            kw=(Token)match(input,21,FOLLOW_21_in_ruleOpMultiAssign2113); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                      current.merge(kw);
                      newLeafNode(kw, grammarAccess.getOpMultiAssignAccess().getPlusSignEqualsSignKeyword()); 
                  
            }

            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOpMultiAssign"


    // $ANTLR start "entryRuleXOrExpression"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:929:1: entryRuleXOrExpression returns [EObject current=null] : iv_ruleXOrExpression= ruleXOrExpression EOF ;
    public final EObject entryRuleXOrExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXOrExpression = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:930:2: (iv_ruleXOrExpression= ruleXOrExpression EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:931:2: iv_ruleXOrExpression= ruleXOrExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getXOrExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleXOrExpression_in_entryRuleXOrExpression2152);
            iv_ruleXOrExpression=ruleXOrExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleXOrExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleXOrExpression2162); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXOrExpression"


    // $ANTLR start "ruleXOrExpression"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:938:1: ruleXOrExpression returns [EObject current=null] : (this_XAndExpression_0= ruleXAndExpression ( ( ( ( () ( ( ruleOpOr ) ) ) )=> ( () ( ( ruleOpOr ) ) ) ) ( (lv_rightOperand_3_0= ruleXAndExpression ) ) )* ) ;
    public final EObject ruleXOrExpression() throws RecognitionException {
        EObject current = null;

        EObject this_XAndExpression_0 = null;

        EObject lv_rightOperand_3_0 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:941:28: ( (this_XAndExpression_0= ruleXAndExpression ( ( ( ( () ( ( ruleOpOr ) ) ) )=> ( () ( ( ruleOpOr ) ) ) ) ( (lv_rightOperand_3_0= ruleXAndExpression ) ) )* ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:942:1: (this_XAndExpression_0= ruleXAndExpression ( ( ( ( () ( ( ruleOpOr ) ) ) )=> ( () ( ( ruleOpOr ) ) ) ) ( (lv_rightOperand_3_0= ruleXAndExpression ) ) )* )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:942:1: (this_XAndExpression_0= ruleXAndExpression ( ( ( ( () ( ( ruleOpOr ) ) ) )=> ( () ( ( ruleOpOr ) ) ) ) ( (lv_rightOperand_3_0= ruleXAndExpression ) ) )* )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:943:5: this_XAndExpression_0= ruleXAndExpression ( ( ( ( () ( ( ruleOpOr ) ) ) )=> ( () ( ( ruleOpOr ) ) ) ) ( (lv_rightOperand_3_0= ruleXAndExpression ) ) )*
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getXOrExpressionAccess().getXAndExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FOLLOW_ruleXAndExpression_in_ruleXOrExpression2209);
            this_XAndExpression_0=ruleXAndExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_XAndExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:951:1: ( ( ( ( () ( ( ruleOpOr ) ) ) )=> ( () ( ( ruleOpOr ) ) ) ) ( (lv_rightOperand_3_0= ruleXAndExpression ) ) )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==22) ) {
                    int LA11_2 = input.LA(2);

                    if ( (synpred2_InternalDeepClone()) ) {
                        alt11=1;
                    }


                }


                switch (alt11) {
            	case 1 :
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:951:2: ( ( ( () ( ( ruleOpOr ) ) ) )=> ( () ( ( ruleOpOr ) ) ) ) ( (lv_rightOperand_3_0= ruleXAndExpression ) )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:951:2: ( ( ( () ( ( ruleOpOr ) ) ) )=> ( () ( ( ruleOpOr ) ) ) )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:951:3: ( ( () ( ( ruleOpOr ) ) ) )=> ( () ( ( ruleOpOr ) ) )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:956:6: ( () ( ( ruleOpOr ) ) )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:956:7: () ( ( ruleOpOr ) )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:956:7: ()
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:957:5: 
            	    {
            	    if ( state.backtracking==0 ) {

            	              current = forceCreateModelElementAndSet(
            	                  grammarAccess.getXOrExpressionAccess().getXBinaryOperationLeftOperandAction_1_0_0_0(),
            	                  current);
            	          
            	    }

            	    }

            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:962:2: ( ( ruleOpOr ) )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:963:1: ( ruleOpOr )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:963:1: ( ruleOpOr )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:964:3: ruleOpOr
            	    {
            	    if ( state.backtracking==0 ) {

            	      			if (current==null) {
            	      	            current = createModelElement(grammarAccess.getXOrExpressionRule());
            	      	        }
            	              
            	    }
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getXOrExpressionAccess().getFeatureJvmIdentifiableElementCrossReference_1_0_0_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleOpOr_in_ruleXOrExpression2262);
            	    ruleOpOr();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {
            	       
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }


            	    }

            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:977:4: ( (lv_rightOperand_3_0= ruleXAndExpression ) )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:978:1: (lv_rightOperand_3_0= ruleXAndExpression )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:978:1: (lv_rightOperand_3_0= ruleXAndExpression )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:979:3: lv_rightOperand_3_0= ruleXAndExpression
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getXOrExpressionAccess().getRightOperandXAndExpressionParserRuleCall_1_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleXAndExpression_in_ruleXOrExpression2285);
            	    lv_rightOperand_3_0=ruleXAndExpression();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getXOrExpressionRule());
            	      	        }
            	             		set(
            	             			current, 
            	             			"rightOperand",
            	              		lv_rightOperand_3_0, 
            	              		"XAndExpression");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXOrExpression"


    // $ANTLR start "entryRuleOpOr"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1003:1: entryRuleOpOr returns [String current=null] : iv_ruleOpOr= ruleOpOr EOF ;
    public final String entryRuleOpOr() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleOpOr = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1004:2: (iv_ruleOpOr= ruleOpOr EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1005:2: iv_ruleOpOr= ruleOpOr EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getOpOrRule()); 
            }
            pushFollow(FOLLOW_ruleOpOr_in_entryRuleOpOr2324);
            iv_ruleOpOr=ruleOpOr();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleOpOr.getText(); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleOpOr2335); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOpOr"


    // $ANTLR start "ruleOpOr"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1012:1: ruleOpOr returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : kw= '||' ;
    public final AntlrDatatypeRuleToken ruleOpOr() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;

         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1015:28: (kw= '||' )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1017:2: kw= '||'
            {
            kw=(Token)match(input,22,FOLLOW_22_in_ruleOpOr2372); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                      current.merge(kw);
                      newLeafNode(kw, grammarAccess.getOpOrAccess().getVerticalLineVerticalLineKeyword()); 
                  
            }

            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOpOr"


    // $ANTLR start "entryRuleXAndExpression"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1030:1: entryRuleXAndExpression returns [EObject current=null] : iv_ruleXAndExpression= ruleXAndExpression EOF ;
    public final EObject entryRuleXAndExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXAndExpression = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1031:2: (iv_ruleXAndExpression= ruleXAndExpression EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1032:2: iv_ruleXAndExpression= ruleXAndExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getXAndExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleXAndExpression_in_entryRuleXAndExpression2411);
            iv_ruleXAndExpression=ruleXAndExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleXAndExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleXAndExpression2421); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXAndExpression"


    // $ANTLR start "ruleXAndExpression"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1039:1: ruleXAndExpression returns [EObject current=null] : (this_XEqualityExpression_0= ruleXEqualityExpression ( ( ( ( () ( ( ruleOpAnd ) ) ) )=> ( () ( ( ruleOpAnd ) ) ) ) ( (lv_rightOperand_3_0= ruleXEqualityExpression ) ) )* ) ;
    public final EObject ruleXAndExpression() throws RecognitionException {
        EObject current = null;

        EObject this_XEqualityExpression_0 = null;

        EObject lv_rightOperand_3_0 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1042:28: ( (this_XEqualityExpression_0= ruleXEqualityExpression ( ( ( ( () ( ( ruleOpAnd ) ) ) )=> ( () ( ( ruleOpAnd ) ) ) ) ( (lv_rightOperand_3_0= ruleXEqualityExpression ) ) )* ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1043:1: (this_XEqualityExpression_0= ruleXEqualityExpression ( ( ( ( () ( ( ruleOpAnd ) ) ) )=> ( () ( ( ruleOpAnd ) ) ) ) ( (lv_rightOperand_3_0= ruleXEqualityExpression ) ) )* )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1043:1: (this_XEqualityExpression_0= ruleXEqualityExpression ( ( ( ( () ( ( ruleOpAnd ) ) ) )=> ( () ( ( ruleOpAnd ) ) ) ) ( (lv_rightOperand_3_0= ruleXEqualityExpression ) ) )* )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1044:5: this_XEqualityExpression_0= ruleXEqualityExpression ( ( ( ( () ( ( ruleOpAnd ) ) ) )=> ( () ( ( ruleOpAnd ) ) ) ) ( (lv_rightOperand_3_0= ruleXEqualityExpression ) ) )*
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getXAndExpressionAccess().getXEqualityExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FOLLOW_ruleXEqualityExpression_in_ruleXAndExpression2468);
            this_XEqualityExpression_0=ruleXEqualityExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_XEqualityExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1052:1: ( ( ( ( () ( ( ruleOpAnd ) ) ) )=> ( () ( ( ruleOpAnd ) ) ) ) ( (lv_rightOperand_3_0= ruleXEqualityExpression ) ) )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==23) ) {
                    int LA12_2 = input.LA(2);

                    if ( (synpred3_InternalDeepClone()) ) {
                        alt12=1;
                    }


                }


                switch (alt12) {
            	case 1 :
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1052:2: ( ( ( () ( ( ruleOpAnd ) ) ) )=> ( () ( ( ruleOpAnd ) ) ) ) ( (lv_rightOperand_3_0= ruleXEqualityExpression ) )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1052:2: ( ( ( () ( ( ruleOpAnd ) ) ) )=> ( () ( ( ruleOpAnd ) ) ) )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1052:3: ( ( () ( ( ruleOpAnd ) ) ) )=> ( () ( ( ruleOpAnd ) ) )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1057:6: ( () ( ( ruleOpAnd ) ) )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1057:7: () ( ( ruleOpAnd ) )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1057:7: ()
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1058:5: 
            	    {
            	    if ( state.backtracking==0 ) {

            	              current = forceCreateModelElementAndSet(
            	                  grammarAccess.getXAndExpressionAccess().getXBinaryOperationLeftOperandAction_1_0_0_0(),
            	                  current);
            	          
            	    }

            	    }

            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1063:2: ( ( ruleOpAnd ) )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1064:1: ( ruleOpAnd )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1064:1: ( ruleOpAnd )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1065:3: ruleOpAnd
            	    {
            	    if ( state.backtracking==0 ) {

            	      			if (current==null) {
            	      	            current = createModelElement(grammarAccess.getXAndExpressionRule());
            	      	        }
            	              
            	    }
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getXAndExpressionAccess().getFeatureJvmIdentifiableElementCrossReference_1_0_0_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleOpAnd_in_ruleXAndExpression2521);
            	    ruleOpAnd();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {
            	       
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }


            	    }

            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1078:4: ( (lv_rightOperand_3_0= ruleXEqualityExpression ) )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1079:1: (lv_rightOperand_3_0= ruleXEqualityExpression )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1079:1: (lv_rightOperand_3_0= ruleXEqualityExpression )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1080:3: lv_rightOperand_3_0= ruleXEqualityExpression
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getXAndExpressionAccess().getRightOperandXEqualityExpressionParserRuleCall_1_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleXEqualityExpression_in_ruleXAndExpression2544);
            	    lv_rightOperand_3_0=ruleXEqualityExpression();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getXAndExpressionRule());
            	      	        }
            	             		set(
            	             			current, 
            	             			"rightOperand",
            	              		lv_rightOperand_3_0, 
            	              		"XEqualityExpression");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXAndExpression"


    // $ANTLR start "entryRuleOpAnd"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1104:1: entryRuleOpAnd returns [String current=null] : iv_ruleOpAnd= ruleOpAnd EOF ;
    public final String entryRuleOpAnd() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleOpAnd = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1105:2: (iv_ruleOpAnd= ruleOpAnd EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1106:2: iv_ruleOpAnd= ruleOpAnd EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getOpAndRule()); 
            }
            pushFollow(FOLLOW_ruleOpAnd_in_entryRuleOpAnd2583);
            iv_ruleOpAnd=ruleOpAnd();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleOpAnd.getText(); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleOpAnd2594); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOpAnd"


    // $ANTLR start "ruleOpAnd"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1113:1: ruleOpAnd returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : kw= '&&' ;
    public final AntlrDatatypeRuleToken ruleOpAnd() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;

         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1116:28: (kw= '&&' )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1118:2: kw= '&&'
            {
            kw=(Token)match(input,23,FOLLOW_23_in_ruleOpAnd2631); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                      current.merge(kw);
                      newLeafNode(kw, grammarAccess.getOpAndAccess().getAmpersandAmpersandKeyword()); 
                  
            }

            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOpAnd"


    // $ANTLR start "entryRuleXEqualityExpression"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1131:1: entryRuleXEqualityExpression returns [EObject current=null] : iv_ruleXEqualityExpression= ruleXEqualityExpression EOF ;
    public final EObject entryRuleXEqualityExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXEqualityExpression = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1132:2: (iv_ruleXEqualityExpression= ruleXEqualityExpression EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1133:2: iv_ruleXEqualityExpression= ruleXEqualityExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getXEqualityExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleXEqualityExpression_in_entryRuleXEqualityExpression2670);
            iv_ruleXEqualityExpression=ruleXEqualityExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleXEqualityExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleXEqualityExpression2680); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXEqualityExpression"


    // $ANTLR start "ruleXEqualityExpression"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1140:1: ruleXEqualityExpression returns [EObject current=null] : (this_XRelationalExpression_0= ruleXRelationalExpression ( ( ( ( () ( ( ruleOpEquality ) ) ) )=> ( () ( ( ruleOpEquality ) ) ) ) ( (lv_rightOperand_3_0= ruleXRelationalExpression ) ) )* ) ;
    public final EObject ruleXEqualityExpression() throws RecognitionException {
        EObject current = null;

        EObject this_XRelationalExpression_0 = null;

        EObject lv_rightOperand_3_0 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1143:28: ( (this_XRelationalExpression_0= ruleXRelationalExpression ( ( ( ( () ( ( ruleOpEquality ) ) ) )=> ( () ( ( ruleOpEquality ) ) ) ) ( (lv_rightOperand_3_0= ruleXRelationalExpression ) ) )* ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1144:1: (this_XRelationalExpression_0= ruleXRelationalExpression ( ( ( ( () ( ( ruleOpEquality ) ) ) )=> ( () ( ( ruleOpEquality ) ) ) ) ( (lv_rightOperand_3_0= ruleXRelationalExpression ) ) )* )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1144:1: (this_XRelationalExpression_0= ruleXRelationalExpression ( ( ( ( () ( ( ruleOpEquality ) ) ) )=> ( () ( ( ruleOpEquality ) ) ) ) ( (lv_rightOperand_3_0= ruleXRelationalExpression ) ) )* )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1145:5: this_XRelationalExpression_0= ruleXRelationalExpression ( ( ( ( () ( ( ruleOpEquality ) ) ) )=> ( () ( ( ruleOpEquality ) ) ) ) ( (lv_rightOperand_3_0= ruleXRelationalExpression ) ) )*
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getXEqualityExpressionAccess().getXRelationalExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FOLLOW_ruleXRelationalExpression_in_ruleXEqualityExpression2727);
            this_XRelationalExpression_0=ruleXRelationalExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_XRelationalExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1153:1: ( ( ( ( () ( ( ruleOpEquality ) ) ) )=> ( () ( ( ruleOpEquality ) ) ) ) ( (lv_rightOperand_3_0= ruleXRelationalExpression ) ) )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==24) ) {
                    int LA13_2 = input.LA(2);

                    if ( (synpred4_InternalDeepClone()) ) {
                        alt13=1;
                    }


                }
                else if ( (LA13_0==25) ) {
                    int LA13_3 = input.LA(2);

                    if ( (synpred4_InternalDeepClone()) ) {
                        alt13=1;
                    }


                }


                switch (alt13) {
            	case 1 :
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1153:2: ( ( ( () ( ( ruleOpEquality ) ) ) )=> ( () ( ( ruleOpEquality ) ) ) ) ( (lv_rightOperand_3_0= ruleXRelationalExpression ) )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1153:2: ( ( ( () ( ( ruleOpEquality ) ) ) )=> ( () ( ( ruleOpEquality ) ) ) )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1153:3: ( ( () ( ( ruleOpEquality ) ) ) )=> ( () ( ( ruleOpEquality ) ) )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1158:6: ( () ( ( ruleOpEquality ) ) )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1158:7: () ( ( ruleOpEquality ) )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1158:7: ()
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1159:5: 
            	    {
            	    if ( state.backtracking==0 ) {

            	              current = forceCreateModelElementAndSet(
            	                  grammarAccess.getXEqualityExpressionAccess().getXBinaryOperationLeftOperandAction_1_0_0_0(),
            	                  current);
            	          
            	    }

            	    }

            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1164:2: ( ( ruleOpEquality ) )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1165:1: ( ruleOpEquality )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1165:1: ( ruleOpEquality )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1166:3: ruleOpEquality
            	    {
            	    if ( state.backtracking==0 ) {

            	      			if (current==null) {
            	      	            current = createModelElement(grammarAccess.getXEqualityExpressionRule());
            	      	        }
            	              
            	    }
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getXEqualityExpressionAccess().getFeatureJvmIdentifiableElementCrossReference_1_0_0_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleOpEquality_in_ruleXEqualityExpression2780);
            	    ruleOpEquality();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {
            	       
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }


            	    }

            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1179:4: ( (lv_rightOperand_3_0= ruleXRelationalExpression ) )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1180:1: (lv_rightOperand_3_0= ruleXRelationalExpression )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1180:1: (lv_rightOperand_3_0= ruleXRelationalExpression )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1181:3: lv_rightOperand_3_0= ruleXRelationalExpression
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getXEqualityExpressionAccess().getRightOperandXRelationalExpressionParserRuleCall_1_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleXRelationalExpression_in_ruleXEqualityExpression2803);
            	    lv_rightOperand_3_0=ruleXRelationalExpression();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getXEqualityExpressionRule());
            	      	        }
            	             		set(
            	             			current, 
            	             			"rightOperand",
            	              		lv_rightOperand_3_0, 
            	              		"XRelationalExpression");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXEqualityExpression"


    // $ANTLR start "entryRuleOpEquality"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1205:1: entryRuleOpEquality returns [String current=null] : iv_ruleOpEquality= ruleOpEquality EOF ;
    public final String entryRuleOpEquality() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleOpEquality = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1206:2: (iv_ruleOpEquality= ruleOpEquality EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1207:2: iv_ruleOpEquality= ruleOpEquality EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getOpEqualityRule()); 
            }
            pushFollow(FOLLOW_ruleOpEquality_in_entryRuleOpEquality2842);
            iv_ruleOpEquality=ruleOpEquality();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleOpEquality.getText(); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleOpEquality2853); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOpEquality"


    // $ANTLR start "ruleOpEquality"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1214:1: ruleOpEquality returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= '==' | kw= '!=' ) ;
    public final AntlrDatatypeRuleToken ruleOpEquality() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;

         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1217:28: ( (kw= '==' | kw= '!=' ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1218:1: (kw= '==' | kw= '!=' )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1218:1: (kw= '==' | kw= '!=' )
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==24) ) {
                alt14=1;
            }
            else if ( (LA14_0==25) ) {
                alt14=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;
            }
            switch (alt14) {
                case 1 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1219:2: kw= '=='
                    {
                    kw=(Token)match(input,24,FOLLOW_24_in_ruleOpEquality2891); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current.merge(kw);
                              newLeafNode(kw, grammarAccess.getOpEqualityAccess().getEqualsSignEqualsSignKeyword_0()); 
                          
                    }

                    }
                    break;
                case 2 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1226:2: kw= '!='
                    {
                    kw=(Token)match(input,25,FOLLOW_25_in_ruleOpEquality2910); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current.merge(kw);
                              newLeafNode(kw, grammarAccess.getOpEqualityAccess().getExclamationMarkEqualsSignKeyword_1()); 
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOpEquality"


    // $ANTLR start "entryRuleXRelationalExpression"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1239:1: entryRuleXRelationalExpression returns [EObject current=null] : iv_ruleXRelationalExpression= ruleXRelationalExpression EOF ;
    public final EObject entryRuleXRelationalExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXRelationalExpression = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1240:2: (iv_ruleXRelationalExpression= ruleXRelationalExpression EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1241:2: iv_ruleXRelationalExpression= ruleXRelationalExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getXRelationalExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleXRelationalExpression_in_entryRuleXRelationalExpression2950);
            iv_ruleXRelationalExpression=ruleXRelationalExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleXRelationalExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleXRelationalExpression2960); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXRelationalExpression"


    // $ANTLR start "ruleXRelationalExpression"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1248:1: ruleXRelationalExpression returns [EObject current=null] : (this_XOtherOperatorExpression_0= ruleXOtherOperatorExpression ( ( ( ( ( () 'instanceof' ) )=> ( () otherlv_2= 'instanceof' ) ) ( (lv_type_3_0= ruleJvmTypeReference ) ) ) | ( ( ( ( () ( ( ruleOpCompare ) ) ) )=> ( () ( ( ruleOpCompare ) ) ) ) ( (lv_rightOperand_6_0= ruleXOtherOperatorExpression ) ) ) )* ) ;
    public final EObject ruleXRelationalExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_XOtherOperatorExpression_0 = null;

        EObject lv_type_3_0 = null;

        EObject lv_rightOperand_6_0 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1251:28: ( (this_XOtherOperatorExpression_0= ruleXOtherOperatorExpression ( ( ( ( ( () 'instanceof' ) )=> ( () otherlv_2= 'instanceof' ) ) ( (lv_type_3_0= ruleJvmTypeReference ) ) ) | ( ( ( ( () ( ( ruleOpCompare ) ) ) )=> ( () ( ( ruleOpCompare ) ) ) ) ( (lv_rightOperand_6_0= ruleXOtherOperatorExpression ) ) ) )* ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1252:1: (this_XOtherOperatorExpression_0= ruleXOtherOperatorExpression ( ( ( ( ( () 'instanceof' ) )=> ( () otherlv_2= 'instanceof' ) ) ( (lv_type_3_0= ruleJvmTypeReference ) ) ) | ( ( ( ( () ( ( ruleOpCompare ) ) ) )=> ( () ( ( ruleOpCompare ) ) ) ) ( (lv_rightOperand_6_0= ruleXOtherOperatorExpression ) ) ) )* )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1252:1: (this_XOtherOperatorExpression_0= ruleXOtherOperatorExpression ( ( ( ( ( () 'instanceof' ) )=> ( () otherlv_2= 'instanceof' ) ) ( (lv_type_3_0= ruleJvmTypeReference ) ) ) | ( ( ( ( () ( ( ruleOpCompare ) ) ) )=> ( () ( ( ruleOpCompare ) ) ) ) ( (lv_rightOperand_6_0= ruleXOtherOperatorExpression ) ) ) )* )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1253:5: this_XOtherOperatorExpression_0= ruleXOtherOperatorExpression ( ( ( ( ( () 'instanceof' ) )=> ( () otherlv_2= 'instanceof' ) ) ( (lv_type_3_0= ruleJvmTypeReference ) ) ) | ( ( ( ( () ( ( ruleOpCompare ) ) ) )=> ( () ( ( ruleOpCompare ) ) ) ) ( (lv_rightOperand_6_0= ruleXOtherOperatorExpression ) ) ) )*
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getXRelationalExpressionAccess().getXOtherOperatorExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FOLLOW_ruleXOtherOperatorExpression_in_ruleXRelationalExpression3007);
            this_XOtherOperatorExpression_0=ruleXOtherOperatorExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_XOtherOperatorExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1261:1: ( ( ( ( ( () 'instanceof' ) )=> ( () otherlv_2= 'instanceof' ) ) ( (lv_type_3_0= ruleJvmTypeReference ) ) ) | ( ( ( ( () ( ( ruleOpCompare ) ) ) )=> ( () ( ( ruleOpCompare ) ) ) ) ( (lv_rightOperand_6_0= ruleXOtherOperatorExpression ) ) ) )*
            loop15:
            do {
                int alt15=3;
                switch ( input.LA(1) ) {
                case 29:
                    {
                    int LA15_2 = input.LA(2);

                    if ( (synpred6_InternalDeepClone()) ) {
                        alt15=2;
                    }


                    }
                    break;
                case 30:
                    {
                    int LA15_3 = input.LA(2);

                    if ( (synpred6_InternalDeepClone()) ) {
                        alt15=2;
                    }


                    }
                    break;
                case 26:
                    {
                    int LA15_4 = input.LA(2);

                    if ( (synpred5_InternalDeepClone()) ) {
                        alt15=1;
                    }


                    }
                    break;
                case 27:
                    {
                    int LA15_5 = input.LA(2);

                    if ( (synpred6_InternalDeepClone()) ) {
                        alt15=2;
                    }


                    }
                    break;
                case 28:
                    {
                    int LA15_6 = input.LA(2);

                    if ( (synpred6_InternalDeepClone()) ) {
                        alt15=2;
                    }


                    }
                    break;

                }

                switch (alt15) {
            	case 1 :
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1261:2: ( ( ( ( () 'instanceof' ) )=> ( () otherlv_2= 'instanceof' ) ) ( (lv_type_3_0= ruleJvmTypeReference ) ) )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1261:2: ( ( ( ( () 'instanceof' ) )=> ( () otherlv_2= 'instanceof' ) ) ( (lv_type_3_0= ruleJvmTypeReference ) ) )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1261:3: ( ( ( () 'instanceof' ) )=> ( () otherlv_2= 'instanceof' ) ) ( (lv_type_3_0= ruleJvmTypeReference ) )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1261:3: ( ( ( () 'instanceof' ) )=> ( () otherlv_2= 'instanceof' ) )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1261:4: ( ( () 'instanceof' ) )=> ( () otherlv_2= 'instanceof' )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1263:5: ( () otherlv_2= 'instanceof' )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1263:6: () otherlv_2= 'instanceof'
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1263:6: ()
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1264:5: 
            	    {
            	    if ( state.backtracking==0 ) {

            	              current = forceCreateModelElementAndSet(
            	                  grammarAccess.getXRelationalExpressionAccess().getXInstanceOfExpressionExpressionAction_1_0_0_0_0(),
            	                  current);
            	          
            	    }

            	    }

            	    otherlv_2=(Token)match(input,26,FOLLOW_26_in_ruleXRelationalExpression3043); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_2, grammarAccess.getXRelationalExpressionAccess().getInstanceofKeyword_1_0_0_0_1());
            	          
            	    }

            	    }


            	    }

            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1273:3: ( (lv_type_3_0= ruleJvmTypeReference ) )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1274:1: (lv_type_3_0= ruleJvmTypeReference )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1274:1: (lv_type_3_0= ruleJvmTypeReference )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1275:3: lv_type_3_0= ruleJvmTypeReference
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getXRelationalExpressionAccess().getTypeJvmTypeReferenceParserRuleCall_1_0_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleJvmTypeReference_in_ruleXRelationalExpression3066);
            	    lv_type_3_0=ruleJvmTypeReference();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getXRelationalExpressionRule());
            	      	        }
            	             		set(
            	             			current, 
            	             			"type",
            	              		lv_type_3_0, 
            	              		"JvmTypeReference");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1292:6: ( ( ( ( () ( ( ruleOpCompare ) ) ) )=> ( () ( ( ruleOpCompare ) ) ) ) ( (lv_rightOperand_6_0= ruleXOtherOperatorExpression ) ) )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1292:6: ( ( ( ( () ( ( ruleOpCompare ) ) ) )=> ( () ( ( ruleOpCompare ) ) ) ) ( (lv_rightOperand_6_0= ruleXOtherOperatorExpression ) ) )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1292:7: ( ( ( () ( ( ruleOpCompare ) ) ) )=> ( () ( ( ruleOpCompare ) ) ) ) ( (lv_rightOperand_6_0= ruleXOtherOperatorExpression ) )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1292:7: ( ( ( () ( ( ruleOpCompare ) ) ) )=> ( () ( ( ruleOpCompare ) ) ) )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1292:8: ( ( () ( ( ruleOpCompare ) ) ) )=> ( () ( ( ruleOpCompare ) ) )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1297:6: ( () ( ( ruleOpCompare ) ) )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1297:7: () ( ( ruleOpCompare ) )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1297:7: ()
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1298:5: 
            	    {
            	    if ( state.backtracking==0 ) {

            	              current = forceCreateModelElementAndSet(
            	                  grammarAccess.getXRelationalExpressionAccess().getXBinaryOperationLeftOperandAction_1_1_0_0_0(),
            	                  current);
            	          
            	    }

            	    }

            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1303:2: ( ( ruleOpCompare ) )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1304:1: ( ruleOpCompare )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1304:1: ( ruleOpCompare )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1305:3: ruleOpCompare
            	    {
            	    if ( state.backtracking==0 ) {

            	      			if (current==null) {
            	      	            current = createModelElement(grammarAccess.getXRelationalExpressionRule());
            	      	        }
            	              
            	    }
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getXRelationalExpressionAccess().getFeatureJvmIdentifiableElementCrossReference_1_1_0_0_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleOpCompare_in_ruleXRelationalExpression3127);
            	    ruleOpCompare();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {
            	       
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }


            	    }

            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1318:4: ( (lv_rightOperand_6_0= ruleXOtherOperatorExpression ) )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1319:1: (lv_rightOperand_6_0= ruleXOtherOperatorExpression )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1319:1: (lv_rightOperand_6_0= ruleXOtherOperatorExpression )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1320:3: lv_rightOperand_6_0= ruleXOtherOperatorExpression
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getXRelationalExpressionAccess().getRightOperandXOtherOperatorExpressionParserRuleCall_1_1_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleXOtherOperatorExpression_in_ruleXRelationalExpression3150);
            	    lv_rightOperand_6_0=ruleXOtherOperatorExpression();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getXRelationalExpressionRule());
            	      	        }
            	             		set(
            	             			current, 
            	             			"rightOperand",
            	              		lv_rightOperand_6_0, 
            	              		"XOtherOperatorExpression");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXRelationalExpression"


    // $ANTLR start "entryRuleOpCompare"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1344:1: entryRuleOpCompare returns [String current=null] : iv_ruleOpCompare= ruleOpCompare EOF ;
    public final String entryRuleOpCompare() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleOpCompare = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1345:2: (iv_ruleOpCompare= ruleOpCompare EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1346:2: iv_ruleOpCompare= ruleOpCompare EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getOpCompareRule()); 
            }
            pushFollow(FOLLOW_ruleOpCompare_in_entryRuleOpCompare3190);
            iv_ruleOpCompare=ruleOpCompare();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleOpCompare.getText(); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleOpCompare3201); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOpCompare"


    // $ANTLR start "ruleOpCompare"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1353:1: ruleOpCompare returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= '>=' | kw= '<=' | kw= '>' | kw= '<' ) ;
    public final AntlrDatatypeRuleToken ruleOpCompare() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;

         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1356:28: ( (kw= '>=' | kw= '<=' | kw= '>' | kw= '<' ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1357:1: (kw= '>=' | kw= '<=' | kw= '>' | kw= '<' )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1357:1: (kw= '>=' | kw= '<=' | kw= '>' | kw= '<' )
            int alt16=4;
            switch ( input.LA(1) ) {
            case 27:
                {
                alt16=1;
                }
                break;
            case 28:
                {
                alt16=2;
                }
                break;
            case 29:
                {
                alt16=3;
                }
                break;
            case 30:
                {
                alt16=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 16, 0, input);

                throw nvae;
            }

            switch (alt16) {
                case 1 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1358:2: kw= '>='
                    {
                    kw=(Token)match(input,27,FOLLOW_27_in_ruleOpCompare3239); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current.merge(kw);
                              newLeafNode(kw, grammarAccess.getOpCompareAccess().getGreaterThanSignEqualsSignKeyword_0()); 
                          
                    }

                    }
                    break;
                case 2 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1365:2: kw= '<='
                    {
                    kw=(Token)match(input,28,FOLLOW_28_in_ruleOpCompare3258); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current.merge(kw);
                              newLeafNode(kw, grammarAccess.getOpCompareAccess().getLessThanSignEqualsSignKeyword_1()); 
                          
                    }

                    }
                    break;
                case 3 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1372:2: kw= '>'
                    {
                    kw=(Token)match(input,29,FOLLOW_29_in_ruleOpCompare3277); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current.merge(kw);
                              newLeafNode(kw, grammarAccess.getOpCompareAccess().getGreaterThanSignKeyword_2()); 
                          
                    }

                    }
                    break;
                case 4 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1379:2: kw= '<'
                    {
                    kw=(Token)match(input,30,FOLLOW_30_in_ruleOpCompare3296); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current.merge(kw);
                              newLeafNode(kw, grammarAccess.getOpCompareAccess().getLessThanSignKeyword_3()); 
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOpCompare"


    // $ANTLR start "entryRuleXOtherOperatorExpression"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1392:1: entryRuleXOtherOperatorExpression returns [EObject current=null] : iv_ruleXOtherOperatorExpression= ruleXOtherOperatorExpression EOF ;
    public final EObject entryRuleXOtherOperatorExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXOtherOperatorExpression = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1393:2: (iv_ruleXOtherOperatorExpression= ruleXOtherOperatorExpression EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1394:2: iv_ruleXOtherOperatorExpression= ruleXOtherOperatorExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getXOtherOperatorExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleXOtherOperatorExpression_in_entryRuleXOtherOperatorExpression3336);
            iv_ruleXOtherOperatorExpression=ruleXOtherOperatorExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleXOtherOperatorExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleXOtherOperatorExpression3346); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXOtherOperatorExpression"


    // $ANTLR start "ruleXOtherOperatorExpression"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1401:1: ruleXOtherOperatorExpression returns [EObject current=null] : (this_XAdditiveExpression_0= ruleXAdditiveExpression ( ( ( ( () ( ( ruleOpOther ) ) ) )=> ( () ( ( ruleOpOther ) ) ) ) ( (lv_rightOperand_3_0= ruleXAdditiveExpression ) ) )* ) ;
    public final EObject ruleXOtherOperatorExpression() throws RecognitionException {
        EObject current = null;

        EObject this_XAdditiveExpression_0 = null;

        EObject lv_rightOperand_3_0 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1404:28: ( (this_XAdditiveExpression_0= ruleXAdditiveExpression ( ( ( ( () ( ( ruleOpOther ) ) ) )=> ( () ( ( ruleOpOther ) ) ) ) ( (lv_rightOperand_3_0= ruleXAdditiveExpression ) ) )* ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1405:1: (this_XAdditiveExpression_0= ruleXAdditiveExpression ( ( ( ( () ( ( ruleOpOther ) ) ) )=> ( () ( ( ruleOpOther ) ) ) ) ( (lv_rightOperand_3_0= ruleXAdditiveExpression ) ) )* )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1405:1: (this_XAdditiveExpression_0= ruleXAdditiveExpression ( ( ( ( () ( ( ruleOpOther ) ) ) )=> ( () ( ( ruleOpOther ) ) ) ) ( (lv_rightOperand_3_0= ruleXAdditiveExpression ) ) )* )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1406:5: this_XAdditiveExpression_0= ruleXAdditiveExpression ( ( ( ( () ( ( ruleOpOther ) ) ) )=> ( () ( ( ruleOpOther ) ) ) ) ( (lv_rightOperand_3_0= ruleXAdditiveExpression ) ) )*
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getXOtherOperatorExpressionAccess().getXAdditiveExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FOLLOW_ruleXAdditiveExpression_in_ruleXOtherOperatorExpression3393);
            this_XAdditiveExpression_0=ruleXAdditiveExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_XAdditiveExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1414:1: ( ( ( ( () ( ( ruleOpOther ) ) ) )=> ( () ( ( ruleOpOther ) ) ) ) ( (lv_rightOperand_3_0= ruleXAdditiveExpression ) ) )*
            loop17:
            do {
                int alt17=2;
                alt17 = dfa17.predict(input);
                switch (alt17) {
            	case 1 :
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1414:2: ( ( ( () ( ( ruleOpOther ) ) ) )=> ( () ( ( ruleOpOther ) ) ) ) ( (lv_rightOperand_3_0= ruleXAdditiveExpression ) )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1414:2: ( ( ( () ( ( ruleOpOther ) ) ) )=> ( () ( ( ruleOpOther ) ) ) )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1414:3: ( ( () ( ( ruleOpOther ) ) ) )=> ( () ( ( ruleOpOther ) ) )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1419:6: ( () ( ( ruleOpOther ) ) )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1419:7: () ( ( ruleOpOther ) )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1419:7: ()
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1420:5: 
            	    {
            	    if ( state.backtracking==0 ) {

            	              current = forceCreateModelElementAndSet(
            	                  grammarAccess.getXOtherOperatorExpressionAccess().getXBinaryOperationLeftOperandAction_1_0_0_0(),
            	                  current);
            	          
            	    }

            	    }

            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1425:2: ( ( ruleOpOther ) )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1426:1: ( ruleOpOther )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1426:1: ( ruleOpOther )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1427:3: ruleOpOther
            	    {
            	    if ( state.backtracking==0 ) {

            	      			if (current==null) {
            	      	            current = createModelElement(grammarAccess.getXOtherOperatorExpressionRule());
            	      	        }
            	              
            	    }
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getXOtherOperatorExpressionAccess().getFeatureJvmIdentifiableElementCrossReference_1_0_0_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleOpOther_in_ruleXOtherOperatorExpression3446);
            	    ruleOpOther();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {
            	       
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }


            	    }

            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1440:4: ( (lv_rightOperand_3_0= ruleXAdditiveExpression ) )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1441:1: (lv_rightOperand_3_0= ruleXAdditiveExpression )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1441:1: (lv_rightOperand_3_0= ruleXAdditiveExpression )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1442:3: lv_rightOperand_3_0= ruleXAdditiveExpression
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getXOtherOperatorExpressionAccess().getRightOperandXAdditiveExpressionParserRuleCall_1_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleXAdditiveExpression_in_ruleXOtherOperatorExpression3469);
            	    lv_rightOperand_3_0=ruleXAdditiveExpression();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getXOtherOperatorExpressionRule());
            	      	        }
            	             		set(
            	             			current, 
            	             			"rightOperand",
            	              		lv_rightOperand_3_0, 
            	              		"XAdditiveExpression");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop17;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXOtherOperatorExpression"


    // $ANTLR start "entryRuleOpOther"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1466:1: entryRuleOpOther returns [String current=null] : iv_ruleOpOther= ruleOpOther EOF ;
    public final String entryRuleOpOther() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleOpOther = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1467:2: (iv_ruleOpOther= ruleOpOther EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1468:2: iv_ruleOpOther= ruleOpOther EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getOpOtherRule()); 
            }
            pushFollow(FOLLOW_ruleOpOther_in_entryRuleOpOther3508);
            iv_ruleOpOther=ruleOpOther();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleOpOther.getText(); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleOpOther3519); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOpOther"


    // $ANTLR start "ruleOpOther"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1475:1: ruleOpOther returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= '->' | kw= '..' | kw= '=>' | (kw= '>' ( ( ( ( '>' '>' ) )=> (kw= '>' kw= '>' ) ) | kw= '>' ) ) | (kw= '<' ( ( ( ( '<' '<' ) )=> (kw= '<' kw= '<' ) ) | kw= '<' ) ) | kw= '<>' | kw= '?:' | kw= '<=>' ) ;
    public final AntlrDatatypeRuleToken ruleOpOther() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;

         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1478:28: ( (kw= '->' | kw= '..' | kw= '=>' | (kw= '>' ( ( ( ( '>' '>' ) )=> (kw= '>' kw= '>' ) ) | kw= '>' ) ) | (kw= '<' ( ( ( ( '<' '<' ) )=> (kw= '<' kw= '<' ) ) | kw= '<' ) ) | kw= '<>' | kw= '?:' | kw= '<=>' ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1479:1: (kw= '->' | kw= '..' | kw= '=>' | (kw= '>' ( ( ( ( '>' '>' ) )=> (kw= '>' kw= '>' ) ) | kw= '>' ) ) | (kw= '<' ( ( ( ( '<' '<' ) )=> (kw= '<' kw= '<' ) ) | kw= '<' ) ) | kw= '<>' | kw= '?:' | kw= '<=>' )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1479:1: (kw= '->' | kw= '..' | kw= '=>' | (kw= '>' ( ( ( ( '>' '>' ) )=> (kw= '>' kw= '>' ) ) | kw= '>' ) ) | (kw= '<' ( ( ( ( '<' '<' ) )=> (kw= '<' kw= '<' ) ) | kw= '<' ) ) | kw= '<>' | kw= '?:' | kw= '<=>' )
            int alt20=8;
            switch ( input.LA(1) ) {
            case 31:
                {
                alt20=1;
                }
                break;
            case 32:
                {
                alt20=2;
                }
                break;
            case 33:
                {
                alt20=3;
                }
                break;
            case 29:
                {
                alt20=4;
                }
                break;
            case 30:
                {
                alt20=5;
                }
                break;
            case 34:
                {
                alt20=6;
                }
                break;
            case 35:
                {
                alt20=7;
                }
                break;
            case 36:
                {
                alt20=8;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 20, 0, input);

                throw nvae;
            }

            switch (alt20) {
                case 1 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1480:2: kw= '->'
                    {
                    kw=(Token)match(input,31,FOLLOW_31_in_ruleOpOther3557); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current.merge(kw);
                              newLeafNode(kw, grammarAccess.getOpOtherAccess().getHyphenMinusGreaterThanSignKeyword_0()); 
                          
                    }

                    }
                    break;
                case 2 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1487:2: kw= '..'
                    {
                    kw=(Token)match(input,32,FOLLOW_32_in_ruleOpOther3576); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current.merge(kw);
                              newLeafNode(kw, grammarAccess.getOpOtherAccess().getFullStopFullStopKeyword_1()); 
                          
                    }

                    }
                    break;
                case 3 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1494:2: kw= '=>'
                    {
                    kw=(Token)match(input,33,FOLLOW_33_in_ruleOpOther3595); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current.merge(kw);
                              newLeafNode(kw, grammarAccess.getOpOtherAccess().getEqualsSignGreaterThanSignKeyword_2()); 
                          
                    }

                    }
                    break;
                case 4 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1500:6: (kw= '>' ( ( ( ( '>' '>' ) )=> (kw= '>' kw= '>' ) ) | kw= '>' ) )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1500:6: (kw= '>' ( ( ( ( '>' '>' ) )=> (kw= '>' kw= '>' ) ) | kw= '>' ) )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1501:2: kw= '>' ( ( ( ( '>' '>' ) )=> (kw= '>' kw= '>' ) ) | kw= '>' )
                    {
                    kw=(Token)match(input,29,FOLLOW_29_in_ruleOpOther3615); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current.merge(kw);
                              newLeafNode(kw, grammarAccess.getOpOtherAccess().getGreaterThanSignKeyword_3_0()); 
                          
                    }
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1506:1: ( ( ( ( '>' '>' ) )=> (kw= '>' kw= '>' ) ) | kw= '>' )
                    int alt18=2;
                    int LA18_0 = input.LA(1);

                    if ( (LA18_0==29) ) {
                        int LA18_1 = input.LA(2);

                        if ( (LA18_1==29) && (synpred8_InternalDeepClone())) {
                            alt18=1;
                        }
                        else if ( (LA18_1==EOF||(LA18_1>=RULE_ID && LA18_1<=RULE_DECIMAL)||LA18_1==16||LA18_1==18||LA18_1==30||LA18_1==37||LA18_1==42||LA18_1==47||LA18_1==49||LA18_1==53||LA18_1==55||(LA18_1>=59 && LA18_1<=61)||LA18_1==64||(LA18_1>=66 && LA18_1<=73)) ) {
                            alt18=2;
                        }
                        else {
                            if (state.backtracking>0) {state.failed=true; return current;}
                            NoViableAltException nvae =
                                new NoViableAltException("", 18, 1, input);

                            throw nvae;
                        }
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 18, 0, input);

                        throw nvae;
                    }
                    switch (alt18) {
                        case 1 :
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1506:2: ( ( ( '>' '>' ) )=> (kw= '>' kw= '>' ) )
                            {
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1506:2: ( ( ( '>' '>' ) )=> (kw= '>' kw= '>' ) )
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1506:3: ( ( '>' '>' ) )=> (kw= '>' kw= '>' )
                            {
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1510:5: (kw= '>' kw= '>' )
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1511:2: kw= '>' kw= '>'
                            {
                            kw=(Token)match(input,29,FOLLOW_29_in_ruleOpOther3646); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      current.merge(kw);
                                      newLeafNode(kw, grammarAccess.getOpOtherAccess().getGreaterThanSignKeyword_3_1_0_0_0()); 
                                  
                            }
                            kw=(Token)match(input,29,FOLLOW_29_in_ruleOpOther3659); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      current.merge(kw);
                                      newLeafNode(kw, grammarAccess.getOpOtherAccess().getGreaterThanSignKeyword_3_1_0_0_1()); 
                                  
                            }

                            }


                            }


                            }
                            break;
                        case 2 :
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1524:2: kw= '>'
                            {
                            kw=(Token)match(input,29,FOLLOW_29_in_ruleOpOther3680); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      current.merge(kw);
                                      newLeafNode(kw, grammarAccess.getOpOtherAccess().getGreaterThanSignKeyword_3_1_1()); 
                                  
                            }

                            }
                            break;

                    }


                    }


                    }
                    break;
                case 5 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1530:6: (kw= '<' ( ( ( ( '<' '<' ) )=> (kw= '<' kw= '<' ) ) | kw= '<' ) )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1530:6: (kw= '<' ( ( ( ( '<' '<' ) )=> (kw= '<' kw= '<' ) ) | kw= '<' ) )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1531:2: kw= '<' ( ( ( ( '<' '<' ) )=> (kw= '<' kw= '<' ) ) | kw= '<' )
                    {
                    kw=(Token)match(input,30,FOLLOW_30_in_ruleOpOther3702); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current.merge(kw);
                              newLeafNode(kw, grammarAccess.getOpOtherAccess().getLessThanSignKeyword_4_0()); 
                          
                    }
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1536:1: ( ( ( ( '<' '<' ) )=> (kw= '<' kw= '<' ) ) | kw= '<' )
                    int alt19=2;
                    int LA19_0 = input.LA(1);

                    if ( (LA19_0==30) ) {
                        int LA19_1 = input.LA(2);

                        if ( (synpred9_InternalDeepClone()) ) {
                            alt19=1;
                        }
                        else if ( (true) ) {
                            alt19=2;
                        }
                        else {
                            if (state.backtracking>0) {state.failed=true; return current;}
                            NoViableAltException nvae =
                                new NoViableAltException("", 19, 1, input);

                            throw nvae;
                        }
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 19, 0, input);

                        throw nvae;
                    }
                    switch (alt19) {
                        case 1 :
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1536:2: ( ( ( '<' '<' ) )=> (kw= '<' kw= '<' ) )
                            {
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1536:2: ( ( ( '<' '<' ) )=> (kw= '<' kw= '<' ) )
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1536:3: ( ( '<' '<' ) )=> (kw= '<' kw= '<' )
                            {
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1540:5: (kw= '<' kw= '<' )
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1541:2: kw= '<' kw= '<'
                            {
                            kw=(Token)match(input,30,FOLLOW_30_in_ruleOpOther3733); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      current.merge(kw);
                                      newLeafNode(kw, grammarAccess.getOpOtherAccess().getLessThanSignKeyword_4_1_0_0_0()); 
                                  
                            }
                            kw=(Token)match(input,30,FOLLOW_30_in_ruleOpOther3746); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      current.merge(kw);
                                      newLeafNode(kw, grammarAccess.getOpOtherAccess().getLessThanSignKeyword_4_1_0_0_1()); 
                                  
                            }

                            }


                            }


                            }
                            break;
                        case 2 :
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1554:2: kw= '<'
                            {
                            kw=(Token)match(input,30,FOLLOW_30_in_ruleOpOther3767); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      current.merge(kw);
                                      newLeafNode(kw, grammarAccess.getOpOtherAccess().getLessThanSignKeyword_4_1_1()); 
                                  
                            }

                            }
                            break;

                    }


                    }


                    }
                    break;
                case 6 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1561:2: kw= '<>'
                    {
                    kw=(Token)match(input,34,FOLLOW_34_in_ruleOpOther3788); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current.merge(kw);
                              newLeafNode(kw, grammarAccess.getOpOtherAccess().getLessThanSignGreaterThanSignKeyword_5()); 
                          
                    }

                    }
                    break;
                case 7 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1568:2: kw= '?:'
                    {
                    kw=(Token)match(input,35,FOLLOW_35_in_ruleOpOther3807); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current.merge(kw);
                              newLeafNode(kw, grammarAccess.getOpOtherAccess().getQuestionMarkColonKeyword_6()); 
                          
                    }

                    }
                    break;
                case 8 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1575:2: kw= '<=>'
                    {
                    kw=(Token)match(input,36,FOLLOW_36_in_ruleOpOther3826); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current.merge(kw);
                              newLeafNode(kw, grammarAccess.getOpOtherAccess().getLessThanSignEqualsSignGreaterThanSignKeyword_7()); 
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOpOther"


    // $ANTLR start "entryRuleXAdditiveExpression"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1588:1: entryRuleXAdditiveExpression returns [EObject current=null] : iv_ruleXAdditiveExpression= ruleXAdditiveExpression EOF ;
    public final EObject entryRuleXAdditiveExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXAdditiveExpression = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1589:2: (iv_ruleXAdditiveExpression= ruleXAdditiveExpression EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1590:2: iv_ruleXAdditiveExpression= ruleXAdditiveExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getXAdditiveExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleXAdditiveExpression_in_entryRuleXAdditiveExpression3866);
            iv_ruleXAdditiveExpression=ruleXAdditiveExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleXAdditiveExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleXAdditiveExpression3876); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXAdditiveExpression"


    // $ANTLR start "ruleXAdditiveExpression"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1597:1: ruleXAdditiveExpression returns [EObject current=null] : (this_XMultiplicativeExpression_0= ruleXMultiplicativeExpression ( ( ( ( () ( ( ruleOpAdd ) ) ) )=> ( () ( ( ruleOpAdd ) ) ) ) ( (lv_rightOperand_3_0= ruleXMultiplicativeExpression ) ) )* ) ;
    public final EObject ruleXAdditiveExpression() throws RecognitionException {
        EObject current = null;

        EObject this_XMultiplicativeExpression_0 = null;

        EObject lv_rightOperand_3_0 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1600:28: ( (this_XMultiplicativeExpression_0= ruleXMultiplicativeExpression ( ( ( ( () ( ( ruleOpAdd ) ) ) )=> ( () ( ( ruleOpAdd ) ) ) ) ( (lv_rightOperand_3_0= ruleXMultiplicativeExpression ) ) )* ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1601:1: (this_XMultiplicativeExpression_0= ruleXMultiplicativeExpression ( ( ( ( () ( ( ruleOpAdd ) ) ) )=> ( () ( ( ruleOpAdd ) ) ) ) ( (lv_rightOperand_3_0= ruleXMultiplicativeExpression ) ) )* )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1601:1: (this_XMultiplicativeExpression_0= ruleXMultiplicativeExpression ( ( ( ( () ( ( ruleOpAdd ) ) ) )=> ( () ( ( ruleOpAdd ) ) ) ) ( (lv_rightOperand_3_0= ruleXMultiplicativeExpression ) ) )* )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1602:5: this_XMultiplicativeExpression_0= ruleXMultiplicativeExpression ( ( ( ( () ( ( ruleOpAdd ) ) ) )=> ( () ( ( ruleOpAdd ) ) ) ) ( (lv_rightOperand_3_0= ruleXMultiplicativeExpression ) ) )*
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getXAdditiveExpressionAccess().getXMultiplicativeExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FOLLOW_ruleXMultiplicativeExpression_in_ruleXAdditiveExpression3923);
            this_XMultiplicativeExpression_0=ruleXMultiplicativeExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_XMultiplicativeExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1610:1: ( ( ( ( () ( ( ruleOpAdd ) ) ) )=> ( () ( ( ruleOpAdd ) ) ) ) ( (lv_rightOperand_3_0= ruleXMultiplicativeExpression ) ) )*
            loop21:
            do {
                int alt21=2;
                int LA21_0 = input.LA(1);

                if ( (LA21_0==37) ) {
                    int LA21_2 = input.LA(2);

                    if ( (synpred10_InternalDeepClone()) ) {
                        alt21=1;
                    }


                }
                else if ( (LA21_0==18) ) {
                    int LA21_3 = input.LA(2);

                    if ( (synpred10_InternalDeepClone()) ) {
                        alt21=1;
                    }


                }


                switch (alt21) {
            	case 1 :
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1610:2: ( ( ( () ( ( ruleOpAdd ) ) ) )=> ( () ( ( ruleOpAdd ) ) ) ) ( (lv_rightOperand_3_0= ruleXMultiplicativeExpression ) )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1610:2: ( ( ( () ( ( ruleOpAdd ) ) ) )=> ( () ( ( ruleOpAdd ) ) ) )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1610:3: ( ( () ( ( ruleOpAdd ) ) ) )=> ( () ( ( ruleOpAdd ) ) )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1615:6: ( () ( ( ruleOpAdd ) ) )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1615:7: () ( ( ruleOpAdd ) )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1615:7: ()
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1616:5: 
            	    {
            	    if ( state.backtracking==0 ) {

            	              current = forceCreateModelElementAndSet(
            	                  grammarAccess.getXAdditiveExpressionAccess().getXBinaryOperationLeftOperandAction_1_0_0_0(),
            	                  current);
            	          
            	    }

            	    }

            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1621:2: ( ( ruleOpAdd ) )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1622:1: ( ruleOpAdd )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1622:1: ( ruleOpAdd )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1623:3: ruleOpAdd
            	    {
            	    if ( state.backtracking==0 ) {

            	      			if (current==null) {
            	      	            current = createModelElement(grammarAccess.getXAdditiveExpressionRule());
            	      	        }
            	              
            	    }
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getXAdditiveExpressionAccess().getFeatureJvmIdentifiableElementCrossReference_1_0_0_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleOpAdd_in_ruleXAdditiveExpression3976);
            	    ruleOpAdd();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {
            	       
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }


            	    }

            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1636:4: ( (lv_rightOperand_3_0= ruleXMultiplicativeExpression ) )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1637:1: (lv_rightOperand_3_0= ruleXMultiplicativeExpression )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1637:1: (lv_rightOperand_3_0= ruleXMultiplicativeExpression )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1638:3: lv_rightOperand_3_0= ruleXMultiplicativeExpression
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getXAdditiveExpressionAccess().getRightOperandXMultiplicativeExpressionParserRuleCall_1_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleXMultiplicativeExpression_in_ruleXAdditiveExpression3999);
            	    lv_rightOperand_3_0=ruleXMultiplicativeExpression();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getXAdditiveExpressionRule());
            	      	        }
            	             		set(
            	             			current, 
            	             			"rightOperand",
            	              		lv_rightOperand_3_0, 
            	              		"XMultiplicativeExpression");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop21;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXAdditiveExpression"


    // $ANTLR start "entryRuleOpAdd"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1662:1: entryRuleOpAdd returns [String current=null] : iv_ruleOpAdd= ruleOpAdd EOF ;
    public final String entryRuleOpAdd() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleOpAdd = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1663:2: (iv_ruleOpAdd= ruleOpAdd EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1664:2: iv_ruleOpAdd= ruleOpAdd EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getOpAddRule()); 
            }
            pushFollow(FOLLOW_ruleOpAdd_in_entryRuleOpAdd4038);
            iv_ruleOpAdd=ruleOpAdd();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleOpAdd.getText(); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleOpAdd4049); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOpAdd"


    // $ANTLR start "ruleOpAdd"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1671:1: ruleOpAdd returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= '+' | kw= '-' ) ;
    public final AntlrDatatypeRuleToken ruleOpAdd() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;

         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1674:28: ( (kw= '+' | kw= '-' ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1675:1: (kw= '+' | kw= '-' )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1675:1: (kw= '+' | kw= '-' )
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==37) ) {
                alt22=1;
            }
            else if ( (LA22_0==18) ) {
                alt22=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 22, 0, input);

                throw nvae;
            }
            switch (alt22) {
                case 1 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1676:2: kw= '+'
                    {
                    kw=(Token)match(input,37,FOLLOW_37_in_ruleOpAdd4087); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current.merge(kw);
                              newLeafNode(kw, grammarAccess.getOpAddAccess().getPlusSignKeyword_0()); 
                          
                    }

                    }
                    break;
                case 2 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1683:2: kw= '-'
                    {
                    kw=(Token)match(input,18,FOLLOW_18_in_ruleOpAdd4106); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current.merge(kw);
                              newLeafNode(kw, grammarAccess.getOpAddAccess().getHyphenMinusKeyword_1()); 
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOpAdd"


    // $ANTLR start "entryRuleXMultiplicativeExpression"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1696:1: entryRuleXMultiplicativeExpression returns [EObject current=null] : iv_ruleXMultiplicativeExpression= ruleXMultiplicativeExpression EOF ;
    public final EObject entryRuleXMultiplicativeExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXMultiplicativeExpression = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1697:2: (iv_ruleXMultiplicativeExpression= ruleXMultiplicativeExpression EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1698:2: iv_ruleXMultiplicativeExpression= ruleXMultiplicativeExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getXMultiplicativeExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleXMultiplicativeExpression_in_entryRuleXMultiplicativeExpression4146);
            iv_ruleXMultiplicativeExpression=ruleXMultiplicativeExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleXMultiplicativeExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleXMultiplicativeExpression4156); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXMultiplicativeExpression"


    // $ANTLR start "ruleXMultiplicativeExpression"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1705:1: ruleXMultiplicativeExpression returns [EObject current=null] : (this_XUnaryOperation_0= ruleXUnaryOperation ( ( ( ( () ( ( ruleOpMulti ) ) ) )=> ( () ( ( ruleOpMulti ) ) ) ) ( (lv_rightOperand_3_0= ruleXUnaryOperation ) ) )* ) ;
    public final EObject ruleXMultiplicativeExpression() throws RecognitionException {
        EObject current = null;

        EObject this_XUnaryOperation_0 = null;

        EObject lv_rightOperand_3_0 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1708:28: ( (this_XUnaryOperation_0= ruleXUnaryOperation ( ( ( ( () ( ( ruleOpMulti ) ) ) )=> ( () ( ( ruleOpMulti ) ) ) ) ( (lv_rightOperand_3_0= ruleXUnaryOperation ) ) )* ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1709:1: (this_XUnaryOperation_0= ruleXUnaryOperation ( ( ( ( () ( ( ruleOpMulti ) ) ) )=> ( () ( ( ruleOpMulti ) ) ) ) ( (lv_rightOperand_3_0= ruleXUnaryOperation ) ) )* )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1709:1: (this_XUnaryOperation_0= ruleXUnaryOperation ( ( ( ( () ( ( ruleOpMulti ) ) ) )=> ( () ( ( ruleOpMulti ) ) ) ) ( (lv_rightOperand_3_0= ruleXUnaryOperation ) ) )* )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1710:5: this_XUnaryOperation_0= ruleXUnaryOperation ( ( ( ( () ( ( ruleOpMulti ) ) ) )=> ( () ( ( ruleOpMulti ) ) ) ) ( (lv_rightOperand_3_0= ruleXUnaryOperation ) ) )*
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getXMultiplicativeExpressionAccess().getXUnaryOperationParserRuleCall_0()); 
                  
            }
            pushFollow(FOLLOW_ruleXUnaryOperation_in_ruleXMultiplicativeExpression4203);
            this_XUnaryOperation_0=ruleXUnaryOperation();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_XUnaryOperation_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1718:1: ( ( ( ( () ( ( ruleOpMulti ) ) ) )=> ( () ( ( ruleOpMulti ) ) ) ) ( (lv_rightOperand_3_0= ruleXUnaryOperation ) ) )*
            loop23:
            do {
                int alt23=2;
                switch ( input.LA(1) ) {
                case 38:
                    {
                    int LA23_2 = input.LA(2);

                    if ( (synpred11_InternalDeepClone()) ) {
                        alt23=1;
                    }


                    }
                    break;
                case 39:
                    {
                    int LA23_3 = input.LA(2);

                    if ( (synpred11_InternalDeepClone()) ) {
                        alt23=1;
                    }


                    }
                    break;
                case 40:
                    {
                    int LA23_4 = input.LA(2);

                    if ( (synpred11_InternalDeepClone()) ) {
                        alt23=1;
                    }


                    }
                    break;
                case 41:
                    {
                    int LA23_5 = input.LA(2);

                    if ( (synpred11_InternalDeepClone()) ) {
                        alt23=1;
                    }


                    }
                    break;

                }

                switch (alt23) {
            	case 1 :
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1718:2: ( ( ( () ( ( ruleOpMulti ) ) ) )=> ( () ( ( ruleOpMulti ) ) ) ) ( (lv_rightOperand_3_0= ruleXUnaryOperation ) )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1718:2: ( ( ( () ( ( ruleOpMulti ) ) ) )=> ( () ( ( ruleOpMulti ) ) ) )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1718:3: ( ( () ( ( ruleOpMulti ) ) ) )=> ( () ( ( ruleOpMulti ) ) )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1723:6: ( () ( ( ruleOpMulti ) ) )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1723:7: () ( ( ruleOpMulti ) )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1723:7: ()
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1724:5: 
            	    {
            	    if ( state.backtracking==0 ) {

            	              current = forceCreateModelElementAndSet(
            	                  grammarAccess.getXMultiplicativeExpressionAccess().getXBinaryOperationLeftOperandAction_1_0_0_0(),
            	                  current);
            	          
            	    }

            	    }

            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1729:2: ( ( ruleOpMulti ) )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1730:1: ( ruleOpMulti )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1730:1: ( ruleOpMulti )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1731:3: ruleOpMulti
            	    {
            	    if ( state.backtracking==0 ) {

            	      			if (current==null) {
            	      	            current = createModelElement(grammarAccess.getXMultiplicativeExpressionRule());
            	      	        }
            	              
            	    }
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getXMultiplicativeExpressionAccess().getFeatureJvmIdentifiableElementCrossReference_1_0_0_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleOpMulti_in_ruleXMultiplicativeExpression4256);
            	    ruleOpMulti();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {
            	       
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }


            	    }

            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1744:4: ( (lv_rightOperand_3_0= ruleXUnaryOperation ) )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1745:1: (lv_rightOperand_3_0= ruleXUnaryOperation )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1745:1: (lv_rightOperand_3_0= ruleXUnaryOperation )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1746:3: lv_rightOperand_3_0= ruleXUnaryOperation
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getXMultiplicativeExpressionAccess().getRightOperandXUnaryOperationParserRuleCall_1_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleXUnaryOperation_in_ruleXMultiplicativeExpression4279);
            	    lv_rightOperand_3_0=ruleXUnaryOperation();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getXMultiplicativeExpressionRule());
            	      	        }
            	             		set(
            	             			current, 
            	             			"rightOperand",
            	              		lv_rightOperand_3_0, 
            	              		"XUnaryOperation");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop23;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXMultiplicativeExpression"


    // $ANTLR start "entryRuleOpMulti"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1770:1: entryRuleOpMulti returns [String current=null] : iv_ruleOpMulti= ruleOpMulti EOF ;
    public final String entryRuleOpMulti() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleOpMulti = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1771:2: (iv_ruleOpMulti= ruleOpMulti EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1772:2: iv_ruleOpMulti= ruleOpMulti EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getOpMultiRule()); 
            }
            pushFollow(FOLLOW_ruleOpMulti_in_entryRuleOpMulti4318);
            iv_ruleOpMulti=ruleOpMulti();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleOpMulti.getText(); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleOpMulti4329); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOpMulti"


    // $ANTLR start "ruleOpMulti"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1779:1: ruleOpMulti returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= '*' | kw= '**' | kw= '/' | kw= '%' ) ;
    public final AntlrDatatypeRuleToken ruleOpMulti() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;

         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1782:28: ( (kw= '*' | kw= '**' | kw= '/' | kw= '%' ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1783:1: (kw= '*' | kw= '**' | kw= '/' | kw= '%' )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1783:1: (kw= '*' | kw= '**' | kw= '/' | kw= '%' )
            int alt24=4;
            switch ( input.LA(1) ) {
            case 38:
                {
                alt24=1;
                }
                break;
            case 39:
                {
                alt24=2;
                }
                break;
            case 40:
                {
                alt24=3;
                }
                break;
            case 41:
                {
                alt24=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 24, 0, input);

                throw nvae;
            }

            switch (alt24) {
                case 1 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1784:2: kw= '*'
                    {
                    kw=(Token)match(input,38,FOLLOW_38_in_ruleOpMulti4367); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current.merge(kw);
                              newLeafNode(kw, grammarAccess.getOpMultiAccess().getAsteriskKeyword_0()); 
                          
                    }

                    }
                    break;
                case 2 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1791:2: kw= '**'
                    {
                    kw=(Token)match(input,39,FOLLOW_39_in_ruleOpMulti4386); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current.merge(kw);
                              newLeafNode(kw, grammarAccess.getOpMultiAccess().getAsteriskAsteriskKeyword_1()); 
                          
                    }

                    }
                    break;
                case 3 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1798:2: kw= '/'
                    {
                    kw=(Token)match(input,40,FOLLOW_40_in_ruleOpMulti4405); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current.merge(kw);
                              newLeafNode(kw, grammarAccess.getOpMultiAccess().getSolidusKeyword_2()); 
                          
                    }

                    }
                    break;
                case 4 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1805:2: kw= '%'
                    {
                    kw=(Token)match(input,41,FOLLOW_41_in_ruleOpMulti4424); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current.merge(kw);
                              newLeafNode(kw, grammarAccess.getOpMultiAccess().getPercentSignKeyword_3()); 
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOpMulti"


    // $ANTLR start "entryRuleXUnaryOperation"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1818:1: entryRuleXUnaryOperation returns [EObject current=null] : iv_ruleXUnaryOperation= ruleXUnaryOperation EOF ;
    public final EObject entryRuleXUnaryOperation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXUnaryOperation = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1819:2: (iv_ruleXUnaryOperation= ruleXUnaryOperation EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1820:2: iv_ruleXUnaryOperation= ruleXUnaryOperation EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getXUnaryOperationRule()); 
            }
            pushFollow(FOLLOW_ruleXUnaryOperation_in_entryRuleXUnaryOperation4464);
            iv_ruleXUnaryOperation=ruleXUnaryOperation();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleXUnaryOperation; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleXUnaryOperation4474); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXUnaryOperation"


    // $ANTLR start "ruleXUnaryOperation"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1827:1: ruleXUnaryOperation returns [EObject current=null] : ( ( () ( ( ruleOpUnary ) ) ( (lv_operand_2_0= ruleXCastedExpression ) ) ) | this_XCastedExpression_3= ruleXCastedExpression ) ;
    public final EObject ruleXUnaryOperation() throws RecognitionException {
        EObject current = null;

        EObject lv_operand_2_0 = null;

        EObject this_XCastedExpression_3 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1830:28: ( ( ( () ( ( ruleOpUnary ) ) ( (lv_operand_2_0= ruleXCastedExpression ) ) ) | this_XCastedExpression_3= ruleXCastedExpression ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1831:1: ( ( () ( ( ruleOpUnary ) ) ( (lv_operand_2_0= ruleXCastedExpression ) ) ) | this_XCastedExpression_3= ruleXCastedExpression )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1831:1: ( ( () ( ( ruleOpUnary ) ) ( (lv_operand_2_0= ruleXCastedExpression ) ) ) | this_XCastedExpression_3= ruleXCastedExpression )
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==18||LA25_0==37||LA25_0==42) ) {
                alt25=1;
            }
            else if ( ((LA25_0>=RULE_ID && LA25_0<=RULE_DECIMAL)||LA25_0==16||LA25_0==30||LA25_0==47||LA25_0==49||LA25_0==53||LA25_0==55||(LA25_0>=59 && LA25_0<=61)||LA25_0==64||(LA25_0>=66 && LA25_0<=73)) ) {
                alt25=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 25, 0, input);

                throw nvae;
            }
            switch (alt25) {
                case 1 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1831:2: ( () ( ( ruleOpUnary ) ) ( (lv_operand_2_0= ruleXCastedExpression ) ) )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1831:2: ( () ( ( ruleOpUnary ) ) ( (lv_operand_2_0= ruleXCastedExpression ) ) )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1831:3: () ( ( ruleOpUnary ) ) ( (lv_operand_2_0= ruleXCastedExpression ) )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1831:3: ()
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1832:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getXUnaryOperationAccess().getXUnaryOperationAction_0_0(),
                                  current);
                          
                    }

                    }

                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1837:2: ( ( ruleOpUnary ) )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1838:1: ( ruleOpUnary )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1838:1: ( ruleOpUnary )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1839:3: ruleOpUnary
                    {
                    if ( state.backtracking==0 ) {

                      			if (current==null) {
                      	            current = createModelElement(grammarAccess.getXUnaryOperationRule());
                      	        }
                              
                    }
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getXUnaryOperationAccess().getFeatureJvmIdentifiableElementCrossReference_0_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleOpUnary_in_ruleXUnaryOperation4532);
                    ruleOpUnary();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1852:2: ( (lv_operand_2_0= ruleXCastedExpression ) )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1853:1: (lv_operand_2_0= ruleXCastedExpression )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1853:1: (lv_operand_2_0= ruleXCastedExpression )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1854:3: lv_operand_2_0= ruleXCastedExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getXUnaryOperationAccess().getOperandXCastedExpressionParserRuleCall_0_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleXCastedExpression_in_ruleXUnaryOperation4553);
                    lv_operand_2_0=ruleXCastedExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getXUnaryOperationRule());
                      	        }
                             		set(
                             			current, 
                             			"operand",
                              		lv_operand_2_0, 
                              		"XCastedExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1872:5: this_XCastedExpression_3= ruleXCastedExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getXUnaryOperationAccess().getXCastedExpressionParserRuleCall_1()); 
                          
                    }
                    pushFollow(FOLLOW_ruleXCastedExpression_in_ruleXUnaryOperation4582);
                    this_XCastedExpression_3=ruleXCastedExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_XCastedExpression_3; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXUnaryOperation"


    // $ANTLR start "entryRuleOpUnary"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1888:1: entryRuleOpUnary returns [String current=null] : iv_ruleOpUnary= ruleOpUnary EOF ;
    public final String entryRuleOpUnary() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleOpUnary = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1889:2: (iv_ruleOpUnary= ruleOpUnary EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1890:2: iv_ruleOpUnary= ruleOpUnary EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getOpUnaryRule()); 
            }
            pushFollow(FOLLOW_ruleOpUnary_in_entryRuleOpUnary4618);
            iv_ruleOpUnary=ruleOpUnary();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleOpUnary.getText(); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleOpUnary4629); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOpUnary"


    // $ANTLR start "ruleOpUnary"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1897:1: ruleOpUnary returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= '!' | kw= '-' | kw= '+' ) ;
    public final AntlrDatatypeRuleToken ruleOpUnary() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;

         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1900:28: ( (kw= '!' | kw= '-' | kw= '+' ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1901:1: (kw= '!' | kw= '-' | kw= '+' )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1901:1: (kw= '!' | kw= '-' | kw= '+' )
            int alt26=3;
            switch ( input.LA(1) ) {
            case 42:
                {
                alt26=1;
                }
                break;
            case 18:
                {
                alt26=2;
                }
                break;
            case 37:
                {
                alt26=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 26, 0, input);

                throw nvae;
            }

            switch (alt26) {
                case 1 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1902:2: kw= '!'
                    {
                    kw=(Token)match(input,42,FOLLOW_42_in_ruleOpUnary4667); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current.merge(kw);
                              newLeafNode(kw, grammarAccess.getOpUnaryAccess().getExclamationMarkKeyword_0()); 
                          
                    }

                    }
                    break;
                case 2 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1909:2: kw= '-'
                    {
                    kw=(Token)match(input,18,FOLLOW_18_in_ruleOpUnary4686); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current.merge(kw);
                              newLeafNode(kw, grammarAccess.getOpUnaryAccess().getHyphenMinusKeyword_1()); 
                          
                    }

                    }
                    break;
                case 3 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1916:2: kw= '+'
                    {
                    kw=(Token)match(input,37,FOLLOW_37_in_ruleOpUnary4705); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current.merge(kw);
                              newLeafNode(kw, grammarAccess.getOpUnaryAccess().getPlusSignKeyword_2()); 
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOpUnary"


    // $ANTLR start "entryRuleXCastedExpression"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1929:1: entryRuleXCastedExpression returns [EObject current=null] : iv_ruleXCastedExpression= ruleXCastedExpression EOF ;
    public final EObject entryRuleXCastedExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXCastedExpression = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1930:2: (iv_ruleXCastedExpression= ruleXCastedExpression EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1931:2: iv_ruleXCastedExpression= ruleXCastedExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getXCastedExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleXCastedExpression_in_entryRuleXCastedExpression4745);
            iv_ruleXCastedExpression=ruleXCastedExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleXCastedExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleXCastedExpression4755); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXCastedExpression"


    // $ANTLR start "ruleXCastedExpression"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1938:1: ruleXCastedExpression returns [EObject current=null] : (this_XMemberFeatureCall_0= ruleXMemberFeatureCall ( ( ( ( () 'as' ) )=> ( () otherlv_2= 'as' ) ) ( (lv_type_3_0= ruleJvmTypeReference ) ) )* ) ;
    public final EObject ruleXCastedExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_XMemberFeatureCall_0 = null;

        EObject lv_type_3_0 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1941:28: ( (this_XMemberFeatureCall_0= ruleXMemberFeatureCall ( ( ( ( () 'as' ) )=> ( () otherlv_2= 'as' ) ) ( (lv_type_3_0= ruleJvmTypeReference ) ) )* ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1942:1: (this_XMemberFeatureCall_0= ruleXMemberFeatureCall ( ( ( ( () 'as' ) )=> ( () otherlv_2= 'as' ) ) ( (lv_type_3_0= ruleJvmTypeReference ) ) )* )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1942:1: (this_XMemberFeatureCall_0= ruleXMemberFeatureCall ( ( ( ( () 'as' ) )=> ( () otherlv_2= 'as' ) ) ( (lv_type_3_0= ruleJvmTypeReference ) ) )* )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1943:5: this_XMemberFeatureCall_0= ruleXMemberFeatureCall ( ( ( ( () 'as' ) )=> ( () otherlv_2= 'as' ) ) ( (lv_type_3_0= ruleJvmTypeReference ) ) )*
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getXCastedExpressionAccess().getXMemberFeatureCallParserRuleCall_0()); 
                  
            }
            pushFollow(FOLLOW_ruleXMemberFeatureCall_in_ruleXCastedExpression4802);
            this_XMemberFeatureCall_0=ruleXMemberFeatureCall();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_XMemberFeatureCall_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1951:1: ( ( ( ( () 'as' ) )=> ( () otherlv_2= 'as' ) ) ( (lv_type_3_0= ruleJvmTypeReference ) ) )*
            loop27:
            do {
                int alt27=2;
                int LA27_0 = input.LA(1);

                if ( (LA27_0==43) ) {
                    int LA27_2 = input.LA(2);

                    if ( (synpred12_InternalDeepClone()) ) {
                        alt27=1;
                    }


                }


                switch (alt27) {
            	case 1 :
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1951:2: ( ( ( () 'as' ) )=> ( () otherlv_2= 'as' ) ) ( (lv_type_3_0= ruleJvmTypeReference ) )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1951:2: ( ( ( () 'as' ) )=> ( () otherlv_2= 'as' ) )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1951:3: ( ( () 'as' ) )=> ( () otherlv_2= 'as' )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1953:5: ( () otherlv_2= 'as' )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1953:6: () otherlv_2= 'as'
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1953:6: ()
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1954:5: 
            	    {
            	    if ( state.backtracking==0 ) {

            	              current = forceCreateModelElementAndSet(
            	                  grammarAccess.getXCastedExpressionAccess().getXCastedExpressionTargetAction_1_0_0_0(),
            	                  current);
            	          
            	    }

            	    }

            	    otherlv_2=(Token)match(input,43,FOLLOW_43_in_ruleXCastedExpression4837); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_2, grammarAccess.getXCastedExpressionAccess().getAsKeyword_1_0_0_1());
            	          
            	    }

            	    }


            	    }

            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1963:3: ( (lv_type_3_0= ruleJvmTypeReference ) )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1964:1: (lv_type_3_0= ruleJvmTypeReference )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1964:1: (lv_type_3_0= ruleJvmTypeReference )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1965:3: lv_type_3_0= ruleJvmTypeReference
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getXCastedExpressionAccess().getTypeJvmTypeReferenceParserRuleCall_1_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleJvmTypeReference_in_ruleXCastedExpression4860);
            	    lv_type_3_0=ruleJvmTypeReference();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getXCastedExpressionRule());
            	      	        }
            	             		set(
            	             			current, 
            	             			"type",
            	              		lv_type_3_0, 
            	              		"JvmTypeReference");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop27;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXCastedExpression"


    // $ANTLR start "entryRuleXMemberFeatureCall"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1989:1: entryRuleXMemberFeatureCall returns [EObject current=null] : iv_ruleXMemberFeatureCall= ruleXMemberFeatureCall EOF ;
    public final EObject entryRuleXMemberFeatureCall() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXMemberFeatureCall = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1990:2: (iv_ruleXMemberFeatureCall= ruleXMemberFeatureCall EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1991:2: iv_ruleXMemberFeatureCall= ruleXMemberFeatureCall EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getXMemberFeatureCallRule()); 
            }
            pushFollow(FOLLOW_ruleXMemberFeatureCall_in_entryRuleXMemberFeatureCall4898);
            iv_ruleXMemberFeatureCall=ruleXMemberFeatureCall();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleXMemberFeatureCall; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleXMemberFeatureCall4908); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXMemberFeatureCall"


    // $ANTLR start "ruleXMemberFeatureCall"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1998:1: ruleXMemberFeatureCall returns [EObject current=null] : (this_XPrimaryExpression_0= ruleXPrimaryExpression ( ( ( ( ( () '.' ( ( ruleValidID ) ) ruleOpSingleAssign ) )=> ( () otherlv_2= '.' ( ( ruleValidID ) ) ruleOpSingleAssign ) ) ( (lv_value_5_0= ruleXAssignment ) ) ) | ( ( ( ( () ( '.' | ( ( '?.' ) ) | ( ( '*.' ) ) ) ) )=> ( () (otherlv_7= '.' | ( (lv_nullSafe_8_0= '?.' ) ) | ( (lv_spreading_9_0= '*.' ) ) ) ) ) (otherlv_10= '<' ( (lv_typeArguments_11_0= ruleJvmArgumentTypeReference ) ) (otherlv_12= ',' ( (lv_typeArguments_13_0= ruleJvmArgumentTypeReference ) ) )* otherlv_14= '>' )? ( ( ruleValidID ) ) ( ( ( ( '(' ) )=> (lv_explicitOperationCall_16_0= '(' ) ) ( ( ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> (lv_memberCallArguments_17_0= ruleXShortClosure ) ) | ( ( (lv_memberCallArguments_18_0= ruleXExpression ) ) (otherlv_19= ',' ( (lv_memberCallArguments_20_0= ruleXExpression ) ) )* ) )? otherlv_21= ')' )? ( ( ( () '[' ) )=> (lv_memberCallArguments_22_0= ruleXClosure ) )? ) )* ) ;
    public final EObject ruleXMemberFeatureCall() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_7=null;
        Token lv_nullSafe_8_0=null;
        Token lv_spreading_9_0=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token lv_explicitOperationCall_16_0=null;
        Token otherlv_19=null;
        Token otherlv_21=null;
        EObject this_XPrimaryExpression_0 = null;

        EObject lv_value_5_0 = null;

        EObject lv_typeArguments_11_0 = null;

        EObject lv_typeArguments_13_0 = null;

        EObject lv_memberCallArguments_17_0 = null;

        EObject lv_memberCallArguments_18_0 = null;

        EObject lv_memberCallArguments_20_0 = null;

        EObject lv_memberCallArguments_22_0 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2001:28: ( (this_XPrimaryExpression_0= ruleXPrimaryExpression ( ( ( ( ( () '.' ( ( ruleValidID ) ) ruleOpSingleAssign ) )=> ( () otherlv_2= '.' ( ( ruleValidID ) ) ruleOpSingleAssign ) ) ( (lv_value_5_0= ruleXAssignment ) ) ) | ( ( ( ( () ( '.' | ( ( '?.' ) ) | ( ( '*.' ) ) ) ) )=> ( () (otherlv_7= '.' | ( (lv_nullSafe_8_0= '?.' ) ) | ( (lv_spreading_9_0= '*.' ) ) ) ) ) (otherlv_10= '<' ( (lv_typeArguments_11_0= ruleJvmArgumentTypeReference ) ) (otherlv_12= ',' ( (lv_typeArguments_13_0= ruleJvmArgumentTypeReference ) ) )* otherlv_14= '>' )? ( ( ruleValidID ) ) ( ( ( ( '(' ) )=> (lv_explicitOperationCall_16_0= '(' ) ) ( ( ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> (lv_memberCallArguments_17_0= ruleXShortClosure ) ) | ( ( (lv_memberCallArguments_18_0= ruleXExpression ) ) (otherlv_19= ',' ( (lv_memberCallArguments_20_0= ruleXExpression ) ) )* ) )? otherlv_21= ')' )? ( ( ( () '[' ) )=> (lv_memberCallArguments_22_0= ruleXClosure ) )? ) )* ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2002:1: (this_XPrimaryExpression_0= ruleXPrimaryExpression ( ( ( ( ( () '.' ( ( ruleValidID ) ) ruleOpSingleAssign ) )=> ( () otherlv_2= '.' ( ( ruleValidID ) ) ruleOpSingleAssign ) ) ( (lv_value_5_0= ruleXAssignment ) ) ) | ( ( ( ( () ( '.' | ( ( '?.' ) ) | ( ( '*.' ) ) ) ) )=> ( () (otherlv_7= '.' | ( (lv_nullSafe_8_0= '?.' ) ) | ( (lv_spreading_9_0= '*.' ) ) ) ) ) (otherlv_10= '<' ( (lv_typeArguments_11_0= ruleJvmArgumentTypeReference ) ) (otherlv_12= ',' ( (lv_typeArguments_13_0= ruleJvmArgumentTypeReference ) ) )* otherlv_14= '>' )? ( ( ruleValidID ) ) ( ( ( ( '(' ) )=> (lv_explicitOperationCall_16_0= '(' ) ) ( ( ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> (lv_memberCallArguments_17_0= ruleXShortClosure ) ) | ( ( (lv_memberCallArguments_18_0= ruleXExpression ) ) (otherlv_19= ',' ( (lv_memberCallArguments_20_0= ruleXExpression ) ) )* ) )? otherlv_21= ')' )? ( ( ( () '[' ) )=> (lv_memberCallArguments_22_0= ruleXClosure ) )? ) )* )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2002:1: (this_XPrimaryExpression_0= ruleXPrimaryExpression ( ( ( ( ( () '.' ( ( ruleValidID ) ) ruleOpSingleAssign ) )=> ( () otherlv_2= '.' ( ( ruleValidID ) ) ruleOpSingleAssign ) ) ( (lv_value_5_0= ruleXAssignment ) ) ) | ( ( ( ( () ( '.' | ( ( '?.' ) ) | ( ( '*.' ) ) ) ) )=> ( () (otherlv_7= '.' | ( (lv_nullSafe_8_0= '?.' ) ) | ( (lv_spreading_9_0= '*.' ) ) ) ) ) (otherlv_10= '<' ( (lv_typeArguments_11_0= ruleJvmArgumentTypeReference ) ) (otherlv_12= ',' ( (lv_typeArguments_13_0= ruleJvmArgumentTypeReference ) ) )* otherlv_14= '>' )? ( ( ruleValidID ) ) ( ( ( ( '(' ) )=> (lv_explicitOperationCall_16_0= '(' ) ) ( ( ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> (lv_memberCallArguments_17_0= ruleXShortClosure ) ) | ( ( (lv_memberCallArguments_18_0= ruleXExpression ) ) (otherlv_19= ',' ( (lv_memberCallArguments_20_0= ruleXExpression ) ) )* ) )? otherlv_21= ')' )? ( ( ( () '[' ) )=> (lv_memberCallArguments_22_0= ruleXClosure ) )? ) )* )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2003:5: this_XPrimaryExpression_0= ruleXPrimaryExpression ( ( ( ( ( () '.' ( ( ruleValidID ) ) ruleOpSingleAssign ) )=> ( () otherlv_2= '.' ( ( ruleValidID ) ) ruleOpSingleAssign ) ) ( (lv_value_5_0= ruleXAssignment ) ) ) | ( ( ( ( () ( '.' | ( ( '?.' ) ) | ( ( '*.' ) ) ) ) )=> ( () (otherlv_7= '.' | ( (lv_nullSafe_8_0= '?.' ) ) | ( (lv_spreading_9_0= '*.' ) ) ) ) ) (otherlv_10= '<' ( (lv_typeArguments_11_0= ruleJvmArgumentTypeReference ) ) (otherlv_12= ',' ( (lv_typeArguments_13_0= ruleJvmArgumentTypeReference ) ) )* otherlv_14= '>' )? ( ( ruleValidID ) ) ( ( ( ( '(' ) )=> (lv_explicitOperationCall_16_0= '(' ) ) ( ( ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> (lv_memberCallArguments_17_0= ruleXShortClosure ) ) | ( ( (lv_memberCallArguments_18_0= ruleXExpression ) ) (otherlv_19= ',' ( (lv_memberCallArguments_20_0= ruleXExpression ) ) )* ) )? otherlv_21= ')' )? ( ( ( () '[' ) )=> (lv_memberCallArguments_22_0= ruleXClosure ) )? ) )*
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getXMemberFeatureCallAccess().getXPrimaryExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FOLLOW_ruleXPrimaryExpression_in_ruleXMemberFeatureCall4955);
            this_XPrimaryExpression_0=ruleXPrimaryExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_XPrimaryExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2011:1: ( ( ( ( ( () '.' ( ( ruleValidID ) ) ruleOpSingleAssign ) )=> ( () otherlv_2= '.' ( ( ruleValidID ) ) ruleOpSingleAssign ) ) ( (lv_value_5_0= ruleXAssignment ) ) ) | ( ( ( ( () ( '.' | ( ( '?.' ) ) | ( ( '*.' ) ) ) ) )=> ( () (otherlv_7= '.' | ( (lv_nullSafe_8_0= '?.' ) ) | ( (lv_spreading_9_0= '*.' ) ) ) ) ) (otherlv_10= '<' ( (lv_typeArguments_11_0= ruleJvmArgumentTypeReference ) ) (otherlv_12= ',' ( (lv_typeArguments_13_0= ruleJvmArgumentTypeReference ) ) )* otherlv_14= '>' )? ( ( ruleValidID ) ) ( ( ( ( '(' ) )=> (lv_explicitOperationCall_16_0= '(' ) ) ( ( ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> (lv_memberCallArguments_17_0= ruleXShortClosure ) ) | ( ( (lv_memberCallArguments_18_0= ruleXExpression ) ) (otherlv_19= ',' ( (lv_memberCallArguments_20_0= ruleXExpression ) ) )* ) )? otherlv_21= ')' )? ( ( ( () '[' ) )=> (lv_memberCallArguments_22_0= ruleXClosure ) )? ) )*
            loop35:
            do {
                int alt35=3;
                switch ( input.LA(1) ) {
                case 15:
                    {
                    int LA35_2 = input.LA(2);

                    if ( (synpred13_InternalDeepClone()) ) {
                        alt35=1;
                    }
                    else if ( (synpred14_InternalDeepClone()) ) {
                        alt35=2;
                    }


                    }
                    break;
                case 44:
                    {
                    int LA35_3 = input.LA(2);

                    if ( (synpred14_InternalDeepClone()) ) {
                        alt35=2;
                    }


                    }
                    break;
                case 45:
                    {
                    int LA35_4 = input.LA(2);

                    if ( (synpred14_InternalDeepClone()) ) {
                        alt35=2;
                    }


                    }
                    break;

                }

                switch (alt35) {
            	case 1 :
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2011:2: ( ( ( ( () '.' ( ( ruleValidID ) ) ruleOpSingleAssign ) )=> ( () otherlv_2= '.' ( ( ruleValidID ) ) ruleOpSingleAssign ) ) ( (lv_value_5_0= ruleXAssignment ) ) )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2011:2: ( ( ( ( () '.' ( ( ruleValidID ) ) ruleOpSingleAssign ) )=> ( () otherlv_2= '.' ( ( ruleValidID ) ) ruleOpSingleAssign ) ) ( (lv_value_5_0= ruleXAssignment ) ) )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2011:3: ( ( ( () '.' ( ( ruleValidID ) ) ruleOpSingleAssign ) )=> ( () otherlv_2= '.' ( ( ruleValidID ) ) ruleOpSingleAssign ) ) ( (lv_value_5_0= ruleXAssignment ) )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2011:3: ( ( ( () '.' ( ( ruleValidID ) ) ruleOpSingleAssign ) )=> ( () otherlv_2= '.' ( ( ruleValidID ) ) ruleOpSingleAssign ) )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2011:4: ( ( () '.' ( ( ruleValidID ) ) ruleOpSingleAssign ) )=> ( () otherlv_2= '.' ( ( ruleValidID ) ) ruleOpSingleAssign )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2017:25: ( () otherlv_2= '.' ( ( ruleValidID ) ) ruleOpSingleAssign )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2017:26: () otherlv_2= '.' ( ( ruleValidID ) ) ruleOpSingleAssign
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2017:26: ()
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2018:5: 
            	    {
            	    if ( state.backtracking==0 ) {

            	              current = forceCreateModelElementAndSet(
            	                  grammarAccess.getXMemberFeatureCallAccess().getXAssignmentAssignableAction_1_0_0_0_0(),
            	                  current);
            	          
            	    }

            	    }

            	    otherlv_2=(Token)match(input,15,FOLLOW_15_in_ruleXMemberFeatureCall5004); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_2, grammarAccess.getXMemberFeatureCallAccess().getFullStopKeyword_1_0_0_0_1());
            	          
            	    }
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2027:1: ( ( ruleValidID ) )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2028:1: ( ruleValidID )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2028:1: ( ruleValidID )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2029:3: ruleValidID
            	    {
            	    if ( state.backtracking==0 ) {

            	      			if (current==null) {
            	      	            current = createModelElement(grammarAccess.getXMemberFeatureCallRule());
            	      	        }
            	              
            	    }
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getXMemberFeatureCallAccess().getFeatureJvmIdentifiableElementCrossReference_1_0_0_0_2_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleValidID_in_ruleXMemberFeatureCall5027);
            	    ruleValidID();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {
            	       
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }

            	    if ( state.backtracking==0 ) {
            	       
            	              newCompositeNode(grammarAccess.getXMemberFeatureCallAccess().getOpSingleAssignParserRuleCall_1_0_0_0_3()); 
            	          
            	    }
            	    pushFollow(FOLLOW_ruleOpSingleAssign_in_ruleXMemberFeatureCall5043);
            	    ruleOpSingleAssign();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {
            	       
            	              afterParserOrEnumRuleCall();
            	          
            	    }

            	    }


            	    }

            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2050:3: ( (lv_value_5_0= ruleXAssignment ) )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2051:1: (lv_value_5_0= ruleXAssignment )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2051:1: (lv_value_5_0= ruleXAssignment )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2052:3: lv_value_5_0= ruleXAssignment
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getXMemberFeatureCallAccess().getValueXAssignmentParserRuleCall_1_0_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleXAssignment_in_ruleXMemberFeatureCall5065);
            	    lv_value_5_0=ruleXAssignment();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getXMemberFeatureCallRule());
            	      	        }
            	             		set(
            	             			current, 
            	             			"value",
            	              		lv_value_5_0, 
            	              		"XAssignment");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2069:6: ( ( ( ( () ( '.' | ( ( '?.' ) ) | ( ( '*.' ) ) ) ) )=> ( () (otherlv_7= '.' | ( (lv_nullSafe_8_0= '?.' ) ) | ( (lv_spreading_9_0= '*.' ) ) ) ) ) (otherlv_10= '<' ( (lv_typeArguments_11_0= ruleJvmArgumentTypeReference ) ) (otherlv_12= ',' ( (lv_typeArguments_13_0= ruleJvmArgumentTypeReference ) ) )* otherlv_14= '>' )? ( ( ruleValidID ) ) ( ( ( ( '(' ) )=> (lv_explicitOperationCall_16_0= '(' ) ) ( ( ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> (lv_memberCallArguments_17_0= ruleXShortClosure ) ) | ( ( (lv_memberCallArguments_18_0= ruleXExpression ) ) (otherlv_19= ',' ( (lv_memberCallArguments_20_0= ruleXExpression ) ) )* ) )? otherlv_21= ')' )? ( ( ( () '[' ) )=> (lv_memberCallArguments_22_0= ruleXClosure ) )? )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2069:6: ( ( ( ( () ( '.' | ( ( '?.' ) ) | ( ( '*.' ) ) ) ) )=> ( () (otherlv_7= '.' | ( (lv_nullSafe_8_0= '?.' ) ) | ( (lv_spreading_9_0= '*.' ) ) ) ) ) (otherlv_10= '<' ( (lv_typeArguments_11_0= ruleJvmArgumentTypeReference ) ) (otherlv_12= ',' ( (lv_typeArguments_13_0= ruleJvmArgumentTypeReference ) ) )* otherlv_14= '>' )? ( ( ruleValidID ) ) ( ( ( ( '(' ) )=> (lv_explicitOperationCall_16_0= '(' ) ) ( ( ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> (lv_memberCallArguments_17_0= ruleXShortClosure ) ) | ( ( (lv_memberCallArguments_18_0= ruleXExpression ) ) (otherlv_19= ',' ( (lv_memberCallArguments_20_0= ruleXExpression ) ) )* ) )? otherlv_21= ')' )? ( ( ( () '[' ) )=> (lv_memberCallArguments_22_0= ruleXClosure ) )? )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2069:7: ( ( ( () ( '.' | ( ( '?.' ) ) | ( ( '*.' ) ) ) ) )=> ( () (otherlv_7= '.' | ( (lv_nullSafe_8_0= '?.' ) ) | ( (lv_spreading_9_0= '*.' ) ) ) ) ) (otherlv_10= '<' ( (lv_typeArguments_11_0= ruleJvmArgumentTypeReference ) ) (otherlv_12= ',' ( (lv_typeArguments_13_0= ruleJvmArgumentTypeReference ) ) )* otherlv_14= '>' )? ( ( ruleValidID ) ) ( ( ( ( '(' ) )=> (lv_explicitOperationCall_16_0= '(' ) ) ( ( ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> (lv_memberCallArguments_17_0= ruleXShortClosure ) ) | ( ( (lv_memberCallArguments_18_0= ruleXExpression ) ) (otherlv_19= ',' ( (lv_memberCallArguments_20_0= ruleXExpression ) ) )* ) )? otherlv_21= ')' )? ( ( ( () '[' ) )=> (lv_memberCallArguments_22_0= ruleXClosure ) )?
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2069:7: ( ( ( () ( '.' | ( ( '?.' ) ) | ( ( '*.' ) ) ) ) )=> ( () (otherlv_7= '.' | ( (lv_nullSafe_8_0= '?.' ) ) | ( (lv_spreading_9_0= '*.' ) ) ) ) )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2069:8: ( ( () ( '.' | ( ( '?.' ) ) | ( ( '*.' ) ) ) ) )=> ( () (otherlv_7= '.' | ( (lv_nullSafe_8_0= '?.' ) ) | ( (lv_spreading_9_0= '*.' ) ) ) )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2085:7: ( () (otherlv_7= '.' | ( (lv_nullSafe_8_0= '?.' ) ) | ( (lv_spreading_9_0= '*.' ) ) ) )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2085:8: () (otherlv_7= '.' | ( (lv_nullSafe_8_0= '?.' ) ) | ( (lv_spreading_9_0= '*.' ) ) )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2085:8: ()
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2086:5: 
            	    {
            	    if ( state.backtracking==0 ) {

            	              current = forceCreateModelElementAndSet(
            	                  grammarAccess.getXMemberFeatureCallAccess().getXMemberFeatureCallMemberCallTargetAction_1_1_0_0_0(),
            	                  current);
            	          
            	    }

            	    }

            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2091:2: (otherlv_7= '.' | ( (lv_nullSafe_8_0= '?.' ) ) | ( (lv_spreading_9_0= '*.' ) ) )
            	    int alt28=3;
            	    switch ( input.LA(1) ) {
            	    case 15:
            	        {
            	        alt28=1;
            	        }
            	        break;
            	    case 44:
            	        {
            	        alt28=2;
            	        }
            	        break;
            	    case 45:
            	        {
            	        alt28=3;
            	        }
            	        break;
            	    default:
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 28, 0, input);

            	        throw nvae;
            	    }

            	    switch (alt28) {
            	        case 1 :
            	            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2091:4: otherlv_7= '.'
            	            {
            	            otherlv_7=(Token)match(input,15,FOLLOW_15_in_ruleXMemberFeatureCall5151); if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	                  	newLeafNode(otherlv_7, grammarAccess.getXMemberFeatureCallAccess().getFullStopKeyword_1_1_0_0_1_0());
            	                  
            	            }

            	            }
            	            break;
            	        case 2 :
            	            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2096:6: ( (lv_nullSafe_8_0= '?.' ) )
            	            {
            	            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2096:6: ( (lv_nullSafe_8_0= '?.' ) )
            	            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2097:1: (lv_nullSafe_8_0= '?.' )
            	            {
            	            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2097:1: (lv_nullSafe_8_0= '?.' )
            	            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2098:3: lv_nullSafe_8_0= '?.'
            	            {
            	            lv_nullSafe_8_0=(Token)match(input,44,FOLLOW_44_in_ruleXMemberFeatureCall5175); if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	                      newLeafNode(lv_nullSafe_8_0, grammarAccess.getXMemberFeatureCallAccess().getNullSafeQuestionMarkFullStopKeyword_1_1_0_0_1_1_0());
            	                  
            	            }
            	            if ( state.backtracking==0 ) {

            	              	        if (current==null) {
            	              	            current = createModelElement(grammarAccess.getXMemberFeatureCallRule());
            	              	        }
            	                     		setWithLastConsumed(current, "nullSafe", true, "?.");
            	              	    
            	            }

            	            }


            	            }


            	            }
            	            break;
            	        case 3 :
            	            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2112:6: ( (lv_spreading_9_0= '*.' ) )
            	            {
            	            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2112:6: ( (lv_spreading_9_0= '*.' ) )
            	            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2113:1: (lv_spreading_9_0= '*.' )
            	            {
            	            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2113:1: (lv_spreading_9_0= '*.' )
            	            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2114:3: lv_spreading_9_0= '*.'
            	            {
            	            lv_spreading_9_0=(Token)match(input,45,FOLLOW_45_in_ruleXMemberFeatureCall5212); if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	                      newLeafNode(lv_spreading_9_0, grammarAccess.getXMemberFeatureCallAccess().getSpreadingAsteriskFullStopKeyword_1_1_0_0_1_2_0());
            	                  
            	            }
            	            if ( state.backtracking==0 ) {

            	              	        if (current==null) {
            	              	            current = createModelElement(grammarAccess.getXMemberFeatureCallRule());
            	              	        }
            	                     		setWithLastConsumed(current, "spreading", true, "*.");
            	              	    
            	            }

            	            }


            	            }


            	            }
            	            break;

            	    }


            	    }


            	    }

            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2127:5: (otherlv_10= '<' ( (lv_typeArguments_11_0= ruleJvmArgumentTypeReference ) ) (otherlv_12= ',' ( (lv_typeArguments_13_0= ruleJvmArgumentTypeReference ) ) )* otherlv_14= '>' )?
            	    int alt30=2;
            	    int LA30_0 = input.LA(1);

            	    if ( (LA30_0==30) ) {
            	        alt30=1;
            	    }
            	    switch (alt30) {
            	        case 1 :
            	            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2127:7: otherlv_10= '<' ( (lv_typeArguments_11_0= ruleJvmArgumentTypeReference ) ) (otherlv_12= ',' ( (lv_typeArguments_13_0= ruleJvmArgumentTypeReference ) ) )* otherlv_14= '>'
            	            {
            	            otherlv_10=(Token)match(input,30,FOLLOW_30_in_ruleXMemberFeatureCall5241); if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	                  	newLeafNode(otherlv_10, grammarAccess.getXMemberFeatureCallAccess().getLessThanSignKeyword_1_1_1_0());
            	                  
            	            }
            	            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2131:1: ( (lv_typeArguments_11_0= ruleJvmArgumentTypeReference ) )
            	            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2132:1: (lv_typeArguments_11_0= ruleJvmArgumentTypeReference )
            	            {
            	            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2132:1: (lv_typeArguments_11_0= ruleJvmArgumentTypeReference )
            	            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2133:3: lv_typeArguments_11_0= ruleJvmArgumentTypeReference
            	            {
            	            if ( state.backtracking==0 ) {
            	               
            	              	        newCompositeNode(grammarAccess.getXMemberFeatureCallAccess().getTypeArgumentsJvmArgumentTypeReferenceParserRuleCall_1_1_1_1_0()); 
            	              	    
            	            }
            	            pushFollow(FOLLOW_ruleJvmArgumentTypeReference_in_ruleXMemberFeatureCall5262);
            	            lv_typeArguments_11_0=ruleJvmArgumentTypeReference();

            	            state._fsp--;
            	            if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	              	        if (current==null) {
            	              	            current = createModelElementForParent(grammarAccess.getXMemberFeatureCallRule());
            	              	        }
            	                     		add(
            	                     			current, 
            	                     			"typeArguments",
            	                      		lv_typeArguments_11_0, 
            	                      		"JvmArgumentTypeReference");
            	              	        afterParserOrEnumRuleCall();
            	              	    
            	            }

            	            }


            	            }

            	            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2149:2: (otherlv_12= ',' ( (lv_typeArguments_13_0= ruleJvmArgumentTypeReference ) ) )*
            	            loop29:
            	            do {
            	                int alt29=2;
            	                int LA29_0 = input.LA(1);

            	                if ( (LA29_0==46) ) {
            	                    alt29=1;
            	                }


            	                switch (alt29) {
            	            	case 1 :
            	            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2149:4: otherlv_12= ',' ( (lv_typeArguments_13_0= ruleJvmArgumentTypeReference ) )
            	            	    {
            	            	    otherlv_12=(Token)match(input,46,FOLLOW_46_in_ruleXMemberFeatureCall5275); if (state.failed) return current;
            	            	    if ( state.backtracking==0 ) {

            	            	          	newLeafNode(otherlv_12, grammarAccess.getXMemberFeatureCallAccess().getCommaKeyword_1_1_1_2_0());
            	            	          
            	            	    }
            	            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2153:1: ( (lv_typeArguments_13_0= ruleJvmArgumentTypeReference ) )
            	            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2154:1: (lv_typeArguments_13_0= ruleJvmArgumentTypeReference )
            	            	    {
            	            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2154:1: (lv_typeArguments_13_0= ruleJvmArgumentTypeReference )
            	            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2155:3: lv_typeArguments_13_0= ruleJvmArgumentTypeReference
            	            	    {
            	            	    if ( state.backtracking==0 ) {
            	            	       
            	            	      	        newCompositeNode(grammarAccess.getXMemberFeatureCallAccess().getTypeArgumentsJvmArgumentTypeReferenceParserRuleCall_1_1_1_2_1_0()); 
            	            	      	    
            	            	    }
            	            	    pushFollow(FOLLOW_ruleJvmArgumentTypeReference_in_ruleXMemberFeatureCall5296);
            	            	    lv_typeArguments_13_0=ruleJvmArgumentTypeReference();

            	            	    state._fsp--;
            	            	    if (state.failed) return current;
            	            	    if ( state.backtracking==0 ) {

            	            	      	        if (current==null) {
            	            	      	            current = createModelElementForParent(grammarAccess.getXMemberFeatureCallRule());
            	            	      	        }
            	            	             		add(
            	            	             			current, 
            	            	             			"typeArguments",
            	            	              		lv_typeArguments_13_0, 
            	            	              		"JvmArgumentTypeReference");
            	            	      	        afterParserOrEnumRuleCall();
            	            	      	    
            	            	    }

            	            	    }


            	            	    }


            	            	    }
            	            	    break;

            	            	default :
            	            	    break loop29;
            	                }
            	            } while (true);

            	            otherlv_14=(Token)match(input,29,FOLLOW_29_in_ruleXMemberFeatureCall5310); if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	                  	newLeafNode(otherlv_14, grammarAccess.getXMemberFeatureCallAccess().getGreaterThanSignKeyword_1_1_1_3());
            	                  
            	            }

            	            }
            	            break;

            	    }

            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2175:3: ( ( ruleValidID ) )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2176:1: ( ruleValidID )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2176:1: ( ruleValidID )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2177:3: ruleValidID
            	    {
            	    if ( state.backtracking==0 ) {

            	      			if (current==null) {
            	      	            current = createModelElement(grammarAccess.getXMemberFeatureCallRule());
            	      	        }
            	              
            	    }
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getXMemberFeatureCallAccess().getFeatureJvmIdentifiableElementCrossReference_1_1_2_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleValidID_in_ruleXMemberFeatureCall5335);
            	    ruleValidID();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {
            	       
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }

            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2190:2: ( ( ( ( '(' ) )=> (lv_explicitOperationCall_16_0= '(' ) ) ( ( ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> (lv_memberCallArguments_17_0= ruleXShortClosure ) ) | ( ( (lv_memberCallArguments_18_0= ruleXExpression ) ) (otherlv_19= ',' ( (lv_memberCallArguments_20_0= ruleXExpression ) ) )* ) )? otherlv_21= ')' )?
            	    int alt33=2;
            	    alt33 = dfa33.predict(input);
            	    switch (alt33) {
            	        case 1 :
            	            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2190:3: ( ( ( '(' ) )=> (lv_explicitOperationCall_16_0= '(' ) ) ( ( ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> (lv_memberCallArguments_17_0= ruleXShortClosure ) ) | ( ( (lv_memberCallArguments_18_0= ruleXExpression ) ) (otherlv_19= ',' ( (lv_memberCallArguments_20_0= ruleXExpression ) ) )* ) )? otherlv_21= ')'
            	            {
            	            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2190:3: ( ( ( '(' ) )=> (lv_explicitOperationCall_16_0= '(' ) )
            	            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2190:4: ( ( '(' ) )=> (lv_explicitOperationCall_16_0= '(' )
            	            {
            	            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2197:1: (lv_explicitOperationCall_16_0= '(' )
            	            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2198:3: lv_explicitOperationCall_16_0= '('
            	            {
            	            lv_explicitOperationCall_16_0=(Token)match(input,47,FOLLOW_47_in_ruleXMemberFeatureCall5369); if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	                      newLeafNode(lv_explicitOperationCall_16_0, grammarAccess.getXMemberFeatureCallAccess().getExplicitOperationCallLeftParenthesisKeyword_1_1_3_0_0());
            	                  
            	            }
            	            if ( state.backtracking==0 ) {

            	              	        if (current==null) {
            	              	            current = createModelElement(grammarAccess.getXMemberFeatureCallRule());
            	              	        }
            	                     		setWithLastConsumed(current, "explicitOperationCall", true, "(");
            	              	    
            	            }

            	            }


            	            }

            	            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2211:2: ( ( ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> (lv_memberCallArguments_17_0= ruleXShortClosure ) ) | ( ( (lv_memberCallArguments_18_0= ruleXExpression ) ) (otherlv_19= ',' ( (lv_memberCallArguments_20_0= ruleXExpression ) ) )* ) )?
            	            int alt32=3;
            	            alt32 = dfa32.predict(input);
            	            switch (alt32) {
            	                case 1 :
            	                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2211:3: ( ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> (lv_memberCallArguments_17_0= ruleXShortClosure ) )
            	                    {
            	                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2211:3: ( ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> (lv_memberCallArguments_17_0= ruleXShortClosure ) )
            	                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2211:4: ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> (lv_memberCallArguments_17_0= ruleXShortClosure )
            	                    {
            	                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2228:1: (lv_memberCallArguments_17_0= ruleXShortClosure )
            	                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2229:3: lv_memberCallArguments_17_0= ruleXShortClosure
            	                    {
            	                    if ( state.backtracking==0 ) {
            	                       
            	                      	        newCompositeNode(grammarAccess.getXMemberFeatureCallAccess().getMemberCallArgumentsXShortClosureParserRuleCall_1_1_3_1_0_0()); 
            	                      	    
            	                    }
            	                    pushFollow(FOLLOW_ruleXShortClosure_in_ruleXMemberFeatureCall5454);
            	                    lv_memberCallArguments_17_0=ruleXShortClosure();

            	                    state._fsp--;
            	                    if (state.failed) return current;
            	                    if ( state.backtracking==0 ) {

            	                      	        if (current==null) {
            	                      	            current = createModelElementForParent(grammarAccess.getXMemberFeatureCallRule());
            	                      	        }
            	                             		add(
            	                             			current, 
            	                             			"memberCallArguments",
            	                              		lv_memberCallArguments_17_0, 
            	                              		"XShortClosure");
            	                      	        afterParserOrEnumRuleCall();
            	                      	    
            	                    }

            	                    }


            	                    }


            	                    }
            	                    break;
            	                case 2 :
            	                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2246:6: ( ( (lv_memberCallArguments_18_0= ruleXExpression ) ) (otherlv_19= ',' ( (lv_memberCallArguments_20_0= ruleXExpression ) ) )* )
            	                    {
            	                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2246:6: ( ( (lv_memberCallArguments_18_0= ruleXExpression ) ) (otherlv_19= ',' ( (lv_memberCallArguments_20_0= ruleXExpression ) ) )* )
            	                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2246:7: ( (lv_memberCallArguments_18_0= ruleXExpression ) ) (otherlv_19= ',' ( (lv_memberCallArguments_20_0= ruleXExpression ) ) )*
            	                    {
            	                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2246:7: ( (lv_memberCallArguments_18_0= ruleXExpression ) )
            	                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2247:1: (lv_memberCallArguments_18_0= ruleXExpression )
            	                    {
            	                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2247:1: (lv_memberCallArguments_18_0= ruleXExpression )
            	                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2248:3: lv_memberCallArguments_18_0= ruleXExpression
            	                    {
            	                    if ( state.backtracking==0 ) {
            	                       
            	                      	        newCompositeNode(grammarAccess.getXMemberFeatureCallAccess().getMemberCallArgumentsXExpressionParserRuleCall_1_1_3_1_1_0_0()); 
            	                      	    
            	                    }
            	                    pushFollow(FOLLOW_ruleXExpression_in_ruleXMemberFeatureCall5482);
            	                    lv_memberCallArguments_18_0=ruleXExpression();

            	                    state._fsp--;
            	                    if (state.failed) return current;
            	                    if ( state.backtracking==0 ) {

            	                      	        if (current==null) {
            	                      	            current = createModelElementForParent(grammarAccess.getXMemberFeatureCallRule());
            	                      	        }
            	                             		add(
            	                             			current, 
            	                             			"memberCallArguments",
            	                              		lv_memberCallArguments_18_0, 
            	                              		"XExpression");
            	                      	        afterParserOrEnumRuleCall();
            	                      	    
            	                    }

            	                    }


            	                    }

            	                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2264:2: (otherlv_19= ',' ( (lv_memberCallArguments_20_0= ruleXExpression ) ) )*
            	                    loop31:
            	                    do {
            	                        int alt31=2;
            	                        int LA31_0 = input.LA(1);

            	                        if ( (LA31_0==46) ) {
            	                            alt31=1;
            	                        }


            	                        switch (alt31) {
            	                    	case 1 :
            	                    	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2264:4: otherlv_19= ',' ( (lv_memberCallArguments_20_0= ruleXExpression ) )
            	                    	    {
            	                    	    otherlv_19=(Token)match(input,46,FOLLOW_46_in_ruleXMemberFeatureCall5495); if (state.failed) return current;
            	                    	    if ( state.backtracking==0 ) {

            	                    	          	newLeafNode(otherlv_19, grammarAccess.getXMemberFeatureCallAccess().getCommaKeyword_1_1_3_1_1_1_0());
            	                    	          
            	                    	    }
            	                    	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2268:1: ( (lv_memberCallArguments_20_0= ruleXExpression ) )
            	                    	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2269:1: (lv_memberCallArguments_20_0= ruleXExpression )
            	                    	    {
            	                    	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2269:1: (lv_memberCallArguments_20_0= ruleXExpression )
            	                    	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2270:3: lv_memberCallArguments_20_0= ruleXExpression
            	                    	    {
            	                    	    if ( state.backtracking==0 ) {
            	                    	       
            	                    	      	        newCompositeNode(grammarAccess.getXMemberFeatureCallAccess().getMemberCallArgumentsXExpressionParserRuleCall_1_1_3_1_1_1_1_0()); 
            	                    	      	    
            	                    	    }
            	                    	    pushFollow(FOLLOW_ruleXExpression_in_ruleXMemberFeatureCall5516);
            	                    	    lv_memberCallArguments_20_0=ruleXExpression();

            	                    	    state._fsp--;
            	                    	    if (state.failed) return current;
            	                    	    if ( state.backtracking==0 ) {

            	                    	      	        if (current==null) {
            	                    	      	            current = createModelElementForParent(grammarAccess.getXMemberFeatureCallRule());
            	                    	      	        }
            	                    	             		add(
            	                    	             			current, 
            	                    	             			"memberCallArguments",
            	                    	              		lv_memberCallArguments_20_0, 
            	                    	              		"XExpression");
            	                    	      	        afterParserOrEnumRuleCall();
            	                    	      	    
            	                    	    }

            	                    	    }


            	                    	    }


            	                    	    }
            	                    	    break;

            	                    	default :
            	                    	    break loop31;
            	                        }
            	                    } while (true);


            	                    }


            	                    }
            	                    break;

            	            }

            	            otherlv_21=(Token)match(input,48,FOLLOW_48_in_ruleXMemberFeatureCall5533); if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	                  	newLeafNode(otherlv_21, grammarAccess.getXMemberFeatureCallAccess().getRightParenthesisKeyword_1_1_3_2());
            	                  
            	            }

            	            }
            	            break;

            	    }

            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2290:3: ( ( ( () '[' ) )=> (lv_memberCallArguments_22_0= ruleXClosure ) )?
            	    int alt34=2;
            	    alt34 = dfa34.predict(input);
            	    switch (alt34) {
            	        case 1 :
            	            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2290:4: ( ( () '[' ) )=> (lv_memberCallArguments_22_0= ruleXClosure )
            	            {
            	            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2293:1: (lv_memberCallArguments_22_0= ruleXClosure )
            	            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2294:3: lv_memberCallArguments_22_0= ruleXClosure
            	            {
            	            if ( state.backtracking==0 ) {
            	               
            	              	        newCompositeNode(grammarAccess.getXMemberFeatureCallAccess().getMemberCallArgumentsXClosureParserRuleCall_1_1_4_0()); 
            	              	    
            	            }
            	            pushFollow(FOLLOW_ruleXClosure_in_ruleXMemberFeatureCall5568);
            	            lv_memberCallArguments_22_0=ruleXClosure();

            	            state._fsp--;
            	            if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	              	        if (current==null) {
            	              	            current = createModelElementForParent(grammarAccess.getXMemberFeatureCallRule());
            	              	        }
            	                     		add(
            	                     			current, 
            	                     			"memberCallArguments",
            	                      		lv_memberCallArguments_22_0, 
            	                      		"XClosure");
            	              	        afterParserOrEnumRuleCall();
            	              	    
            	            }

            	            }


            	            }
            	            break;

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop35;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXMemberFeatureCall"


    // $ANTLR start "entryRuleXPrimaryExpression"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2318:1: entryRuleXPrimaryExpression returns [EObject current=null] : iv_ruleXPrimaryExpression= ruleXPrimaryExpression EOF ;
    public final EObject entryRuleXPrimaryExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXPrimaryExpression = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2319:2: (iv_ruleXPrimaryExpression= ruleXPrimaryExpression EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2320:2: iv_ruleXPrimaryExpression= ruleXPrimaryExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getXPrimaryExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleXPrimaryExpression_in_entryRuleXPrimaryExpression5608);
            iv_ruleXPrimaryExpression=ruleXPrimaryExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleXPrimaryExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleXPrimaryExpression5618); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXPrimaryExpression"


    // $ANTLR start "ruleXPrimaryExpression"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2327:1: ruleXPrimaryExpression returns [EObject current=null] : (this_XConstructorCall_0= ruleXConstructorCall | this_XBlockExpression_1= ruleXBlockExpression | this_XSwitchExpression_2= ruleXSwitchExpression | this_XFeatureCall_3= ruleXFeatureCall | this_XLiteral_4= ruleXLiteral | this_XIfExpression_5= ruleXIfExpression | this_XForLoopExpression_6= ruleXForLoopExpression | this_XWhileExpression_7= ruleXWhileExpression | this_XDoWhileExpression_8= ruleXDoWhileExpression | this_XThrowExpression_9= ruleXThrowExpression | this_XReturnExpression_10= ruleXReturnExpression | this_XTryCatchFinallyExpression_11= ruleXTryCatchFinallyExpression | this_XParenthesizedExpression_12= ruleXParenthesizedExpression ) ;
    public final EObject ruleXPrimaryExpression() throws RecognitionException {
        EObject current = null;

        EObject this_XConstructorCall_0 = null;

        EObject this_XBlockExpression_1 = null;

        EObject this_XSwitchExpression_2 = null;

        EObject this_XFeatureCall_3 = null;

        EObject this_XLiteral_4 = null;

        EObject this_XIfExpression_5 = null;

        EObject this_XForLoopExpression_6 = null;

        EObject this_XWhileExpression_7 = null;

        EObject this_XDoWhileExpression_8 = null;

        EObject this_XThrowExpression_9 = null;

        EObject this_XReturnExpression_10 = null;

        EObject this_XTryCatchFinallyExpression_11 = null;

        EObject this_XParenthesizedExpression_12 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2330:28: ( (this_XConstructorCall_0= ruleXConstructorCall | this_XBlockExpression_1= ruleXBlockExpression | this_XSwitchExpression_2= ruleXSwitchExpression | this_XFeatureCall_3= ruleXFeatureCall | this_XLiteral_4= ruleXLiteral | this_XIfExpression_5= ruleXIfExpression | this_XForLoopExpression_6= ruleXForLoopExpression | this_XWhileExpression_7= ruleXWhileExpression | this_XDoWhileExpression_8= ruleXDoWhileExpression | this_XThrowExpression_9= ruleXThrowExpression | this_XReturnExpression_10= ruleXReturnExpression | this_XTryCatchFinallyExpression_11= ruleXTryCatchFinallyExpression | this_XParenthesizedExpression_12= ruleXParenthesizedExpression ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2331:1: (this_XConstructorCall_0= ruleXConstructorCall | this_XBlockExpression_1= ruleXBlockExpression | this_XSwitchExpression_2= ruleXSwitchExpression | this_XFeatureCall_3= ruleXFeatureCall | this_XLiteral_4= ruleXLiteral | this_XIfExpression_5= ruleXIfExpression | this_XForLoopExpression_6= ruleXForLoopExpression | this_XWhileExpression_7= ruleXWhileExpression | this_XDoWhileExpression_8= ruleXDoWhileExpression | this_XThrowExpression_9= ruleXThrowExpression | this_XReturnExpression_10= ruleXReturnExpression | this_XTryCatchFinallyExpression_11= ruleXTryCatchFinallyExpression | this_XParenthesizedExpression_12= ruleXParenthesizedExpression )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2331:1: (this_XConstructorCall_0= ruleXConstructorCall | this_XBlockExpression_1= ruleXBlockExpression | this_XSwitchExpression_2= ruleXSwitchExpression | this_XFeatureCall_3= ruleXFeatureCall | this_XLiteral_4= ruleXLiteral | this_XIfExpression_5= ruleXIfExpression | this_XForLoopExpression_6= ruleXForLoopExpression | this_XWhileExpression_7= ruleXWhileExpression | this_XDoWhileExpression_8= ruleXDoWhileExpression | this_XThrowExpression_9= ruleXThrowExpression | this_XReturnExpression_10= ruleXReturnExpression | this_XTryCatchFinallyExpression_11= ruleXTryCatchFinallyExpression | this_XParenthesizedExpression_12= ruleXParenthesizedExpression )
            int alt36=13;
            alt36 = dfa36.predict(input);
            switch (alt36) {
                case 1 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2332:5: this_XConstructorCall_0= ruleXConstructorCall
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getXPrimaryExpressionAccess().getXConstructorCallParserRuleCall_0()); 
                          
                    }
                    pushFollow(FOLLOW_ruleXConstructorCall_in_ruleXPrimaryExpression5665);
                    this_XConstructorCall_0=ruleXConstructorCall();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_XConstructorCall_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2342:5: this_XBlockExpression_1= ruleXBlockExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getXPrimaryExpressionAccess().getXBlockExpressionParserRuleCall_1()); 
                          
                    }
                    pushFollow(FOLLOW_ruleXBlockExpression_in_ruleXPrimaryExpression5692);
                    this_XBlockExpression_1=ruleXBlockExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_XBlockExpression_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2352:5: this_XSwitchExpression_2= ruleXSwitchExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getXPrimaryExpressionAccess().getXSwitchExpressionParserRuleCall_2()); 
                          
                    }
                    pushFollow(FOLLOW_ruleXSwitchExpression_in_ruleXPrimaryExpression5719);
                    this_XSwitchExpression_2=ruleXSwitchExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_XSwitchExpression_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 4 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2362:5: this_XFeatureCall_3= ruleXFeatureCall
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getXPrimaryExpressionAccess().getXFeatureCallParserRuleCall_3()); 
                          
                    }
                    pushFollow(FOLLOW_ruleXFeatureCall_in_ruleXPrimaryExpression5746);
                    this_XFeatureCall_3=ruleXFeatureCall();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_XFeatureCall_3; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 5 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2372:5: this_XLiteral_4= ruleXLiteral
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getXPrimaryExpressionAccess().getXLiteralParserRuleCall_4()); 
                          
                    }
                    pushFollow(FOLLOW_ruleXLiteral_in_ruleXPrimaryExpression5773);
                    this_XLiteral_4=ruleXLiteral();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_XLiteral_4; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 6 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2382:5: this_XIfExpression_5= ruleXIfExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getXPrimaryExpressionAccess().getXIfExpressionParserRuleCall_5()); 
                          
                    }
                    pushFollow(FOLLOW_ruleXIfExpression_in_ruleXPrimaryExpression5800);
                    this_XIfExpression_5=ruleXIfExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_XIfExpression_5; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 7 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2392:5: this_XForLoopExpression_6= ruleXForLoopExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getXPrimaryExpressionAccess().getXForLoopExpressionParserRuleCall_6()); 
                          
                    }
                    pushFollow(FOLLOW_ruleXForLoopExpression_in_ruleXPrimaryExpression5827);
                    this_XForLoopExpression_6=ruleXForLoopExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_XForLoopExpression_6; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 8 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2402:5: this_XWhileExpression_7= ruleXWhileExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getXPrimaryExpressionAccess().getXWhileExpressionParserRuleCall_7()); 
                          
                    }
                    pushFollow(FOLLOW_ruleXWhileExpression_in_ruleXPrimaryExpression5854);
                    this_XWhileExpression_7=ruleXWhileExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_XWhileExpression_7; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 9 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2412:5: this_XDoWhileExpression_8= ruleXDoWhileExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getXPrimaryExpressionAccess().getXDoWhileExpressionParserRuleCall_8()); 
                          
                    }
                    pushFollow(FOLLOW_ruleXDoWhileExpression_in_ruleXPrimaryExpression5881);
                    this_XDoWhileExpression_8=ruleXDoWhileExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_XDoWhileExpression_8; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 10 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2422:5: this_XThrowExpression_9= ruleXThrowExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getXPrimaryExpressionAccess().getXThrowExpressionParserRuleCall_9()); 
                          
                    }
                    pushFollow(FOLLOW_ruleXThrowExpression_in_ruleXPrimaryExpression5908);
                    this_XThrowExpression_9=ruleXThrowExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_XThrowExpression_9; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 11 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2432:5: this_XReturnExpression_10= ruleXReturnExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getXPrimaryExpressionAccess().getXReturnExpressionParserRuleCall_10()); 
                          
                    }
                    pushFollow(FOLLOW_ruleXReturnExpression_in_ruleXPrimaryExpression5935);
                    this_XReturnExpression_10=ruleXReturnExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_XReturnExpression_10; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 12 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2442:5: this_XTryCatchFinallyExpression_11= ruleXTryCatchFinallyExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getXPrimaryExpressionAccess().getXTryCatchFinallyExpressionParserRuleCall_11()); 
                          
                    }
                    pushFollow(FOLLOW_ruleXTryCatchFinallyExpression_in_ruleXPrimaryExpression5962);
                    this_XTryCatchFinallyExpression_11=ruleXTryCatchFinallyExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_XTryCatchFinallyExpression_11; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 13 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2452:5: this_XParenthesizedExpression_12= ruleXParenthesizedExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getXPrimaryExpressionAccess().getXParenthesizedExpressionParserRuleCall_12()); 
                          
                    }
                    pushFollow(FOLLOW_ruleXParenthesizedExpression_in_ruleXPrimaryExpression5989);
                    this_XParenthesizedExpression_12=ruleXParenthesizedExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_XParenthesizedExpression_12; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXPrimaryExpression"


    // $ANTLR start "entryRuleXLiteral"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2468:1: entryRuleXLiteral returns [EObject current=null] : iv_ruleXLiteral= ruleXLiteral EOF ;
    public final EObject entryRuleXLiteral() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXLiteral = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2469:2: (iv_ruleXLiteral= ruleXLiteral EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2470:2: iv_ruleXLiteral= ruleXLiteral EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getXLiteralRule()); 
            }
            pushFollow(FOLLOW_ruleXLiteral_in_entryRuleXLiteral6024);
            iv_ruleXLiteral=ruleXLiteral();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleXLiteral; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleXLiteral6034); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXLiteral"


    // $ANTLR start "ruleXLiteral"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2477:1: ruleXLiteral returns [EObject current=null] : ( ( ( ( () '[' ) )=>this_XClosure_0= ruleXClosure ) | this_XBooleanLiteral_1= ruleXBooleanLiteral | this_XNumberLiteral_2= ruleXNumberLiteral | this_XNullLiteral_3= ruleXNullLiteral | this_XStringLiteral_4= ruleXStringLiteral | this_XTypeLiteral_5= ruleXTypeLiteral ) ;
    public final EObject ruleXLiteral() throws RecognitionException {
        EObject current = null;

        EObject this_XClosure_0 = null;

        EObject this_XBooleanLiteral_1 = null;

        EObject this_XNumberLiteral_2 = null;

        EObject this_XNullLiteral_3 = null;

        EObject this_XStringLiteral_4 = null;

        EObject this_XTypeLiteral_5 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2480:28: ( ( ( ( ( () '[' ) )=>this_XClosure_0= ruleXClosure ) | this_XBooleanLiteral_1= ruleXBooleanLiteral | this_XNumberLiteral_2= ruleXNumberLiteral | this_XNullLiteral_3= ruleXNullLiteral | this_XStringLiteral_4= ruleXStringLiteral | this_XTypeLiteral_5= ruleXTypeLiteral ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2481:1: ( ( ( ( () '[' ) )=>this_XClosure_0= ruleXClosure ) | this_XBooleanLiteral_1= ruleXBooleanLiteral | this_XNumberLiteral_2= ruleXNumberLiteral | this_XNullLiteral_3= ruleXNullLiteral | this_XStringLiteral_4= ruleXStringLiteral | this_XTypeLiteral_5= ruleXTypeLiteral )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2481:1: ( ( ( ( () '[' ) )=>this_XClosure_0= ruleXClosure ) | this_XBooleanLiteral_1= ruleXBooleanLiteral | this_XNumberLiteral_2= ruleXNumberLiteral | this_XNullLiteral_3= ruleXNullLiteral | this_XStringLiteral_4= ruleXStringLiteral | this_XTypeLiteral_5= ruleXTypeLiteral )
            int alt37=6;
            int LA37_0 = input.LA(1);

            if ( (LA37_0==49) && (synpred18_InternalDeepClone())) {
                alt37=1;
            }
            else if ( ((LA37_0>=67 && LA37_0<=68)) ) {
                alt37=2;
            }
            else if ( ((LA37_0>=RULE_HEX && LA37_0<=RULE_DECIMAL)) ) {
                alt37=3;
            }
            else if ( (LA37_0==69) ) {
                alt37=4;
            }
            else if ( (LA37_0==RULE_STRING) ) {
                alt37=5;
            }
            else if ( (LA37_0==70) ) {
                alt37=6;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 37, 0, input);

                throw nvae;
            }
            switch (alt37) {
                case 1 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2481:2: ( ( ( () '[' ) )=>this_XClosure_0= ruleXClosure )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2481:2: ( ( ( () '[' ) )=>this_XClosure_0= ruleXClosure )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2481:3: ( ( () '[' ) )=>this_XClosure_0= ruleXClosure
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getXLiteralAccess().getXClosureParserRuleCall_0()); 
                          
                    }
                    pushFollow(FOLLOW_ruleXClosure_in_ruleXLiteral6094);
                    this_XClosure_0=ruleXClosure();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_XClosure_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2494:5: this_XBooleanLiteral_1= ruleXBooleanLiteral
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getXLiteralAccess().getXBooleanLiteralParserRuleCall_1()); 
                          
                    }
                    pushFollow(FOLLOW_ruleXBooleanLiteral_in_ruleXLiteral6122);
                    this_XBooleanLiteral_1=ruleXBooleanLiteral();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_XBooleanLiteral_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2504:5: this_XNumberLiteral_2= ruleXNumberLiteral
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getXLiteralAccess().getXNumberLiteralParserRuleCall_2()); 
                          
                    }
                    pushFollow(FOLLOW_ruleXNumberLiteral_in_ruleXLiteral6149);
                    this_XNumberLiteral_2=ruleXNumberLiteral();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_XNumberLiteral_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 4 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2514:5: this_XNullLiteral_3= ruleXNullLiteral
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getXLiteralAccess().getXNullLiteralParserRuleCall_3()); 
                          
                    }
                    pushFollow(FOLLOW_ruleXNullLiteral_in_ruleXLiteral6176);
                    this_XNullLiteral_3=ruleXNullLiteral();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_XNullLiteral_3; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 5 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2524:5: this_XStringLiteral_4= ruleXStringLiteral
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getXLiteralAccess().getXStringLiteralParserRuleCall_4()); 
                          
                    }
                    pushFollow(FOLLOW_ruleXStringLiteral_in_ruleXLiteral6203);
                    this_XStringLiteral_4=ruleXStringLiteral();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_XStringLiteral_4; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 6 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2534:5: this_XTypeLiteral_5= ruleXTypeLiteral
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getXLiteralAccess().getXTypeLiteralParserRuleCall_5()); 
                          
                    }
                    pushFollow(FOLLOW_ruleXTypeLiteral_in_ruleXLiteral6230);
                    this_XTypeLiteral_5=ruleXTypeLiteral();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_XTypeLiteral_5; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXLiteral"


    // $ANTLR start "entryRuleXClosure"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2550:1: entryRuleXClosure returns [EObject current=null] : iv_ruleXClosure= ruleXClosure EOF ;
    public final EObject entryRuleXClosure() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXClosure = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2551:2: (iv_ruleXClosure= ruleXClosure EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2552:2: iv_ruleXClosure= ruleXClosure EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getXClosureRule()); 
            }
            pushFollow(FOLLOW_ruleXClosure_in_entryRuleXClosure6265);
            iv_ruleXClosure=ruleXClosure();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleXClosure; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleXClosure6275); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXClosure"


    // $ANTLR start "ruleXClosure"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2559:1: ruleXClosure returns [EObject current=null] : ( ( ( ( () '[' ) )=> ( () otherlv_1= '[' ) ) ( ( ( ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> ( ( ( (lv_declaredFormalParameters_2_0= ruleJvmFormalParameter ) ) (otherlv_3= ',' ( (lv_declaredFormalParameters_4_0= ruleJvmFormalParameter ) ) )* )? ( (lv_explicitSyntax_5_0= '|' ) ) ) )? ( (lv_expression_6_0= ruleXExpressionInClosure ) ) otherlv_7= ']' ) ;
    public final EObject ruleXClosure() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token lv_explicitSyntax_5_0=null;
        Token otherlv_7=null;
        EObject lv_declaredFormalParameters_2_0 = null;

        EObject lv_declaredFormalParameters_4_0 = null;

        EObject lv_expression_6_0 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2562:28: ( ( ( ( ( () '[' ) )=> ( () otherlv_1= '[' ) ) ( ( ( ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> ( ( ( (lv_declaredFormalParameters_2_0= ruleJvmFormalParameter ) ) (otherlv_3= ',' ( (lv_declaredFormalParameters_4_0= ruleJvmFormalParameter ) ) )* )? ( (lv_explicitSyntax_5_0= '|' ) ) ) )? ( (lv_expression_6_0= ruleXExpressionInClosure ) ) otherlv_7= ']' ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2563:1: ( ( ( ( () '[' ) )=> ( () otherlv_1= '[' ) ) ( ( ( ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> ( ( ( (lv_declaredFormalParameters_2_0= ruleJvmFormalParameter ) ) (otherlv_3= ',' ( (lv_declaredFormalParameters_4_0= ruleJvmFormalParameter ) ) )* )? ( (lv_explicitSyntax_5_0= '|' ) ) ) )? ( (lv_expression_6_0= ruleXExpressionInClosure ) ) otherlv_7= ']' )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2563:1: ( ( ( ( () '[' ) )=> ( () otherlv_1= '[' ) ) ( ( ( ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> ( ( ( (lv_declaredFormalParameters_2_0= ruleJvmFormalParameter ) ) (otherlv_3= ',' ( (lv_declaredFormalParameters_4_0= ruleJvmFormalParameter ) ) )* )? ( (lv_explicitSyntax_5_0= '|' ) ) ) )? ( (lv_expression_6_0= ruleXExpressionInClosure ) ) otherlv_7= ']' )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2563:2: ( ( ( () '[' ) )=> ( () otherlv_1= '[' ) ) ( ( ( ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> ( ( ( (lv_declaredFormalParameters_2_0= ruleJvmFormalParameter ) ) (otherlv_3= ',' ( (lv_declaredFormalParameters_4_0= ruleJvmFormalParameter ) ) )* )? ( (lv_explicitSyntax_5_0= '|' ) ) ) )? ( (lv_expression_6_0= ruleXExpressionInClosure ) ) otherlv_7= ']'
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2563:2: ( ( ( () '[' ) )=> ( () otherlv_1= '[' ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2563:3: ( ( () '[' ) )=> ( () otherlv_1= '[' )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2565:5: ( () otherlv_1= '[' )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2565:6: () otherlv_1= '['
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2565:6: ()
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2566:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getXClosureAccess().getXClosureAction_0_0_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,49,FOLLOW_49_in_ruleXClosure6335); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getXClosureAccess().getLeftSquareBracketKeyword_0_0_1());
                  
            }

            }


            }

            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2575:3: ( ( ( ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> ( ( ( (lv_declaredFormalParameters_2_0= ruleJvmFormalParameter ) ) (otherlv_3= ',' ( (lv_declaredFormalParameters_4_0= ruleJvmFormalParameter ) ) )* )? ( (lv_explicitSyntax_5_0= '|' ) ) ) )?
            int alt40=2;
            alt40 = dfa40.predict(input);
            switch (alt40) {
                case 1 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2575:4: ( ( ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> ( ( ( (lv_declaredFormalParameters_2_0= ruleJvmFormalParameter ) ) (otherlv_3= ',' ( (lv_declaredFormalParameters_4_0= ruleJvmFormalParameter ) ) )* )? ( (lv_explicitSyntax_5_0= '|' ) ) )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2590:6: ( ( ( (lv_declaredFormalParameters_2_0= ruleJvmFormalParameter ) ) (otherlv_3= ',' ( (lv_declaredFormalParameters_4_0= ruleJvmFormalParameter ) ) )* )? ( (lv_explicitSyntax_5_0= '|' ) ) )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2590:7: ( ( (lv_declaredFormalParameters_2_0= ruleJvmFormalParameter ) ) (otherlv_3= ',' ( (lv_declaredFormalParameters_4_0= ruleJvmFormalParameter ) ) )* )? ( (lv_explicitSyntax_5_0= '|' ) )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2590:7: ( ( (lv_declaredFormalParameters_2_0= ruleJvmFormalParameter ) ) (otherlv_3= ',' ( (lv_declaredFormalParameters_4_0= ruleJvmFormalParameter ) ) )* )?
                    int alt39=2;
                    int LA39_0 = input.LA(1);

                    if ( (LA39_0==RULE_ID||LA39_0==33||LA39_0==47) ) {
                        alt39=1;
                    }
                    switch (alt39) {
                        case 1 :
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2590:8: ( (lv_declaredFormalParameters_2_0= ruleJvmFormalParameter ) ) (otherlv_3= ',' ( (lv_declaredFormalParameters_4_0= ruleJvmFormalParameter ) ) )*
                            {
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2590:8: ( (lv_declaredFormalParameters_2_0= ruleJvmFormalParameter ) )
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2591:1: (lv_declaredFormalParameters_2_0= ruleJvmFormalParameter )
                            {
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2591:1: (lv_declaredFormalParameters_2_0= ruleJvmFormalParameter )
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2592:3: lv_declaredFormalParameters_2_0= ruleJvmFormalParameter
                            {
                            if ( state.backtracking==0 ) {
                               
                              	        newCompositeNode(grammarAccess.getXClosureAccess().getDeclaredFormalParametersJvmFormalParameterParserRuleCall_1_0_0_0_0()); 
                              	    
                            }
                            pushFollow(FOLLOW_ruleJvmFormalParameter_in_ruleXClosure6408);
                            lv_declaredFormalParameters_2_0=ruleJvmFormalParameter();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElementForParent(grammarAccess.getXClosureRule());
                              	        }
                                     		add(
                                     			current, 
                                     			"declaredFormalParameters",
                                      		lv_declaredFormalParameters_2_0, 
                                      		"JvmFormalParameter");
                              	        afterParserOrEnumRuleCall();
                              	    
                            }

                            }


                            }

                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2608:2: (otherlv_3= ',' ( (lv_declaredFormalParameters_4_0= ruleJvmFormalParameter ) ) )*
                            loop38:
                            do {
                                int alt38=2;
                                int LA38_0 = input.LA(1);

                                if ( (LA38_0==46) ) {
                                    alt38=1;
                                }


                                switch (alt38) {
                            	case 1 :
                            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2608:4: otherlv_3= ',' ( (lv_declaredFormalParameters_4_0= ruleJvmFormalParameter ) )
                            	    {
                            	    otherlv_3=(Token)match(input,46,FOLLOW_46_in_ruleXClosure6421); if (state.failed) return current;
                            	    if ( state.backtracking==0 ) {

                            	          	newLeafNode(otherlv_3, grammarAccess.getXClosureAccess().getCommaKeyword_1_0_0_1_0());
                            	          
                            	    }
                            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2612:1: ( (lv_declaredFormalParameters_4_0= ruleJvmFormalParameter ) )
                            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2613:1: (lv_declaredFormalParameters_4_0= ruleJvmFormalParameter )
                            	    {
                            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2613:1: (lv_declaredFormalParameters_4_0= ruleJvmFormalParameter )
                            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2614:3: lv_declaredFormalParameters_4_0= ruleJvmFormalParameter
                            	    {
                            	    if ( state.backtracking==0 ) {
                            	       
                            	      	        newCompositeNode(grammarAccess.getXClosureAccess().getDeclaredFormalParametersJvmFormalParameterParserRuleCall_1_0_0_1_1_0()); 
                            	      	    
                            	    }
                            	    pushFollow(FOLLOW_ruleJvmFormalParameter_in_ruleXClosure6442);
                            	    lv_declaredFormalParameters_4_0=ruleJvmFormalParameter();

                            	    state._fsp--;
                            	    if (state.failed) return current;
                            	    if ( state.backtracking==0 ) {

                            	      	        if (current==null) {
                            	      	            current = createModelElementForParent(grammarAccess.getXClosureRule());
                            	      	        }
                            	             		add(
                            	             			current, 
                            	             			"declaredFormalParameters",
                            	              		lv_declaredFormalParameters_4_0, 
                            	              		"JvmFormalParameter");
                            	      	        afterParserOrEnumRuleCall();
                            	      	    
                            	    }

                            	    }


                            	    }


                            	    }
                            	    break;

                            	default :
                            	    break loop38;
                                }
                            } while (true);


                            }
                            break;

                    }

                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2630:6: ( (lv_explicitSyntax_5_0= '|' ) )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2631:1: (lv_explicitSyntax_5_0= '|' )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2631:1: (lv_explicitSyntax_5_0= '|' )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2632:3: lv_explicitSyntax_5_0= '|'
                    {
                    lv_explicitSyntax_5_0=(Token)match(input,50,FOLLOW_50_in_ruleXClosure6464); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_explicitSyntax_5_0, grammarAccess.getXClosureAccess().getExplicitSyntaxVerticalLineKeyword_1_0_1_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getXClosureRule());
                      	        }
                             		setWithLastConsumed(current, "explicitSyntax", true, "|");
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;

            }

            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2645:5: ( (lv_expression_6_0= ruleXExpressionInClosure ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2646:1: (lv_expression_6_0= ruleXExpressionInClosure )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2646:1: (lv_expression_6_0= ruleXExpressionInClosure )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2647:3: lv_expression_6_0= ruleXExpressionInClosure
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getXClosureAccess().getExpressionXExpressionInClosureParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleXExpressionInClosure_in_ruleXClosure6501);
            lv_expression_6_0=ruleXExpressionInClosure();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getXClosureRule());
              	        }
                     		set(
                     			current, 
                     			"expression",
                      		lv_expression_6_0, 
                      		"XExpressionInClosure");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_7=(Token)match(input,51,FOLLOW_51_in_ruleXClosure6513); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_7, grammarAccess.getXClosureAccess().getRightSquareBracketKeyword_3());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXClosure"


    // $ANTLR start "entryRuleXExpressionInClosure"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2675:1: entryRuleXExpressionInClosure returns [EObject current=null] : iv_ruleXExpressionInClosure= ruleXExpressionInClosure EOF ;
    public final EObject entryRuleXExpressionInClosure() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXExpressionInClosure = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2676:2: (iv_ruleXExpressionInClosure= ruleXExpressionInClosure EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2677:2: iv_ruleXExpressionInClosure= ruleXExpressionInClosure EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getXExpressionInClosureRule()); 
            }
            pushFollow(FOLLOW_ruleXExpressionInClosure_in_entryRuleXExpressionInClosure6549);
            iv_ruleXExpressionInClosure=ruleXExpressionInClosure();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleXExpressionInClosure; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleXExpressionInClosure6559); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXExpressionInClosure"


    // $ANTLR start "ruleXExpressionInClosure"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2684:1: ruleXExpressionInClosure returns [EObject current=null] : ( () ( ( (lv_expressions_1_0= ruleXExpressionInsideBlock ) ) (otherlv_2= ';' )? )* ) ;
    public final EObject ruleXExpressionInClosure() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject lv_expressions_1_0 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2687:28: ( ( () ( ( (lv_expressions_1_0= ruleXExpressionInsideBlock ) ) (otherlv_2= ';' )? )* ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2688:1: ( () ( ( (lv_expressions_1_0= ruleXExpressionInsideBlock ) ) (otherlv_2= ';' )? )* )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2688:1: ( () ( ( (lv_expressions_1_0= ruleXExpressionInsideBlock ) ) (otherlv_2= ';' )? )* )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2688:2: () ( ( (lv_expressions_1_0= ruleXExpressionInsideBlock ) ) (otherlv_2= ';' )? )*
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2688:2: ()
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2689:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getXExpressionInClosureAccess().getXBlockExpressionAction_0(),
                          current);
                  
            }

            }

            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2694:2: ( ( (lv_expressions_1_0= ruleXExpressionInsideBlock ) ) (otherlv_2= ';' )? )*
            loop42:
            do {
                int alt42=2;
                int LA42_0 = input.LA(1);

                if ( ((LA42_0>=RULE_ID && LA42_0<=RULE_DECIMAL)||LA42_0==16||LA42_0==18||LA42_0==30||LA42_0==37||LA42_0==42||LA42_0==47||LA42_0==49||LA42_0==53||LA42_0==55||(LA42_0>=59 && LA42_0<=64)||(LA42_0>=66 && LA42_0<=73)) ) {
                    alt42=1;
                }


                switch (alt42) {
            	case 1 :
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2694:3: ( (lv_expressions_1_0= ruleXExpressionInsideBlock ) ) (otherlv_2= ';' )?
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2694:3: ( (lv_expressions_1_0= ruleXExpressionInsideBlock ) )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2695:1: (lv_expressions_1_0= ruleXExpressionInsideBlock )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2695:1: (lv_expressions_1_0= ruleXExpressionInsideBlock )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2696:3: lv_expressions_1_0= ruleXExpressionInsideBlock
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getXExpressionInClosureAccess().getExpressionsXExpressionInsideBlockParserRuleCall_1_0_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleXExpressionInsideBlock_in_ruleXExpressionInClosure6615);
            	    lv_expressions_1_0=ruleXExpressionInsideBlock();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getXExpressionInClosureRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"expressions",
            	              		lv_expressions_1_0, 
            	              		"XExpressionInsideBlock");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }

            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2712:2: (otherlv_2= ';' )?
            	    int alt41=2;
            	    int LA41_0 = input.LA(1);

            	    if ( (LA41_0==52) ) {
            	        alt41=1;
            	    }
            	    switch (alt41) {
            	        case 1 :
            	            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2712:4: otherlv_2= ';'
            	            {
            	            otherlv_2=(Token)match(input,52,FOLLOW_52_in_ruleXExpressionInClosure6628); if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	                  	newLeafNode(otherlv_2, grammarAccess.getXExpressionInClosureAccess().getSemicolonKeyword_1_1());
            	                  
            	            }

            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop42;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXExpressionInClosure"


    // $ANTLR start "entryRuleXShortClosure"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2724:1: entryRuleXShortClosure returns [EObject current=null] : iv_ruleXShortClosure= ruleXShortClosure EOF ;
    public final EObject entryRuleXShortClosure() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXShortClosure = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2725:2: (iv_ruleXShortClosure= ruleXShortClosure EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2726:2: iv_ruleXShortClosure= ruleXShortClosure EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getXShortClosureRule()); 
            }
            pushFollow(FOLLOW_ruleXShortClosure_in_entryRuleXShortClosure6668);
            iv_ruleXShortClosure=ruleXShortClosure();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleXShortClosure; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleXShortClosure6678); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXShortClosure"


    // $ANTLR start "ruleXShortClosure"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2733:1: ruleXShortClosure returns [EObject current=null] : ( ( ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> ( () ( ( (lv_declaredFormalParameters_1_0= ruleJvmFormalParameter ) ) (otherlv_2= ',' ( (lv_declaredFormalParameters_3_0= ruleJvmFormalParameter ) ) )* )? ( (lv_explicitSyntax_4_0= '|' ) ) ) ) ( (lv_expression_5_0= ruleXExpression ) ) ) ;
    public final EObject ruleXShortClosure() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token lv_explicitSyntax_4_0=null;
        EObject lv_declaredFormalParameters_1_0 = null;

        EObject lv_declaredFormalParameters_3_0 = null;

        EObject lv_expression_5_0 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2736:28: ( ( ( ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> ( () ( ( (lv_declaredFormalParameters_1_0= ruleJvmFormalParameter ) ) (otherlv_2= ',' ( (lv_declaredFormalParameters_3_0= ruleJvmFormalParameter ) ) )* )? ( (lv_explicitSyntax_4_0= '|' ) ) ) ) ( (lv_expression_5_0= ruleXExpression ) ) ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2737:1: ( ( ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> ( () ( ( (lv_declaredFormalParameters_1_0= ruleJvmFormalParameter ) ) (otherlv_2= ',' ( (lv_declaredFormalParameters_3_0= ruleJvmFormalParameter ) ) )* )? ( (lv_explicitSyntax_4_0= '|' ) ) ) ) ( (lv_expression_5_0= ruleXExpression ) ) )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2737:1: ( ( ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> ( () ( ( (lv_declaredFormalParameters_1_0= ruleJvmFormalParameter ) ) (otherlv_2= ',' ( (lv_declaredFormalParameters_3_0= ruleJvmFormalParameter ) ) )* )? ( (lv_explicitSyntax_4_0= '|' ) ) ) ) ( (lv_expression_5_0= ruleXExpression ) ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2737:2: ( ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> ( () ( ( (lv_declaredFormalParameters_1_0= ruleJvmFormalParameter ) ) (otherlv_2= ',' ( (lv_declaredFormalParameters_3_0= ruleJvmFormalParameter ) ) )* )? ( (lv_explicitSyntax_4_0= '|' ) ) ) ) ( (lv_expression_5_0= ruleXExpression ) )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2737:2: ( ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> ( () ( ( (lv_declaredFormalParameters_1_0= ruleJvmFormalParameter ) ) (otherlv_2= ',' ( (lv_declaredFormalParameters_3_0= ruleJvmFormalParameter ) ) )* )? ( (lv_explicitSyntax_4_0= '|' ) ) ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2737:3: ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> ( () ( ( (lv_declaredFormalParameters_1_0= ruleJvmFormalParameter ) ) (otherlv_2= ',' ( (lv_declaredFormalParameters_3_0= ruleJvmFormalParameter ) ) )* )? ( (lv_explicitSyntax_4_0= '|' ) ) )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2753:6: ( () ( ( (lv_declaredFormalParameters_1_0= ruleJvmFormalParameter ) ) (otherlv_2= ',' ( (lv_declaredFormalParameters_3_0= ruleJvmFormalParameter ) ) )* )? ( (lv_explicitSyntax_4_0= '|' ) ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2753:7: () ( ( (lv_declaredFormalParameters_1_0= ruleJvmFormalParameter ) ) (otherlv_2= ',' ( (lv_declaredFormalParameters_3_0= ruleJvmFormalParameter ) ) )* )? ( (lv_explicitSyntax_4_0= '|' ) )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2753:7: ()
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2754:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getXShortClosureAccess().getXClosureAction_0_0_0(),
                          current);
                  
            }

            }

            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2759:2: ( ( (lv_declaredFormalParameters_1_0= ruleJvmFormalParameter ) ) (otherlv_2= ',' ( (lv_declaredFormalParameters_3_0= ruleJvmFormalParameter ) ) )* )?
            int alt44=2;
            int LA44_0 = input.LA(1);

            if ( (LA44_0==RULE_ID||LA44_0==33||LA44_0==47) ) {
                alt44=1;
            }
            switch (alt44) {
                case 1 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2759:3: ( (lv_declaredFormalParameters_1_0= ruleJvmFormalParameter ) ) (otherlv_2= ',' ( (lv_declaredFormalParameters_3_0= ruleJvmFormalParameter ) ) )*
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2759:3: ( (lv_declaredFormalParameters_1_0= ruleJvmFormalParameter ) )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2760:1: (lv_declaredFormalParameters_1_0= ruleJvmFormalParameter )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2760:1: (lv_declaredFormalParameters_1_0= ruleJvmFormalParameter )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2761:3: lv_declaredFormalParameters_1_0= ruleJvmFormalParameter
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getXShortClosureAccess().getDeclaredFormalParametersJvmFormalParameterParserRuleCall_0_0_1_0_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleJvmFormalParameter_in_ruleXShortClosure6786);
                    lv_declaredFormalParameters_1_0=ruleJvmFormalParameter();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getXShortClosureRule());
                      	        }
                             		add(
                             			current, 
                             			"declaredFormalParameters",
                              		lv_declaredFormalParameters_1_0, 
                              		"JvmFormalParameter");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2777:2: (otherlv_2= ',' ( (lv_declaredFormalParameters_3_0= ruleJvmFormalParameter ) ) )*
                    loop43:
                    do {
                        int alt43=2;
                        int LA43_0 = input.LA(1);

                        if ( (LA43_0==46) ) {
                            alt43=1;
                        }


                        switch (alt43) {
                    	case 1 :
                    	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2777:4: otherlv_2= ',' ( (lv_declaredFormalParameters_3_0= ruleJvmFormalParameter ) )
                    	    {
                    	    otherlv_2=(Token)match(input,46,FOLLOW_46_in_ruleXShortClosure6799); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	          	newLeafNode(otherlv_2, grammarAccess.getXShortClosureAccess().getCommaKeyword_0_0_1_1_0());
                    	          
                    	    }
                    	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2781:1: ( (lv_declaredFormalParameters_3_0= ruleJvmFormalParameter ) )
                    	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2782:1: (lv_declaredFormalParameters_3_0= ruleJvmFormalParameter )
                    	    {
                    	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2782:1: (lv_declaredFormalParameters_3_0= ruleJvmFormalParameter )
                    	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2783:3: lv_declaredFormalParameters_3_0= ruleJvmFormalParameter
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getXShortClosureAccess().getDeclaredFormalParametersJvmFormalParameterParserRuleCall_0_0_1_1_1_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FOLLOW_ruleJvmFormalParameter_in_ruleXShortClosure6820);
                    	    lv_declaredFormalParameters_3_0=ruleJvmFormalParameter();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getXShortClosureRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"declaredFormalParameters",
                    	              		lv_declaredFormalParameters_3_0, 
                    	              		"JvmFormalParameter");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop43;
                        }
                    } while (true);


                    }
                    break;

            }

            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2799:6: ( (lv_explicitSyntax_4_0= '|' ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2800:1: (lv_explicitSyntax_4_0= '|' )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2800:1: (lv_explicitSyntax_4_0= '|' )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2801:3: lv_explicitSyntax_4_0= '|'
            {
            lv_explicitSyntax_4_0=(Token)match(input,50,FOLLOW_50_in_ruleXShortClosure6842); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                      newLeafNode(lv_explicitSyntax_4_0, grammarAccess.getXShortClosureAccess().getExplicitSyntaxVerticalLineKeyword_0_0_2_0());
                  
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getXShortClosureRule());
              	        }
                     		setWithLastConsumed(current, "explicitSyntax", true, "|");
              	    
            }

            }


            }


            }


            }

            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2814:4: ( (lv_expression_5_0= ruleXExpression ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2815:1: (lv_expression_5_0= ruleXExpression )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2815:1: (lv_expression_5_0= ruleXExpression )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2816:3: lv_expression_5_0= ruleXExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getXShortClosureAccess().getExpressionXExpressionParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleXExpression_in_ruleXShortClosure6878);
            lv_expression_5_0=ruleXExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getXShortClosureRule());
              	        }
                     		set(
                     			current, 
                     			"expression",
                      		lv_expression_5_0, 
                      		"XExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXShortClosure"


    // $ANTLR start "entryRuleXParenthesizedExpression"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2840:1: entryRuleXParenthesizedExpression returns [EObject current=null] : iv_ruleXParenthesizedExpression= ruleXParenthesizedExpression EOF ;
    public final EObject entryRuleXParenthesizedExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXParenthesizedExpression = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2841:2: (iv_ruleXParenthesizedExpression= ruleXParenthesizedExpression EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2842:2: iv_ruleXParenthesizedExpression= ruleXParenthesizedExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getXParenthesizedExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleXParenthesizedExpression_in_entryRuleXParenthesizedExpression6914);
            iv_ruleXParenthesizedExpression=ruleXParenthesizedExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleXParenthesizedExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleXParenthesizedExpression6924); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXParenthesizedExpression"


    // $ANTLR start "ruleXParenthesizedExpression"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2849:1: ruleXParenthesizedExpression returns [EObject current=null] : (otherlv_0= '(' this_XExpression_1= ruleXExpression otherlv_2= ')' ) ;
    public final EObject ruleXParenthesizedExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject this_XExpression_1 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2852:28: ( (otherlv_0= '(' this_XExpression_1= ruleXExpression otherlv_2= ')' ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2853:1: (otherlv_0= '(' this_XExpression_1= ruleXExpression otherlv_2= ')' )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2853:1: (otherlv_0= '(' this_XExpression_1= ruleXExpression otherlv_2= ')' )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2853:3: otherlv_0= '(' this_XExpression_1= ruleXExpression otherlv_2= ')'
            {
            otherlv_0=(Token)match(input,47,FOLLOW_47_in_ruleXParenthesizedExpression6961); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getXParenthesizedExpressionAccess().getLeftParenthesisKeyword_0());
                  
            }
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getXParenthesizedExpressionAccess().getXExpressionParserRuleCall_1()); 
                  
            }
            pushFollow(FOLLOW_ruleXExpression_in_ruleXParenthesizedExpression6983);
            this_XExpression_1=ruleXExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_XExpression_1; 
                      afterParserOrEnumRuleCall();
                  
            }
            otherlv_2=(Token)match(input,48,FOLLOW_48_in_ruleXParenthesizedExpression6994); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getXParenthesizedExpressionAccess().getRightParenthesisKeyword_2());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXParenthesizedExpression"


    // $ANTLR start "entryRuleXIfExpression"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2878:1: entryRuleXIfExpression returns [EObject current=null] : iv_ruleXIfExpression= ruleXIfExpression EOF ;
    public final EObject entryRuleXIfExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXIfExpression = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2879:2: (iv_ruleXIfExpression= ruleXIfExpression EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2880:2: iv_ruleXIfExpression= ruleXIfExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getXIfExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleXIfExpression_in_entryRuleXIfExpression7030);
            iv_ruleXIfExpression=ruleXIfExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleXIfExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleXIfExpression7040); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXIfExpression"


    // $ANTLR start "ruleXIfExpression"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2887:1: ruleXIfExpression returns [EObject current=null] : ( () otherlv_1= 'if' otherlv_2= '(' ( (lv_if_3_0= ruleXExpression ) ) otherlv_4= ')' ( (lv_then_5_0= ruleXExpression ) ) ( ( ( 'else' )=>otherlv_6= 'else' ) ( (lv_else_7_0= ruleXExpression ) ) )? ) ;
    public final EObject ruleXIfExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_if_3_0 = null;

        EObject lv_then_5_0 = null;

        EObject lv_else_7_0 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2890:28: ( ( () otherlv_1= 'if' otherlv_2= '(' ( (lv_if_3_0= ruleXExpression ) ) otherlv_4= ')' ( (lv_then_5_0= ruleXExpression ) ) ( ( ( 'else' )=>otherlv_6= 'else' ) ( (lv_else_7_0= ruleXExpression ) ) )? ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2891:1: ( () otherlv_1= 'if' otherlv_2= '(' ( (lv_if_3_0= ruleXExpression ) ) otherlv_4= ')' ( (lv_then_5_0= ruleXExpression ) ) ( ( ( 'else' )=>otherlv_6= 'else' ) ( (lv_else_7_0= ruleXExpression ) ) )? )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2891:1: ( () otherlv_1= 'if' otherlv_2= '(' ( (lv_if_3_0= ruleXExpression ) ) otherlv_4= ')' ( (lv_then_5_0= ruleXExpression ) ) ( ( ( 'else' )=>otherlv_6= 'else' ) ( (lv_else_7_0= ruleXExpression ) ) )? )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2891:2: () otherlv_1= 'if' otherlv_2= '(' ( (lv_if_3_0= ruleXExpression ) ) otherlv_4= ')' ( (lv_then_5_0= ruleXExpression ) ) ( ( ( 'else' )=>otherlv_6= 'else' ) ( (lv_else_7_0= ruleXExpression ) ) )?
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2891:2: ()
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2892:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getXIfExpressionAccess().getXIfExpressionAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,53,FOLLOW_53_in_ruleXIfExpression7086); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getXIfExpressionAccess().getIfKeyword_1());
                  
            }
            otherlv_2=(Token)match(input,47,FOLLOW_47_in_ruleXIfExpression7098); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getXIfExpressionAccess().getLeftParenthesisKeyword_2());
                  
            }
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2905:1: ( (lv_if_3_0= ruleXExpression ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2906:1: (lv_if_3_0= ruleXExpression )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2906:1: (lv_if_3_0= ruleXExpression )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2907:3: lv_if_3_0= ruleXExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getXIfExpressionAccess().getIfXExpressionParserRuleCall_3_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleXExpression_in_ruleXIfExpression7119);
            lv_if_3_0=ruleXExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getXIfExpressionRule());
              	        }
                     		set(
                     			current, 
                     			"if",
                      		lv_if_3_0, 
                      		"XExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_4=(Token)match(input,48,FOLLOW_48_in_ruleXIfExpression7131); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getXIfExpressionAccess().getRightParenthesisKeyword_4());
                  
            }
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2927:1: ( (lv_then_5_0= ruleXExpression ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2928:1: (lv_then_5_0= ruleXExpression )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2928:1: (lv_then_5_0= ruleXExpression )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2929:3: lv_then_5_0= ruleXExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getXIfExpressionAccess().getThenXExpressionParserRuleCall_5_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleXExpression_in_ruleXIfExpression7152);
            lv_then_5_0=ruleXExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getXIfExpressionRule());
              	        }
                     		set(
                     			current, 
                     			"then",
                      		lv_then_5_0, 
                      		"XExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2945:2: ( ( ( 'else' )=>otherlv_6= 'else' ) ( (lv_else_7_0= ruleXExpression ) ) )?
            int alt45=2;
            int LA45_0 = input.LA(1);

            if ( (LA45_0==54) ) {
                int LA45_1 = input.LA(2);

                if ( (synpred22_InternalDeepClone()) ) {
                    alt45=1;
                }
            }
            switch (alt45) {
                case 1 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2945:3: ( ( 'else' )=>otherlv_6= 'else' ) ( (lv_else_7_0= ruleXExpression ) )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2945:3: ( ( 'else' )=>otherlv_6= 'else' )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2945:4: ( 'else' )=>otherlv_6= 'else'
                    {
                    otherlv_6=(Token)match(input,54,FOLLOW_54_in_ruleXIfExpression7173); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_6, grammarAccess.getXIfExpressionAccess().getElseKeyword_6_0());
                          
                    }

                    }

                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2950:2: ( (lv_else_7_0= ruleXExpression ) )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2951:1: (lv_else_7_0= ruleXExpression )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2951:1: (lv_else_7_0= ruleXExpression )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2952:3: lv_else_7_0= ruleXExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getXIfExpressionAccess().getElseXExpressionParserRuleCall_6_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleXExpression_in_ruleXIfExpression7195);
                    lv_else_7_0=ruleXExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getXIfExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"else",
                              		lv_else_7_0, 
                              		"XExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXIfExpression"


    // $ANTLR start "entryRuleXSwitchExpression"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2976:1: entryRuleXSwitchExpression returns [EObject current=null] : iv_ruleXSwitchExpression= ruleXSwitchExpression EOF ;
    public final EObject entryRuleXSwitchExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXSwitchExpression = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2977:2: (iv_ruleXSwitchExpression= ruleXSwitchExpression EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2978:2: iv_ruleXSwitchExpression= ruleXSwitchExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getXSwitchExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleXSwitchExpression_in_entryRuleXSwitchExpression7233);
            iv_ruleXSwitchExpression=ruleXSwitchExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleXSwitchExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleXSwitchExpression7243); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXSwitchExpression"


    // $ANTLR start "ruleXSwitchExpression"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2985:1: ruleXSwitchExpression returns [EObject current=null] : ( () otherlv_1= 'switch' ( ( ( ( ( ( ( ruleValidID ) ) ':' ) )=> ( ( (lv_localVarName_2_0= ruleValidID ) ) otherlv_3= ':' ) )? ( (lv_switch_4_0= ruleXExpression ) ) ) | ( ( ( ( '(' ( ( ruleValidID ) ) ':' ) )=> (otherlv_5= '(' ( (lv_localVarName_6_0= ruleValidID ) ) otherlv_7= ':' ) ) ( (lv_switch_8_0= ruleXExpression ) ) otherlv_9= ')' ) ) otherlv_10= '{' ( (lv_cases_11_0= ruleXCasePart ) )+ (otherlv_12= 'default' otherlv_13= ':' ( (lv_default_14_0= ruleXExpression ) ) )? otherlv_15= '}' ) ;
    public final EObject ruleXSwitchExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        AntlrDatatypeRuleToken lv_localVarName_2_0 = null;

        EObject lv_switch_4_0 = null;

        AntlrDatatypeRuleToken lv_localVarName_6_0 = null;

        EObject lv_switch_8_0 = null;

        EObject lv_cases_11_0 = null;

        EObject lv_default_14_0 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2988:28: ( ( () otherlv_1= 'switch' ( ( ( ( ( ( ( ruleValidID ) ) ':' ) )=> ( ( (lv_localVarName_2_0= ruleValidID ) ) otherlv_3= ':' ) )? ( (lv_switch_4_0= ruleXExpression ) ) ) | ( ( ( ( '(' ( ( ruleValidID ) ) ':' ) )=> (otherlv_5= '(' ( (lv_localVarName_6_0= ruleValidID ) ) otherlv_7= ':' ) ) ( (lv_switch_8_0= ruleXExpression ) ) otherlv_9= ')' ) ) otherlv_10= '{' ( (lv_cases_11_0= ruleXCasePart ) )+ (otherlv_12= 'default' otherlv_13= ':' ( (lv_default_14_0= ruleXExpression ) ) )? otherlv_15= '}' ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2989:1: ( () otherlv_1= 'switch' ( ( ( ( ( ( ( ruleValidID ) ) ':' ) )=> ( ( (lv_localVarName_2_0= ruleValidID ) ) otherlv_3= ':' ) )? ( (lv_switch_4_0= ruleXExpression ) ) ) | ( ( ( ( '(' ( ( ruleValidID ) ) ':' ) )=> (otherlv_5= '(' ( (lv_localVarName_6_0= ruleValidID ) ) otherlv_7= ':' ) ) ( (lv_switch_8_0= ruleXExpression ) ) otherlv_9= ')' ) ) otherlv_10= '{' ( (lv_cases_11_0= ruleXCasePart ) )+ (otherlv_12= 'default' otherlv_13= ':' ( (lv_default_14_0= ruleXExpression ) ) )? otherlv_15= '}' )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2989:1: ( () otherlv_1= 'switch' ( ( ( ( ( ( ( ruleValidID ) ) ':' ) )=> ( ( (lv_localVarName_2_0= ruleValidID ) ) otherlv_3= ':' ) )? ( (lv_switch_4_0= ruleXExpression ) ) ) | ( ( ( ( '(' ( ( ruleValidID ) ) ':' ) )=> (otherlv_5= '(' ( (lv_localVarName_6_0= ruleValidID ) ) otherlv_7= ':' ) ) ( (lv_switch_8_0= ruleXExpression ) ) otherlv_9= ')' ) ) otherlv_10= '{' ( (lv_cases_11_0= ruleXCasePart ) )+ (otherlv_12= 'default' otherlv_13= ':' ( (lv_default_14_0= ruleXExpression ) ) )? otherlv_15= '}' )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2989:2: () otherlv_1= 'switch' ( ( ( ( ( ( ( ruleValidID ) ) ':' ) )=> ( ( (lv_localVarName_2_0= ruleValidID ) ) otherlv_3= ':' ) )? ( (lv_switch_4_0= ruleXExpression ) ) ) | ( ( ( ( '(' ( ( ruleValidID ) ) ':' ) )=> (otherlv_5= '(' ( (lv_localVarName_6_0= ruleValidID ) ) otherlv_7= ':' ) ) ( (lv_switch_8_0= ruleXExpression ) ) otherlv_9= ')' ) ) otherlv_10= '{' ( (lv_cases_11_0= ruleXCasePart ) )+ (otherlv_12= 'default' otherlv_13= ':' ( (lv_default_14_0= ruleXExpression ) ) )? otherlv_15= '}'
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2989:2: ()
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2990:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getXSwitchExpressionAccess().getXSwitchExpressionAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,55,FOLLOW_55_in_ruleXSwitchExpression7289); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getXSwitchExpressionAccess().getSwitchKeyword_1());
                  
            }
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2999:1: ( ( ( ( ( ( ( ruleValidID ) ) ':' ) )=> ( ( (lv_localVarName_2_0= ruleValidID ) ) otherlv_3= ':' ) )? ( (lv_switch_4_0= ruleXExpression ) ) ) | ( ( ( ( '(' ( ( ruleValidID ) ) ':' ) )=> (otherlv_5= '(' ( (lv_localVarName_6_0= ruleValidID ) ) otherlv_7= ':' ) ) ( (lv_switch_8_0= ruleXExpression ) ) otherlv_9= ')' ) )
            int alt47=2;
            int LA47_0 = input.LA(1);

            if ( ((LA47_0>=RULE_ID && LA47_0<=RULE_DECIMAL)||LA47_0==16||LA47_0==18||LA47_0==30||LA47_0==37||LA47_0==42||LA47_0==49||LA47_0==53||LA47_0==55||(LA47_0>=59 && LA47_0<=61)||LA47_0==64||(LA47_0>=66 && LA47_0<=73)) ) {
                alt47=1;
            }
            else if ( (LA47_0==47) ) {
                int LA47_2 = input.LA(2);

                if ( (LA47_2==RULE_ID) ) {
                    int LA47_3 = input.LA(3);

                    if ( (LA47_3==15||LA47_3==18||(LA47_3>=20 && LA47_3<=41)||(LA47_3>=43 && LA47_3<=45)||(LA47_3>=47 && LA47_3<=49)||LA47_3==65) ) {
                        alt47=1;
                    }
                    else if ( (LA47_3==56) && (synpred24_InternalDeepClone())) {
                        alt47=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 47, 3, input);

                        throw nvae;
                    }
                }
                else if ( ((LA47_2>=RULE_STRING && LA47_2<=RULE_DECIMAL)||LA47_2==16||LA47_2==18||LA47_2==30||LA47_2==37||LA47_2==42||LA47_2==47||LA47_2==49||LA47_2==53||LA47_2==55||(LA47_2>=59 && LA47_2<=61)||LA47_2==64||(LA47_2>=66 && LA47_2<=73)) ) {
                    alt47=1;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 47, 2, input);

                    throw nvae;
                }
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 47, 0, input);

                throw nvae;
            }
            switch (alt47) {
                case 1 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2999:2: ( ( ( ( ( ( ruleValidID ) ) ':' ) )=> ( ( (lv_localVarName_2_0= ruleValidID ) ) otherlv_3= ':' ) )? ( (lv_switch_4_0= ruleXExpression ) ) )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2999:2: ( ( ( ( ( ( ruleValidID ) ) ':' ) )=> ( ( (lv_localVarName_2_0= ruleValidID ) ) otherlv_3= ':' ) )? ( (lv_switch_4_0= ruleXExpression ) ) )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2999:3: ( ( ( ( ( ruleValidID ) ) ':' ) )=> ( ( (lv_localVarName_2_0= ruleValidID ) ) otherlv_3= ':' ) )? ( (lv_switch_4_0= ruleXExpression ) )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2999:3: ( ( ( ( ( ruleValidID ) ) ':' ) )=> ( ( (lv_localVarName_2_0= ruleValidID ) ) otherlv_3= ':' ) )?
                    int alt46=2;
                    int LA46_0 = input.LA(1);

                    if ( (LA46_0==RULE_ID) ) {
                        int LA46_1 = input.LA(2);

                        if ( (LA46_1==56) && (synpred23_InternalDeepClone())) {
                            alt46=1;
                        }
                    }
                    switch (alt46) {
                        case 1 :
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2999:4: ( ( ( ( ruleValidID ) ) ':' ) )=> ( ( (lv_localVarName_2_0= ruleValidID ) ) otherlv_3= ':' )
                            {
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3004:5: ( ( (lv_localVarName_2_0= ruleValidID ) ) otherlv_3= ':' )
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3004:6: ( (lv_localVarName_2_0= ruleValidID ) ) otherlv_3= ':'
                            {
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3004:6: ( (lv_localVarName_2_0= ruleValidID ) )
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3005:1: (lv_localVarName_2_0= ruleValidID )
                            {
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3005:1: (lv_localVarName_2_0= ruleValidID )
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3006:3: lv_localVarName_2_0= ruleValidID
                            {
                            if ( state.backtracking==0 ) {
                               
                              	        newCompositeNode(grammarAccess.getXSwitchExpressionAccess().getLocalVarNameValidIDParserRuleCall_2_0_0_0_0_0()); 
                              	    
                            }
                            pushFollow(FOLLOW_ruleValidID_in_ruleXSwitchExpression7332);
                            lv_localVarName_2_0=ruleValidID();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElementForParent(grammarAccess.getXSwitchExpressionRule());
                              	        }
                                     		set(
                                     			current, 
                                     			"localVarName",
                                      		lv_localVarName_2_0, 
                                      		"ValidID");
                              	        afterParserOrEnumRuleCall();
                              	    
                            }

                            }


                            }

                            otherlv_3=(Token)match(input,56,FOLLOW_56_in_ruleXSwitchExpression7344); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                  	newLeafNode(otherlv_3, grammarAccess.getXSwitchExpressionAccess().getColonKeyword_2_0_0_0_1());
                                  
                            }

                            }


                            }
                            break;

                    }

                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3026:4: ( (lv_switch_4_0= ruleXExpression ) )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3027:1: (lv_switch_4_0= ruleXExpression )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3027:1: (lv_switch_4_0= ruleXExpression )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3028:3: lv_switch_4_0= ruleXExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getXSwitchExpressionAccess().getSwitchXExpressionParserRuleCall_2_0_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleXExpression_in_ruleXSwitchExpression7368);
                    lv_switch_4_0=ruleXExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getXSwitchExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"switch",
                              		lv_switch_4_0, 
                              		"XExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3045:6: ( ( ( ( '(' ( ( ruleValidID ) ) ':' ) )=> (otherlv_5= '(' ( (lv_localVarName_6_0= ruleValidID ) ) otherlv_7= ':' ) ) ( (lv_switch_8_0= ruleXExpression ) ) otherlv_9= ')' )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3045:6: ( ( ( ( '(' ( ( ruleValidID ) ) ':' ) )=> (otherlv_5= '(' ( (lv_localVarName_6_0= ruleValidID ) ) otherlv_7= ':' ) ) ( (lv_switch_8_0= ruleXExpression ) ) otherlv_9= ')' )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3045:7: ( ( ( '(' ( ( ruleValidID ) ) ':' ) )=> (otherlv_5= '(' ( (lv_localVarName_6_0= ruleValidID ) ) otherlv_7= ':' ) ) ( (lv_switch_8_0= ruleXExpression ) ) otherlv_9= ')'
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3045:7: ( ( ( '(' ( ( ruleValidID ) ) ':' ) )=> (otherlv_5= '(' ( (lv_localVarName_6_0= ruleValidID ) ) otherlv_7= ':' ) )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3045:8: ( ( '(' ( ( ruleValidID ) ) ':' ) )=> (otherlv_5= '(' ( (lv_localVarName_6_0= ruleValidID ) ) otherlv_7= ':' )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3051:5: (otherlv_5= '(' ( (lv_localVarName_6_0= ruleValidID ) ) otherlv_7= ':' )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3051:7: otherlv_5= '(' ( (lv_localVarName_6_0= ruleValidID ) ) otherlv_7= ':'
                    {
                    otherlv_5=(Token)match(input,47,FOLLOW_47_in_ruleXSwitchExpression7412); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_5, grammarAccess.getXSwitchExpressionAccess().getLeftParenthesisKeyword_2_1_0_0_0());
                          
                    }
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3055:1: ( (lv_localVarName_6_0= ruleValidID ) )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3056:1: (lv_localVarName_6_0= ruleValidID )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3056:1: (lv_localVarName_6_0= ruleValidID )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3057:3: lv_localVarName_6_0= ruleValidID
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getXSwitchExpressionAccess().getLocalVarNameValidIDParserRuleCall_2_1_0_0_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleValidID_in_ruleXSwitchExpression7433);
                    lv_localVarName_6_0=ruleValidID();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getXSwitchExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"localVarName",
                              		lv_localVarName_6_0, 
                              		"ValidID");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    otherlv_7=(Token)match(input,56,FOLLOW_56_in_ruleXSwitchExpression7445); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_7, grammarAccess.getXSwitchExpressionAccess().getColonKeyword_2_1_0_0_2());
                          
                    }

                    }


                    }

                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3077:3: ( (lv_switch_8_0= ruleXExpression ) )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3078:1: (lv_switch_8_0= ruleXExpression )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3078:1: (lv_switch_8_0= ruleXExpression )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3079:3: lv_switch_8_0= ruleXExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getXSwitchExpressionAccess().getSwitchXExpressionParserRuleCall_2_1_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleXExpression_in_ruleXSwitchExpression7468);
                    lv_switch_8_0=ruleXExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getXSwitchExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"switch",
                              		lv_switch_8_0, 
                              		"XExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    otherlv_9=(Token)match(input,48,FOLLOW_48_in_ruleXSwitchExpression7480); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_9, grammarAccess.getXSwitchExpressionAccess().getRightParenthesisKeyword_2_1_2());
                          
                    }

                    }


                    }
                    break;

            }

            otherlv_10=(Token)match(input,16,FOLLOW_16_in_ruleXSwitchExpression7494); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_10, grammarAccess.getXSwitchExpressionAccess().getLeftCurlyBracketKeyword_3());
                  
            }
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3103:1: ( (lv_cases_11_0= ruleXCasePart ) )+
            int cnt48=0;
            loop48:
            do {
                int alt48=2;
                int LA48_0 = input.LA(1);

                if ( (LA48_0==RULE_ID||LA48_0==33||LA48_0==47||LA48_0==56||LA48_0==58) ) {
                    alt48=1;
                }


                switch (alt48) {
            	case 1 :
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3104:1: (lv_cases_11_0= ruleXCasePart )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3104:1: (lv_cases_11_0= ruleXCasePart )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3105:3: lv_cases_11_0= ruleXCasePart
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getXSwitchExpressionAccess().getCasesXCasePartParserRuleCall_4_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleXCasePart_in_ruleXSwitchExpression7515);
            	    lv_cases_11_0=ruleXCasePart();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getXSwitchExpressionRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"cases",
            	              		lv_cases_11_0, 
            	              		"XCasePart");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt48 >= 1 ) break loop48;
            	    if (state.backtracking>0) {state.failed=true; return current;}
                        EarlyExitException eee =
                            new EarlyExitException(48, input);
                        throw eee;
                }
                cnt48++;
            } while (true);

            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3121:3: (otherlv_12= 'default' otherlv_13= ':' ( (lv_default_14_0= ruleXExpression ) ) )?
            int alt49=2;
            int LA49_0 = input.LA(1);

            if ( (LA49_0==57) ) {
                alt49=1;
            }
            switch (alt49) {
                case 1 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3121:5: otherlv_12= 'default' otherlv_13= ':' ( (lv_default_14_0= ruleXExpression ) )
                    {
                    otherlv_12=(Token)match(input,57,FOLLOW_57_in_ruleXSwitchExpression7529); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_12, grammarAccess.getXSwitchExpressionAccess().getDefaultKeyword_5_0());
                          
                    }
                    otherlv_13=(Token)match(input,56,FOLLOW_56_in_ruleXSwitchExpression7541); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_13, grammarAccess.getXSwitchExpressionAccess().getColonKeyword_5_1());
                          
                    }
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3129:1: ( (lv_default_14_0= ruleXExpression ) )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3130:1: (lv_default_14_0= ruleXExpression )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3130:1: (lv_default_14_0= ruleXExpression )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3131:3: lv_default_14_0= ruleXExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getXSwitchExpressionAccess().getDefaultXExpressionParserRuleCall_5_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleXExpression_in_ruleXSwitchExpression7562);
                    lv_default_14_0=ruleXExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getXSwitchExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"default",
                              		lv_default_14_0, 
                              		"XExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_15=(Token)match(input,17,FOLLOW_17_in_ruleXSwitchExpression7576); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_15, grammarAccess.getXSwitchExpressionAccess().getRightCurlyBracketKeyword_6());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXSwitchExpression"


    // $ANTLR start "entryRuleXCasePart"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3159:1: entryRuleXCasePart returns [EObject current=null] : iv_ruleXCasePart= ruleXCasePart EOF ;
    public final EObject entryRuleXCasePart() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXCasePart = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3160:2: (iv_ruleXCasePart= ruleXCasePart EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3161:2: iv_ruleXCasePart= ruleXCasePart EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getXCasePartRule()); 
            }
            pushFollow(FOLLOW_ruleXCasePart_in_entryRuleXCasePart7612);
            iv_ruleXCasePart=ruleXCasePart();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleXCasePart; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleXCasePart7622); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXCasePart"


    // $ANTLR start "ruleXCasePart"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3168:1: ruleXCasePart returns [EObject current=null] : ( ( (lv_typeGuard_0_0= ruleJvmTypeReference ) )? (otherlv_1= 'case' ( (lv_case_2_0= ruleXExpression ) ) )? otherlv_3= ':' ( (lv_then_4_0= ruleXExpression ) ) ) ;
    public final EObject ruleXCasePart() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_typeGuard_0_0 = null;

        EObject lv_case_2_0 = null;

        EObject lv_then_4_0 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3171:28: ( ( ( (lv_typeGuard_0_0= ruleJvmTypeReference ) )? (otherlv_1= 'case' ( (lv_case_2_0= ruleXExpression ) ) )? otherlv_3= ':' ( (lv_then_4_0= ruleXExpression ) ) ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3172:1: ( ( (lv_typeGuard_0_0= ruleJvmTypeReference ) )? (otherlv_1= 'case' ( (lv_case_2_0= ruleXExpression ) ) )? otherlv_3= ':' ( (lv_then_4_0= ruleXExpression ) ) )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3172:1: ( ( (lv_typeGuard_0_0= ruleJvmTypeReference ) )? (otherlv_1= 'case' ( (lv_case_2_0= ruleXExpression ) ) )? otherlv_3= ':' ( (lv_then_4_0= ruleXExpression ) ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3172:2: ( (lv_typeGuard_0_0= ruleJvmTypeReference ) )? (otherlv_1= 'case' ( (lv_case_2_0= ruleXExpression ) ) )? otherlv_3= ':' ( (lv_then_4_0= ruleXExpression ) )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3172:2: ( (lv_typeGuard_0_0= ruleJvmTypeReference ) )?
            int alt50=2;
            int LA50_0 = input.LA(1);

            if ( (LA50_0==RULE_ID||LA50_0==33||LA50_0==47) ) {
                alt50=1;
            }
            switch (alt50) {
                case 1 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3173:1: (lv_typeGuard_0_0= ruleJvmTypeReference )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3173:1: (lv_typeGuard_0_0= ruleJvmTypeReference )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3174:3: lv_typeGuard_0_0= ruleJvmTypeReference
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getXCasePartAccess().getTypeGuardJvmTypeReferenceParserRuleCall_0_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleJvmTypeReference_in_ruleXCasePart7668);
                    lv_typeGuard_0_0=ruleJvmTypeReference();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getXCasePartRule());
                      	        }
                             		set(
                             			current, 
                             			"typeGuard",
                              		lv_typeGuard_0_0, 
                              		"JvmTypeReference");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }

            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3190:3: (otherlv_1= 'case' ( (lv_case_2_0= ruleXExpression ) ) )?
            int alt51=2;
            int LA51_0 = input.LA(1);

            if ( (LA51_0==58) ) {
                alt51=1;
            }
            switch (alt51) {
                case 1 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3190:5: otherlv_1= 'case' ( (lv_case_2_0= ruleXExpression ) )
                    {
                    otherlv_1=(Token)match(input,58,FOLLOW_58_in_ruleXCasePart7682); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_1, grammarAccess.getXCasePartAccess().getCaseKeyword_1_0());
                          
                    }
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3194:1: ( (lv_case_2_0= ruleXExpression ) )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3195:1: (lv_case_2_0= ruleXExpression )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3195:1: (lv_case_2_0= ruleXExpression )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3196:3: lv_case_2_0= ruleXExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getXCasePartAccess().getCaseXExpressionParserRuleCall_1_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleXExpression_in_ruleXCasePart7703);
                    lv_case_2_0=ruleXExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getXCasePartRule());
                      	        }
                             		set(
                             			current, 
                             			"case",
                              		lv_case_2_0, 
                              		"XExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_3=(Token)match(input,56,FOLLOW_56_in_ruleXCasePart7717); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getXCasePartAccess().getColonKeyword_2());
                  
            }
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3216:1: ( (lv_then_4_0= ruleXExpression ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3217:1: (lv_then_4_0= ruleXExpression )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3217:1: (lv_then_4_0= ruleXExpression )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3218:3: lv_then_4_0= ruleXExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getXCasePartAccess().getThenXExpressionParserRuleCall_3_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleXExpression_in_ruleXCasePart7738);
            lv_then_4_0=ruleXExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getXCasePartRule());
              	        }
                     		set(
                     			current, 
                     			"then",
                      		lv_then_4_0, 
                      		"XExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXCasePart"


    // $ANTLR start "entryRuleXForLoopExpression"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3242:1: entryRuleXForLoopExpression returns [EObject current=null] : iv_ruleXForLoopExpression= ruleXForLoopExpression EOF ;
    public final EObject entryRuleXForLoopExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXForLoopExpression = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3243:2: (iv_ruleXForLoopExpression= ruleXForLoopExpression EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3244:2: iv_ruleXForLoopExpression= ruleXForLoopExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getXForLoopExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleXForLoopExpression_in_entryRuleXForLoopExpression7774);
            iv_ruleXForLoopExpression=ruleXForLoopExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleXForLoopExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleXForLoopExpression7784); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXForLoopExpression"


    // $ANTLR start "ruleXForLoopExpression"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3251:1: ruleXForLoopExpression returns [EObject current=null] : ( () otherlv_1= 'for' otherlv_2= '(' ( (lv_declaredParam_3_0= ruleJvmFormalParameter ) ) otherlv_4= ':' ( (lv_forExpression_5_0= ruleXExpression ) ) otherlv_6= ')' ( (lv_eachExpression_7_0= ruleXExpression ) ) ) ;
    public final EObject ruleXForLoopExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_declaredParam_3_0 = null;

        EObject lv_forExpression_5_0 = null;

        EObject lv_eachExpression_7_0 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3254:28: ( ( () otherlv_1= 'for' otherlv_2= '(' ( (lv_declaredParam_3_0= ruleJvmFormalParameter ) ) otherlv_4= ':' ( (lv_forExpression_5_0= ruleXExpression ) ) otherlv_6= ')' ( (lv_eachExpression_7_0= ruleXExpression ) ) ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3255:1: ( () otherlv_1= 'for' otherlv_2= '(' ( (lv_declaredParam_3_0= ruleJvmFormalParameter ) ) otherlv_4= ':' ( (lv_forExpression_5_0= ruleXExpression ) ) otherlv_6= ')' ( (lv_eachExpression_7_0= ruleXExpression ) ) )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3255:1: ( () otherlv_1= 'for' otherlv_2= '(' ( (lv_declaredParam_3_0= ruleJvmFormalParameter ) ) otherlv_4= ':' ( (lv_forExpression_5_0= ruleXExpression ) ) otherlv_6= ')' ( (lv_eachExpression_7_0= ruleXExpression ) ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3255:2: () otherlv_1= 'for' otherlv_2= '(' ( (lv_declaredParam_3_0= ruleJvmFormalParameter ) ) otherlv_4= ':' ( (lv_forExpression_5_0= ruleXExpression ) ) otherlv_6= ')' ( (lv_eachExpression_7_0= ruleXExpression ) )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3255:2: ()
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3256:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getXForLoopExpressionAccess().getXForLoopExpressionAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,59,FOLLOW_59_in_ruleXForLoopExpression7830); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getXForLoopExpressionAccess().getForKeyword_1());
                  
            }
            otherlv_2=(Token)match(input,47,FOLLOW_47_in_ruleXForLoopExpression7842); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getXForLoopExpressionAccess().getLeftParenthesisKeyword_2());
                  
            }
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3269:1: ( (lv_declaredParam_3_0= ruleJvmFormalParameter ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3270:1: (lv_declaredParam_3_0= ruleJvmFormalParameter )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3270:1: (lv_declaredParam_3_0= ruleJvmFormalParameter )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3271:3: lv_declaredParam_3_0= ruleJvmFormalParameter
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getXForLoopExpressionAccess().getDeclaredParamJvmFormalParameterParserRuleCall_3_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleJvmFormalParameter_in_ruleXForLoopExpression7863);
            lv_declaredParam_3_0=ruleJvmFormalParameter();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getXForLoopExpressionRule());
              	        }
                     		set(
                     			current, 
                     			"declaredParam",
                      		lv_declaredParam_3_0, 
                      		"JvmFormalParameter");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_4=(Token)match(input,56,FOLLOW_56_in_ruleXForLoopExpression7875); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getXForLoopExpressionAccess().getColonKeyword_4());
                  
            }
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3291:1: ( (lv_forExpression_5_0= ruleXExpression ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3292:1: (lv_forExpression_5_0= ruleXExpression )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3292:1: (lv_forExpression_5_0= ruleXExpression )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3293:3: lv_forExpression_5_0= ruleXExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getXForLoopExpressionAccess().getForExpressionXExpressionParserRuleCall_5_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleXExpression_in_ruleXForLoopExpression7896);
            lv_forExpression_5_0=ruleXExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getXForLoopExpressionRule());
              	        }
                     		set(
                     			current, 
                     			"forExpression",
                      		lv_forExpression_5_0, 
                      		"XExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_6=(Token)match(input,48,FOLLOW_48_in_ruleXForLoopExpression7908); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_6, grammarAccess.getXForLoopExpressionAccess().getRightParenthesisKeyword_6());
                  
            }
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3313:1: ( (lv_eachExpression_7_0= ruleXExpression ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3314:1: (lv_eachExpression_7_0= ruleXExpression )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3314:1: (lv_eachExpression_7_0= ruleXExpression )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3315:3: lv_eachExpression_7_0= ruleXExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getXForLoopExpressionAccess().getEachExpressionXExpressionParserRuleCall_7_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleXExpression_in_ruleXForLoopExpression7929);
            lv_eachExpression_7_0=ruleXExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getXForLoopExpressionRule());
              	        }
                     		set(
                     			current, 
                     			"eachExpression",
                      		lv_eachExpression_7_0, 
                      		"XExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXForLoopExpression"


    // $ANTLR start "entryRuleXWhileExpression"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3339:1: entryRuleXWhileExpression returns [EObject current=null] : iv_ruleXWhileExpression= ruleXWhileExpression EOF ;
    public final EObject entryRuleXWhileExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXWhileExpression = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3340:2: (iv_ruleXWhileExpression= ruleXWhileExpression EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3341:2: iv_ruleXWhileExpression= ruleXWhileExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getXWhileExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleXWhileExpression_in_entryRuleXWhileExpression7965);
            iv_ruleXWhileExpression=ruleXWhileExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleXWhileExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleXWhileExpression7975); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXWhileExpression"


    // $ANTLR start "ruleXWhileExpression"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3348:1: ruleXWhileExpression returns [EObject current=null] : ( () otherlv_1= 'while' otherlv_2= '(' ( (lv_predicate_3_0= ruleXExpression ) ) otherlv_4= ')' ( (lv_body_5_0= ruleXExpression ) ) ) ;
    public final EObject ruleXWhileExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_predicate_3_0 = null;

        EObject lv_body_5_0 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3351:28: ( ( () otherlv_1= 'while' otherlv_2= '(' ( (lv_predicate_3_0= ruleXExpression ) ) otherlv_4= ')' ( (lv_body_5_0= ruleXExpression ) ) ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3352:1: ( () otherlv_1= 'while' otherlv_2= '(' ( (lv_predicate_3_0= ruleXExpression ) ) otherlv_4= ')' ( (lv_body_5_0= ruleXExpression ) ) )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3352:1: ( () otherlv_1= 'while' otherlv_2= '(' ( (lv_predicate_3_0= ruleXExpression ) ) otherlv_4= ')' ( (lv_body_5_0= ruleXExpression ) ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3352:2: () otherlv_1= 'while' otherlv_2= '(' ( (lv_predicate_3_0= ruleXExpression ) ) otherlv_4= ')' ( (lv_body_5_0= ruleXExpression ) )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3352:2: ()
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3353:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getXWhileExpressionAccess().getXWhileExpressionAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,60,FOLLOW_60_in_ruleXWhileExpression8021); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getXWhileExpressionAccess().getWhileKeyword_1());
                  
            }
            otherlv_2=(Token)match(input,47,FOLLOW_47_in_ruleXWhileExpression8033); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getXWhileExpressionAccess().getLeftParenthesisKeyword_2());
                  
            }
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3366:1: ( (lv_predicate_3_0= ruleXExpression ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3367:1: (lv_predicate_3_0= ruleXExpression )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3367:1: (lv_predicate_3_0= ruleXExpression )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3368:3: lv_predicate_3_0= ruleXExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getXWhileExpressionAccess().getPredicateXExpressionParserRuleCall_3_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleXExpression_in_ruleXWhileExpression8054);
            lv_predicate_3_0=ruleXExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getXWhileExpressionRule());
              	        }
                     		set(
                     			current, 
                     			"predicate",
                      		lv_predicate_3_0, 
                      		"XExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_4=(Token)match(input,48,FOLLOW_48_in_ruleXWhileExpression8066); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getXWhileExpressionAccess().getRightParenthesisKeyword_4());
                  
            }
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3388:1: ( (lv_body_5_0= ruleXExpression ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3389:1: (lv_body_5_0= ruleXExpression )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3389:1: (lv_body_5_0= ruleXExpression )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3390:3: lv_body_5_0= ruleXExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getXWhileExpressionAccess().getBodyXExpressionParserRuleCall_5_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleXExpression_in_ruleXWhileExpression8087);
            lv_body_5_0=ruleXExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getXWhileExpressionRule());
              	        }
                     		set(
                     			current, 
                     			"body",
                      		lv_body_5_0, 
                      		"XExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXWhileExpression"


    // $ANTLR start "entryRuleXDoWhileExpression"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3414:1: entryRuleXDoWhileExpression returns [EObject current=null] : iv_ruleXDoWhileExpression= ruleXDoWhileExpression EOF ;
    public final EObject entryRuleXDoWhileExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXDoWhileExpression = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3415:2: (iv_ruleXDoWhileExpression= ruleXDoWhileExpression EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3416:2: iv_ruleXDoWhileExpression= ruleXDoWhileExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getXDoWhileExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleXDoWhileExpression_in_entryRuleXDoWhileExpression8123);
            iv_ruleXDoWhileExpression=ruleXDoWhileExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleXDoWhileExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleXDoWhileExpression8133); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXDoWhileExpression"


    // $ANTLR start "ruleXDoWhileExpression"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3423:1: ruleXDoWhileExpression returns [EObject current=null] : ( () otherlv_1= 'do' ( (lv_body_2_0= ruleXExpression ) ) otherlv_3= 'while' otherlv_4= '(' ( (lv_predicate_5_0= ruleXExpression ) ) otherlv_6= ')' ) ;
    public final EObject ruleXDoWhileExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_body_2_0 = null;

        EObject lv_predicate_5_0 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3426:28: ( ( () otherlv_1= 'do' ( (lv_body_2_0= ruleXExpression ) ) otherlv_3= 'while' otherlv_4= '(' ( (lv_predicate_5_0= ruleXExpression ) ) otherlv_6= ')' ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3427:1: ( () otherlv_1= 'do' ( (lv_body_2_0= ruleXExpression ) ) otherlv_3= 'while' otherlv_4= '(' ( (lv_predicate_5_0= ruleXExpression ) ) otherlv_6= ')' )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3427:1: ( () otherlv_1= 'do' ( (lv_body_2_0= ruleXExpression ) ) otherlv_3= 'while' otherlv_4= '(' ( (lv_predicate_5_0= ruleXExpression ) ) otherlv_6= ')' )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3427:2: () otherlv_1= 'do' ( (lv_body_2_0= ruleXExpression ) ) otherlv_3= 'while' otherlv_4= '(' ( (lv_predicate_5_0= ruleXExpression ) ) otherlv_6= ')'
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3427:2: ()
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3428:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getXDoWhileExpressionAccess().getXDoWhileExpressionAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,61,FOLLOW_61_in_ruleXDoWhileExpression8179); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getXDoWhileExpressionAccess().getDoKeyword_1());
                  
            }
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3437:1: ( (lv_body_2_0= ruleXExpression ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3438:1: (lv_body_2_0= ruleXExpression )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3438:1: (lv_body_2_0= ruleXExpression )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3439:3: lv_body_2_0= ruleXExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getXDoWhileExpressionAccess().getBodyXExpressionParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleXExpression_in_ruleXDoWhileExpression8200);
            lv_body_2_0=ruleXExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getXDoWhileExpressionRule());
              	        }
                     		set(
                     			current, 
                     			"body",
                      		lv_body_2_0, 
                      		"XExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_3=(Token)match(input,60,FOLLOW_60_in_ruleXDoWhileExpression8212); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getXDoWhileExpressionAccess().getWhileKeyword_3());
                  
            }
            otherlv_4=(Token)match(input,47,FOLLOW_47_in_ruleXDoWhileExpression8224); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getXDoWhileExpressionAccess().getLeftParenthesisKeyword_4());
                  
            }
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3463:1: ( (lv_predicate_5_0= ruleXExpression ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3464:1: (lv_predicate_5_0= ruleXExpression )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3464:1: (lv_predicate_5_0= ruleXExpression )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3465:3: lv_predicate_5_0= ruleXExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getXDoWhileExpressionAccess().getPredicateXExpressionParserRuleCall_5_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleXExpression_in_ruleXDoWhileExpression8245);
            lv_predicate_5_0=ruleXExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getXDoWhileExpressionRule());
              	        }
                     		set(
                     			current, 
                     			"predicate",
                      		lv_predicate_5_0, 
                      		"XExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_6=(Token)match(input,48,FOLLOW_48_in_ruleXDoWhileExpression8257); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_6, grammarAccess.getXDoWhileExpressionAccess().getRightParenthesisKeyword_6());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXDoWhileExpression"


    // $ANTLR start "entryRuleXBlockExpression"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3493:1: entryRuleXBlockExpression returns [EObject current=null] : iv_ruleXBlockExpression= ruleXBlockExpression EOF ;
    public final EObject entryRuleXBlockExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXBlockExpression = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3494:2: (iv_ruleXBlockExpression= ruleXBlockExpression EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3495:2: iv_ruleXBlockExpression= ruleXBlockExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getXBlockExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleXBlockExpression_in_entryRuleXBlockExpression8293);
            iv_ruleXBlockExpression=ruleXBlockExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleXBlockExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleXBlockExpression8303); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXBlockExpression"


    // $ANTLR start "ruleXBlockExpression"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3502:1: ruleXBlockExpression returns [EObject current=null] : ( () otherlv_1= '{' ( ( (lv_expressions_2_0= ruleXExpressionInsideBlock ) ) (otherlv_3= ';' )? )* otherlv_4= '}' ) ;
    public final EObject ruleXBlockExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        EObject lv_expressions_2_0 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3505:28: ( ( () otherlv_1= '{' ( ( (lv_expressions_2_0= ruleXExpressionInsideBlock ) ) (otherlv_3= ';' )? )* otherlv_4= '}' ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3506:1: ( () otherlv_1= '{' ( ( (lv_expressions_2_0= ruleXExpressionInsideBlock ) ) (otherlv_3= ';' )? )* otherlv_4= '}' )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3506:1: ( () otherlv_1= '{' ( ( (lv_expressions_2_0= ruleXExpressionInsideBlock ) ) (otherlv_3= ';' )? )* otherlv_4= '}' )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3506:2: () otherlv_1= '{' ( ( (lv_expressions_2_0= ruleXExpressionInsideBlock ) ) (otherlv_3= ';' )? )* otherlv_4= '}'
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3506:2: ()
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3507:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getXBlockExpressionAccess().getXBlockExpressionAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,16,FOLLOW_16_in_ruleXBlockExpression8349); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getXBlockExpressionAccess().getLeftCurlyBracketKeyword_1());
                  
            }
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3516:1: ( ( (lv_expressions_2_0= ruleXExpressionInsideBlock ) ) (otherlv_3= ';' )? )*
            loop53:
            do {
                int alt53=2;
                int LA53_0 = input.LA(1);

                if ( ((LA53_0>=RULE_ID && LA53_0<=RULE_DECIMAL)||LA53_0==16||LA53_0==18||LA53_0==30||LA53_0==37||LA53_0==42||LA53_0==47||LA53_0==49||LA53_0==53||LA53_0==55||(LA53_0>=59 && LA53_0<=64)||(LA53_0>=66 && LA53_0<=73)) ) {
                    alt53=1;
                }


                switch (alt53) {
            	case 1 :
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3516:2: ( (lv_expressions_2_0= ruleXExpressionInsideBlock ) ) (otherlv_3= ';' )?
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3516:2: ( (lv_expressions_2_0= ruleXExpressionInsideBlock ) )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3517:1: (lv_expressions_2_0= ruleXExpressionInsideBlock )
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3517:1: (lv_expressions_2_0= ruleXExpressionInsideBlock )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3518:3: lv_expressions_2_0= ruleXExpressionInsideBlock
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getXBlockExpressionAccess().getExpressionsXExpressionInsideBlockParserRuleCall_2_0_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleXExpressionInsideBlock_in_ruleXBlockExpression8371);
            	    lv_expressions_2_0=ruleXExpressionInsideBlock();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getXBlockExpressionRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"expressions",
            	              		lv_expressions_2_0, 
            	              		"XExpressionInsideBlock");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }

            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3534:2: (otherlv_3= ';' )?
            	    int alt52=2;
            	    int LA52_0 = input.LA(1);

            	    if ( (LA52_0==52) ) {
            	        alt52=1;
            	    }
            	    switch (alt52) {
            	        case 1 :
            	            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3534:4: otherlv_3= ';'
            	            {
            	            otherlv_3=(Token)match(input,52,FOLLOW_52_in_ruleXBlockExpression8384); if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	                  	newLeafNode(otherlv_3, grammarAccess.getXBlockExpressionAccess().getSemicolonKeyword_2_1());
            	                  
            	            }

            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop53;
                }
            } while (true);

            otherlv_4=(Token)match(input,17,FOLLOW_17_in_ruleXBlockExpression8400); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getXBlockExpressionAccess().getRightCurlyBracketKeyword_3());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXBlockExpression"


    // $ANTLR start "entryRuleXExpressionInsideBlock"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3550:1: entryRuleXExpressionInsideBlock returns [EObject current=null] : iv_ruleXExpressionInsideBlock= ruleXExpressionInsideBlock EOF ;
    public final EObject entryRuleXExpressionInsideBlock() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXExpressionInsideBlock = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3551:2: (iv_ruleXExpressionInsideBlock= ruleXExpressionInsideBlock EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3552:2: iv_ruleXExpressionInsideBlock= ruleXExpressionInsideBlock EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getXExpressionInsideBlockRule()); 
            }
            pushFollow(FOLLOW_ruleXExpressionInsideBlock_in_entryRuleXExpressionInsideBlock8436);
            iv_ruleXExpressionInsideBlock=ruleXExpressionInsideBlock();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleXExpressionInsideBlock; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleXExpressionInsideBlock8446); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXExpressionInsideBlock"


    // $ANTLR start "ruleXExpressionInsideBlock"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3559:1: ruleXExpressionInsideBlock returns [EObject current=null] : (this_XVariableDeclaration_0= ruleXVariableDeclaration | this_XExpression_1= ruleXExpression ) ;
    public final EObject ruleXExpressionInsideBlock() throws RecognitionException {
        EObject current = null;

        EObject this_XVariableDeclaration_0 = null;

        EObject this_XExpression_1 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3562:28: ( (this_XVariableDeclaration_0= ruleXVariableDeclaration | this_XExpression_1= ruleXExpression ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3563:1: (this_XVariableDeclaration_0= ruleXVariableDeclaration | this_XExpression_1= ruleXExpression )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3563:1: (this_XVariableDeclaration_0= ruleXVariableDeclaration | this_XExpression_1= ruleXExpression )
            int alt54=2;
            int LA54_0 = input.LA(1);

            if ( ((LA54_0>=62 && LA54_0<=63)) ) {
                alt54=1;
            }
            else if ( ((LA54_0>=RULE_ID && LA54_0<=RULE_DECIMAL)||LA54_0==16||LA54_0==18||LA54_0==30||LA54_0==37||LA54_0==42||LA54_0==47||LA54_0==49||LA54_0==53||LA54_0==55||(LA54_0>=59 && LA54_0<=61)||LA54_0==64||(LA54_0>=66 && LA54_0<=73)) ) {
                alt54=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 54, 0, input);

                throw nvae;
            }
            switch (alt54) {
                case 1 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3564:5: this_XVariableDeclaration_0= ruleXVariableDeclaration
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getXExpressionInsideBlockAccess().getXVariableDeclarationParserRuleCall_0()); 
                          
                    }
                    pushFollow(FOLLOW_ruleXVariableDeclaration_in_ruleXExpressionInsideBlock8493);
                    this_XVariableDeclaration_0=ruleXVariableDeclaration();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_XVariableDeclaration_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3574:5: this_XExpression_1= ruleXExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getXExpressionInsideBlockAccess().getXExpressionParserRuleCall_1()); 
                          
                    }
                    pushFollow(FOLLOW_ruleXExpression_in_ruleXExpressionInsideBlock8520);
                    this_XExpression_1=ruleXExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_XExpression_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXExpressionInsideBlock"


    // $ANTLR start "entryRuleXVariableDeclaration"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3590:1: entryRuleXVariableDeclaration returns [EObject current=null] : iv_ruleXVariableDeclaration= ruleXVariableDeclaration EOF ;
    public final EObject entryRuleXVariableDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXVariableDeclaration = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3591:2: (iv_ruleXVariableDeclaration= ruleXVariableDeclaration EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3592:2: iv_ruleXVariableDeclaration= ruleXVariableDeclaration EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getXVariableDeclarationRule()); 
            }
            pushFollow(FOLLOW_ruleXVariableDeclaration_in_entryRuleXVariableDeclaration8555);
            iv_ruleXVariableDeclaration=ruleXVariableDeclaration();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleXVariableDeclaration; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleXVariableDeclaration8565); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXVariableDeclaration"


    // $ANTLR start "ruleXVariableDeclaration"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3599:1: ruleXVariableDeclaration returns [EObject current=null] : ( () ( ( (lv_writeable_1_0= 'var' ) ) | otherlv_2= 'val' ) ( ( ( ( ( ( ruleJvmTypeReference ) ) ( ( ruleValidID ) ) ) )=> ( ( (lv_type_3_0= ruleJvmTypeReference ) ) ( (lv_name_4_0= ruleValidID ) ) ) ) | ( (lv_name_5_0= ruleValidID ) ) ) (otherlv_6= '=' ( (lv_right_7_0= ruleXExpression ) ) )? ) ;
    public final EObject ruleXVariableDeclaration() throws RecognitionException {
        EObject current = null;

        Token lv_writeable_1_0=null;
        Token otherlv_2=null;
        Token otherlv_6=null;
        EObject lv_type_3_0 = null;

        AntlrDatatypeRuleToken lv_name_4_0 = null;

        AntlrDatatypeRuleToken lv_name_5_0 = null;

        EObject lv_right_7_0 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3602:28: ( ( () ( ( (lv_writeable_1_0= 'var' ) ) | otherlv_2= 'val' ) ( ( ( ( ( ( ruleJvmTypeReference ) ) ( ( ruleValidID ) ) ) )=> ( ( (lv_type_3_0= ruleJvmTypeReference ) ) ( (lv_name_4_0= ruleValidID ) ) ) ) | ( (lv_name_5_0= ruleValidID ) ) ) (otherlv_6= '=' ( (lv_right_7_0= ruleXExpression ) ) )? ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3603:1: ( () ( ( (lv_writeable_1_0= 'var' ) ) | otherlv_2= 'val' ) ( ( ( ( ( ( ruleJvmTypeReference ) ) ( ( ruleValidID ) ) ) )=> ( ( (lv_type_3_0= ruleJvmTypeReference ) ) ( (lv_name_4_0= ruleValidID ) ) ) ) | ( (lv_name_5_0= ruleValidID ) ) ) (otherlv_6= '=' ( (lv_right_7_0= ruleXExpression ) ) )? )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3603:1: ( () ( ( (lv_writeable_1_0= 'var' ) ) | otherlv_2= 'val' ) ( ( ( ( ( ( ruleJvmTypeReference ) ) ( ( ruleValidID ) ) ) )=> ( ( (lv_type_3_0= ruleJvmTypeReference ) ) ( (lv_name_4_0= ruleValidID ) ) ) ) | ( (lv_name_5_0= ruleValidID ) ) ) (otherlv_6= '=' ( (lv_right_7_0= ruleXExpression ) ) )? )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3603:2: () ( ( (lv_writeable_1_0= 'var' ) ) | otherlv_2= 'val' ) ( ( ( ( ( ( ruleJvmTypeReference ) ) ( ( ruleValidID ) ) ) )=> ( ( (lv_type_3_0= ruleJvmTypeReference ) ) ( (lv_name_4_0= ruleValidID ) ) ) ) | ( (lv_name_5_0= ruleValidID ) ) ) (otherlv_6= '=' ( (lv_right_7_0= ruleXExpression ) ) )?
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3603:2: ()
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3604:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getXVariableDeclarationAccess().getXVariableDeclarationAction_0(),
                          current);
                  
            }

            }

            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3609:2: ( ( (lv_writeable_1_0= 'var' ) ) | otherlv_2= 'val' )
            int alt55=2;
            int LA55_0 = input.LA(1);

            if ( (LA55_0==62) ) {
                alt55=1;
            }
            else if ( (LA55_0==63) ) {
                alt55=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 55, 0, input);

                throw nvae;
            }
            switch (alt55) {
                case 1 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3609:3: ( (lv_writeable_1_0= 'var' ) )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3609:3: ( (lv_writeable_1_0= 'var' ) )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3610:1: (lv_writeable_1_0= 'var' )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3610:1: (lv_writeable_1_0= 'var' )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3611:3: lv_writeable_1_0= 'var'
                    {
                    lv_writeable_1_0=(Token)match(input,62,FOLLOW_62_in_ruleXVariableDeclaration8618); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_writeable_1_0, grammarAccess.getXVariableDeclarationAccess().getWriteableVarKeyword_1_0_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getXVariableDeclarationRule());
                      	        }
                             		setWithLastConsumed(current, "writeable", true, "var");
                      	    
                    }

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3625:7: otherlv_2= 'val'
                    {
                    otherlv_2=(Token)match(input,63,FOLLOW_63_in_ruleXVariableDeclaration8649); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_2, grammarAccess.getXVariableDeclarationAccess().getValKeyword_1_1());
                          
                    }

                    }
                    break;

            }

            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3629:2: ( ( ( ( ( ( ruleJvmTypeReference ) ) ( ( ruleValidID ) ) ) )=> ( ( (lv_type_3_0= ruleJvmTypeReference ) ) ( (lv_name_4_0= ruleValidID ) ) ) ) | ( (lv_name_5_0= ruleValidID ) ) )
            int alt56=2;
            int LA56_0 = input.LA(1);

            if ( (LA56_0==RULE_ID) ) {
                int LA56_1 = input.LA(2);

                if ( (synpred25_InternalDeepClone()) ) {
                    alt56=1;
                }
                else if ( (true) ) {
                    alt56=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 56, 1, input);

                    throw nvae;
                }
            }
            else if ( (LA56_0==47) && (synpred25_InternalDeepClone())) {
                alt56=1;
            }
            else if ( (LA56_0==33) && (synpred25_InternalDeepClone())) {
                alt56=1;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 56, 0, input);

                throw nvae;
            }
            switch (alt56) {
                case 1 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3629:3: ( ( ( ( ( ruleJvmTypeReference ) ) ( ( ruleValidID ) ) ) )=> ( ( (lv_type_3_0= ruleJvmTypeReference ) ) ( (lv_name_4_0= ruleValidID ) ) ) )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3629:3: ( ( ( ( ( ruleJvmTypeReference ) ) ( ( ruleValidID ) ) ) )=> ( ( (lv_type_3_0= ruleJvmTypeReference ) ) ( (lv_name_4_0= ruleValidID ) ) ) )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3629:4: ( ( ( ( ruleJvmTypeReference ) ) ( ( ruleValidID ) ) ) )=> ( ( (lv_type_3_0= ruleJvmTypeReference ) ) ( (lv_name_4_0= ruleValidID ) ) )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3637:6: ( ( (lv_type_3_0= ruleJvmTypeReference ) ) ( (lv_name_4_0= ruleValidID ) ) )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3637:7: ( (lv_type_3_0= ruleJvmTypeReference ) ) ( (lv_name_4_0= ruleValidID ) )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3637:7: ( (lv_type_3_0= ruleJvmTypeReference ) )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3638:1: (lv_type_3_0= ruleJvmTypeReference )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3638:1: (lv_type_3_0= ruleJvmTypeReference )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3639:3: lv_type_3_0= ruleJvmTypeReference
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getXVariableDeclarationAccess().getTypeJvmTypeReferenceParserRuleCall_2_0_0_0_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleJvmTypeReference_in_ruleXVariableDeclaration8697);
                    lv_type_3_0=ruleJvmTypeReference();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getXVariableDeclarationRule());
                      	        }
                             		set(
                             			current, 
                             			"type",
                              		lv_type_3_0, 
                              		"JvmTypeReference");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3655:2: ( (lv_name_4_0= ruleValidID ) )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3656:1: (lv_name_4_0= ruleValidID )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3656:1: (lv_name_4_0= ruleValidID )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3657:3: lv_name_4_0= ruleValidID
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getXVariableDeclarationAccess().getNameValidIDParserRuleCall_2_0_0_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleValidID_in_ruleXVariableDeclaration8718);
                    lv_name_4_0=ruleValidID();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getXVariableDeclarationRule());
                      	        }
                             		set(
                             			current, 
                             			"name",
                              		lv_name_4_0, 
                              		"ValidID");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3674:6: ( (lv_name_5_0= ruleValidID ) )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3674:6: ( (lv_name_5_0= ruleValidID ) )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3675:1: (lv_name_5_0= ruleValidID )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3675:1: (lv_name_5_0= ruleValidID )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3676:3: lv_name_5_0= ruleValidID
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getXVariableDeclarationAccess().getNameValidIDParserRuleCall_2_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleValidID_in_ruleXVariableDeclaration8747);
                    lv_name_5_0=ruleValidID();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getXVariableDeclarationRule());
                      	        }
                             		set(
                             			current, 
                             			"name",
                              		lv_name_5_0, 
                              		"ValidID");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }

            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3692:3: (otherlv_6= '=' ( (lv_right_7_0= ruleXExpression ) ) )?
            int alt57=2;
            int LA57_0 = input.LA(1);

            if ( (LA57_0==20) ) {
                alt57=1;
            }
            switch (alt57) {
                case 1 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3692:5: otherlv_6= '=' ( (lv_right_7_0= ruleXExpression ) )
                    {
                    otherlv_6=(Token)match(input,20,FOLLOW_20_in_ruleXVariableDeclaration8761); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_6, grammarAccess.getXVariableDeclarationAccess().getEqualsSignKeyword_3_0());
                          
                    }
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3696:1: ( (lv_right_7_0= ruleXExpression ) )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3697:1: (lv_right_7_0= ruleXExpression )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3697:1: (lv_right_7_0= ruleXExpression )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3698:3: lv_right_7_0= ruleXExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getXVariableDeclarationAccess().getRightXExpressionParserRuleCall_3_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleXExpression_in_ruleXVariableDeclaration8782);
                    lv_right_7_0=ruleXExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getXVariableDeclarationRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_7_0, 
                              		"XExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXVariableDeclaration"


    // $ANTLR start "entryRuleJvmFormalParameter"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3722:1: entryRuleJvmFormalParameter returns [EObject current=null] : iv_ruleJvmFormalParameter= ruleJvmFormalParameter EOF ;
    public final EObject entryRuleJvmFormalParameter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleJvmFormalParameter = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3723:2: (iv_ruleJvmFormalParameter= ruleJvmFormalParameter EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3724:2: iv_ruleJvmFormalParameter= ruleJvmFormalParameter EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getJvmFormalParameterRule()); 
            }
            pushFollow(FOLLOW_ruleJvmFormalParameter_in_entryRuleJvmFormalParameter8820);
            iv_ruleJvmFormalParameter=ruleJvmFormalParameter();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleJvmFormalParameter; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleJvmFormalParameter8830); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleJvmFormalParameter"


    // $ANTLR start "ruleJvmFormalParameter"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3731:1: ruleJvmFormalParameter returns [EObject current=null] : ( ( (lv_parameterType_0_0= ruleJvmTypeReference ) )? ( (lv_name_1_0= ruleValidID ) ) ) ;
    public final EObject ruleJvmFormalParameter() throws RecognitionException {
        EObject current = null;

        EObject lv_parameterType_0_0 = null;

        AntlrDatatypeRuleToken lv_name_1_0 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3734:28: ( ( ( (lv_parameterType_0_0= ruleJvmTypeReference ) )? ( (lv_name_1_0= ruleValidID ) ) ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3735:1: ( ( (lv_parameterType_0_0= ruleJvmTypeReference ) )? ( (lv_name_1_0= ruleValidID ) ) )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3735:1: ( ( (lv_parameterType_0_0= ruleJvmTypeReference ) )? ( (lv_name_1_0= ruleValidID ) ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3735:2: ( (lv_parameterType_0_0= ruleJvmTypeReference ) )? ( (lv_name_1_0= ruleValidID ) )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3735:2: ( (lv_parameterType_0_0= ruleJvmTypeReference ) )?
            int alt58=2;
            int LA58_0 = input.LA(1);

            if ( (LA58_0==RULE_ID) ) {
                int LA58_1 = input.LA(2);

                if ( (LA58_1==RULE_ID||LA58_1==15||LA58_1==30||LA58_1==49) ) {
                    alt58=1;
                }
            }
            else if ( (LA58_0==33||LA58_0==47) ) {
                alt58=1;
            }
            switch (alt58) {
                case 1 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3736:1: (lv_parameterType_0_0= ruleJvmTypeReference )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3736:1: (lv_parameterType_0_0= ruleJvmTypeReference )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3737:3: lv_parameterType_0_0= ruleJvmTypeReference
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getJvmFormalParameterAccess().getParameterTypeJvmTypeReferenceParserRuleCall_0_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleJvmTypeReference_in_ruleJvmFormalParameter8876);
                    lv_parameterType_0_0=ruleJvmTypeReference();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getJvmFormalParameterRule());
                      	        }
                             		set(
                             			current, 
                             			"parameterType",
                              		lv_parameterType_0_0, 
                              		"JvmTypeReference");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }

            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3753:3: ( (lv_name_1_0= ruleValidID ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3754:1: (lv_name_1_0= ruleValidID )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3754:1: (lv_name_1_0= ruleValidID )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3755:3: lv_name_1_0= ruleValidID
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getJvmFormalParameterAccess().getNameValidIDParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleValidID_in_ruleJvmFormalParameter8898);
            lv_name_1_0=ruleValidID();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getJvmFormalParameterRule());
              	        }
                     		set(
                     			current, 
                     			"name",
                      		lv_name_1_0, 
                      		"ValidID");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleJvmFormalParameter"


    // $ANTLR start "entryRuleFullJvmFormalParameter"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3779:1: entryRuleFullJvmFormalParameter returns [EObject current=null] : iv_ruleFullJvmFormalParameter= ruleFullJvmFormalParameter EOF ;
    public final EObject entryRuleFullJvmFormalParameter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFullJvmFormalParameter = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3780:2: (iv_ruleFullJvmFormalParameter= ruleFullJvmFormalParameter EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3781:2: iv_ruleFullJvmFormalParameter= ruleFullJvmFormalParameter EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFullJvmFormalParameterRule()); 
            }
            pushFollow(FOLLOW_ruleFullJvmFormalParameter_in_entryRuleFullJvmFormalParameter8934);
            iv_ruleFullJvmFormalParameter=ruleFullJvmFormalParameter();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFullJvmFormalParameter; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleFullJvmFormalParameter8944); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFullJvmFormalParameter"


    // $ANTLR start "ruleFullJvmFormalParameter"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3788:1: ruleFullJvmFormalParameter returns [EObject current=null] : ( ( (lv_parameterType_0_0= ruleJvmTypeReference ) ) ( (lv_name_1_0= ruleValidID ) ) ) ;
    public final EObject ruleFullJvmFormalParameter() throws RecognitionException {
        EObject current = null;

        EObject lv_parameterType_0_0 = null;

        AntlrDatatypeRuleToken lv_name_1_0 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3791:28: ( ( ( (lv_parameterType_0_0= ruleJvmTypeReference ) ) ( (lv_name_1_0= ruleValidID ) ) ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3792:1: ( ( (lv_parameterType_0_0= ruleJvmTypeReference ) ) ( (lv_name_1_0= ruleValidID ) ) )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3792:1: ( ( (lv_parameterType_0_0= ruleJvmTypeReference ) ) ( (lv_name_1_0= ruleValidID ) ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3792:2: ( (lv_parameterType_0_0= ruleJvmTypeReference ) ) ( (lv_name_1_0= ruleValidID ) )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3792:2: ( (lv_parameterType_0_0= ruleJvmTypeReference ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3793:1: (lv_parameterType_0_0= ruleJvmTypeReference )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3793:1: (lv_parameterType_0_0= ruleJvmTypeReference )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3794:3: lv_parameterType_0_0= ruleJvmTypeReference
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getFullJvmFormalParameterAccess().getParameterTypeJvmTypeReferenceParserRuleCall_0_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleJvmTypeReference_in_ruleFullJvmFormalParameter8990);
            lv_parameterType_0_0=ruleJvmTypeReference();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getFullJvmFormalParameterRule());
              	        }
                     		set(
                     			current, 
                     			"parameterType",
                      		lv_parameterType_0_0, 
                      		"JvmTypeReference");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3810:2: ( (lv_name_1_0= ruleValidID ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3811:1: (lv_name_1_0= ruleValidID )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3811:1: (lv_name_1_0= ruleValidID )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3812:3: lv_name_1_0= ruleValidID
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getFullJvmFormalParameterAccess().getNameValidIDParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleValidID_in_ruleFullJvmFormalParameter9011);
            lv_name_1_0=ruleValidID();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getFullJvmFormalParameterRule());
              	        }
                     		set(
                     			current, 
                     			"name",
                      		lv_name_1_0, 
                      		"ValidID");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFullJvmFormalParameter"


    // $ANTLR start "entryRuleXFeatureCall"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3836:1: entryRuleXFeatureCall returns [EObject current=null] : iv_ruleXFeatureCall= ruleXFeatureCall EOF ;
    public final EObject entryRuleXFeatureCall() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXFeatureCall = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3837:2: (iv_ruleXFeatureCall= ruleXFeatureCall EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3838:2: iv_ruleXFeatureCall= ruleXFeatureCall EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getXFeatureCallRule()); 
            }
            pushFollow(FOLLOW_ruleXFeatureCall_in_entryRuleXFeatureCall9047);
            iv_ruleXFeatureCall=ruleXFeatureCall();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleXFeatureCall; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleXFeatureCall9057); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXFeatureCall"


    // $ANTLR start "ruleXFeatureCall"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3845:1: ruleXFeatureCall returns [EObject current=null] : ( () ( ( ruleStaticQualifier ) )? (otherlv_2= '<' ( (lv_typeArguments_3_0= ruleJvmArgumentTypeReference ) ) (otherlv_4= ',' ( (lv_typeArguments_5_0= ruleJvmArgumentTypeReference ) ) )* otherlv_6= '>' )? ( ( ruleIdOrSuper ) ) ( ( ( ( '(' ) )=> (lv_explicitOperationCall_8_0= '(' ) ) ( ( ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> (lv_featureCallArguments_9_0= ruleXShortClosure ) ) | ( ( (lv_featureCallArguments_10_0= ruleXExpression ) ) (otherlv_11= ',' ( (lv_featureCallArguments_12_0= ruleXExpression ) ) )* ) )? otherlv_13= ')' )? ( ( ( () '[' ) )=> (lv_featureCallArguments_14_0= ruleXClosure ) )? ) ;
    public final EObject ruleXFeatureCall() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token lv_explicitOperationCall_8_0=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        EObject lv_typeArguments_3_0 = null;

        EObject lv_typeArguments_5_0 = null;

        EObject lv_featureCallArguments_9_0 = null;

        EObject lv_featureCallArguments_10_0 = null;

        EObject lv_featureCallArguments_12_0 = null;

        EObject lv_featureCallArguments_14_0 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3848:28: ( ( () ( ( ruleStaticQualifier ) )? (otherlv_2= '<' ( (lv_typeArguments_3_0= ruleJvmArgumentTypeReference ) ) (otherlv_4= ',' ( (lv_typeArguments_5_0= ruleJvmArgumentTypeReference ) ) )* otherlv_6= '>' )? ( ( ruleIdOrSuper ) ) ( ( ( ( '(' ) )=> (lv_explicitOperationCall_8_0= '(' ) ) ( ( ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> (lv_featureCallArguments_9_0= ruleXShortClosure ) ) | ( ( (lv_featureCallArguments_10_0= ruleXExpression ) ) (otherlv_11= ',' ( (lv_featureCallArguments_12_0= ruleXExpression ) ) )* ) )? otherlv_13= ')' )? ( ( ( () '[' ) )=> (lv_featureCallArguments_14_0= ruleXClosure ) )? ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3849:1: ( () ( ( ruleStaticQualifier ) )? (otherlv_2= '<' ( (lv_typeArguments_3_0= ruleJvmArgumentTypeReference ) ) (otherlv_4= ',' ( (lv_typeArguments_5_0= ruleJvmArgumentTypeReference ) ) )* otherlv_6= '>' )? ( ( ruleIdOrSuper ) ) ( ( ( ( '(' ) )=> (lv_explicitOperationCall_8_0= '(' ) ) ( ( ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> (lv_featureCallArguments_9_0= ruleXShortClosure ) ) | ( ( (lv_featureCallArguments_10_0= ruleXExpression ) ) (otherlv_11= ',' ( (lv_featureCallArguments_12_0= ruleXExpression ) ) )* ) )? otherlv_13= ')' )? ( ( ( () '[' ) )=> (lv_featureCallArguments_14_0= ruleXClosure ) )? )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3849:1: ( () ( ( ruleStaticQualifier ) )? (otherlv_2= '<' ( (lv_typeArguments_3_0= ruleJvmArgumentTypeReference ) ) (otherlv_4= ',' ( (lv_typeArguments_5_0= ruleJvmArgumentTypeReference ) ) )* otherlv_6= '>' )? ( ( ruleIdOrSuper ) ) ( ( ( ( '(' ) )=> (lv_explicitOperationCall_8_0= '(' ) ) ( ( ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> (lv_featureCallArguments_9_0= ruleXShortClosure ) ) | ( ( (lv_featureCallArguments_10_0= ruleXExpression ) ) (otherlv_11= ',' ( (lv_featureCallArguments_12_0= ruleXExpression ) ) )* ) )? otherlv_13= ')' )? ( ( ( () '[' ) )=> (lv_featureCallArguments_14_0= ruleXClosure ) )? )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3849:2: () ( ( ruleStaticQualifier ) )? (otherlv_2= '<' ( (lv_typeArguments_3_0= ruleJvmArgumentTypeReference ) ) (otherlv_4= ',' ( (lv_typeArguments_5_0= ruleJvmArgumentTypeReference ) ) )* otherlv_6= '>' )? ( ( ruleIdOrSuper ) ) ( ( ( ( '(' ) )=> (lv_explicitOperationCall_8_0= '(' ) ) ( ( ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> (lv_featureCallArguments_9_0= ruleXShortClosure ) ) | ( ( (lv_featureCallArguments_10_0= ruleXExpression ) ) (otherlv_11= ',' ( (lv_featureCallArguments_12_0= ruleXExpression ) ) )* ) )? otherlv_13= ')' )? ( ( ( () '[' ) )=> (lv_featureCallArguments_14_0= ruleXClosure ) )?
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3849:2: ()
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3850:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getXFeatureCallAccess().getXFeatureCallAction_0(),
                          current);
                  
            }

            }

            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3855:2: ( ( ruleStaticQualifier ) )?
            int alt59=2;
            int LA59_0 = input.LA(1);

            if ( (LA59_0==RULE_ID) ) {
                int LA59_1 = input.LA(2);

                if ( (LA59_1==65) ) {
                    alt59=1;
                }
            }
            switch (alt59) {
                case 1 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3856:1: ( ruleStaticQualifier )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3856:1: ( ruleStaticQualifier )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3857:3: ruleStaticQualifier
                    {
                    if ( state.backtracking==0 ) {

                      			if (current==null) {
                      	            current = createModelElement(grammarAccess.getXFeatureCallRule());
                      	        }
                              
                    }
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getXFeatureCallAccess().getDeclaringTypeJvmDeclaredTypeCrossReference_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleStaticQualifier_in_ruleXFeatureCall9114);
                    ruleStaticQualifier();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }

            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3870:3: (otherlv_2= '<' ( (lv_typeArguments_3_0= ruleJvmArgumentTypeReference ) ) (otherlv_4= ',' ( (lv_typeArguments_5_0= ruleJvmArgumentTypeReference ) ) )* otherlv_6= '>' )?
            int alt61=2;
            int LA61_0 = input.LA(1);

            if ( (LA61_0==30) ) {
                alt61=1;
            }
            switch (alt61) {
                case 1 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3870:5: otherlv_2= '<' ( (lv_typeArguments_3_0= ruleJvmArgumentTypeReference ) ) (otherlv_4= ',' ( (lv_typeArguments_5_0= ruleJvmArgumentTypeReference ) ) )* otherlv_6= '>'
                    {
                    otherlv_2=(Token)match(input,30,FOLLOW_30_in_ruleXFeatureCall9128); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_2, grammarAccess.getXFeatureCallAccess().getLessThanSignKeyword_2_0());
                          
                    }
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3874:1: ( (lv_typeArguments_3_0= ruleJvmArgumentTypeReference ) )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3875:1: (lv_typeArguments_3_0= ruleJvmArgumentTypeReference )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3875:1: (lv_typeArguments_3_0= ruleJvmArgumentTypeReference )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3876:3: lv_typeArguments_3_0= ruleJvmArgumentTypeReference
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getXFeatureCallAccess().getTypeArgumentsJvmArgumentTypeReferenceParserRuleCall_2_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleJvmArgumentTypeReference_in_ruleXFeatureCall9149);
                    lv_typeArguments_3_0=ruleJvmArgumentTypeReference();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getXFeatureCallRule());
                      	        }
                             		add(
                             			current, 
                             			"typeArguments",
                              		lv_typeArguments_3_0, 
                              		"JvmArgumentTypeReference");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3892:2: (otherlv_4= ',' ( (lv_typeArguments_5_0= ruleJvmArgumentTypeReference ) ) )*
                    loop60:
                    do {
                        int alt60=2;
                        int LA60_0 = input.LA(1);

                        if ( (LA60_0==46) ) {
                            alt60=1;
                        }


                        switch (alt60) {
                    	case 1 :
                    	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3892:4: otherlv_4= ',' ( (lv_typeArguments_5_0= ruleJvmArgumentTypeReference ) )
                    	    {
                    	    otherlv_4=(Token)match(input,46,FOLLOW_46_in_ruleXFeatureCall9162); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	          	newLeafNode(otherlv_4, grammarAccess.getXFeatureCallAccess().getCommaKeyword_2_2_0());
                    	          
                    	    }
                    	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3896:1: ( (lv_typeArguments_5_0= ruleJvmArgumentTypeReference ) )
                    	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3897:1: (lv_typeArguments_5_0= ruleJvmArgumentTypeReference )
                    	    {
                    	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3897:1: (lv_typeArguments_5_0= ruleJvmArgumentTypeReference )
                    	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3898:3: lv_typeArguments_5_0= ruleJvmArgumentTypeReference
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getXFeatureCallAccess().getTypeArgumentsJvmArgumentTypeReferenceParserRuleCall_2_2_1_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FOLLOW_ruleJvmArgumentTypeReference_in_ruleXFeatureCall9183);
                    	    lv_typeArguments_5_0=ruleJvmArgumentTypeReference();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getXFeatureCallRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"typeArguments",
                    	              		lv_typeArguments_5_0, 
                    	              		"JvmArgumentTypeReference");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop60;
                        }
                    } while (true);

                    otherlv_6=(Token)match(input,29,FOLLOW_29_in_ruleXFeatureCall9197); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_6, grammarAccess.getXFeatureCallAccess().getGreaterThanSignKeyword_2_3());
                          
                    }

                    }
                    break;

            }

            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3918:3: ( ( ruleIdOrSuper ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3919:1: ( ruleIdOrSuper )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3919:1: ( ruleIdOrSuper )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3920:3: ruleIdOrSuper
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getXFeatureCallRule());
              	        }
                      
            }
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getXFeatureCallAccess().getFeatureJvmIdentifiableElementCrossReference_3_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleIdOrSuper_in_ruleXFeatureCall9222);
            ruleIdOrSuper();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3933:2: ( ( ( ( '(' ) )=> (lv_explicitOperationCall_8_0= '(' ) ) ( ( ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> (lv_featureCallArguments_9_0= ruleXShortClosure ) ) | ( ( (lv_featureCallArguments_10_0= ruleXExpression ) ) (otherlv_11= ',' ( (lv_featureCallArguments_12_0= ruleXExpression ) ) )* ) )? otherlv_13= ')' )?
            int alt64=2;
            alt64 = dfa64.predict(input);
            switch (alt64) {
                case 1 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3933:3: ( ( ( '(' ) )=> (lv_explicitOperationCall_8_0= '(' ) ) ( ( ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> (lv_featureCallArguments_9_0= ruleXShortClosure ) ) | ( ( (lv_featureCallArguments_10_0= ruleXExpression ) ) (otherlv_11= ',' ( (lv_featureCallArguments_12_0= ruleXExpression ) ) )* ) )? otherlv_13= ')'
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3933:3: ( ( ( '(' ) )=> (lv_explicitOperationCall_8_0= '(' ) )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3933:4: ( ( '(' ) )=> (lv_explicitOperationCall_8_0= '(' )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3940:1: (lv_explicitOperationCall_8_0= '(' )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3941:3: lv_explicitOperationCall_8_0= '('
                    {
                    lv_explicitOperationCall_8_0=(Token)match(input,47,FOLLOW_47_in_ruleXFeatureCall9256); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_explicitOperationCall_8_0, grammarAccess.getXFeatureCallAccess().getExplicitOperationCallLeftParenthesisKeyword_4_0_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getXFeatureCallRule());
                      	        }
                             		setWithLastConsumed(current, "explicitOperationCall", true, "(");
                      	    
                    }

                    }


                    }

                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3954:2: ( ( ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> (lv_featureCallArguments_9_0= ruleXShortClosure ) ) | ( ( (lv_featureCallArguments_10_0= ruleXExpression ) ) (otherlv_11= ',' ( (lv_featureCallArguments_12_0= ruleXExpression ) ) )* ) )?
                    int alt63=3;
                    alt63 = dfa63.predict(input);
                    switch (alt63) {
                        case 1 :
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3954:3: ( ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> (lv_featureCallArguments_9_0= ruleXShortClosure ) )
                            {
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3954:3: ( ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> (lv_featureCallArguments_9_0= ruleXShortClosure ) )
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3954:4: ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> (lv_featureCallArguments_9_0= ruleXShortClosure )
                            {
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3971:1: (lv_featureCallArguments_9_0= ruleXShortClosure )
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3972:3: lv_featureCallArguments_9_0= ruleXShortClosure
                            {
                            if ( state.backtracking==0 ) {
                               
                              	        newCompositeNode(grammarAccess.getXFeatureCallAccess().getFeatureCallArgumentsXShortClosureParserRuleCall_4_1_0_0()); 
                              	    
                            }
                            pushFollow(FOLLOW_ruleXShortClosure_in_ruleXFeatureCall9341);
                            lv_featureCallArguments_9_0=ruleXShortClosure();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElementForParent(grammarAccess.getXFeatureCallRule());
                              	        }
                                     		add(
                                     			current, 
                                     			"featureCallArguments",
                                      		lv_featureCallArguments_9_0, 
                                      		"XShortClosure");
                              	        afterParserOrEnumRuleCall();
                              	    
                            }

                            }


                            }


                            }
                            break;
                        case 2 :
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3989:6: ( ( (lv_featureCallArguments_10_0= ruleXExpression ) ) (otherlv_11= ',' ( (lv_featureCallArguments_12_0= ruleXExpression ) ) )* )
                            {
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3989:6: ( ( (lv_featureCallArguments_10_0= ruleXExpression ) ) (otherlv_11= ',' ( (lv_featureCallArguments_12_0= ruleXExpression ) ) )* )
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3989:7: ( (lv_featureCallArguments_10_0= ruleXExpression ) ) (otherlv_11= ',' ( (lv_featureCallArguments_12_0= ruleXExpression ) ) )*
                            {
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3989:7: ( (lv_featureCallArguments_10_0= ruleXExpression ) )
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3990:1: (lv_featureCallArguments_10_0= ruleXExpression )
                            {
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3990:1: (lv_featureCallArguments_10_0= ruleXExpression )
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3991:3: lv_featureCallArguments_10_0= ruleXExpression
                            {
                            if ( state.backtracking==0 ) {
                               
                              	        newCompositeNode(grammarAccess.getXFeatureCallAccess().getFeatureCallArgumentsXExpressionParserRuleCall_4_1_1_0_0()); 
                              	    
                            }
                            pushFollow(FOLLOW_ruleXExpression_in_ruleXFeatureCall9369);
                            lv_featureCallArguments_10_0=ruleXExpression();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElementForParent(grammarAccess.getXFeatureCallRule());
                              	        }
                                     		add(
                                     			current, 
                                     			"featureCallArguments",
                                      		lv_featureCallArguments_10_0, 
                                      		"XExpression");
                              	        afterParserOrEnumRuleCall();
                              	    
                            }

                            }


                            }

                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4007:2: (otherlv_11= ',' ( (lv_featureCallArguments_12_0= ruleXExpression ) ) )*
                            loop62:
                            do {
                                int alt62=2;
                                int LA62_0 = input.LA(1);

                                if ( (LA62_0==46) ) {
                                    alt62=1;
                                }


                                switch (alt62) {
                            	case 1 :
                            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4007:4: otherlv_11= ',' ( (lv_featureCallArguments_12_0= ruleXExpression ) )
                            	    {
                            	    otherlv_11=(Token)match(input,46,FOLLOW_46_in_ruleXFeatureCall9382); if (state.failed) return current;
                            	    if ( state.backtracking==0 ) {

                            	          	newLeafNode(otherlv_11, grammarAccess.getXFeatureCallAccess().getCommaKeyword_4_1_1_1_0());
                            	          
                            	    }
                            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4011:1: ( (lv_featureCallArguments_12_0= ruleXExpression ) )
                            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4012:1: (lv_featureCallArguments_12_0= ruleXExpression )
                            	    {
                            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4012:1: (lv_featureCallArguments_12_0= ruleXExpression )
                            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4013:3: lv_featureCallArguments_12_0= ruleXExpression
                            	    {
                            	    if ( state.backtracking==0 ) {
                            	       
                            	      	        newCompositeNode(grammarAccess.getXFeatureCallAccess().getFeatureCallArgumentsXExpressionParserRuleCall_4_1_1_1_1_0()); 
                            	      	    
                            	    }
                            	    pushFollow(FOLLOW_ruleXExpression_in_ruleXFeatureCall9403);
                            	    lv_featureCallArguments_12_0=ruleXExpression();

                            	    state._fsp--;
                            	    if (state.failed) return current;
                            	    if ( state.backtracking==0 ) {

                            	      	        if (current==null) {
                            	      	            current = createModelElementForParent(grammarAccess.getXFeatureCallRule());
                            	      	        }
                            	             		add(
                            	             			current, 
                            	             			"featureCallArguments",
                            	              		lv_featureCallArguments_12_0, 
                            	              		"XExpression");
                            	      	        afterParserOrEnumRuleCall();
                            	      	    
                            	    }

                            	    }


                            	    }


                            	    }
                            	    break;

                            	default :
                            	    break loop62;
                                }
                            } while (true);


                            }


                            }
                            break;

                    }

                    otherlv_13=(Token)match(input,48,FOLLOW_48_in_ruleXFeatureCall9420); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_13, grammarAccess.getXFeatureCallAccess().getRightParenthesisKeyword_4_2());
                          
                    }

                    }
                    break;

            }

            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4033:3: ( ( ( () '[' ) )=> (lv_featureCallArguments_14_0= ruleXClosure ) )?
            int alt65=2;
            alt65 = dfa65.predict(input);
            switch (alt65) {
                case 1 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4033:4: ( ( () '[' ) )=> (lv_featureCallArguments_14_0= ruleXClosure )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4036:1: (lv_featureCallArguments_14_0= ruleXClosure )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4037:3: lv_featureCallArguments_14_0= ruleXClosure
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getXFeatureCallAccess().getFeatureCallArgumentsXClosureParserRuleCall_5_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleXClosure_in_ruleXFeatureCall9455);
                    lv_featureCallArguments_14_0=ruleXClosure();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getXFeatureCallRule());
                      	        }
                             		add(
                             			current, 
                             			"featureCallArguments",
                              		lv_featureCallArguments_14_0, 
                              		"XClosure");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXFeatureCall"


    // $ANTLR start "entryRuleIdOrSuper"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4061:1: entryRuleIdOrSuper returns [String current=null] : iv_ruleIdOrSuper= ruleIdOrSuper EOF ;
    public final String entryRuleIdOrSuper() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleIdOrSuper = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4062:2: (iv_ruleIdOrSuper= ruleIdOrSuper EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4063:2: iv_ruleIdOrSuper= ruleIdOrSuper EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIdOrSuperRule()); 
            }
            pushFollow(FOLLOW_ruleIdOrSuper_in_entryRuleIdOrSuper9493);
            iv_ruleIdOrSuper=ruleIdOrSuper();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIdOrSuper.getText(); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleIdOrSuper9504); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIdOrSuper"


    // $ANTLR start "ruleIdOrSuper"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4070:1: ruleIdOrSuper returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ValidID_0= ruleValidID | kw= 'super' ) ;
    public final AntlrDatatypeRuleToken ruleIdOrSuper() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        AntlrDatatypeRuleToken this_ValidID_0 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4073:28: ( (this_ValidID_0= ruleValidID | kw= 'super' ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4074:1: (this_ValidID_0= ruleValidID | kw= 'super' )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4074:1: (this_ValidID_0= ruleValidID | kw= 'super' )
            int alt66=2;
            int LA66_0 = input.LA(1);

            if ( (LA66_0==RULE_ID) ) {
                alt66=1;
            }
            else if ( (LA66_0==64) ) {
                alt66=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 66, 0, input);

                throw nvae;
            }
            switch (alt66) {
                case 1 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4075:5: this_ValidID_0= ruleValidID
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getIdOrSuperAccess().getValidIDParserRuleCall_0()); 
                          
                    }
                    pushFollow(FOLLOW_ruleValidID_in_ruleIdOrSuper9551);
                    this_ValidID_0=ruleValidID();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		current.merge(this_ValidID_0);
                          
                    }
                    if ( state.backtracking==0 ) {
                       
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4087:2: kw= 'super'
                    {
                    kw=(Token)match(input,64,FOLLOW_64_in_ruleIdOrSuper9575); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current.merge(kw);
                              newLeafNode(kw, grammarAccess.getIdOrSuperAccess().getSuperKeyword_1()); 
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIdOrSuper"


    // $ANTLR start "entryRuleStaticQualifier"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4100:1: entryRuleStaticQualifier returns [String current=null] : iv_ruleStaticQualifier= ruleStaticQualifier EOF ;
    public final String entryRuleStaticQualifier() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleStaticQualifier = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4101:2: (iv_ruleStaticQualifier= ruleStaticQualifier EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4102:2: iv_ruleStaticQualifier= ruleStaticQualifier EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getStaticQualifierRule()); 
            }
            pushFollow(FOLLOW_ruleStaticQualifier_in_entryRuleStaticQualifier9616);
            iv_ruleStaticQualifier=ruleStaticQualifier();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleStaticQualifier.getText(); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleStaticQualifier9627); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStaticQualifier"


    // $ANTLR start "ruleStaticQualifier"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4109:1: ruleStaticQualifier returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ValidID_0= ruleValidID kw= '::' )+ ;
    public final AntlrDatatypeRuleToken ruleStaticQualifier() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        AntlrDatatypeRuleToken this_ValidID_0 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4112:28: ( (this_ValidID_0= ruleValidID kw= '::' )+ )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4113:1: (this_ValidID_0= ruleValidID kw= '::' )+
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4113:1: (this_ValidID_0= ruleValidID kw= '::' )+
            int cnt67=0;
            loop67:
            do {
                int alt67=2;
                int LA67_0 = input.LA(1);

                if ( (LA67_0==RULE_ID) ) {
                    int LA67_2 = input.LA(2);

                    if ( (LA67_2==65) ) {
                        alt67=1;
                    }


                }


                switch (alt67) {
            	case 1 :
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4114:5: this_ValidID_0= ruleValidID kw= '::'
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	              newCompositeNode(grammarAccess.getStaticQualifierAccess().getValidIDParserRuleCall_0()); 
            	          
            	    }
            	    pushFollow(FOLLOW_ruleValidID_in_ruleStaticQualifier9674);
            	    this_ValidID_0=ruleValidID();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      		current.merge(this_ValidID_0);
            	          
            	    }
            	    if ( state.backtracking==0 ) {
            	       
            	              afterParserOrEnumRuleCall();
            	          
            	    }
            	    kw=(Token)match(input,65,FOLLOW_65_in_ruleStaticQualifier9692); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	              current.merge(kw);
            	              newLeafNode(kw, grammarAccess.getStaticQualifierAccess().getColonColonKeyword_1()); 
            	          
            	    }

            	    }
            	    break;

            	default :
            	    if ( cnt67 >= 1 ) break loop67;
            	    if (state.backtracking>0) {state.failed=true; return current;}
                        EarlyExitException eee =
                            new EarlyExitException(67, input);
                        throw eee;
                }
                cnt67++;
            } while (true);


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStaticQualifier"


    // $ANTLR start "entryRuleXConstructorCall"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4138:1: entryRuleXConstructorCall returns [EObject current=null] : iv_ruleXConstructorCall= ruleXConstructorCall EOF ;
    public final EObject entryRuleXConstructorCall() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXConstructorCall = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4139:2: (iv_ruleXConstructorCall= ruleXConstructorCall EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4140:2: iv_ruleXConstructorCall= ruleXConstructorCall EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getXConstructorCallRule()); 
            }
            pushFollow(FOLLOW_ruleXConstructorCall_in_entryRuleXConstructorCall9733);
            iv_ruleXConstructorCall=ruleXConstructorCall();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleXConstructorCall; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleXConstructorCall9743); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXConstructorCall"


    // $ANTLR start "ruleXConstructorCall"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4147:1: ruleXConstructorCall returns [EObject current=null] : ( () otherlv_1= 'new' ( ( ruleQualifiedName ) ) ( ( ( '<' )=>otherlv_3= '<' ) ( (lv_typeArguments_4_0= ruleJvmArgumentTypeReference ) ) (otherlv_5= ',' ( (lv_typeArguments_6_0= ruleJvmArgumentTypeReference ) ) )* otherlv_7= '>' )? ( ( ( '(' )=>otherlv_8= '(' ) ( ( ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> (lv_arguments_9_0= ruleXShortClosure ) ) | ( ( (lv_arguments_10_0= ruleXExpression ) ) (otherlv_11= ',' ( (lv_arguments_12_0= ruleXExpression ) ) )* ) )? otherlv_13= ')' )? ( ( ( () '[' ) )=> (lv_arguments_14_0= ruleXClosure ) )? ) ;
    public final EObject ruleXConstructorCall() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        EObject lv_typeArguments_4_0 = null;

        EObject lv_typeArguments_6_0 = null;

        EObject lv_arguments_9_0 = null;

        EObject lv_arguments_10_0 = null;

        EObject lv_arguments_12_0 = null;

        EObject lv_arguments_14_0 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4150:28: ( ( () otherlv_1= 'new' ( ( ruleQualifiedName ) ) ( ( ( '<' )=>otherlv_3= '<' ) ( (lv_typeArguments_4_0= ruleJvmArgumentTypeReference ) ) (otherlv_5= ',' ( (lv_typeArguments_6_0= ruleJvmArgumentTypeReference ) ) )* otherlv_7= '>' )? ( ( ( '(' )=>otherlv_8= '(' ) ( ( ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> (lv_arguments_9_0= ruleXShortClosure ) ) | ( ( (lv_arguments_10_0= ruleXExpression ) ) (otherlv_11= ',' ( (lv_arguments_12_0= ruleXExpression ) ) )* ) )? otherlv_13= ')' )? ( ( ( () '[' ) )=> (lv_arguments_14_0= ruleXClosure ) )? ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4151:1: ( () otherlv_1= 'new' ( ( ruleQualifiedName ) ) ( ( ( '<' )=>otherlv_3= '<' ) ( (lv_typeArguments_4_0= ruleJvmArgumentTypeReference ) ) (otherlv_5= ',' ( (lv_typeArguments_6_0= ruleJvmArgumentTypeReference ) ) )* otherlv_7= '>' )? ( ( ( '(' )=>otherlv_8= '(' ) ( ( ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> (lv_arguments_9_0= ruleXShortClosure ) ) | ( ( (lv_arguments_10_0= ruleXExpression ) ) (otherlv_11= ',' ( (lv_arguments_12_0= ruleXExpression ) ) )* ) )? otherlv_13= ')' )? ( ( ( () '[' ) )=> (lv_arguments_14_0= ruleXClosure ) )? )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4151:1: ( () otherlv_1= 'new' ( ( ruleQualifiedName ) ) ( ( ( '<' )=>otherlv_3= '<' ) ( (lv_typeArguments_4_0= ruleJvmArgumentTypeReference ) ) (otherlv_5= ',' ( (lv_typeArguments_6_0= ruleJvmArgumentTypeReference ) ) )* otherlv_7= '>' )? ( ( ( '(' )=>otherlv_8= '(' ) ( ( ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> (lv_arguments_9_0= ruleXShortClosure ) ) | ( ( (lv_arguments_10_0= ruleXExpression ) ) (otherlv_11= ',' ( (lv_arguments_12_0= ruleXExpression ) ) )* ) )? otherlv_13= ')' )? ( ( ( () '[' ) )=> (lv_arguments_14_0= ruleXClosure ) )? )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4151:2: () otherlv_1= 'new' ( ( ruleQualifiedName ) ) ( ( ( '<' )=>otherlv_3= '<' ) ( (lv_typeArguments_4_0= ruleJvmArgumentTypeReference ) ) (otherlv_5= ',' ( (lv_typeArguments_6_0= ruleJvmArgumentTypeReference ) ) )* otherlv_7= '>' )? ( ( ( '(' )=>otherlv_8= '(' ) ( ( ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> (lv_arguments_9_0= ruleXShortClosure ) ) | ( ( (lv_arguments_10_0= ruleXExpression ) ) (otherlv_11= ',' ( (lv_arguments_12_0= ruleXExpression ) ) )* ) )? otherlv_13= ')' )? ( ( ( () '[' ) )=> (lv_arguments_14_0= ruleXClosure ) )?
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4151:2: ()
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4152:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getXConstructorCallAccess().getXConstructorCallAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,66,FOLLOW_66_in_ruleXConstructorCall9789); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getXConstructorCallAccess().getNewKeyword_1());
                  
            }
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4161:1: ( ( ruleQualifiedName ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4162:1: ( ruleQualifiedName )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4162:1: ( ruleQualifiedName )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4163:3: ruleQualifiedName
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getXConstructorCallRule());
              	        }
                      
            }
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getXConstructorCallAccess().getConstructorJvmConstructorCrossReference_2_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleQualifiedName_in_ruleXConstructorCall9812);
            ruleQualifiedName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4176:2: ( ( ( '<' )=>otherlv_3= '<' ) ( (lv_typeArguments_4_0= ruleJvmArgumentTypeReference ) ) (otherlv_5= ',' ( (lv_typeArguments_6_0= ruleJvmArgumentTypeReference ) ) )* otherlv_7= '>' )?
            int alt69=2;
            alt69 = dfa69.predict(input);
            switch (alt69) {
                case 1 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4176:3: ( ( '<' )=>otherlv_3= '<' ) ( (lv_typeArguments_4_0= ruleJvmArgumentTypeReference ) ) (otherlv_5= ',' ( (lv_typeArguments_6_0= ruleJvmArgumentTypeReference ) ) )* otherlv_7= '>'
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4176:3: ( ( '<' )=>otherlv_3= '<' )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4176:4: ( '<' )=>otherlv_3= '<'
                    {
                    otherlv_3=(Token)match(input,30,FOLLOW_30_in_ruleXConstructorCall9833); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_3, grammarAccess.getXConstructorCallAccess().getLessThanSignKeyword_3_0());
                          
                    }

                    }

                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4181:2: ( (lv_typeArguments_4_0= ruleJvmArgumentTypeReference ) )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4182:1: (lv_typeArguments_4_0= ruleJvmArgumentTypeReference )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4182:1: (lv_typeArguments_4_0= ruleJvmArgumentTypeReference )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4183:3: lv_typeArguments_4_0= ruleJvmArgumentTypeReference
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getXConstructorCallAccess().getTypeArgumentsJvmArgumentTypeReferenceParserRuleCall_3_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleJvmArgumentTypeReference_in_ruleXConstructorCall9855);
                    lv_typeArguments_4_0=ruleJvmArgumentTypeReference();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getXConstructorCallRule());
                      	        }
                             		add(
                             			current, 
                             			"typeArguments",
                              		lv_typeArguments_4_0, 
                              		"JvmArgumentTypeReference");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4199:2: (otherlv_5= ',' ( (lv_typeArguments_6_0= ruleJvmArgumentTypeReference ) ) )*
                    loop68:
                    do {
                        int alt68=2;
                        int LA68_0 = input.LA(1);

                        if ( (LA68_0==46) ) {
                            alt68=1;
                        }


                        switch (alt68) {
                    	case 1 :
                    	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4199:4: otherlv_5= ',' ( (lv_typeArguments_6_0= ruleJvmArgumentTypeReference ) )
                    	    {
                    	    otherlv_5=(Token)match(input,46,FOLLOW_46_in_ruleXConstructorCall9868); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	          	newLeafNode(otherlv_5, grammarAccess.getXConstructorCallAccess().getCommaKeyword_3_2_0());
                    	          
                    	    }
                    	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4203:1: ( (lv_typeArguments_6_0= ruleJvmArgumentTypeReference ) )
                    	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4204:1: (lv_typeArguments_6_0= ruleJvmArgumentTypeReference )
                    	    {
                    	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4204:1: (lv_typeArguments_6_0= ruleJvmArgumentTypeReference )
                    	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4205:3: lv_typeArguments_6_0= ruleJvmArgumentTypeReference
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getXConstructorCallAccess().getTypeArgumentsJvmArgumentTypeReferenceParserRuleCall_3_2_1_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FOLLOW_ruleJvmArgumentTypeReference_in_ruleXConstructorCall9889);
                    	    lv_typeArguments_6_0=ruleJvmArgumentTypeReference();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getXConstructorCallRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"typeArguments",
                    	              		lv_typeArguments_6_0, 
                    	              		"JvmArgumentTypeReference");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop68;
                        }
                    } while (true);

                    otherlv_7=(Token)match(input,29,FOLLOW_29_in_ruleXConstructorCall9903); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_7, grammarAccess.getXConstructorCallAccess().getGreaterThanSignKeyword_3_3());
                          
                    }

                    }
                    break;

            }

            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4225:3: ( ( ( '(' )=>otherlv_8= '(' ) ( ( ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> (lv_arguments_9_0= ruleXShortClosure ) ) | ( ( (lv_arguments_10_0= ruleXExpression ) ) (otherlv_11= ',' ( (lv_arguments_12_0= ruleXExpression ) ) )* ) )? otherlv_13= ')' )?
            int alt72=2;
            alt72 = dfa72.predict(input);
            switch (alt72) {
                case 1 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4225:4: ( ( '(' )=>otherlv_8= '(' ) ( ( ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> (lv_arguments_9_0= ruleXShortClosure ) ) | ( ( (lv_arguments_10_0= ruleXExpression ) ) (otherlv_11= ',' ( (lv_arguments_12_0= ruleXExpression ) ) )* ) )? otherlv_13= ')'
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4225:4: ( ( '(' )=>otherlv_8= '(' )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4225:5: ( '(' )=>otherlv_8= '('
                    {
                    otherlv_8=(Token)match(input,47,FOLLOW_47_in_ruleXConstructorCall9926); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_8, grammarAccess.getXConstructorCallAccess().getLeftParenthesisKeyword_4_0());
                          
                    }

                    }

                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4230:2: ( ( ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> (lv_arguments_9_0= ruleXShortClosure ) ) | ( ( (lv_arguments_10_0= ruleXExpression ) ) (otherlv_11= ',' ( (lv_arguments_12_0= ruleXExpression ) ) )* ) )?
                    int alt71=3;
                    alt71 = dfa71.predict(input);
                    switch (alt71) {
                        case 1 :
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4230:3: ( ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> (lv_arguments_9_0= ruleXShortClosure ) )
                            {
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4230:3: ( ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> (lv_arguments_9_0= ruleXShortClosure ) )
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4230:4: ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> (lv_arguments_9_0= ruleXShortClosure )
                            {
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4247:1: (lv_arguments_9_0= ruleXShortClosure )
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4248:3: lv_arguments_9_0= ruleXShortClosure
                            {
                            if ( state.backtracking==0 ) {
                               
                              	        newCompositeNode(grammarAccess.getXConstructorCallAccess().getArgumentsXShortClosureParserRuleCall_4_1_0_0()); 
                              	    
                            }
                            pushFollow(FOLLOW_ruleXShortClosure_in_ruleXConstructorCall9999);
                            lv_arguments_9_0=ruleXShortClosure();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElementForParent(grammarAccess.getXConstructorCallRule());
                              	        }
                                     		add(
                                     			current, 
                                     			"arguments",
                                      		lv_arguments_9_0, 
                                      		"XShortClosure");
                              	        afterParserOrEnumRuleCall();
                              	    
                            }

                            }


                            }


                            }
                            break;
                        case 2 :
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4265:6: ( ( (lv_arguments_10_0= ruleXExpression ) ) (otherlv_11= ',' ( (lv_arguments_12_0= ruleXExpression ) ) )* )
                            {
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4265:6: ( ( (lv_arguments_10_0= ruleXExpression ) ) (otherlv_11= ',' ( (lv_arguments_12_0= ruleXExpression ) ) )* )
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4265:7: ( (lv_arguments_10_0= ruleXExpression ) ) (otherlv_11= ',' ( (lv_arguments_12_0= ruleXExpression ) ) )*
                            {
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4265:7: ( (lv_arguments_10_0= ruleXExpression ) )
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4266:1: (lv_arguments_10_0= ruleXExpression )
                            {
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4266:1: (lv_arguments_10_0= ruleXExpression )
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4267:3: lv_arguments_10_0= ruleXExpression
                            {
                            if ( state.backtracking==0 ) {
                               
                              	        newCompositeNode(grammarAccess.getXConstructorCallAccess().getArgumentsXExpressionParserRuleCall_4_1_1_0_0()); 
                              	    
                            }
                            pushFollow(FOLLOW_ruleXExpression_in_ruleXConstructorCall10027);
                            lv_arguments_10_0=ruleXExpression();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElementForParent(grammarAccess.getXConstructorCallRule());
                              	        }
                                     		add(
                                     			current, 
                                     			"arguments",
                                      		lv_arguments_10_0, 
                                      		"XExpression");
                              	        afterParserOrEnumRuleCall();
                              	    
                            }

                            }


                            }

                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4283:2: (otherlv_11= ',' ( (lv_arguments_12_0= ruleXExpression ) ) )*
                            loop70:
                            do {
                                int alt70=2;
                                int LA70_0 = input.LA(1);

                                if ( (LA70_0==46) ) {
                                    alt70=1;
                                }


                                switch (alt70) {
                            	case 1 :
                            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4283:4: otherlv_11= ',' ( (lv_arguments_12_0= ruleXExpression ) )
                            	    {
                            	    otherlv_11=(Token)match(input,46,FOLLOW_46_in_ruleXConstructorCall10040); if (state.failed) return current;
                            	    if ( state.backtracking==0 ) {

                            	          	newLeafNode(otherlv_11, grammarAccess.getXConstructorCallAccess().getCommaKeyword_4_1_1_1_0());
                            	          
                            	    }
                            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4287:1: ( (lv_arguments_12_0= ruleXExpression ) )
                            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4288:1: (lv_arguments_12_0= ruleXExpression )
                            	    {
                            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4288:1: (lv_arguments_12_0= ruleXExpression )
                            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4289:3: lv_arguments_12_0= ruleXExpression
                            	    {
                            	    if ( state.backtracking==0 ) {
                            	       
                            	      	        newCompositeNode(grammarAccess.getXConstructorCallAccess().getArgumentsXExpressionParserRuleCall_4_1_1_1_1_0()); 
                            	      	    
                            	    }
                            	    pushFollow(FOLLOW_ruleXExpression_in_ruleXConstructorCall10061);
                            	    lv_arguments_12_0=ruleXExpression();

                            	    state._fsp--;
                            	    if (state.failed) return current;
                            	    if ( state.backtracking==0 ) {

                            	      	        if (current==null) {
                            	      	            current = createModelElementForParent(grammarAccess.getXConstructorCallRule());
                            	      	        }
                            	             		add(
                            	             			current, 
                            	             			"arguments",
                            	              		lv_arguments_12_0, 
                            	              		"XExpression");
                            	      	        afterParserOrEnumRuleCall();
                            	      	    
                            	    }

                            	    }


                            	    }


                            	    }
                            	    break;

                            	default :
                            	    break loop70;
                                }
                            } while (true);


                            }


                            }
                            break;

                    }

                    otherlv_13=(Token)match(input,48,FOLLOW_48_in_ruleXConstructorCall10078); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_13, grammarAccess.getXConstructorCallAccess().getRightParenthesisKeyword_4_2());
                          
                    }

                    }
                    break;

            }

            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4309:3: ( ( ( () '[' ) )=> (lv_arguments_14_0= ruleXClosure ) )?
            int alt73=2;
            alt73 = dfa73.predict(input);
            switch (alt73) {
                case 1 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4309:4: ( ( () '[' ) )=> (lv_arguments_14_0= ruleXClosure )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4312:1: (lv_arguments_14_0= ruleXClosure )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4313:3: lv_arguments_14_0= ruleXClosure
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getXConstructorCallAccess().getArgumentsXClosureParserRuleCall_5_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleXClosure_in_ruleXConstructorCall10113);
                    lv_arguments_14_0=ruleXClosure();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getXConstructorCallRule());
                      	        }
                             		add(
                             			current, 
                             			"arguments",
                              		lv_arguments_14_0, 
                              		"XClosure");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXConstructorCall"


    // $ANTLR start "entryRuleXBooleanLiteral"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4337:1: entryRuleXBooleanLiteral returns [EObject current=null] : iv_ruleXBooleanLiteral= ruleXBooleanLiteral EOF ;
    public final EObject entryRuleXBooleanLiteral() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXBooleanLiteral = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4338:2: (iv_ruleXBooleanLiteral= ruleXBooleanLiteral EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4339:2: iv_ruleXBooleanLiteral= ruleXBooleanLiteral EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getXBooleanLiteralRule()); 
            }
            pushFollow(FOLLOW_ruleXBooleanLiteral_in_entryRuleXBooleanLiteral10150);
            iv_ruleXBooleanLiteral=ruleXBooleanLiteral();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleXBooleanLiteral; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleXBooleanLiteral10160); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXBooleanLiteral"


    // $ANTLR start "ruleXBooleanLiteral"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4346:1: ruleXBooleanLiteral returns [EObject current=null] : ( () (otherlv_1= 'false' | ( (lv_isTrue_2_0= 'true' ) ) ) ) ;
    public final EObject ruleXBooleanLiteral() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_isTrue_2_0=null;

         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4349:28: ( ( () (otherlv_1= 'false' | ( (lv_isTrue_2_0= 'true' ) ) ) ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4350:1: ( () (otherlv_1= 'false' | ( (lv_isTrue_2_0= 'true' ) ) ) )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4350:1: ( () (otherlv_1= 'false' | ( (lv_isTrue_2_0= 'true' ) ) ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4350:2: () (otherlv_1= 'false' | ( (lv_isTrue_2_0= 'true' ) ) )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4350:2: ()
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4351:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getXBooleanLiteralAccess().getXBooleanLiteralAction_0(),
                          current);
                  
            }

            }

            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4356:2: (otherlv_1= 'false' | ( (lv_isTrue_2_0= 'true' ) ) )
            int alt74=2;
            int LA74_0 = input.LA(1);

            if ( (LA74_0==67) ) {
                alt74=1;
            }
            else if ( (LA74_0==68) ) {
                alt74=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 74, 0, input);

                throw nvae;
            }
            switch (alt74) {
                case 1 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4356:4: otherlv_1= 'false'
                    {
                    otherlv_1=(Token)match(input,67,FOLLOW_67_in_ruleXBooleanLiteral10207); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_1, grammarAccess.getXBooleanLiteralAccess().getFalseKeyword_1_0());
                          
                    }

                    }
                    break;
                case 2 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4361:6: ( (lv_isTrue_2_0= 'true' ) )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4361:6: ( (lv_isTrue_2_0= 'true' ) )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4362:1: (lv_isTrue_2_0= 'true' )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4362:1: (lv_isTrue_2_0= 'true' )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4363:3: lv_isTrue_2_0= 'true'
                    {
                    lv_isTrue_2_0=(Token)match(input,68,FOLLOW_68_in_ruleXBooleanLiteral10231); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_isTrue_2_0, grammarAccess.getXBooleanLiteralAccess().getIsTrueTrueKeyword_1_1_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getXBooleanLiteralRule());
                      	        }
                             		setWithLastConsumed(current, "isTrue", true, "true");
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXBooleanLiteral"


    // $ANTLR start "entryRuleXNullLiteral"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4384:1: entryRuleXNullLiteral returns [EObject current=null] : iv_ruleXNullLiteral= ruleXNullLiteral EOF ;
    public final EObject entryRuleXNullLiteral() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXNullLiteral = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4385:2: (iv_ruleXNullLiteral= ruleXNullLiteral EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4386:2: iv_ruleXNullLiteral= ruleXNullLiteral EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getXNullLiteralRule()); 
            }
            pushFollow(FOLLOW_ruleXNullLiteral_in_entryRuleXNullLiteral10281);
            iv_ruleXNullLiteral=ruleXNullLiteral();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleXNullLiteral; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleXNullLiteral10291); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXNullLiteral"


    // $ANTLR start "ruleXNullLiteral"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4393:1: ruleXNullLiteral returns [EObject current=null] : ( () otherlv_1= 'null' ) ;
    public final EObject ruleXNullLiteral() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;

         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4396:28: ( ( () otherlv_1= 'null' ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4397:1: ( () otherlv_1= 'null' )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4397:1: ( () otherlv_1= 'null' )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4397:2: () otherlv_1= 'null'
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4397:2: ()
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4398:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getXNullLiteralAccess().getXNullLiteralAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,69,FOLLOW_69_in_ruleXNullLiteral10337); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getXNullLiteralAccess().getNullKeyword_1());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXNullLiteral"


    // $ANTLR start "entryRuleXNumberLiteral"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4415:1: entryRuleXNumberLiteral returns [EObject current=null] : iv_ruleXNumberLiteral= ruleXNumberLiteral EOF ;
    public final EObject entryRuleXNumberLiteral() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXNumberLiteral = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4416:2: (iv_ruleXNumberLiteral= ruleXNumberLiteral EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4417:2: iv_ruleXNumberLiteral= ruleXNumberLiteral EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getXNumberLiteralRule()); 
            }
            pushFollow(FOLLOW_ruleXNumberLiteral_in_entryRuleXNumberLiteral10373);
            iv_ruleXNumberLiteral=ruleXNumberLiteral();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleXNumberLiteral; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleXNumberLiteral10383); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXNumberLiteral"


    // $ANTLR start "ruleXNumberLiteral"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4424:1: ruleXNumberLiteral returns [EObject current=null] : ( () ( (lv_value_1_0= ruleNumber ) ) ) ;
    public final EObject ruleXNumberLiteral() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_value_1_0 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4427:28: ( ( () ( (lv_value_1_0= ruleNumber ) ) ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4428:1: ( () ( (lv_value_1_0= ruleNumber ) ) )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4428:1: ( () ( (lv_value_1_0= ruleNumber ) ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4428:2: () ( (lv_value_1_0= ruleNumber ) )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4428:2: ()
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4429:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getXNumberLiteralAccess().getXNumberLiteralAction_0(),
                          current);
                  
            }

            }

            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4434:2: ( (lv_value_1_0= ruleNumber ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4435:1: (lv_value_1_0= ruleNumber )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4435:1: (lv_value_1_0= ruleNumber )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4436:3: lv_value_1_0= ruleNumber
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getXNumberLiteralAccess().getValueNumberParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleNumber_in_ruleXNumberLiteral10438);
            lv_value_1_0=ruleNumber();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getXNumberLiteralRule());
              	        }
                     		set(
                     			current, 
                     			"value",
                      		lv_value_1_0, 
                      		"Number");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXNumberLiteral"


    // $ANTLR start "entryRuleXStringLiteral"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4460:1: entryRuleXStringLiteral returns [EObject current=null] : iv_ruleXStringLiteral= ruleXStringLiteral EOF ;
    public final EObject entryRuleXStringLiteral() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXStringLiteral = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4461:2: (iv_ruleXStringLiteral= ruleXStringLiteral EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4462:2: iv_ruleXStringLiteral= ruleXStringLiteral EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getXStringLiteralRule()); 
            }
            pushFollow(FOLLOW_ruleXStringLiteral_in_entryRuleXStringLiteral10474);
            iv_ruleXStringLiteral=ruleXStringLiteral();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleXStringLiteral; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleXStringLiteral10484); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXStringLiteral"


    // $ANTLR start "ruleXStringLiteral"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4469:1: ruleXStringLiteral returns [EObject current=null] : ( () ( (lv_value_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleXStringLiteral() throws RecognitionException {
        EObject current = null;

        Token lv_value_1_0=null;

         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4472:28: ( ( () ( (lv_value_1_0= RULE_STRING ) ) ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4473:1: ( () ( (lv_value_1_0= RULE_STRING ) ) )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4473:1: ( () ( (lv_value_1_0= RULE_STRING ) ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4473:2: () ( (lv_value_1_0= RULE_STRING ) )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4473:2: ()
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4474:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getXStringLiteralAccess().getXStringLiteralAction_0(),
                          current);
                  
            }

            }

            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4479:2: ( (lv_value_1_0= RULE_STRING ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4480:1: (lv_value_1_0= RULE_STRING )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4480:1: (lv_value_1_0= RULE_STRING )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4481:3: lv_value_1_0= RULE_STRING
            {
            lv_value_1_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleXStringLiteral10535); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_value_1_0, grammarAccess.getXStringLiteralAccess().getValueSTRINGTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getXStringLiteralRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"value",
                      		lv_value_1_0, 
                      		"STRING");
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXStringLiteral"


    // $ANTLR start "entryRuleXTypeLiteral"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4505:1: entryRuleXTypeLiteral returns [EObject current=null] : iv_ruleXTypeLiteral= ruleXTypeLiteral EOF ;
    public final EObject entryRuleXTypeLiteral() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXTypeLiteral = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4506:2: (iv_ruleXTypeLiteral= ruleXTypeLiteral EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4507:2: iv_ruleXTypeLiteral= ruleXTypeLiteral EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getXTypeLiteralRule()); 
            }
            pushFollow(FOLLOW_ruleXTypeLiteral_in_entryRuleXTypeLiteral10576);
            iv_ruleXTypeLiteral=ruleXTypeLiteral();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleXTypeLiteral; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleXTypeLiteral10586); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXTypeLiteral"


    // $ANTLR start "ruleXTypeLiteral"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4514:1: ruleXTypeLiteral returns [EObject current=null] : ( () otherlv_1= 'typeof' otherlv_2= '(' ( ( ruleQualifiedName ) ) otherlv_4= ')' ) ;
    public final EObject ruleXTypeLiteral() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;

         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4517:28: ( ( () otherlv_1= 'typeof' otherlv_2= '(' ( ( ruleQualifiedName ) ) otherlv_4= ')' ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4518:1: ( () otherlv_1= 'typeof' otherlv_2= '(' ( ( ruleQualifiedName ) ) otherlv_4= ')' )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4518:1: ( () otherlv_1= 'typeof' otherlv_2= '(' ( ( ruleQualifiedName ) ) otherlv_4= ')' )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4518:2: () otherlv_1= 'typeof' otherlv_2= '(' ( ( ruleQualifiedName ) ) otherlv_4= ')'
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4518:2: ()
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4519:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getXTypeLiteralAccess().getXTypeLiteralAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,70,FOLLOW_70_in_ruleXTypeLiteral10632); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getXTypeLiteralAccess().getTypeofKeyword_1());
                  
            }
            otherlv_2=(Token)match(input,47,FOLLOW_47_in_ruleXTypeLiteral10644); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getXTypeLiteralAccess().getLeftParenthesisKeyword_2());
                  
            }
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4532:1: ( ( ruleQualifiedName ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4533:1: ( ruleQualifiedName )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4533:1: ( ruleQualifiedName )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4534:3: ruleQualifiedName
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getXTypeLiteralRule());
              	        }
                      
            }
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getXTypeLiteralAccess().getTypeJvmTypeCrossReference_3_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleQualifiedName_in_ruleXTypeLiteral10667);
            ruleQualifiedName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_4=(Token)match(input,48,FOLLOW_48_in_ruleXTypeLiteral10679); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getXTypeLiteralAccess().getRightParenthesisKeyword_4());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXTypeLiteral"


    // $ANTLR start "entryRuleXThrowExpression"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4559:1: entryRuleXThrowExpression returns [EObject current=null] : iv_ruleXThrowExpression= ruleXThrowExpression EOF ;
    public final EObject entryRuleXThrowExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXThrowExpression = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4560:2: (iv_ruleXThrowExpression= ruleXThrowExpression EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4561:2: iv_ruleXThrowExpression= ruleXThrowExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getXThrowExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleXThrowExpression_in_entryRuleXThrowExpression10715);
            iv_ruleXThrowExpression=ruleXThrowExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleXThrowExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleXThrowExpression10725); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXThrowExpression"


    // $ANTLR start "ruleXThrowExpression"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4568:1: ruleXThrowExpression returns [EObject current=null] : ( () otherlv_1= 'throw' ( (lv_expression_2_0= ruleXExpression ) ) ) ;
    public final EObject ruleXThrowExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_expression_2_0 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4571:28: ( ( () otherlv_1= 'throw' ( (lv_expression_2_0= ruleXExpression ) ) ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4572:1: ( () otherlv_1= 'throw' ( (lv_expression_2_0= ruleXExpression ) ) )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4572:1: ( () otherlv_1= 'throw' ( (lv_expression_2_0= ruleXExpression ) ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4572:2: () otherlv_1= 'throw' ( (lv_expression_2_0= ruleXExpression ) )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4572:2: ()
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4573:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getXThrowExpressionAccess().getXThrowExpressionAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,71,FOLLOW_71_in_ruleXThrowExpression10771); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getXThrowExpressionAccess().getThrowKeyword_1());
                  
            }
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4582:1: ( (lv_expression_2_0= ruleXExpression ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4583:1: (lv_expression_2_0= ruleXExpression )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4583:1: (lv_expression_2_0= ruleXExpression )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4584:3: lv_expression_2_0= ruleXExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getXThrowExpressionAccess().getExpressionXExpressionParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleXExpression_in_ruleXThrowExpression10792);
            lv_expression_2_0=ruleXExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getXThrowExpressionRule());
              	        }
                     		set(
                     			current, 
                     			"expression",
                      		lv_expression_2_0, 
                      		"XExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXThrowExpression"


    // $ANTLR start "entryRuleXReturnExpression"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4608:1: entryRuleXReturnExpression returns [EObject current=null] : iv_ruleXReturnExpression= ruleXReturnExpression EOF ;
    public final EObject entryRuleXReturnExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXReturnExpression = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4609:2: (iv_ruleXReturnExpression= ruleXReturnExpression EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4610:2: iv_ruleXReturnExpression= ruleXReturnExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getXReturnExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleXReturnExpression_in_entryRuleXReturnExpression10828);
            iv_ruleXReturnExpression=ruleXReturnExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleXReturnExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleXReturnExpression10838); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXReturnExpression"


    // $ANTLR start "ruleXReturnExpression"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4617:1: ruleXReturnExpression returns [EObject current=null] : ( () otherlv_1= 'return' ( ( ( ruleXExpression ) )=> (lv_expression_2_0= ruleXExpression ) )? ) ;
    public final EObject ruleXReturnExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_expression_2_0 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4620:28: ( ( () otherlv_1= 'return' ( ( ( ruleXExpression ) )=> (lv_expression_2_0= ruleXExpression ) )? ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4621:1: ( () otherlv_1= 'return' ( ( ( ruleXExpression ) )=> (lv_expression_2_0= ruleXExpression ) )? )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4621:1: ( () otherlv_1= 'return' ( ( ( ruleXExpression ) )=> (lv_expression_2_0= ruleXExpression ) )? )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4621:2: () otherlv_1= 'return' ( ( ( ruleXExpression ) )=> (lv_expression_2_0= ruleXExpression ) )?
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4621:2: ()
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4622:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getXReturnExpressionAccess().getXReturnExpressionAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,72,FOLLOW_72_in_ruleXReturnExpression10884); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getXReturnExpressionAccess().getReturnKeyword_1());
                  
            }
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4631:1: ( ( ( ruleXExpression ) )=> (lv_expression_2_0= ruleXExpression ) )?
            int alt75=2;
            alt75 = dfa75.predict(input);
            switch (alt75) {
                case 1 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4631:2: ( ( ruleXExpression ) )=> (lv_expression_2_0= ruleXExpression )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4636:1: (lv_expression_2_0= ruleXExpression )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4637:3: lv_expression_2_0= ruleXExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getXReturnExpressionAccess().getExpressionXExpressionParserRuleCall_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleXExpression_in_ruleXReturnExpression10915);
                    lv_expression_2_0=ruleXExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getXReturnExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"expression",
                              		lv_expression_2_0, 
                              		"XExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXReturnExpression"


    // $ANTLR start "entryRuleXTryCatchFinallyExpression"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4661:1: entryRuleXTryCatchFinallyExpression returns [EObject current=null] : iv_ruleXTryCatchFinallyExpression= ruleXTryCatchFinallyExpression EOF ;
    public final EObject entryRuleXTryCatchFinallyExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXTryCatchFinallyExpression = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4662:2: (iv_ruleXTryCatchFinallyExpression= ruleXTryCatchFinallyExpression EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4663:2: iv_ruleXTryCatchFinallyExpression= ruleXTryCatchFinallyExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getXTryCatchFinallyExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleXTryCatchFinallyExpression_in_entryRuleXTryCatchFinallyExpression10952);
            iv_ruleXTryCatchFinallyExpression=ruleXTryCatchFinallyExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleXTryCatchFinallyExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleXTryCatchFinallyExpression10962); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXTryCatchFinallyExpression"


    // $ANTLR start "ruleXTryCatchFinallyExpression"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4670:1: ruleXTryCatchFinallyExpression returns [EObject current=null] : ( () otherlv_1= 'try' ( (lv_expression_2_0= ruleXExpression ) ) ( ( ( ( 'catch' )=> (lv_catchClauses_3_0= ruleXCatchClause ) )+ ( ( ( 'finally' )=>otherlv_4= 'finally' ) ( (lv_finallyExpression_5_0= ruleXExpression ) ) )? ) | (otherlv_6= 'finally' ( (lv_finallyExpression_7_0= ruleXExpression ) ) ) ) ) ;
    public final EObject ruleXTryCatchFinallyExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_expression_2_0 = null;

        EObject lv_catchClauses_3_0 = null;

        EObject lv_finallyExpression_5_0 = null;

        EObject lv_finallyExpression_7_0 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4673:28: ( ( () otherlv_1= 'try' ( (lv_expression_2_0= ruleXExpression ) ) ( ( ( ( 'catch' )=> (lv_catchClauses_3_0= ruleXCatchClause ) )+ ( ( ( 'finally' )=>otherlv_4= 'finally' ) ( (lv_finallyExpression_5_0= ruleXExpression ) ) )? ) | (otherlv_6= 'finally' ( (lv_finallyExpression_7_0= ruleXExpression ) ) ) ) ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4674:1: ( () otherlv_1= 'try' ( (lv_expression_2_0= ruleXExpression ) ) ( ( ( ( 'catch' )=> (lv_catchClauses_3_0= ruleXCatchClause ) )+ ( ( ( 'finally' )=>otherlv_4= 'finally' ) ( (lv_finallyExpression_5_0= ruleXExpression ) ) )? ) | (otherlv_6= 'finally' ( (lv_finallyExpression_7_0= ruleXExpression ) ) ) ) )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4674:1: ( () otherlv_1= 'try' ( (lv_expression_2_0= ruleXExpression ) ) ( ( ( ( 'catch' )=> (lv_catchClauses_3_0= ruleXCatchClause ) )+ ( ( ( 'finally' )=>otherlv_4= 'finally' ) ( (lv_finallyExpression_5_0= ruleXExpression ) ) )? ) | (otherlv_6= 'finally' ( (lv_finallyExpression_7_0= ruleXExpression ) ) ) ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4674:2: () otherlv_1= 'try' ( (lv_expression_2_0= ruleXExpression ) ) ( ( ( ( 'catch' )=> (lv_catchClauses_3_0= ruleXCatchClause ) )+ ( ( ( 'finally' )=>otherlv_4= 'finally' ) ( (lv_finallyExpression_5_0= ruleXExpression ) ) )? ) | (otherlv_6= 'finally' ( (lv_finallyExpression_7_0= ruleXExpression ) ) ) )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4674:2: ()
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4675:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getXTryCatchFinallyExpressionAccess().getXTryCatchFinallyExpressionAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,73,FOLLOW_73_in_ruleXTryCatchFinallyExpression11008); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getXTryCatchFinallyExpressionAccess().getTryKeyword_1());
                  
            }
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4684:1: ( (lv_expression_2_0= ruleXExpression ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4685:1: (lv_expression_2_0= ruleXExpression )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4685:1: (lv_expression_2_0= ruleXExpression )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4686:3: lv_expression_2_0= ruleXExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getXTryCatchFinallyExpressionAccess().getExpressionXExpressionParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleXExpression_in_ruleXTryCatchFinallyExpression11029);
            lv_expression_2_0=ruleXExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getXTryCatchFinallyExpressionRule());
              	        }
                     		set(
                     			current, 
                     			"expression",
                      		lv_expression_2_0, 
                      		"XExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4702:2: ( ( ( ( 'catch' )=> (lv_catchClauses_3_0= ruleXCatchClause ) )+ ( ( ( 'finally' )=>otherlv_4= 'finally' ) ( (lv_finallyExpression_5_0= ruleXExpression ) ) )? ) | (otherlv_6= 'finally' ( (lv_finallyExpression_7_0= ruleXExpression ) ) ) )
            int alt78=2;
            int LA78_0 = input.LA(1);

            if ( (LA78_0==75) ) {
                alt78=1;
            }
            else if ( (LA78_0==74) ) {
                alt78=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 78, 0, input);

                throw nvae;
            }
            switch (alt78) {
                case 1 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4702:3: ( ( ( 'catch' )=> (lv_catchClauses_3_0= ruleXCatchClause ) )+ ( ( ( 'finally' )=>otherlv_4= 'finally' ) ( (lv_finallyExpression_5_0= ruleXExpression ) ) )? )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4702:3: ( ( ( 'catch' )=> (lv_catchClauses_3_0= ruleXCatchClause ) )+ ( ( ( 'finally' )=>otherlv_4= 'finally' ) ( (lv_finallyExpression_5_0= ruleXExpression ) ) )? )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4702:4: ( ( 'catch' )=> (lv_catchClauses_3_0= ruleXCatchClause ) )+ ( ( ( 'finally' )=>otherlv_4= 'finally' ) ( (lv_finallyExpression_5_0= ruleXExpression ) ) )?
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4702:4: ( ( 'catch' )=> (lv_catchClauses_3_0= ruleXCatchClause ) )+
                    int cnt76=0;
                    loop76:
                    do {
                        int alt76=2;
                        int LA76_0 = input.LA(1);

                        if ( (LA76_0==75) ) {
                            int LA76_2 = input.LA(2);

                            if ( (synpred34_InternalDeepClone()) ) {
                                alt76=1;
                            }


                        }


                        switch (alt76) {
                    	case 1 :
                    	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4702:5: ( 'catch' )=> (lv_catchClauses_3_0= ruleXCatchClause )
                    	    {
                    	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4704:1: (lv_catchClauses_3_0= ruleXCatchClause )
                    	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4705:3: lv_catchClauses_3_0= ruleXCatchClause
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getXTryCatchFinallyExpressionAccess().getCatchClausesXCatchClauseParserRuleCall_3_0_0_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FOLLOW_ruleXCatchClause_in_ruleXTryCatchFinallyExpression11059);
                    	    lv_catchClauses_3_0=ruleXCatchClause();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getXTryCatchFinallyExpressionRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"catchClauses",
                    	              		lv_catchClauses_3_0, 
                    	              		"XCatchClause");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt76 >= 1 ) break loop76;
                    	    if (state.backtracking>0) {state.failed=true; return current;}
                                EarlyExitException eee =
                                    new EarlyExitException(76, input);
                                throw eee;
                        }
                        cnt76++;
                    } while (true);

                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4721:3: ( ( ( 'finally' )=>otherlv_4= 'finally' ) ( (lv_finallyExpression_5_0= ruleXExpression ) ) )?
                    int alt77=2;
                    int LA77_0 = input.LA(1);

                    if ( (LA77_0==74) ) {
                        int LA77_1 = input.LA(2);

                        if ( (synpred35_InternalDeepClone()) ) {
                            alt77=1;
                        }
                    }
                    switch (alt77) {
                        case 1 :
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4721:4: ( ( 'finally' )=>otherlv_4= 'finally' ) ( (lv_finallyExpression_5_0= ruleXExpression ) )
                            {
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4721:4: ( ( 'finally' )=>otherlv_4= 'finally' )
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4721:5: ( 'finally' )=>otherlv_4= 'finally'
                            {
                            otherlv_4=(Token)match(input,74,FOLLOW_74_in_ruleXTryCatchFinallyExpression11081); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                  	newLeafNode(otherlv_4, grammarAccess.getXTryCatchFinallyExpressionAccess().getFinallyKeyword_3_0_1_0());
                                  
                            }

                            }

                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4726:2: ( (lv_finallyExpression_5_0= ruleXExpression ) )
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4727:1: (lv_finallyExpression_5_0= ruleXExpression )
                            {
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4727:1: (lv_finallyExpression_5_0= ruleXExpression )
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4728:3: lv_finallyExpression_5_0= ruleXExpression
                            {
                            if ( state.backtracking==0 ) {
                               
                              	        newCompositeNode(grammarAccess.getXTryCatchFinallyExpressionAccess().getFinallyExpressionXExpressionParserRuleCall_3_0_1_1_0()); 
                              	    
                            }
                            pushFollow(FOLLOW_ruleXExpression_in_ruleXTryCatchFinallyExpression11103);
                            lv_finallyExpression_5_0=ruleXExpression();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElementForParent(grammarAccess.getXTryCatchFinallyExpressionRule());
                              	        }
                                     		set(
                                     			current, 
                                     			"finallyExpression",
                                      		lv_finallyExpression_5_0, 
                                      		"XExpression");
                              	        afterParserOrEnumRuleCall();
                              	    
                            }

                            }


                            }


                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4745:6: (otherlv_6= 'finally' ( (lv_finallyExpression_7_0= ruleXExpression ) ) )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4745:6: (otherlv_6= 'finally' ( (lv_finallyExpression_7_0= ruleXExpression ) ) )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4745:8: otherlv_6= 'finally' ( (lv_finallyExpression_7_0= ruleXExpression ) )
                    {
                    otherlv_6=(Token)match(input,74,FOLLOW_74_in_ruleXTryCatchFinallyExpression11125); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_6, grammarAccess.getXTryCatchFinallyExpressionAccess().getFinallyKeyword_3_1_0());
                          
                    }
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4749:1: ( (lv_finallyExpression_7_0= ruleXExpression ) )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4750:1: (lv_finallyExpression_7_0= ruleXExpression )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4750:1: (lv_finallyExpression_7_0= ruleXExpression )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4751:3: lv_finallyExpression_7_0= ruleXExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getXTryCatchFinallyExpressionAccess().getFinallyExpressionXExpressionParserRuleCall_3_1_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleXExpression_in_ruleXTryCatchFinallyExpression11146);
                    lv_finallyExpression_7_0=ruleXExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getXTryCatchFinallyExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"finallyExpression",
                              		lv_finallyExpression_7_0, 
                              		"XExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXTryCatchFinallyExpression"


    // $ANTLR start "entryRuleXCatchClause"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4775:1: entryRuleXCatchClause returns [EObject current=null] : iv_ruleXCatchClause= ruleXCatchClause EOF ;
    public final EObject entryRuleXCatchClause() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXCatchClause = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4776:2: (iv_ruleXCatchClause= ruleXCatchClause EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4777:2: iv_ruleXCatchClause= ruleXCatchClause EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getXCatchClauseRule()); 
            }
            pushFollow(FOLLOW_ruleXCatchClause_in_entryRuleXCatchClause11184);
            iv_ruleXCatchClause=ruleXCatchClause();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleXCatchClause; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleXCatchClause11194); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXCatchClause"


    // $ANTLR start "ruleXCatchClause"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4784:1: ruleXCatchClause returns [EObject current=null] : ( ( ( 'catch' )=>otherlv_0= 'catch' ) otherlv_1= '(' ( (lv_declaredParam_2_0= ruleFullJvmFormalParameter ) ) otherlv_3= ')' ( (lv_expression_4_0= ruleXExpression ) ) ) ;
    public final EObject ruleXCatchClause() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_declaredParam_2_0 = null;

        EObject lv_expression_4_0 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4787:28: ( ( ( ( 'catch' )=>otherlv_0= 'catch' ) otherlv_1= '(' ( (lv_declaredParam_2_0= ruleFullJvmFormalParameter ) ) otherlv_3= ')' ( (lv_expression_4_0= ruleXExpression ) ) ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4788:1: ( ( ( 'catch' )=>otherlv_0= 'catch' ) otherlv_1= '(' ( (lv_declaredParam_2_0= ruleFullJvmFormalParameter ) ) otherlv_3= ')' ( (lv_expression_4_0= ruleXExpression ) ) )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4788:1: ( ( ( 'catch' )=>otherlv_0= 'catch' ) otherlv_1= '(' ( (lv_declaredParam_2_0= ruleFullJvmFormalParameter ) ) otherlv_3= ')' ( (lv_expression_4_0= ruleXExpression ) ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4788:2: ( ( 'catch' )=>otherlv_0= 'catch' ) otherlv_1= '(' ( (lv_declaredParam_2_0= ruleFullJvmFormalParameter ) ) otherlv_3= ')' ( (lv_expression_4_0= ruleXExpression ) )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4788:2: ( ( 'catch' )=>otherlv_0= 'catch' )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4788:3: ( 'catch' )=>otherlv_0= 'catch'
            {
            otherlv_0=(Token)match(input,75,FOLLOW_75_in_ruleXCatchClause11239); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getXCatchClauseAccess().getCatchKeyword_0());
                  
            }

            }

            otherlv_1=(Token)match(input,47,FOLLOW_47_in_ruleXCatchClause11252); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getXCatchClauseAccess().getLeftParenthesisKeyword_1());
                  
            }
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4797:1: ( (lv_declaredParam_2_0= ruleFullJvmFormalParameter ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4798:1: (lv_declaredParam_2_0= ruleFullJvmFormalParameter )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4798:1: (lv_declaredParam_2_0= ruleFullJvmFormalParameter )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4799:3: lv_declaredParam_2_0= ruleFullJvmFormalParameter
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getXCatchClauseAccess().getDeclaredParamFullJvmFormalParameterParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleFullJvmFormalParameter_in_ruleXCatchClause11273);
            lv_declaredParam_2_0=ruleFullJvmFormalParameter();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getXCatchClauseRule());
              	        }
                     		set(
                     			current, 
                     			"declaredParam",
                      		lv_declaredParam_2_0, 
                      		"FullJvmFormalParameter");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_3=(Token)match(input,48,FOLLOW_48_in_ruleXCatchClause11285); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getXCatchClauseAccess().getRightParenthesisKeyword_3());
                  
            }
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4819:1: ( (lv_expression_4_0= ruleXExpression ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4820:1: (lv_expression_4_0= ruleXExpression )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4820:1: (lv_expression_4_0= ruleXExpression )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4821:3: lv_expression_4_0= ruleXExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getXCatchClauseAccess().getExpressionXExpressionParserRuleCall_4_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleXExpression_in_ruleXCatchClause11306);
            lv_expression_4_0=ruleXExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getXCatchClauseRule());
              	        }
                     		set(
                     			current, 
                     			"expression",
                      		lv_expression_4_0, 
                      		"XExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXCatchClause"


    // $ANTLR start "entryRuleQualifiedName"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4845:1: entryRuleQualifiedName returns [String current=null] : iv_ruleQualifiedName= ruleQualifiedName EOF ;
    public final String entryRuleQualifiedName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualifiedName = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4846:2: (iv_ruleQualifiedName= ruleQualifiedName EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4847:2: iv_ruleQualifiedName= ruleQualifiedName EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getQualifiedNameRule()); 
            }
            pushFollow(FOLLOW_ruleQualifiedName_in_entryRuleQualifiedName11343);
            iv_ruleQualifiedName=ruleQualifiedName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleQualifiedName.getText(); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleQualifiedName11354); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4854:1: ruleQualifiedName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ValidID_0= ruleValidID ( ( ( '.' )=>kw= '.' ) this_ValidID_2= ruleValidID )* ) ;
    public final AntlrDatatypeRuleToken ruleQualifiedName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        AntlrDatatypeRuleToken this_ValidID_0 = null;

        AntlrDatatypeRuleToken this_ValidID_2 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4857:28: ( (this_ValidID_0= ruleValidID ( ( ( '.' )=>kw= '.' ) this_ValidID_2= ruleValidID )* ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4858:1: (this_ValidID_0= ruleValidID ( ( ( '.' )=>kw= '.' ) this_ValidID_2= ruleValidID )* )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4858:1: (this_ValidID_0= ruleValidID ( ( ( '.' )=>kw= '.' ) this_ValidID_2= ruleValidID )* )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4859:5: this_ValidID_0= ruleValidID ( ( ( '.' )=>kw= '.' ) this_ValidID_2= ruleValidID )*
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getQualifiedNameAccess().getValidIDParserRuleCall_0()); 
                  
            }
            pushFollow(FOLLOW_ruleValidID_in_ruleQualifiedName11401);
            this_ValidID_0=ruleValidID();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		current.merge(this_ValidID_0);
                  
            }
            if ( state.backtracking==0 ) {
               
                      afterParserOrEnumRuleCall();
                  
            }
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4869:1: ( ( ( '.' )=>kw= '.' ) this_ValidID_2= ruleValidID )*
            loop79:
            do {
                int alt79=2;
                int LA79_0 = input.LA(1);

                if ( (LA79_0==15) ) {
                    int LA79_2 = input.LA(2);

                    if ( (LA79_2==RULE_ID) ) {
                        int LA79_3 = input.LA(3);

                        if ( (synpred37_InternalDeepClone()) ) {
                            alt79=1;
                        }


                    }


                }


                switch (alt79) {
            	case 1 :
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4869:2: ( ( '.' )=>kw= '.' ) this_ValidID_2= ruleValidID
            	    {
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4869:2: ( ( '.' )=>kw= '.' )
            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4869:3: ( '.' )=>kw= '.'
            	    {
            	    kw=(Token)match(input,15,FOLLOW_15_in_ruleQualifiedName11429); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	              current.merge(kw);
            	              newLeafNode(kw, grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0()); 
            	          
            	    }

            	    }

            	    if ( state.backtracking==0 ) {
            	       
            	              newCompositeNode(grammarAccess.getQualifiedNameAccess().getValidIDParserRuleCall_1_1()); 
            	          
            	    }
            	    pushFollow(FOLLOW_ruleValidID_in_ruleQualifiedName11452);
            	    this_ValidID_2=ruleValidID();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      		current.merge(this_ValidID_2);
            	          
            	    }
            	    if ( state.backtracking==0 ) {
            	       
            	              afterParserOrEnumRuleCall();
            	          
            	    }

            	    }
            	    break;

            	default :
            	    break loop79;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedName"


    // $ANTLR start "entryRuleNumber"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4896:1: entryRuleNumber returns [String current=null] : iv_ruleNumber= ruleNumber EOF ;
    public final String entryRuleNumber() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleNumber = null;


         
        		HiddenTokens myHiddenTokenState = ((XtextTokenStream)input).setHiddenTokens();
        	
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4900:2: (iv_ruleNumber= ruleNumber EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4901:2: iv_ruleNumber= ruleNumber EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNumberRule()); 
            }
            pushFollow(FOLLOW_ruleNumber_in_entryRuleNumber11506);
            iv_ruleNumber=ruleNumber();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNumber.getText(); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleNumber11517); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {

            	myHiddenTokenState.restore();

        }
        return current;
    }
    // $ANTLR end "entryRuleNumber"


    // $ANTLR start "ruleNumber"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4911:1: ruleNumber returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_HEX_0= RULE_HEX | ( (this_INT_1= RULE_INT | this_DECIMAL_2= RULE_DECIMAL ) (kw= '.' (this_INT_4= RULE_INT | this_DECIMAL_5= RULE_DECIMAL ) )? ) ) ;
    public final AntlrDatatypeRuleToken ruleNumber() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_HEX_0=null;
        Token this_INT_1=null;
        Token this_DECIMAL_2=null;
        Token kw=null;
        Token this_INT_4=null;
        Token this_DECIMAL_5=null;

         enterRule(); 
        		HiddenTokens myHiddenTokenState = ((XtextTokenStream)input).setHiddenTokens();
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4915:28: ( (this_HEX_0= RULE_HEX | ( (this_INT_1= RULE_INT | this_DECIMAL_2= RULE_DECIMAL ) (kw= '.' (this_INT_4= RULE_INT | this_DECIMAL_5= RULE_DECIMAL ) )? ) ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4916:1: (this_HEX_0= RULE_HEX | ( (this_INT_1= RULE_INT | this_DECIMAL_2= RULE_DECIMAL ) (kw= '.' (this_INT_4= RULE_INT | this_DECIMAL_5= RULE_DECIMAL ) )? ) )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4916:1: (this_HEX_0= RULE_HEX | ( (this_INT_1= RULE_INT | this_DECIMAL_2= RULE_DECIMAL ) (kw= '.' (this_INT_4= RULE_INT | this_DECIMAL_5= RULE_DECIMAL ) )? ) )
            int alt83=2;
            int LA83_0 = input.LA(1);

            if ( (LA83_0==RULE_HEX) ) {
                alt83=1;
            }
            else if ( ((LA83_0>=RULE_INT && LA83_0<=RULE_DECIMAL)) ) {
                alt83=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 83, 0, input);

                throw nvae;
            }
            switch (alt83) {
                case 1 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4916:6: this_HEX_0= RULE_HEX
                    {
                    this_HEX_0=(Token)match(input,RULE_HEX,FOLLOW_RULE_HEX_in_ruleNumber11561); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		current.merge(this_HEX_0);
                          
                    }
                    if ( state.backtracking==0 ) {
                       
                          newLeafNode(this_HEX_0, grammarAccess.getNumberAccess().getHEXTerminalRuleCall_0()); 
                          
                    }

                    }
                    break;
                case 2 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4924:6: ( (this_INT_1= RULE_INT | this_DECIMAL_2= RULE_DECIMAL ) (kw= '.' (this_INT_4= RULE_INT | this_DECIMAL_5= RULE_DECIMAL ) )? )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4924:6: ( (this_INT_1= RULE_INT | this_DECIMAL_2= RULE_DECIMAL ) (kw= '.' (this_INT_4= RULE_INT | this_DECIMAL_5= RULE_DECIMAL ) )? )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4924:7: (this_INT_1= RULE_INT | this_DECIMAL_2= RULE_DECIMAL ) (kw= '.' (this_INT_4= RULE_INT | this_DECIMAL_5= RULE_DECIMAL ) )?
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4924:7: (this_INT_1= RULE_INT | this_DECIMAL_2= RULE_DECIMAL )
                    int alt80=2;
                    int LA80_0 = input.LA(1);

                    if ( (LA80_0==RULE_INT) ) {
                        alt80=1;
                    }
                    else if ( (LA80_0==RULE_DECIMAL) ) {
                        alt80=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 80, 0, input);

                        throw nvae;
                    }
                    switch (alt80) {
                        case 1 :
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4924:12: this_INT_1= RULE_INT
                            {
                            this_INT_1=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleNumber11589); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              		current.merge(this_INT_1);
                                  
                            }
                            if ( state.backtracking==0 ) {
                               
                                  newLeafNode(this_INT_1, grammarAccess.getNumberAccess().getINTTerminalRuleCall_1_0_0()); 
                                  
                            }

                            }
                            break;
                        case 2 :
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4932:10: this_DECIMAL_2= RULE_DECIMAL
                            {
                            this_DECIMAL_2=(Token)match(input,RULE_DECIMAL,FOLLOW_RULE_DECIMAL_in_ruleNumber11615); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              		current.merge(this_DECIMAL_2);
                                  
                            }
                            if ( state.backtracking==0 ) {
                               
                                  newLeafNode(this_DECIMAL_2, grammarAccess.getNumberAccess().getDECIMALTerminalRuleCall_1_0_1()); 
                                  
                            }

                            }
                            break;

                    }

                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4939:2: (kw= '.' (this_INT_4= RULE_INT | this_DECIMAL_5= RULE_DECIMAL ) )?
                    int alt82=2;
                    int LA82_0 = input.LA(1);

                    if ( (LA82_0==15) ) {
                        int LA82_1 = input.LA(2);

                        if ( ((LA82_1>=RULE_INT && LA82_1<=RULE_DECIMAL)) ) {
                            alt82=1;
                        }
                    }
                    switch (alt82) {
                        case 1 :
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4940:2: kw= '.' (this_INT_4= RULE_INT | this_DECIMAL_5= RULE_DECIMAL )
                            {
                            kw=(Token)match(input,15,FOLLOW_15_in_ruleNumber11635); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      current.merge(kw);
                                      newLeafNode(kw, grammarAccess.getNumberAccess().getFullStopKeyword_1_1_0()); 
                                  
                            }
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4945:1: (this_INT_4= RULE_INT | this_DECIMAL_5= RULE_DECIMAL )
                            int alt81=2;
                            int LA81_0 = input.LA(1);

                            if ( (LA81_0==RULE_INT) ) {
                                alt81=1;
                            }
                            else if ( (LA81_0==RULE_DECIMAL) ) {
                                alt81=2;
                            }
                            else {
                                if (state.backtracking>0) {state.failed=true; return current;}
                                NoViableAltException nvae =
                                    new NoViableAltException("", 81, 0, input);

                                throw nvae;
                            }
                            switch (alt81) {
                                case 1 :
                                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4945:6: this_INT_4= RULE_INT
                                    {
                                    this_INT_4=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleNumber11651); if (state.failed) return current;
                                    if ( state.backtracking==0 ) {

                                      		current.merge(this_INT_4);
                                          
                                    }
                                    if ( state.backtracking==0 ) {
                                       
                                          newLeafNode(this_INT_4, grammarAccess.getNumberAccess().getINTTerminalRuleCall_1_1_1_0()); 
                                          
                                    }

                                    }
                                    break;
                                case 2 :
                                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4953:10: this_DECIMAL_5= RULE_DECIMAL
                                    {
                                    this_DECIMAL_5=(Token)match(input,RULE_DECIMAL,FOLLOW_RULE_DECIMAL_in_ruleNumber11677); if (state.failed) return current;
                                    if ( state.backtracking==0 ) {

                                      		current.merge(this_DECIMAL_5);
                                          
                                    }
                                    if ( state.backtracking==0 ) {
                                       
                                          newLeafNode(this_DECIMAL_5, grammarAccess.getNumberAccess().getDECIMALTerminalRuleCall_1_1_1_1()); 
                                          
                                    }

                                    }
                                    break;

                            }


                            }
                            break;

                    }


                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {

            	myHiddenTokenState.restore();

        }
        return current;
    }
    // $ANTLR end "ruleNumber"


    // $ANTLR start "entryRuleJvmTypeReference"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4971:1: entryRuleJvmTypeReference returns [EObject current=null] : iv_ruleJvmTypeReference= ruleJvmTypeReference EOF ;
    public final EObject entryRuleJvmTypeReference() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleJvmTypeReference = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4972:2: (iv_ruleJvmTypeReference= ruleJvmTypeReference EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4973:2: iv_ruleJvmTypeReference= ruleJvmTypeReference EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getJvmTypeReferenceRule()); 
            }
            pushFollow(FOLLOW_ruleJvmTypeReference_in_entryRuleJvmTypeReference11730);
            iv_ruleJvmTypeReference=ruleJvmTypeReference();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleJvmTypeReference; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleJvmTypeReference11740); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleJvmTypeReference"


    // $ANTLR start "ruleJvmTypeReference"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4980:1: ruleJvmTypeReference returns [EObject current=null] : ( (this_JvmParameterizedTypeReference_0= ruleJvmParameterizedTypeReference ( ( ( () '[' ']' ) )=> ( () otherlv_2= '[' otherlv_3= ']' ) )* ) | this_XFunctionTypeRef_4= ruleXFunctionTypeRef ) ;
    public final EObject ruleJvmTypeReference() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_3=null;
        EObject this_JvmParameterizedTypeReference_0 = null;

        EObject this_XFunctionTypeRef_4 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4983:28: ( ( (this_JvmParameterizedTypeReference_0= ruleJvmParameterizedTypeReference ( ( ( () '[' ']' ) )=> ( () otherlv_2= '[' otherlv_3= ']' ) )* ) | this_XFunctionTypeRef_4= ruleXFunctionTypeRef ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4984:1: ( (this_JvmParameterizedTypeReference_0= ruleJvmParameterizedTypeReference ( ( ( () '[' ']' ) )=> ( () otherlv_2= '[' otherlv_3= ']' ) )* ) | this_XFunctionTypeRef_4= ruleXFunctionTypeRef )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4984:1: ( (this_JvmParameterizedTypeReference_0= ruleJvmParameterizedTypeReference ( ( ( () '[' ']' ) )=> ( () otherlv_2= '[' otherlv_3= ']' ) )* ) | this_XFunctionTypeRef_4= ruleXFunctionTypeRef )
            int alt85=2;
            int LA85_0 = input.LA(1);

            if ( (LA85_0==RULE_ID) ) {
                alt85=1;
            }
            else if ( (LA85_0==33||LA85_0==47) ) {
                alt85=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 85, 0, input);

                throw nvae;
            }
            switch (alt85) {
                case 1 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4984:2: (this_JvmParameterizedTypeReference_0= ruleJvmParameterizedTypeReference ( ( ( () '[' ']' ) )=> ( () otherlv_2= '[' otherlv_3= ']' ) )* )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4984:2: (this_JvmParameterizedTypeReference_0= ruleJvmParameterizedTypeReference ( ( ( () '[' ']' ) )=> ( () otherlv_2= '[' otherlv_3= ']' ) )* )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4985:5: this_JvmParameterizedTypeReference_0= ruleJvmParameterizedTypeReference ( ( ( () '[' ']' ) )=> ( () otherlv_2= '[' otherlv_3= ']' ) )*
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getJvmTypeReferenceAccess().getJvmParameterizedTypeReferenceParserRuleCall_0_0()); 
                          
                    }
                    pushFollow(FOLLOW_ruleJvmParameterizedTypeReference_in_ruleJvmTypeReference11788);
                    this_JvmParameterizedTypeReference_0=ruleJvmParameterizedTypeReference();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_JvmParameterizedTypeReference_0; 
                              afterParserOrEnumRuleCall();
                          
                    }
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4993:1: ( ( ( () '[' ']' ) )=> ( () otherlv_2= '[' otherlv_3= ']' ) )*
                    loop84:
                    do {
                        int alt84=2;
                        int LA84_0 = input.LA(1);

                        if ( (LA84_0==49) ) {
                            int LA84_2 = input.LA(2);

                            if ( (LA84_2==51) ) {
                                int LA84_3 = input.LA(3);

                                if ( (synpred38_InternalDeepClone()) ) {
                                    alt84=1;
                                }


                            }


                        }


                        switch (alt84) {
                    	case 1 :
                    	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4993:2: ( ( () '[' ']' ) )=> ( () otherlv_2= '[' otherlv_3= ']' )
                    	    {
                    	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4996:5: ( () otherlv_2= '[' otherlv_3= ']' )
                    	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4996:6: () otherlv_2= '[' otherlv_3= ']'
                    	    {
                    	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4996:6: ()
                    	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4997:5: 
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	              current = forceCreateModelElementAndSet(
                    	                  grammarAccess.getJvmTypeReferenceAccess().getJvmGenericArrayTypeReferenceComponentTypeAction_0_1_0_0(),
                    	                  current);
                    	          
                    	    }

                    	    }

                    	    otherlv_2=(Token)match(input,49,FOLLOW_49_in_ruleJvmTypeReference11826); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	          	newLeafNode(otherlv_2, grammarAccess.getJvmTypeReferenceAccess().getLeftSquareBracketKeyword_0_1_0_1());
                    	          
                    	    }
                    	    otherlv_3=(Token)match(input,51,FOLLOW_51_in_ruleJvmTypeReference11838); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	          	newLeafNode(otherlv_3, grammarAccess.getJvmTypeReferenceAccess().getRightSquareBracketKeyword_0_1_0_2());
                    	          
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop84;
                        }
                    } while (true);


                    }


                    }
                    break;
                case 2 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5012:5: this_XFunctionTypeRef_4= ruleXFunctionTypeRef
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getJvmTypeReferenceAccess().getXFunctionTypeRefParserRuleCall_1()); 
                          
                    }
                    pushFollow(FOLLOW_ruleXFunctionTypeRef_in_ruleJvmTypeReference11870);
                    this_XFunctionTypeRef_4=ruleXFunctionTypeRef();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_XFunctionTypeRef_4; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleJvmTypeReference"


    // $ANTLR start "entryRuleXFunctionTypeRef"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5028:1: entryRuleXFunctionTypeRef returns [EObject current=null] : iv_ruleXFunctionTypeRef= ruleXFunctionTypeRef EOF ;
    public final EObject entryRuleXFunctionTypeRef() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXFunctionTypeRef = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5029:2: (iv_ruleXFunctionTypeRef= ruleXFunctionTypeRef EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5030:2: iv_ruleXFunctionTypeRef= ruleXFunctionTypeRef EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getXFunctionTypeRefRule()); 
            }
            pushFollow(FOLLOW_ruleXFunctionTypeRef_in_entryRuleXFunctionTypeRef11905);
            iv_ruleXFunctionTypeRef=ruleXFunctionTypeRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleXFunctionTypeRef; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleXFunctionTypeRef11915); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXFunctionTypeRef"


    // $ANTLR start "ruleXFunctionTypeRef"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5037:1: ruleXFunctionTypeRef returns [EObject current=null] : ( (otherlv_0= '(' ( ( (lv_paramTypes_1_0= ruleJvmTypeReference ) ) (otherlv_2= ',' ( (lv_paramTypes_3_0= ruleJvmTypeReference ) ) )* )? otherlv_4= ')' )? otherlv_5= '=>' ( (lv_returnType_6_0= ruleJvmTypeReference ) ) ) ;
    public final EObject ruleXFunctionTypeRef() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        EObject lv_paramTypes_1_0 = null;

        EObject lv_paramTypes_3_0 = null;

        EObject lv_returnType_6_0 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5040:28: ( ( (otherlv_0= '(' ( ( (lv_paramTypes_1_0= ruleJvmTypeReference ) ) (otherlv_2= ',' ( (lv_paramTypes_3_0= ruleJvmTypeReference ) ) )* )? otherlv_4= ')' )? otherlv_5= '=>' ( (lv_returnType_6_0= ruleJvmTypeReference ) ) ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5041:1: ( (otherlv_0= '(' ( ( (lv_paramTypes_1_0= ruleJvmTypeReference ) ) (otherlv_2= ',' ( (lv_paramTypes_3_0= ruleJvmTypeReference ) ) )* )? otherlv_4= ')' )? otherlv_5= '=>' ( (lv_returnType_6_0= ruleJvmTypeReference ) ) )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5041:1: ( (otherlv_0= '(' ( ( (lv_paramTypes_1_0= ruleJvmTypeReference ) ) (otherlv_2= ',' ( (lv_paramTypes_3_0= ruleJvmTypeReference ) ) )* )? otherlv_4= ')' )? otherlv_5= '=>' ( (lv_returnType_6_0= ruleJvmTypeReference ) ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5041:2: (otherlv_0= '(' ( ( (lv_paramTypes_1_0= ruleJvmTypeReference ) ) (otherlv_2= ',' ( (lv_paramTypes_3_0= ruleJvmTypeReference ) ) )* )? otherlv_4= ')' )? otherlv_5= '=>' ( (lv_returnType_6_0= ruleJvmTypeReference ) )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5041:2: (otherlv_0= '(' ( ( (lv_paramTypes_1_0= ruleJvmTypeReference ) ) (otherlv_2= ',' ( (lv_paramTypes_3_0= ruleJvmTypeReference ) ) )* )? otherlv_4= ')' )?
            int alt88=2;
            int LA88_0 = input.LA(1);

            if ( (LA88_0==47) ) {
                alt88=1;
            }
            switch (alt88) {
                case 1 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5041:4: otherlv_0= '(' ( ( (lv_paramTypes_1_0= ruleJvmTypeReference ) ) (otherlv_2= ',' ( (lv_paramTypes_3_0= ruleJvmTypeReference ) ) )* )? otherlv_4= ')'
                    {
                    otherlv_0=(Token)match(input,47,FOLLOW_47_in_ruleXFunctionTypeRef11953); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_0, grammarAccess.getXFunctionTypeRefAccess().getLeftParenthesisKeyword_0_0());
                          
                    }
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5045:1: ( ( (lv_paramTypes_1_0= ruleJvmTypeReference ) ) (otherlv_2= ',' ( (lv_paramTypes_3_0= ruleJvmTypeReference ) ) )* )?
                    int alt87=2;
                    int LA87_0 = input.LA(1);

                    if ( (LA87_0==RULE_ID||LA87_0==33||LA87_0==47) ) {
                        alt87=1;
                    }
                    switch (alt87) {
                        case 1 :
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5045:2: ( (lv_paramTypes_1_0= ruleJvmTypeReference ) ) (otherlv_2= ',' ( (lv_paramTypes_3_0= ruleJvmTypeReference ) ) )*
                            {
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5045:2: ( (lv_paramTypes_1_0= ruleJvmTypeReference ) )
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5046:1: (lv_paramTypes_1_0= ruleJvmTypeReference )
                            {
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5046:1: (lv_paramTypes_1_0= ruleJvmTypeReference )
                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5047:3: lv_paramTypes_1_0= ruleJvmTypeReference
                            {
                            if ( state.backtracking==0 ) {
                               
                              	        newCompositeNode(grammarAccess.getXFunctionTypeRefAccess().getParamTypesJvmTypeReferenceParserRuleCall_0_1_0_0()); 
                              	    
                            }
                            pushFollow(FOLLOW_ruleJvmTypeReference_in_ruleXFunctionTypeRef11975);
                            lv_paramTypes_1_0=ruleJvmTypeReference();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElementForParent(grammarAccess.getXFunctionTypeRefRule());
                              	        }
                                     		add(
                                     			current, 
                                     			"paramTypes",
                                      		lv_paramTypes_1_0, 
                                      		"JvmTypeReference");
                              	        afterParserOrEnumRuleCall();
                              	    
                            }

                            }


                            }

                            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5063:2: (otherlv_2= ',' ( (lv_paramTypes_3_0= ruleJvmTypeReference ) ) )*
                            loop86:
                            do {
                                int alt86=2;
                                int LA86_0 = input.LA(1);

                                if ( (LA86_0==46) ) {
                                    alt86=1;
                                }


                                switch (alt86) {
                            	case 1 :
                            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5063:4: otherlv_2= ',' ( (lv_paramTypes_3_0= ruleJvmTypeReference ) )
                            	    {
                            	    otherlv_2=(Token)match(input,46,FOLLOW_46_in_ruleXFunctionTypeRef11988); if (state.failed) return current;
                            	    if ( state.backtracking==0 ) {

                            	          	newLeafNode(otherlv_2, grammarAccess.getXFunctionTypeRefAccess().getCommaKeyword_0_1_1_0());
                            	          
                            	    }
                            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5067:1: ( (lv_paramTypes_3_0= ruleJvmTypeReference ) )
                            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5068:1: (lv_paramTypes_3_0= ruleJvmTypeReference )
                            	    {
                            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5068:1: (lv_paramTypes_3_0= ruleJvmTypeReference )
                            	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5069:3: lv_paramTypes_3_0= ruleJvmTypeReference
                            	    {
                            	    if ( state.backtracking==0 ) {
                            	       
                            	      	        newCompositeNode(grammarAccess.getXFunctionTypeRefAccess().getParamTypesJvmTypeReferenceParserRuleCall_0_1_1_1_0()); 
                            	      	    
                            	    }
                            	    pushFollow(FOLLOW_ruleJvmTypeReference_in_ruleXFunctionTypeRef12009);
                            	    lv_paramTypes_3_0=ruleJvmTypeReference();

                            	    state._fsp--;
                            	    if (state.failed) return current;
                            	    if ( state.backtracking==0 ) {

                            	      	        if (current==null) {
                            	      	            current = createModelElementForParent(grammarAccess.getXFunctionTypeRefRule());
                            	      	        }
                            	             		add(
                            	             			current, 
                            	             			"paramTypes",
                            	              		lv_paramTypes_3_0, 
                            	              		"JvmTypeReference");
                            	      	        afterParserOrEnumRuleCall();
                            	      	    
                            	    }

                            	    }


                            	    }


                            	    }
                            	    break;

                            	default :
                            	    break loop86;
                                }
                            } while (true);


                            }
                            break;

                    }

                    otherlv_4=(Token)match(input,48,FOLLOW_48_in_ruleXFunctionTypeRef12025); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_4, grammarAccess.getXFunctionTypeRefAccess().getRightParenthesisKeyword_0_2());
                          
                    }

                    }
                    break;

            }

            otherlv_5=(Token)match(input,33,FOLLOW_33_in_ruleXFunctionTypeRef12039); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_5, grammarAccess.getXFunctionTypeRefAccess().getEqualsSignGreaterThanSignKeyword_1());
                  
            }
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5093:1: ( (lv_returnType_6_0= ruleJvmTypeReference ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5094:1: (lv_returnType_6_0= ruleJvmTypeReference )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5094:1: (lv_returnType_6_0= ruleJvmTypeReference )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5095:3: lv_returnType_6_0= ruleJvmTypeReference
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getXFunctionTypeRefAccess().getReturnTypeJvmTypeReferenceParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleJvmTypeReference_in_ruleXFunctionTypeRef12060);
            lv_returnType_6_0=ruleJvmTypeReference();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getXFunctionTypeRefRule());
              	        }
                     		set(
                     			current, 
                     			"returnType",
                      		lv_returnType_6_0, 
                      		"JvmTypeReference");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXFunctionTypeRef"


    // $ANTLR start "entryRuleJvmParameterizedTypeReference"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5119:1: entryRuleJvmParameterizedTypeReference returns [EObject current=null] : iv_ruleJvmParameterizedTypeReference= ruleJvmParameterizedTypeReference EOF ;
    public final EObject entryRuleJvmParameterizedTypeReference() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleJvmParameterizedTypeReference = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5120:2: (iv_ruleJvmParameterizedTypeReference= ruleJvmParameterizedTypeReference EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5121:2: iv_ruleJvmParameterizedTypeReference= ruleJvmParameterizedTypeReference EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getJvmParameterizedTypeReferenceRule()); 
            }
            pushFollow(FOLLOW_ruleJvmParameterizedTypeReference_in_entryRuleJvmParameterizedTypeReference12096);
            iv_ruleJvmParameterizedTypeReference=ruleJvmParameterizedTypeReference();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleJvmParameterizedTypeReference; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleJvmParameterizedTypeReference12106); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleJvmParameterizedTypeReference"


    // $ANTLR start "ruleJvmParameterizedTypeReference"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5128:1: ruleJvmParameterizedTypeReference returns [EObject current=null] : ( ( ( ruleQualifiedName ) ) ( ( ( '<' )=>otherlv_1= '<' ) ( (lv_arguments_2_0= ruleJvmArgumentTypeReference ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleJvmArgumentTypeReference ) ) )* otherlv_5= '>' )? ) ;
    public final EObject ruleJvmParameterizedTypeReference() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_arguments_2_0 = null;

        EObject lv_arguments_4_0 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5131:28: ( ( ( ( ruleQualifiedName ) ) ( ( ( '<' )=>otherlv_1= '<' ) ( (lv_arguments_2_0= ruleJvmArgumentTypeReference ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleJvmArgumentTypeReference ) ) )* otherlv_5= '>' )? ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5132:1: ( ( ( ruleQualifiedName ) ) ( ( ( '<' )=>otherlv_1= '<' ) ( (lv_arguments_2_0= ruleJvmArgumentTypeReference ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleJvmArgumentTypeReference ) ) )* otherlv_5= '>' )? )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5132:1: ( ( ( ruleQualifiedName ) ) ( ( ( '<' )=>otherlv_1= '<' ) ( (lv_arguments_2_0= ruleJvmArgumentTypeReference ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleJvmArgumentTypeReference ) ) )* otherlv_5= '>' )? )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5132:2: ( ( ruleQualifiedName ) ) ( ( ( '<' )=>otherlv_1= '<' ) ( (lv_arguments_2_0= ruleJvmArgumentTypeReference ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleJvmArgumentTypeReference ) ) )* otherlv_5= '>' )?
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5132:2: ( ( ruleQualifiedName ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5133:1: ( ruleQualifiedName )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5133:1: ( ruleQualifiedName )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5134:3: ruleQualifiedName
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getJvmParameterizedTypeReferenceRule());
              	        }
                      
            }
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getJvmParameterizedTypeReferenceAccess().getTypeJvmTypeCrossReference_0_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleQualifiedName_in_ruleJvmParameterizedTypeReference12154);
            ruleQualifiedName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5147:2: ( ( ( '<' )=>otherlv_1= '<' ) ( (lv_arguments_2_0= ruleJvmArgumentTypeReference ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleJvmArgumentTypeReference ) ) )* otherlv_5= '>' )?
            int alt90=2;
            alt90 = dfa90.predict(input);
            switch (alt90) {
                case 1 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5147:3: ( ( '<' )=>otherlv_1= '<' ) ( (lv_arguments_2_0= ruleJvmArgumentTypeReference ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleJvmArgumentTypeReference ) ) )* otherlv_5= '>'
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5147:3: ( ( '<' )=>otherlv_1= '<' )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5147:4: ( '<' )=>otherlv_1= '<'
                    {
                    otherlv_1=(Token)match(input,30,FOLLOW_30_in_ruleJvmParameterizedTypeReference12175); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_1, grammarAccess.getJvmParameterizedTypeReferenceAccess().getLessThanSignKeyword_1_0());
                          
                    }

                    }

                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5152:2: ( (lv_arguments_2_0= ruleJvmArgumentTypeReference ) )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5153:1: (lv_arguments_2_0= ruleJvmArgumentTypeReference )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5153:1: (lv_arguments_2_0= ruleJvmArgumentTypeReference )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5154:3: lv_arguments_2_0= ruleJvmArgumentTypeReference
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getJvmParameterizedTypeReferenceAccess().getArgumentsJvmArgumentTypeReferenceParserRuleCall_1_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleJvmArgumentTypeReference_in_ruleJvmParameterizedTypeReference12197);
                    lv_arguments_2_0=ruleJvmArgumentTypeReference();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getJvmParameterizedTypeReferenceRule());
                      	        }
                             		add(
                             			current, 
                             			"arguments",
                              		lv_arguments_2_0, 
                              		"JvmArgumentTypeReference");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5170:2: (otherlv_3= ',' ( (lv_arguments_4_0= ruleJvmArgumentTypeReference ) ) )*
                    loop89:
                    do {
                        int alt89=2;
                        int LA89_0 = input.LA(1);

                        if ( (LA89_0==46) ) {
                            alt89=1;
                        }


                        switch (alt89) {
                    	case 1 :
                    	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5170:4: otherlv_3= ',' ( (lv_arguments_4_0= ruleJvmArgumentTypeReference ) )
                    	    {
                    	    otherlv_3=(Token)match(input,46,FOLLOW_46_in_ruleJvmParameterizedTypeReference12210); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	          	newLeafNode(otherlv_3, grammarAccess.getJvmParameterizedTypeReferenceAccess().getCommaKeyword_1_2_0());
                    	          
                    	    }
                    	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5174:1: ( (lv_arguments_4_0= ruleJvmArgumentTypeReference ) )
                    	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5175:1: (lv_arguments_4_0= ruleJvmArgumentTypeReference )
                    	    {
                    	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5175:1: (lv_arguments_4_0= ruleJvmArgumentTypeReference )
                    	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5176:3: lv_arguments_4_0= ruleJvmArgumentTypeReference
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getJvmParameterizedTypeReferenceAccess().getArgumentsJvmArgumentTypeReferenceParserRuleCall_1_2_1_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FOLLOW_ruleJvmArgumentTypeReference_in_ruleJvmParameterizedTypeReference12231);
                    	    lv_arguments_4_0=ruleJvmArgumentTypeReference();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getJvmParameterizedTypeReferenceRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"arguments",
                    	              		lv_arguments_4_0, 
                    	              		"JvmArgumentTypeReference");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop89;
                        }
                    } while (true);

                    otherlv_5=(Token)match(input,29,FOLLOW_29_in_ruleJvmParameterizedTypeReference12245); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_5, grammarAccess.getJvmParameterizedTypeReferenceAccess().getGreaterThanSignKeyword_1_3());
                          
                    }

                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleJvmParameterizedTypeReference"


    // $ANTLR start "entryRuleJvmArgumentTypeReference"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5204:1: entryRuleJvmArgumentTypeReference returns [EObject current=null] : iv_ruleJvmArgumentTypeReference= ruleJvmArgumentTypeReference EOF ;
    public final EObject entryRuleJvmArgumentTypeReference() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleJvmArgumentTypeReference = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5205:2: (iv_ruleJvmArgumentTypeReference= ruleJvmArgumentTypeReference EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5206:2: iv_ruleJvmArgumentTypeReference= ruleJvmArgumentTypeReference EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getJvmArgumentTypeReferenceRule()); 
            }
            pushFollow(FOLLOW_ruleJvmArgumentTypeReference_in_entryRuleJvmArgumentTypeReference12283);
            iv_ruleJvmArgumentTypeReference=ruleJvmArgumentTypeReference();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleJvmArgumentTypeReference; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleJvmArgumentTypeReference12293); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleJvmArgumentTypeReference"


    // $ANTLR start "ruleJvmArgumentTypeReference"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5213:1: ruleJvmArgumentTypeReference returns [EObject current=null] : (this_JvmTypeReference_0= ruleJvmTypeReference | this_JvmWildcardTypeReference_1= ruleJvmWildcardTypeReference ) ;
    public final EObject ruleJvmArgumentTypeReference() throws RecognitionException {
        EObject current = null;

        EObject this_JvmTypeReference_0 = null;

        EObject this_JvmWildcardTypeReference_1 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5216:28: ( (this_JvmTypeReference_0= ruleJvmTypeReference | this_JvmWildcardTypeReference_1= ruleJvmWildcardTypeReference ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5217:1: (this_JvmTypeReference_0= ruleJvmTypeReference | this_JvmWildcardTypeReference_1= ruleJvmWildcardTypeReference )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5217:1: (this_JvmTypeReference_0= ruleJvmTypeReference | this_JvmWildcardTypeReference_1= ruleJvmWildcardTypeReference )
            int alt91=2;
            int LA91_0 = input.LA(1);

            if ( (LA91_0==RULE_ID||LA91_0==33||LA91_0==47) ) {
                alt91=1;
            }
            else if ( (LA91_0==76) ) {
                alt91=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 91, 0, input);

                throw nvae;
            }
            switch (alt91) {
                case 1 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5218:5: this_JvmTypeReference_0= ruleJvmTypeReference
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getJvmArgumentTypeReferenceAccess().getJvmTypeReferenceParserRuleCall_0()); 
                          
                    }
                    pushFollow(FOLLOW_ruleJvmTypeReference_in_ruleJvmArgumentTypeReference12340);
                    this_JvmTypeReference_0=ruleJvmTypeReference();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_JvmTypeReference_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5228:5: this_JvmWildcardTypeReference_1= ruleJvmWildcardTypeReference
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getJvmArgumentTypeReferenceAccess().getJvmWildcardTypeReferenceParserRuleCall_1()); 
                          
                    }
                    pushFollow(FOLLOW_ruleJvmWildcardTypeReference_in_ruleJvmArgumentTypeReference12367);
                    this_JvmWildcardTypeReference_1=ruleJvmWildcardTypeReference();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_JvmWildcardTypeReference_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleJvmArgumentTypeReference"


    // $ANTLR start "entryRuleJvmWildcardTypeReference"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5244:1: entryRuleJvmWildcardTypeReference returns [EObject current=null] : iv_ruleJvmWildcardTypeReference= ruleJvmWildcardTypeReference EOF ;
    public final EObject entryRuleJvmWildcardTypeReference() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleJvmWildcardTypeReference = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5245:2: (iv_ruleJvmWildcardTypeReference= ruleJvmWildcardTypeReference EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5246:2: iv_ruleJvmWildcardTypeReference= ruleJvmWildcardTypeReference EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getJvmWildcardTypeReferenceRule()); 
            }
            pushFollow(FOLLOW_ruleJvmWildcardTypeReference_in_entryRuleJvmWildcardTypeReference12402);
            iv_ruleJvmWildcardTypeReference=ruleJvmWildcardTypeReference();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleJvmWildcardTypeReference; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleJvmWildcardTypeReference12412); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleJvmWildcardTypeReference"


    // $ANTLR start "ruleJvmWildcardTypeReference"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5253:1: ruleJvmWildcardTypeReference returns [EObject current=null] : ( () otherlv_1= '?' ( ( (lv_constraints_2_0= ruleJvmUpperBound ) ) | ( (lv_constraints_3_0= ruleJvmLowerBound ) ) )? ) ;
    public final EObject ruleJvmWildcardTypeReference() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_constraints_2_0 = null;

        EObject lv_constraints_3_0 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5256:28: ( ( () otherlv_1= '?' ( ( (lv_constraints_2_0= ruleJvmUpperBound ) ) | ( (lv_constraints_3_0= ruleJvmLowerBound ) ) )? ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5257:1: ( () otherlv_1= '?' ( ( (lv_constraints_2_0= ruleJvmUpperBound ) ) | ( (lv_constraints_3_0= ruleJvmLowerBound ) ) )? )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5257:1: ( () otherlv_1= '?' ( ( (lv_constraints_2_0= ruleJvmUpperBound ) ) | ( (lv_constraints_3_0= ruleJvmLowerBound ) ) )? )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5257:2: () otherlv_1= '?' ( ( (lv_constraints_2_0= ruleJvmUpperBound ) ) | ( (lv_constraints_3_0= ruleJvmLowerBound ) ) )?
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5257:2: ()
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5258:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getJvmWildcardTypeReferenceAccess().getJvmWildcardTypeReferenceAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,76,FOLLOW_76_in_ruleJvmWildcardTypeReference12458); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getJvmWildcardTypeReferenceAccess().getQuestionMarkKeyword_1());
                  
            }
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5267:1: ( ( (lv_constraints_2_0= ruleJvmUpperBound ) ) | ( (lv_constraints_3_0= ruleJvmLowerBound ) ) )?
            int alt92=3;
            int LA92_0 = input.LA(1);

            if ( (LA92_0==77) ) {
                alt92=1;
            }
            else if ( (LA92_0==64) ) {
                alt92=2;
            }
            switch (alt92) {
                case 1 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5267:2: ( (lv_constraints_2_0= ruleJvmUpperBound ) )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5267:2: ( (lv_constraints_2_0= ruleJvmUpperBound ) )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5268:1: (lv_constraints_2_0= ruleJvmUpperBound )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5268:1: (lv_constraints_2_0= ruleJvmUpperBound )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5269:3: lv_constraints_2_0= ruleJvmUpperBound
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getJvmWildcardTypeReferenceAccess().getConstraintsJvmUpperBoundParserRuleCall_2_0_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleJvmUpperBound_in_ruleJvmWildcardTypeReference12480);
                    lv_constraints_2_0=ruleJvmUpperBound();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getJvmWildcardTypeReferenceRule());
                      	        }
                             		add(
                             			current, 
                             			"constraints",
                              		lv_constraints_2_0, 
                              		"JvmUpperBound");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5286:6: ( (lv_constraints_3_0= ruleJvmLowerBound ) )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5286:6: ( (lv_constraints_3_0= ruleJvmLowerBound ) )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5287:1: (lv_constraints_3_0= ruleJvmLowerBound )
                    {
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5287:1: (lv_constraints_3_0= ruleJvmLowerBound )
                    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5288:3: lv_constraints_3_0= ruleJvmLowerBound
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getJvmWildcardTypeReferenceAccess().getConstraintsJvmLowerBoundParserRuleCall_2_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleJvmLowerBound_in_ruleJvmWildcardTypeReference12507);
                    lv_constraints_3_0=ruleJvmLowerBound();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getJvmWildcardTypeReferenceRule());
                      	        }
                             		add(
                             			current, 
                             			"constraints",
                              		lv_constraints_3_0, 
                              		"JvmLowerBound");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleJvmWildcardTypeReference"


    // $ANTLR start "entryRuleJvmUpperBound"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5312:1: entryRuleJvmUpperBound returns [EObject current=null] : iv_ruleJvmUpperBound= ruleJvmUpperBound EOF ;
    public final EObject entryRuleJvmUpperBound() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleJvmUpperBound = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5313:2: (iv_ruleJvmUpperBound= ruleJvmUpperBound EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5314:2: iv_ruleJvmUpperBound= ruleJvmUpperBound EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getJvmUpperBoundRule()); 
            }
            pushFollow(FOLLOW_ruleJvmUpperBound_in_entryRuleJvmUpperBound12545);
            iv_ruleJvmUpperBound=ruleJvmUpperBound();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleJvmUpperBound; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleJvmUpperBound12555); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleJvmUpperBound"


    // $ANTLR start "ruleJvmUpperBound"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5321:1: ruleJvmUpperBound returns [EObject current=null] : (otherlv_0= 'extends' ( (lv_typeReference_1_0= ruleJvmTypeReference ) ) ) ;
    public final EObject ruleJvmUpperBound() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_typeReference_1_0 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5324:28: ( (otherlv_0= 'extends' ( (lv_typeReference_1_0= ruleJvmTypeReference ) ) ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5325:1: (otherlv_0= 'extends' ( (lv_typeReference_1_0= ruleJvmTypeReference ) ) )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5325:1: (otherlv_0= 'extends' ( (lv_typeReference_1_0= ruleJvmTypeReference ) ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5325:3: otherlv_0= 'extends' ( (lv_typeReference_1_0= ruleJvmTypeReference ) )
            {
            otherlv_0=(Token)match(input,77,FOLLOW_77_in_ruleJvmUpperBound12592); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getJvmUpperBoundAccess().getExtendsKeyword_0());
                  
            }
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5329:1: ( (lv_typeReference_1_0= ruleJvmTypeReference ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5330:1: (lv_typeReference_1_0= ruleJvmTypeReference )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5330:1: (lv_typeReference_1_0= ruleJvmTypeReference )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5331:3: lv_typeReference_1_0= ruleJvmTypeReference
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getJvmUpperBoundAccess().getTypeReferenceJvmTypeReferenceParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleJvmTypeReference_in_ruleJvmUpperBound12613);
            lv_typeReference_1_0=ruleJvmTypeReference();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getJvmUpperBoundRule());
              	        }
                     		set(
                     			current, 
                     			"typeReference",
                      		lv_typeReference_1_0, 
                      		"JvmTypeReference");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleJvmUpperBound"


    // $ANTLR start "entryRuleJvmUpperBoundAnded"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5355:1: entryRuleJvmUpperBoundAnded returns [EObject current=null] : iv_ruleJvmUpperBoundAnded= ruleJvmUpperBoundAnded EOF ;
    public final EObject entryRuleJvmUpperBoundAnded() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleJvmUpperBoundAnded = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5356:2: (iv_ruleJvmUpperBoundAnded= ruleJvmUpperBoundAnded EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5357:2: iv_ruleJvmUpperBoundAnded= ruleJvmUpperBoundAnded EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getJvmUpperBoundAndedRule()); 
            }
            pushFollow(FOLLOW_ruleJvmUpperBoundAnded_in_entryRuleJvmUpperBoundAnded12649);
            iv_ruleJvmUpperBoundAnded=ruleJvmUpperBoundAnded();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleJvmUpperBoundAnded; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleJvmUpperBoundAnded12659); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleJvmUpperBoundAnded"


    // $ANTLR start "ruleJvmUpperBoundAnded"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5364:1: ruleJvmUpperBoundAnded returns [EObject current=null] : (otherlv_0= '&' ( (lv_typeReference_1_0= ruleJvmTypeReference ) ) ) ;
    public final EObject ruleJvmUpperBoundAnded() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_typeReference_1_0 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5367:28: ( (otherlv_0= '&' ( (lv_typeReference_1_0= ruleJvmTypeReference ) ) ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5368:1: (otherlv_0= '&' ( (lv_typeReference_1_0= ruleJvmTypeReference ) ) )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5368:1: (otherlv_0= '&' ( (lv_typeReference_1_0= ruleJvmTypeReference ) ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5368:3: otherlv_0= '&' ( (lv_typeReference_1_0= ruleJvmTypeReference ) )
            {
            otherlv_0=(Token)match(input,19,FOLLOW_19_in_ruleJvmUpperBoundAnded12696); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getJvmUpperBoundAndedAccess().getAmpersandKeyword_0());
                  
            }
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5372:1: ( (lv_typeReference_1_0= ruleJvmTypeReference ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5373:1: (lv_typeReference_1_0= ruleJvmTypeReference )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5373:1: (lv_typeReference_1_0= ruleJvmTypeReference )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5374:3: lv_typeReference_1_0= ruleJvmTypeReference
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getJvmUpperBoundAndedAccess().getTypeReferenceJvmTypeReferenceParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleJvmTypeReference_in_ruleJvmUpperBoundAnded12717);
            lv_typeReference_1_0=ruleJvmTypeReference();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getJvmUpperBoundAndedRule());
              	        }
                     		set(
                     			current, 
                     			"typeReference",
                      		lv_typeReference_1_0, 
                      		"JvmTypeReference");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleJvmUpperBoundAnded"


    // $ANTLR start "entryRuleJvmLowerBound"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5398:1: entryRuleJvmLowerBound returns [EObject current=null] : iv_ruleJvmLowerBound= ruleJvmLowerBound EOF ;
    public final EObject entryRuleJvmLowerBound() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleJvmLowerBound = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5399:2: (iv_ruleJvmLowerBound= ruleJvmLowerBound EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5400:2: iv_ruleJvmLowerBound= ruleJvmLowerBound EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getJvmLowerBoundRule()); 
            }
            pushFollow(FOLLOW_ruleJvmLowerBound_in_entryRuleJvmLowerBound12753);
            iv_ruleJvmLowerBound=ruleJvmLowerBound();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleJvmLowerBound; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleJvmLowerBound12763); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleJvmLowerBound"


    // $ANTLR start "ruleJvmLowerBound"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5407:1: ruleJvmLowerBound returns [EObject current=null] : (otherlv_0= 'super' ( (lv_typeReference_1_0= ruleJvmTypeReference ) ) ) ;
    public final EObject ruleJvmLowerBound() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_typeReference_1_0 = null;


         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5410:28: ( (otherlv_0= 'super' ( (lv_typeReference_1_0= ruleJvmTypeReference ) ) ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5411:1: (otherlv_0= 'super' ( (lv_typeReference_1_0= ruleJvmTypeReference ) ) )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5411:1: (otherlv_0= 'super' ( (lv_typeReference_1_0= ruleJvmTypeReference ) ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5411:3: otherlv_0= 'super' ( (lv_typeReference_1_0= ruleJvmTypeReference ) )
            {
            otherlv_0=(Token)match(input,64,FOLLOW_64_in_ruleJvmLowerBound12800); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getJvmLowerBoundAccess().getSuperKeyword_0());
                  
            }
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5415:1: ( (lv_typeReference_1_0= ruleJvmTypeReference ) )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5416:1: (lv_typeReference_1_0= ruleJvmTypeReference )
            {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5416:1: (lv_typeReference_1_0= ruleJvmTypeReference )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5417:3: lv_typeReference_1_0= ruleJvmTypeReference
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getJvmLowerBoundAccess().getTypeReferenceJvmTypeReferenceParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleJvmTypeReference_in_ruleJvmLowerBound12821);
            lv_typeReference_1_0=ruleJvmTypeReference();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getJvmLowerBoundRule());
              	        }
                     		set(
                     			current, 
                     			"typeReference",
                      		lv_typeReference_1_0, 
                      		"JvmTypeReference");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleJvmLowerBound"


    // $ANTLR start "entryRuleValidID"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5443:1: entryRuleValidID returns [String current=null] : iv_ruleValidID= ruleValidID EOF ;
    public final String entryRuleValidID() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleValidID = null;


        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5444:2: (iv_ruleValidID= ruleValidID EOF )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5445:2: iv_ruleValidID= ruleValidID EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getValidIDRule()); 
            }
            pushFollow(FOLLOW_ruleValidID_in_entryRuleValidID12860);
            iv_ruleValidID=ruleValidID();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleValidID.getText(); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleValidID12871); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleValidID"


    // $ANTLR start "ruleValidID"
    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5452:1: ruleValidID returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : this_ID_0= RULE_ID ;
    public final AntlrDatatypeRuleToken ruleValidID() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;

         enterRule(); 
            
        try {
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5455:28: (this_ID_0= RULE_ID )
            // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5456:5: this_ID_0= RULE_ID
            {
            this_ID_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleValidID12910); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		current.merge(this_ID_0);
                  
            }
            if ( state.backtracking==0 ) {
               
                  newLeafNode(this_ID_0, grammarAccess.getValidIDAccess().getIDTerminalRuleCall()); 
                  
            }

            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleValidID"

    // $ANTLR start synpred1_InternalDeepClone
    public final void synpred1_InternalDeepClone_fragment() throws RecognitionException {   
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:823:3: ( ( () ( ( ruleOpMultiAssign ) ) ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:823:4: ( () ( ( ruleOpMultiAssign ) ) )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:823:4: ( () ( ( ruleOpMultiAssign ) ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:823:5: () ( ( ruleOpMultiAssign ) )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:823:5: ()
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:824:1: 
        {
        }

        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:824:2: ( ( ruleOpMultiAssign ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:825:1: ( ruleOpMultiAssign )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:825:1: ( ruleOpMultiAssign )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:826:3: ruleOpMultiAssign
        {
        pushFollow(FOLLOW_ruleOpMultiAssign_in_synpred1_InternalDeepClone1882);
        ruleOpMultiAssign();

        state._fsp--;
        if (state.failed) return ;

        }


        }


        }


        }
    }
    // $ANTLR end synpred1_InternalDeepClone

    // $ANTLR start synpred2_InternalDeepClone
    public final void synpred2_InternalDeepClone_fragment() throws RecognitionException {   
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:951:3: ( ( () ( ( ruleOpOr ) ) ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:951:4: ( () ( ( ruleOpOr ) ) )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:951:4: ( () ( ( ruleOpOr ) ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:951:5: () ( ( ruleOpOr ) )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:951:5: ()
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:952:1: 
        {
        }

        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:952:2: ( ( ruleOpOr ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:953:1: ( ruleOpOr )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:953:1: ( ruleOpOr )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:954:3: ruleOpOr
        {
        pushFollow(FOLLOW_ruleOpOr_in_synpred2_InternalDeepClone2230);
        ruleOpOr();

        state._fsp--;
        if (state.failed) return ;

        }


        }


        }


        }
    }
    // $ANTLR end synpred2_InternalDeepClone

    // $ANTLR start synpred3_InternalDeepClone
    public final void synpred3_InternalDeepClone_fragment() throws RecognitionException {   
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1052:3: ( ( () ( ( ruleOpAnd ) ) ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1052:4: ( () ( ( ruleOpAnd ) ) )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1052:4: ( () ( ( ruleOpAnd ) ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1052:5: () ( ( ruleOpAnd ) )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1052:5: ()
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1053:1: 
        {
        }

        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1053:2: ( ( ruleOpAnd ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1054:1: ( ruleOpAnd )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1054:1: ( ruleOpAnd )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1055:3: ruleOpAnd
        {
        pushFollow(FOLLOW_ruleOpAnd_in_synpred3_InternalDeepClone2489);
        ruleOpAnd();

        state._fsp--;
        if (state.failed) return ;

        }


        }


        }


        }
    }
    // $ANTLR end synpred3_InternalDeepClone

    // $ANTLR start synpred4_InternalDeepClone
    public final void synpred4_InternalDeepClone_fragment() throws RecognitionException {   
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1153:3: ( ( () ( ( ruleOpEquality ) ) ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1153:4: ( () ( ( ruleOpEquality ) ) )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1153:4: ( () ( ( ruleOpEquality ) ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1153:5: () ( ( ruleOpEquality ) )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1153:5: ()
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1154:1: 
        {
        }

        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1154:2: ( ( ruleOpEquality ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1155:1: ( ruleOpEquality )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1155:1: ( ruleOpEquality )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1156:3: ruleOpEquality
        {
        pushFollow(FOLLOW_ruleOpEquality_in_synpred4_InternalDeepClone2748);
        ruleOpEquality();

        state._fsp--;
        if (state.failed) return ;

        }


        }


        }


        }
    }
    // $ANTLR end synpred4_InternalDeepClone

    // $ANTLR start synpred5_InternalDeepClone
    public final void synpred5_InternalDeepClone_fragment() throws RecognitionException {   
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1261:4: ( ( () 'instanceof' ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1261:5: ( () 'instanceof' )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1261:5: ( () 'instanceof' )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1261:6: () 'instanceof'
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1261:6: ()
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1262:1: 
        {
        }

        match(input,26,FOLLOW_26_in_synpred5_InternalDeepClone3024); if (state.failed) return ;

        }


        }
    }
    // $ANTLR end synpred5_InternalDeepClone

    // $ANTLR start synpred6_InternalDeepClone
    public final void synpred6_InternalDeepClone_fragment() throws RecognitionException {   
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1292:8: ( ( () ( ( ruleOpCompare ) ) ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1292:9: ( () ( ( ruleOpCompare ) ) )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1292:9: ( () ( ( ruleOpCompare ) ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1292:10: () ( ( ruleOpCompare ) )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1292:10: ()
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1293:1: 
        {
        }

        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1293:2: ( ( ruleOpCompare ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1294:1: ( ruleOpCompare )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1294:1: ( ruleOpCompare )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1295:3: ruleOpCompare
        {
        pushFollow(FOLLOW_ruleOpCompare_in_synpred6_InternalDeepClone3095);
        ruleOpCompare();

        state._fsp--;
        if (state.failed) return ;

        }


        }


        }


        }
    }
    // $ANTLR end synpred6_InternalDeepClone

    // $ANTLR start synpred7_InternalDeepClone
    public final void synpred7_InternalDeepClone_fragment() throws RecognitionException {   
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1414:3: ( ( () ( ( ruleOpOther ) ) ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1414:4: ( () ( ( ruleOpOther ) ) )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1414:4: ( () ( ( ruleOpOther ) ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1414:5: () ( ( ruleOpOther ) )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1414:5: ()
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1415:1: 
        {
        }

        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1415:2: ( ( ruleOpOther ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1416:1: ( ruleOpOther )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1416:1: ( ruleOpOther )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1417:3: ruleOpOther
        {
        pushFollow(FOLLOW_ruleOpOther_in_synpred7_InternalDeepClone3414);
        ruleOpOther();

        state._fsp--;
        if (state.failed) return ;

        }


        }


        }


        }
    }
    // $ANTLR end synpred7_InternalDeepClone

    // $ANTLR start synpred8_InternalDeepClone
    public final void synpred8_InternalDeepClone_fragment() throws RecognitionException {   
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1506:3: ( ( '>' '>' ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1506:4: ( '>' '>' )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1506:4: ( '>' '>' )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1507:2: '>' '>'
        {
        match(input,29,FOLLOW_29_in_synpred8_InternalDeepClone3630); if (state.failed) return ;
        match(input,29,FOLLOW_29_in_synpred8_InternalDeepClone3635); if (state.failed) return ;

        }


        }
    }
    // $ANTLR end synpred8_InternalDeepClone

    // $ANTLR start synpred9_InternalDeepClone
    public final void synpred9_InternalDeepClone_fragment() throws RecognitionException {   
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1536:3: ( ( '<' '<' ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1536:4: ( '<' '<' )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1536:4: ( '<' '<' )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1537:2: '<' '<'
        {
        match(input,30,FOLLOW_30_in_synpred9_InternalDeepClone3717); if (state.failed) return ;
        match(input,30,FOLLOW_30_in_synpred9_InternalDeepClone3722); if (state.failed) return ;

        }


        }
    }
    // $ANTLR end synpred9_InternalDeepClone

    // $ANTLR start synpred10_InternalDeepClone
    public final void synpred10_InternalDeepClone_fragment() throws RecognitionException {   
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1610:3: ( ( () ( ( ruleOpAdd ) ) ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1610:4: ( () ( ( ruleOpAdd ) ) )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1610:4: ( () ( ( ruleOpAdd ) ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1610:5: () ( ( ruleOpAdd ) )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1610:5: ()
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1611:1: 
        {
        }

        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1611:2: ( ( ruleOpAdd ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1612:1: ( ruleOpAdd )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1612:1: ( ruleOpAdd )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1613:3: ruleOpAdd
        {
        pushFollow(FOLLOW_ruleOpAdd_in_synpred10_InternalDeepClone3944);
        ruleOpAdd();

        state._fsp--;
        if (state.failed) return ;

        }


        }


        }


        }
    }
    // $ANTLR end synpred10_InternalDeepClone

    // $ANTLR start synpred11_InternalDeepClone
    public final void synpred11_InternalDeepClone_fragment() throws RecognitionException {   
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1718:3: ( ( () ( ( ruleOpMulti ) ) ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1718:4: ( () ( ( ruleOpMulti ) ) )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1718:4: ( () ( ( ruleOpMulti ) ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1718:5: () ( ( ruleOpMulti ) )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1718:5: ()
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1719:1: 
        {
        }

        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1719:2: ( ( ruleOpMulti ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1720:1: ( ruleOpMulti )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1720:1: ( ruleOpMulti )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1721:3: ruleOpMulti
        {
        pushFollow(FOLLOW_ruleOpMulti_in_synpred11_InternalDeepClone4224);
        ruleOpMulti();

        state._fsp--;
        if (state.failed) return ;

        }


        }


        }


        }
    }
    // $ANTLR end synpred11_InternalDeepClone

    // $ANTLR start synpred12_InternalDeepClone
    public final void synpred12_InternalDeepClone_fragment() throws RecognitionException {   
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1951:3: ( ( () 'as' ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1951:4: ( () 'as' )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1951:4: ( () 'as' )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1951:5: () 'as'
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1951:5: ()
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:1952:1: 
        {
        }

        match(input,43,FOLLOW_43_in_synpred12_InternalDeepClone4818); if (state.failed) return ;

        }


        }
    }
    // $ANTLR end synpred12_InternalDeepClone

    // $ANTLR start synpred13_InternalDeepClone
    public final void synpred13_InternalDeepClone_fragment() throws RecognitionException {   
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2011:4: ( ( () '.' ( ( ruleValidID ) ) ruleOpSingleAssign ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2011:5: ( () '.' ( ( ruleValidID ) ) ruleOpSingleAssign )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2011:5: ( () '.' ( ( ruleValidID ) ) ruleOpSingleAssign )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2011:6: () '.' ( ( ruleValidID ) ) ruleOpSingleAssign
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2011:6: ()
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2012:1: 
        {
        }

        match(input,15,FOLLOW_15_in_synpred13_InternalDeepClone4972); if (state.failed) return ;
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2013:1: ( ( ruleValidID ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2014:1: ( ruleValidID )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2014:1: ( ruleValidID )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2015:3: ruleValidID
        {
        pushFollow(FOLLOW_ruleValidID_in_synpred13_InternalDeepClone4981);
        ruleValidID();

        state._fsp--;
        if (state.failed) return ;

        }


        }

        pushFollow(FOLLOW_ruleOpSingleAssign_in_synpred13_InternalDeepClone4987);
        ruleOpSingleAssign();

        state._fsp--;
        if (state.failed) return ;

        }


        }
    }
    // $ANTLR end synpred13_InternalDeepClone

    // $ANTLR start synpred14_InternalDeepClone
    public final void synpred14_InternalDeepClone_fragment() throws RecognitionException {   
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2069:8: ( ( () ( '.' | ( ( '?.' ) ) | ( ( '*.' ) ) ) ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2069:9: ( () ( '.' | ( ( '?.' ) ) | ( ( '*.' ) ) ) )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2069:9: ( () ( '.' | ( ( '?.' ) ) | ( ( '*.' ) ) ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2069:10: () ( '.' | ( ( '?.' ) ) | ( ( '*.' ) ) )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2069:10: ()
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2070:1: 
        {
        }

        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2070:2: ( '.' | ( ( '?.' ) ) | ( ( '*.' ) ) )
        int alt93=3;
        switch ( input.LA(1) ) {
        case 15:
            {
            alt93=1;
            }
            break;
        case 44:
            {
            alt93=2;
            }
            break;
        case 45:
            {
            alt93=3;
            }
            break;
        default:
            if (state.backtracking>0) {state.failed=true; return ;}
            NoViableAltException nvae =
                new NoViableAltException("", 93, 0, input);

            throw nvae;
        }

        switch (alt93) {
            case 1 :
                // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2070:4: '.'
                {
                match(input,15,FOLLOW_15_in_synpred14_InternalDeepClone5090); if (state.failed) return ;

                }
                break;
            case 2 :
                // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2072:6: ( ( '?.' ) )
                {
                // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2072:6: ( ( '?.' ) )
                // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2073:1: ( '?.' )
                {
                // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2073:1: ( '?.' )
                // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2074:2: '?.'
                {
                match(input,44,FOLLOW_44_in_synpred14_InternalDeepClone5104); if (state.failed) return ;

                }


                }


                }
                break;
            case 3 :
                // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2079:6: ( ( '*.' ) )
                {
                // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2079:6: ( ( '*.' ) )
                // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2080:1: ( '*.' )
                {
                // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2080:1: ( '*.' )
                // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2081:2: '*.'
                {
                match(input,45,FOLLOW_45_in_synpred14_InternalDeepClone5124); if (state.failed) return ;

                }


                }


                }
                break;

        }


        }


        }
    }
    // $ANTLR end synpred14_InternalDeepClone

    // $ANTLR start synpred15_InternalDeepClone
    public final void synpred15_InternalDeepClone_fragment() throws RecognitionException {   
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2190:4: ( ( '(' ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2191:1: ( '(' )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2191:1: ( '(' )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2192:2: '('
        {
        match(input,47,FOLLOW_47_in_synpred15_InternalDeepClone5351); if (state.failed) return ;

        }


        }
    }
    // $ANTLR end synpred15_InternalDeepClone

    // $ANTLR start synpred16_InternalDeepClone
    public final void synpred16_InternalDeepClone_fragment() throws RecognitionException {   
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2211:4: ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2211:5: ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2211:5: ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2211:6: () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2211:6: ()
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2212:1: 
        {
        }

        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2212:2: ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )?
        int alt95=2;
        int LA95_0 = input.LA(1);

        if ( (LA95_0==RULE_ID||LA95_0==33||LA95_0==47) ) {
            alt95=1;
        }
        switch (alt95) {
            case 1 :
                // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2212:3: ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )*
                {
                // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2212:3: ( ( ruleJvmFormalParameter ) )
                // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2213:1: ( ruleJvmFormalParameter )
                {
                // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2213:1: ( ruleJvmFormalParameter )
                // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2214:1: ruleJvmFormalParameter
                {
                pushFollow(FOLLOW_ruleJvmFormalParameter_in_synpred16_InternalDeepClone5403);
                ruleJvmFormalParameter();

                state._fsp--;
                if (state.failed) return ;

                }


                }

                // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2216:2: ( ',' ( ( ruleJvmFormalParameter ) ) )*
                loop94:
                do {
                    int alt94=2;
                    int LA94_0 = input.LA(1);

                    if ( (LA94_0==46) ) {
                        alt94=1;
                    }


                    switch (alt94) {
                	case 1 :
                	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2216:4: ',' ( ( ruleJvmFormalParameter ) )
                	    {
                	    match(input,46,FOLLOW_46_in_synpred16_InternalDeepClone5410); if (state.failed) return ;
                	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2217:1: ( ( ruleJvmFormalParameter ) )
                	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2218:1: ( ruleJvmFormalParameter )
                	    {
                	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2218:1: ( ruleJvmFormalParameter )
                	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2219:1: ruleJvmFormalParameter
                	    {
                	    pushFollow(FOLLOW_ruleJvmFormalParameter_in_synpred16_InternalDeepClone5417);
                	    ruleJvmFormalParameter();

                	    state._fsp--;
                	    if (state.failed) return ;

                	    }


                	    }


                	    }
                	    break;

                	default :
                	    break loop94;
                    }
                } while (true);


                }
                break;

        }

        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2221:6: ( ( '|' ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2222:1: ( '|' )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2222:1: ( '|' )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2223:2: '|'
        {
        match(input,50,FOLLOW_50_in_synpred16_InternalDeepClone5431); if (state.failed) return ;

        }


        }


        }


        }
    }
    // $ANTLR end synpred16_InternalDeepClone

    // $ANTLR start synpred17_InternalDeepClone
    public final void synpred17_InternalDeepClone_fragment() throws RecognitionException {   
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2290:4: ( ( () '[' ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2290:5: ( () '[' )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2290:5: ( () '[' )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2290:6: () '['
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2290:6: ()
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2291:1: 
        {
        }

        match(input,49,FOLLOW_49_in_synpred17_InternalDeepClone5551); if (state.failed) return ;

        }


        }
    }
    // $ANTLR end synpred17_InternalDeepClone

    // $ANTLR start synpred18_InternalDeepClone
    public final void synpred18_InternalDeepClone_fragment() throws RecognitionException {   
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2481:3: ( ( () '[' ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2481:4: ( () '[' )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2481:4: ( () '[' )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2481:5: () '['
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2481:5: ()
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2482:1: 
        {
        }

        match(input,49,FOLLOW_49_in_synpred18_InternalDeepClone6075); if (state.failed) return ;

        }


        }
    }
    // $ANTLR end synpred18_InternalDeepClone

    // $ANTLR start synpred20_InternalDeepClone
    public final void synpred20_InternalDeepClone_fragment() throws RecognitionException {   
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2575:4: ( ( ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2575:5: ( ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2575:5: ( ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2575:6: ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2575:6: ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )?
        int alt97=2;
        int LA97_0 = input.LA(1);

        if ( (LA97_0==RULE_ID||LA97_0==33||LA97_0==47) ) {
            alt97=1;
        }
        switch (alt97) {
            case 1 :
                // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2575:7: ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )*
                {
                // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2575:7: ( ( ruleJvmFormalParameter ) )
                // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2576:1: ( ruleJvmFormalParameter )
                {
                // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2576:1: ( ruleJvmFormalParameter )
                // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2577:1: ruleJvmFormalParameter
                {
                pushFollow(FOLLOW_ruleJvmFormalParameter_in_synpred20_InternalDeepClone6354);
                ruleJvmFormalParameter();

                state._fsp--;
                if (state.failed) return ;

                }


                }

                // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2579:2: ( ',' ( ( ruleJvmFormalParameter ) ) )*
                loop96:
                do {
                    int alt96=2;
                    int LA96_0 = input.LA(1);

                    if ( (LA96_0==46) ) {
                        alt96=1;
                    }


                    switch (alt96) {
                	case 1 :
                	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2579:4: ',' ( ( ruleJvmFormalParameter ) )
                	    {
                	    match(input,46,FOLLOW_46_in_synpred20_InternalDeepClone6361); if (state.failed) return ;
                	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2580:1: ( ( ruleJvmFormalParameter ) )
                	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2581:1: ( ruleJvmFormalParameter )
                	    {
                	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2581:1: ( ruleJvmFormalParameter )
                	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2582:1: ruleJvmFormalParameter
                	    {
                	    pushFollow(FOLLOW_ruleJvmFormalParameter_in_synpred20_InternalDeepClone6368);
                	    ruleJvmFormalParameter();

                	    state._fsp--;
                	    if (state.failed) return ;

                	    }


                	    }


                	    }
                	    break;

                	default :
                	    break loop96;
                    }
                } while (true);


                }
                break;

        }

        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2584:6: ( ( '|' ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2585:1: ( '|' )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2585:1: ( '|' )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2586:2: '|'
        {
        match(input,50,FOLLOW_50_in_synpred20_InternalDeepClone6382); if (state.failed) return ;

        }


        }


        }


        }
    }
    // $ANTLR end synpred20_InternalDeepClone

    // $ANTLR start synpred22_InternalDeepClone
    public final void synpred22_InternalDeepClone_fragment() throws RecognitionException {   
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2945:4: ( 'else' )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2945:6: 'else'
        {
        match(input,54,FOLLOW_54_in_synpred22_InternalDeepClone7165); if (state.failed) return ;

        }
    }
    // $ANTLR end synpred22_InternalDeepClone

    // $ANTLR start synpred23_InternalDeepClone
    public final void synpred23_InternalDeepClone_fragment() throws RecognitionException {   
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2999:4: ( ( ( ( ruleValidID ) ) ':' ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2999:5: ( ( ( ruleValidID ) ) ':' )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2999:5: ( ( ( ruleValidID ) ) ':' )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2999:6: ( ( ruleValidID ) ) ':'
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:2999:6: ( ( ruleValidID ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3000:1: ( ruleValidID )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3000:1: ( ruleValidID )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3001:1: ruleValidID
        {
        pushFollow(FOLLOW_ruleValidID_in_synpred23_InternalDeepClone7307);
        ruleValidID();

        state._fsp--;
        if (state.failed) return ;

        }


        }

        match(input,56,FOLLOW_56_in_synpred23_InternalDeepClone7313); if (state.failed) return ;

        }


        }
    }
    // $ANTLR end synpred23_InternalDeepClone

    // $ANTLR start synpred24_InternalDeepClone
    public final void synpred24_InternalDeepClone_fragment() throws RecognitionException {   
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3045:8: ( ( '(' ( ( ruleValidID ) ) ':' ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3045:9: ( '(' ( ( ruleValidID ) ) ':' )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3045:9: ( '(' ( ( ruleValidID ) ) ':' )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3045:11: '(' ( ( ruleValidID ) ) ':'
        {
        match(input,47,FOLLOW_47_in_synpred24_InternalDeepClone7389); if (state.failed) return ;
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3046:1: ( ( ruleValidID ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3047:1: ( ruleValidID )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3047:1: ( ruleValidID )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3048:1: ruleValidID
        {
        pushFollow(FOLLOW_ruleValidID_in_synpred24_InternalDeepClone7396);
        ruleValidID();

        state._fsp--;
        if (state.failed) return ;

        }


        }

        match(input,56,FOLLOW_56_in_synpred24_InternalDeepClone7402); if (state.failed) return ;

        }


        }
    }
    // $ANTLR end synpred24_InternalDeepClone

    // $ANTLR start synpred25_InternalDeepClone
    public final void synpred25_InternalDeepClone_fragment() throws RecognitionException {   
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3629:4: ( ( ( ( ruleJvmTypeReference ) ) ( ( ruleValidID ) ) ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3629:5: ( ( ( ruleJvmTypeReference ) ) ( ( ruleValidID ) ) )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3629:5: ( ( ( ruleJvmTypeReference ) ) ( ( ruleValidID ) ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3629:6: ( ( ruleJvmTypeReference ) ) ( ( ruleValidID ) )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3629:6: ( ( ruleJvmTypeReference ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3630:1: ( ruleJvmTypeReference )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3630:1: ( ruleJvmTypeReference )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3631:1: ruleJvmTypeReference
        {
        pushFollow(FOLLOW_ruleJvmTypeReference_in_synpred25_InternalDeepClone8667);
        ruleJvmTypeReference();

        state._fsp--;
        if (state.failed) return ;

        }


        }

        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3633:2: ( ( ruleValidID ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3634:1: ( ruleValidID )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3634:1: ( ruleValidID )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3635:1: ruleValidID
        {
        pushFollow(FOLLOW_ruleValidID_in_synpred25_InternalDeepClone8676);
        ruleValidID();

        state._fsp--;
        if (state.failed) return ;

        }


        }


        }


        }
    }
    // $ANTLR end synpred25_InternalDeepClone

    // $ANTLR start synpred26_InternalDeepClone
    public final void synpred26_InternalDeepClone_fragment() throws RecognitionException {   
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3933:4: ( ( '(' ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3934:1: ( '(' )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3934:1: ( '(' )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3935:2: '('
        {
        match(input,47,FOLLOW_47_in_synpred26_InternalDeepClone9238); if (state.failed) return ;

        }


        }
    }
    // $ANTLR end synpred26_InternalDeepClone

    // $ANTLR start synpred27_InternalDeepClone
    public final void synpred27_InternalDeepClone_fragment() throws RecognitionException {   
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3954:4: ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3954:5: ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3954:5: ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3954:6: () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3954:6: ()
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3955:1: 
        {
        }

        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3955:2: ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )?
        int alt101=2;
        int LA101_0 = input.LA(1);

        if ( (LA101_0==RULE_ID||LA101_0==33||LA101_0==47) ) {
            alt101=1;
        }
        switch (alt101) {
            case 1 :
                // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3955:3: ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )*
                {
                // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3955:3: ( ( ruleJvmFormalParameter ) )
                // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3956:1: ( ruleJvmFormalParameter )
                {
                // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3956:1: ( ruleJvmFormalParameter )
                // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3957:1: ruleJvmFormalParameter
                {
                pushFollow(FOLLOW_ruleJvmFormalParameter_in_synpred27_InternalDeepClone9290);
                ruleJvmFormalParameter();

                state._fsp--;
                if (state.failed) return ;

                }


                }

                // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3959:2: ( ',' ( ( ruleJvmFormalParameter ) ) )*
                loop100:
                do {
                    int alt100=2;
                    int LA100_0 = input.LA(1);

                    if ( (LA100_0==46) ) {
                        alt100=1;
                    }


                    switch (alt100) {
                	case 1 :
                	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3959:4: ',' ( ( ruleJvmFormalParameter ) )
                	    {
                	    match(input,46,FOLLOW_46_in_synpred27_InternalDeepClone9297); if (state.failed) return ;
                	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3960:1: ( ( ruleJvmFormalParameter ) )
                	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3961:1: ( ruleJvmFormalParameter )
                	    {
                	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3961:1: ( ruleJvmFormalParameter )
                	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3962:1: ruleJvmFormalParameter
                	    {
                	    pushFollow(FOLLOW_ruleJvmFormalParameter_in_synpred27_InternalDeepClone9304);
                	    ruleJvmFormalParameter();

                	    state._fsp--;
                	    if (state.failed) return ;

                	    }


                	    }


                	    }
                	    break;

                	default :
                	    break loop100;
                    }
                } while (true);


                }
                break;

        }

        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3964:6: ( ( '|' ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3965:1: ( '|' )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3965:1: ( '|' )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:3966:2: '|'
        {
        match(input,50,FOLLOW_50_in_synpred27_InternalDeepClone9318); if (state.failed) return ;

        }


        }


        }


        }
    }
    // $ANTLR end synpred27_InternalDeepClone

    // $ANTLR start synpred28_InternalDeepClone
    public final void synpred28_InternalDeepClone_fragment() throws RecognitionException {   
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4033:4: ( ( () '[' ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4033:5: ( () '[' )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4033:5: ( () '[' )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4033:6: () '['
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4033:6: ()
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4034:1: 
        {
        }

        match(input,49,FOLLOW_49_in_synpred28_InternalDeepClone9438); if (state.failed) return ;

        }


        }
    }
    // $ANTLR end synpred28_InternalDeepClone

    // $ANTLR start synpred29_InternalDeepClone
    public final void synpred29_InternalDeepClone_fragment() throws RecognitionException {   
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4176:4: ( '<' )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4176:6: '<'
        {
        match(input,30,FOLLOW_30_in_synpred29_InternalDeepClone9825); if (state.failed) return ;

        }
    }
    // $ANTLR end synpred29_InternalDeepClone

    // $ANTLR start synpred30_InternalDeepClone
    public final void synpred30_InternalDeepClone_fragment() throws RecognitionException {   
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4225:5: ( '(' )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4225:7: '('
        {
        match(input,47,FOLLOW_47_in_synpred30_InternalDeepClone9918); if (state.failed) return ;

        }
    }
    // $ANTLR end synpred30_InternalDeepClone

    // $ANTLR start synpred31_InternalDeepClone
    public final void synpred31_InternalDeepClone_fragment() throws RecognitionException {   
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4230:4: ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4230:5: ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4230:5: ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4230:6: () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4230:6: ()
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4231:1: 
        {
        }

        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4231:2: ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )?
        int alt103=2;
        int LA103_0 = input.LA(1);

        if ( (LA103_0==RULE_ID||LA103_0==33||LA103_0==47) ) {
            alt103=1;
        }
        switch (alt103) {
            case 1 :
                // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4231:3: ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )*
                {
                // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4231:3: ( ( ruleJvmFormalParameter ) )
                // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4232:1: ( ruleJvmFormalParameter )
                {
                // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4232:1: ( ruleJvmFormalParameter )
                // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4233:1: ruleJvmFormalParameter
                {
                pushFollow(FOLLOW_ruleJvmFormalParameter_in_synpred31_InternalDeepClone9948);
                ruleJvmFormalParameter();

                state._fsp--;
                if (state.failed) return ;

                }


                }

                // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4235:2: ( ',' ( ( ruleJvmFormalParameter ) ) )*
                loop102:
                do {
                    int alt102=2;
                    int LA102_0 = input.LA(1);

                    if ( (LA102_0==46) ) {
                        alt102=1;
                    }


                    switch (alt102) {
                	case 1 :
                	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4235:4: ',' ( ( ruleJvmFormalParameter ) )
                	    {
                	    match(input,46,FOLLOW_46_in_synpred31_InternalDeepClone9955); if (state.failed) return ;
                	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4236:1: ( ( ruleJvmFormalParameter ) )
                	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4237:1: ( ruleJvmFormalParameter )
                	    {
                	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4237:1: ( ruleJvmFormalParameter )
                	    // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4238:1: ruleJvmFormalParameter
                	    {
                	    pushFollow(FOLLOW_ruleJvmFormalParameter_in_synpred31_InternalDeepClone9962);
                	    ruleJvmFormalParameter();

                	    state._fsp--;
                	    if (state.failed) return ;

                	    }


                	    }


                	    }
                	    break;

                	default :
                	    break loop102;
                    }
                } while (true);


                }
                break;

        }

        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4240:6: ( ( '|' ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4241:1: ( '|' )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4241:1: ( '|' )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4242:2: '|'
        {
        match(input,50,FOLLOW_50_in_synpred31_InternalDeepClone9976); if (state.failed) return ;

        }


        }


        }


        }
    }
    // $ANTLR end synpred31_InternalDeepClone

    // $ANTLR start synpred32_InternalDeepClone
    public final void synpred32_InternalDeepClone_fragment() throws RecognitionException {   
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4309:4: ( ( () '[' ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4309:5: ( () '[' )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4309:5: ( () '[' )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4309:6: () '['
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4309:6: ()
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4310:1: 
        {
        }

        match(input,49,FOLLOW_49_in_synpred32_InternalDeepClone10096); if (state.failed) return ;

        }


        }
    }
    // $ANTLR end synpred32_InternalDeepClone

    // $ANTLR start synpred33_InternalDeepClone
    public final void synpred33_InternalDeepClone_fragment() throws RecognitionException {   
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4631:2: ( ( ruleXExpression ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4632:1: ( ruleXExpression )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4632:1: ( ruleXExpression )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4633:1: ruleXExpression
        {
        pushFollow(FOLLOW_ruleXExpression_in_synpred33_InternalDeepClone10898);
        ruleXExpression();

        state._fsp--;
        if (state.failed) return ;

        }


        }
    }
    // $ANTLR end synpred33_InternalDeepClone

    // $ANTLR start synpred34_InternalDeepClone
    public final void synpred34_InternalDeepClone_fragment() throws RecognitionException {   
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4702:5: ( 'catch' )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4702:7: 'catch'
        {
        match(input,75,FOLLOW_75_in_synpred34_InternalDeepClone11043); if (state.failed) return ;

        }
    }
    // $ANTLR end synpred34_InternalDeepClone

    // $ANTLR start synpred35_InternalDeepClone
    public final void synpred35_InternalDeepClone_fragment() throws RecognitionException {   
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4721:5: ( 'finally' )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4721:7: 'finally'
        {
        match(input,74,FOLLOW_74_in_synpred35_InternalDeepClone11073); if (state.failed) return ;

        }
    }
    // $ANTLR end synpred35_InternalDeepClone

    // $ANTLR start synpred37_InternalDeepClone
    public final void synpred37_InternalDeepClone_fragment() throws RecognitionException {   
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4869:3: ( '.' )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4870:2: '.'
        {
        match(input,15,FOLLOW_15_in_synpred37_InternalDeepClone11420); if (state.failed) return ;

        }
    }
    // $ANTLR end synpred37_InternalDeepClone

    // $ANTLR start synpred38_InternalDeepClone
    public final void synpred38_InternalDeepClone_fragment() throws RecognitionException {   
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4993:2: ( ( () '[' ']' ) )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4993:3: ( () '[' ']' )
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4993:3: ( () '[' ']' )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4993:4: () '[' ']'
        {
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4993:4: ()
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:4994:1: 
        {
        }

        match(input,49,FOLLOW_49_in_synpred38_InternalDeepClone11803); if (state.failed) return ;
        match(input,51,FOLLOW_51_in_synpred38_InternalDeepClone11807); if (state.failed) return ;

        }


        }
    }
    // $ANTLR end synpred38_InternalDeepClone

    // $ANTLR start synpred39_InternalDeepClone
    public final void synpred39_InternalDeepClone_fragment() throws RecognitionException {   
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5147:4: ( '<' )
        // ../my.home.dsl.DeepClone/src-gen/my/home/dsl/parser/antlr/internal/InternalDeepClone.g:5147:6: '<'
        {
        match(input,30,FOLLOW_30_in_synpred39_InternalDeepClone12167); if (state.failed) return ;

        }
    }
    // $ANTLR end synpred39_InternalDeepClone

    // Delegated rules

    public final boolean synpred37_InternalDeepClone() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred37_InternalDeepClone_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred2_InternalDeepClone() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred2_InternalDeepClone_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred14_InternalDeepClone() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred14_InternalDeepClone_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred12_InternalDeepClone() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred12_InternalDeepClone_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred29_InternalDeepClone() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred29_InternalDeepClone_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred38_InternalDeepClone() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred38_InternalDeepClone_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred26_InternalDeepClone() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred26_InternalDeepClone_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred15_InternalDeepClone() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred15_InternalDeepClone_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred32_InternalDeepClone() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred32_InternalDeepClone_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred7_InternalDeepClone() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred7_InternalDeepClone_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred9_InternalDeepClone() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred9_InternalDeepClone_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred20_InternalDeepClone() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred20_InternalDeepClone_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred27_InternalDeepClone() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred27_InternalDeepClone_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred31_InternalDeepClone() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred31_InternalDeepClone_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred22_InternalDeepClone() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred22_InternalDeepClone_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred35_InternalDeepClone() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred35_InternalDeepClone_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred24_InternalDeepClone() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred24_InternalDeepClone_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred23_InternalDeepClone() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred23_InternalDeepClone_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred16_InternalDeepClone() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred16_InternalDeepClone_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred4_InternalDeepClone() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred4_InternalDeepClone_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred13_InternalDeepClone() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred13_InternalDeepClone_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred3_InternalDeepClone() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred3_InternalDeepClone_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred10_InternalDeepClone() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred10_InternalDeepClone_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred25_InternalDeepClone() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred25_InternalDeepClone_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred8_InternalDeepClone() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred8_InternalDeepClone_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred30_InternalDeepClone() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred30_InternalDeepClone_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred17_InternalDeepClone() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred17_InternalDeepClone_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred6_InternalDeepClone() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred6_InternalDeepClone_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred1_InternalDeepClone() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred1_InternalDeepClone_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred33_InternalDeepClone() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred33_InternalDeepClone_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred5_InternalDeepClone() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred5_InternalDeepClone_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred34_InternalDeepClone() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred34_InternalDeepClone_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred18_InternalDeepClone() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred18_InternalDeepClone_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred11_InternalDeepClone() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred11_InternalDeepClone_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred39_InternalDeepClone() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred39_InternalDeepClone_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred28_InternalDeepClone() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred28_InternalDeepClone_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }


    protected DFA17 dfa17 = new DFA17(this);
    protected DFA33 dfa33 = new DFA33(this);
    protected DFA32 dfa32 = new DFA32(this);
    protected DFA34 dfa34 = new DFA34(this);
    protected DFA36 dfa36 = new DFA36(this);
    protected DFA40 dfa40 = new DFA40(this);
    protected DFA64 dfa64 = new DFA64(this);
    protected DFA63 dfa63 = new DFA63(this);
    protected DFA65 dfa65 = new DFA65(this);
    protected DFA69 dfa69 = new DFA69(this);
    protected DFA72 dfa72 = new DFA72(this);
    protected DFA71 dfa71 = new DFA71(this);
    protected DFA73 dfa73 = new DFA73(this);
    protected DFA75 dfa75 = new DFA75(this);
    protected DFA90 dfa90 = new DFA90(this);
    static final String DFA17_eotS =
        "\13\uffff";
    static final String DFA17_eofS =
        "\1\1\12\uffff";
    static final String DFA17_minS =
        "\1\4\1\uffff\10\0\1\uffff";
    static final String DFA17_maxS =
        "\1\113\1\uffff\10\0\1\uffff";
    static final String DFA17_acceptS =
        "\1\uffff\1\2\10\uffff\1\1";
    static final String DFA17_specialS =
        "\2\uffff\1\3\1\2\1\4\1\5\1\7\1\6\1\0\1\1\1\uffff}>";
    static final String[] DFA17_transitionS = {
            "\5\1\6\uffff\4\1\2\uffff\10\1\1\2\1\3\1\4\1\5\1\6\1\7\1\10"+
            "\1\11\15\1\1\uffff\16\1\1\uffff\12\1",
            "",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            ""
    };

    static final short[] DFA17_eot = DFA.unpackEncodedString(DFA17_eotS);
    static final short[] DFA17_eof = DFA.unpackEncodedString(DFA17_eofS);
    static final char[] DFA17_min = DFA.unpackEncodedStringToUnsignedChars(DFA17_minS);
    static final char[] DFA17_max = DFA.unpackEncodedStringToUnsignedChars(DFA17_maxS);
    static final short[] DFA17_accept = DFA.unpackEncodedString(DFA17_acceptS);
    static final short[] DFA17_special = DFA.unpackEncodedString(DFA17_specialS);
    static final short[][] DFA17_transition;

    static {
        int numStates = DFA17_transitionS.length;
        DFA17_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA17_transition[i] = DFA.unpackEncodedString(DFA17_transitionS[i]);
        }
    }

    class DFA17 extends DFA {

        public DFA17(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 17;
            this.eot = DFA17_eot;
            this.eof = DFA17_eof;
            this.min = DFA17_min;
            this.max = DFA17_max;
            this.accept = DFA17_accept;
            this.special = DFA17_special;
            this.transition = DFA17_transition;
        }
        public String getDescription() {
            return "()* loopback of 1414:1: ( ( ( ( () ( ( ruleOpOther ) ) ) )=> ( () ( ( ruleOpOther ) ) ) ) ( (lv_rightOperand_3_0= ruleXAdditiveExpression ) ) )*";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA17_8 = input.LA(1);

                         
                        int index17_8 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred7_InternalDeepClone()) ) {s = 10;}

                        else if ( (true) ) {s = 1;}

                         
                        input.seek(index17_8);
                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA17_9 = input.LA(1);

                         
                        int index17_9 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred7_InternalDeepClone()) ) {s = 10;}

                        else if ( (true) ) {s = 1;}

                         
                        input.seek(index17_9);
                        if ( s>=0 ) return s;
                        break;
                    case 2 : 
                        int LA17_3 = input.LA(1);

                         
                        int index17_3 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred7_InternalDeepClone()) ) {s = 10;}

                        else if ( (true) ) {s = 1;}

                         
                        input.seek(index17_3);
                        if ( s>=0 ) return s;
                        break;
                    case 3 : 
                        int LA17_2 = input.LA(1);

                         
                        int index17_2 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred7_InternalDeepClone()) ) {s = 10;}

                        else if ( (true) ) {s = 1;}

                         
                        input.seek(index17_2);
                        if ( s>=0 ) return s;
                        break;
                    case 4 : 
                        int LA17_4 = input.LA(1);

                         
                        int index17_4 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred7_InternalDeepClone()) ) {s = 10;}

                        else if ( (true) ) {s = 1;}

                         
                        input.seek(index17_4);
                        if ( s>=0 ) return s;
                        break;
                    case 5 : 
                        int LA17_5 = input.LA(1);

                         
                        int index17_5 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred7_InternalDeepClone()) ) {s = 10;}

                        else if ( (true) ) {s = 1;}

                         
                        input.seek(index17_5);
                        if ( s>=0 ) return s;
                        break;
                    case 6 : 
                        int LA17_7 = input.LA(1);

                         
                        int index17_7 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred7_InternalDeepClone()) ) {s = 10;}

                        else if ( (true) ) {s = 1;}

                         
                        input.seek(index17_7);
                        if ( s>=0 ) return s;
                        break;
                    case 7 : 
                        int LA17_6 = input.LA(1);

                         
                        int index17_6 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred7_InternalDeepClone()) ) {s = 10;}

                        else if ( (true) ) {s = 1;}

                         
                        input.seek(index17_6);
                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 17, _s, input);
            error(nvae);
            throw nvae;
        }
    }
    static final String DFA33_eotS =
        "\101\uffff";
    static final String DFA33_eofS =
        "\1\2\100\uffff";
    static final String DFA33_minS =
        "\1\4\1\0\77\uffff";
    static final String DFA33_maxS =
        "\1\113\1\0\77\uffff";
    static final String DFA33_acceptS =
        "\2\uffff\1\2\75\uffff\1\1";
    static final String DFA33_specialS =
        "\1\uffff\1\0\77\uffff}>";
    static final String[] DFA33_transitionS = {
            "\5\2\6\uffff\4\2\2\uffff\32\2\1\1\2\2\1\uffff\16\2\1\uffff"+
            "\12\2",
            "\1\uffff",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA33_eot = DFA.unpackEncodedString(DFA33_eotS);
    static final short[] DFA33_eof = DFA.unpackEncodedString(DFA33_eofS);
    static final char[] DFA33_min = DFA.unpackEncodedStringToUnsignedChars(DFA33_minS);
    static final char[] DFA33_max = DFA.unpackEncodedStringToUnsignedChars(DFA33_maxS);
    static final short[] DFA33_accept = DFA.unpackEncodedString(DFA33_acceptS);
    static final short[] DFA33_special = DFA.unpackEncodedString(DFA33_specialS);
    static final short[][] DFA33_transition;

    static {
        int numStates = DFA33_transitionS.length;
        DFA33_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA33_transition[i] = DFA.unpackEncodedString(DFA33_transitionS[i]);
        }
    }

    class DFA33 extends DFA {

        public DFA33(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 33;
            this.eot = DFA33_eot;
            this.eof = DFA33_eof;
            this.min = DFA33_min;
            this.max = DFA33_max;
            this.accept = DFA33_accept;
            this.special = DFA33_special;
            this.transition = DFA33_transition;
        }
        public String getDescription() {
            return "2190:2: ( ( ( ( '(' ) )=> (lv_explicitOperationCall_16_0= '(' ) ) ( ( ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> (lv_memberCallArguments_17_0= ruleXShortClosure ) ) | ( ( (lv_memberCallArguments_18_0= ruleXExpression ) ) (otherlv_19= ',' ( (lv_memberCallArguments_20_0= ruleXExpression ) ) )* ) )? otherlv_21= ')' )?";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA33_1 = input.LA(1);

                         
                        int index33_1 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred15_InternalDeepClone()) ) {s = 64;}

                        else if ( (true) ) {s = 2;}

                         
                        input.seek(index33_1);
                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 33, _s, input);
            error(nvae);
            throw nvae;
        }
    }
    static final String DFA32_eotS =
        "\36\uffff";
    static final String DFA32_eofS =
        "\36\uffff";
    static final String DFA32_minS =
        "\1\4\2\0\33\uffff";
    static final String DFA32_maxS =
        "\1\111\2\0\33\uffff";
    static final String DFA32_acceptS =
        "\3\uffff\2\1\1\2\27\uffff\1\3";
    static final String DFA32_specialS =
        "\1\0\1\1\1\2\33\uffff}>";
    static final String[] DFA32_transitionS = {
            "\1\1\4\5\7\uffff\1\5\1\uffff\1\5\13\uffff\1\5\2\uffff\1\3\3"+
            "\uffff\1\5\4\uffff\1\5\4\uffff\1\2\1\35\1\5\1\4\2\uffff\1\5"+
            "\1\uffff\1\5\3\uffff\3\5\2\uffff\1\5\1\uffff\10\5",
            "\1\uffff",
            "\1\uffff",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA32_eot = DFA.unpackEncodedString(DFA32_eotS);
    static final short[] DFA32_eof = DFA.unpackEncodedString(DFA32_eofS);
    static final char[] DFA32_min = DFA.unpackEncodedStringToUnsignedChars(DFA32_minS);
    static final char[] DFA32_max = DFA.unpackEncodedStringToUnsignedChars(DFA32_maxS);
    static final short[] DFA32_accept = DFA.unpackEncodedString(DFA32_acceptS);
    static final short[] DFA32_special = DFA.unpackEncodedString(DFA32_specialS);
    static final short[][] DFA32_transition;

    static {
        int numStates = DFA32_transitionS.length;
        DFA32_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA32_transition[i] = DFA.unpackEncodedString(DFA32_transitionS[i]);
        }
    }

    class DFA32 extends DFA {

        public DFA32(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 32;
            this.eot = DFA32_eot;
            this.eof = DFA32_eof;
            this.min = DFA32_min;
            this.max = DFA32_max;
            this.accept = DFA32_accept;
            this.special = DFA32_special;
            this.transition = DFA32_transition;
        }
        public String getDescription() {
            return "2211:2: ( ( ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> (lv_memberCallArguments_17_0= ruleXShortClosure ) ) | ( ( (lv_memberCallArguments_18_0= ruleXExpression ) ) (otherlv_19= ',' ( (lv_memberCallArguments_20_0= ruleXExpression ) ) )* ) )?";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA32_0 = input.LA(1);

                         
                        int index32_0 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (LA32_0==RULE_ID) ) {s = 1;}

                        else if ( (LA32_0==47) ) {s = 2;}

                        else if ( (LA32_0==33) && (synpred16_InternalDeepClone())) {s = 3;}

                        else if ( (LA32_0==50) && (synpred16_InternalDeepClone())) {s = 4;}

                        else if ( ((LA32_0>=RULE_STRING && LA32_0<=RULE_DECIMAL)||LA32_0==16||LA32_0==18||LA32_0==30||LA32_0==37||LA32_0==42||LA32_0==49||LA32_0==53||LA32_0==55||(LA32_0>=59 && LA32_0<=61)||LA32_0==64||(LA32_0>=66 && LA32_0<=73)) ) {s = 5;}

                        else if ( (LA32_0==48) ) {s = 29;}

                         
                        input.seek(index32_0);
                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA32_1 = input.LA(1);

                         
                        int index32_1 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred16_InternalDeepClone()) ) {s = 4;}

                        else if ( (true) ) {s = 5;}

                         
                        input.seek(index32_1);
                        if ( s>=0 ) return s;
                        break;
                    case 2 : 
                        int LA32_2 = input.LA(1);

                         
                        int index32_2 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred16_InternalDeepClone()) ) {s = 4;}

                        else if ( (true) ) {s = 5;}

                         
                        input.seek(index32_2);
                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 32, _s, input);
            error(nvae);
            throw nvae;
        }
    }
    static final String DFA34_eotS =
        "\101\uffff";
    static final String DFA34_eofS =
        "\1\2\100\uffff";
    static final String DFA34_minS =
        "\1\4\1\0\77\uffff";
    static final String DFA34_maxS =
        "\1\113\1\0\77\uffff";
    static final String DFA34_acceptS =
        "\2\uffff\1\2\75\uffff\1\1";
    static final String DFA34_specialS =
        "\1\uffff\1\0\77\uffff}>";
    static final String[] DFA34_transitionS = {
            "\5\2\6\uffff\4\2\2\uffff\34\2\1\1\1\uffff\16\2\1\uffff\12\2",
            "\1\uffff",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA34_eot = DFA.unpackEncodedString(DFA34_eotS);
    static final short[] DFA34_eof = DFA.unpackEncodedString(DFA34_eofS);
    static final char[] DFA34_min = DFA.unpackEncodedStringToUnsignedChars(DFA34_minS);
    static final char[] DFA34_max = DFA.unpackEncodedStringToUnsignedChars(DFA34_maxS);
    static final short[] DFA34_accept = DFA.unpackEncodedString(DFA34_acceptS);
    static final short[] DFA34_special = DFA.unpackEncodedString(DFA34_specialS);
    static final short[][] DFA34_transition;

    static {
        int numStates = DFA34_transitionS.length;
        DFA34_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA34_transition[i] = DFA.unpackEncodedString(DFA34_transitionS[i]);
        }
    }

    class DFA34 extends DFA {

        public DFA34(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 34;
            this.eot = DFA34_eot;
            this.eof = DFA34_eof;
            this.min = DFA34_min;
            this.max = DFA34_max;
            this.accept = DFA34_accept;
            this.special = DFA34_special;
            this.transition = DFA34_transition;
        }
        public String getDescription() {
            return "2290:3: ( ( ( () '[' ) )=> (lv_memberCallArguments_22_0= ruleXClosure ) )?";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA34_1 = input.LA(1);

                         
                        int index34_1 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred17_InternalDeepClone()) ) {s = 64;}

                        else if ( (true) ) {s = 2;}

                         
                        input.seek(index34_1);
                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 34, _s, input);
            error(nvae);
            throw nvae;
        }
    }
    static final String DFA36_eotS =
        "\16\uffff";
    static final String DFA36_eofS =
        "\16\uffff";
    static final String DFA36_minS =
        "\1\4\15\uffff";
    static final String DFA36_maxS =
        "\1\111\15\uffff";
    static final String DFA36_acceptS =
        "\1\uffff\1\1\1\2\1\3\1\4\1\5\1\6\1\7\1\10\1\11\1\12\1\13\1\14\1"+
        "\15";
    static final String DFA36_specialS =
        "\16\uffff}>";
    static final String[] DFA36_transitionS = {
            "\1\4\4\5\7\uffff\1\2\15\uffff\1\4\20\uffff\1\15\1\uffff\1\5"+
            "\3\uffff\1\6\1\uffff\1\3\3\uffff\1\7\1\10\1\11\2\uffff\1\4\1"+
            "\uffff\1\1\4\5\1\12\1\13\1\14",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA36_eot = DFA.unpackEncodedString(DFA36_eotS);
    static final short[] DFA36_eof = DFA.unpackEncodedString(DFA36_eofS);
    static final char[] DFA36_min = DFA.unpackEncodedStringToUnsignedChars(DFA36_minS);
    static final char[] DFA36_max = DFA.unpackEncodedStringToUnsignedChars(DFA36_maxS);
    static final short[] DFA36_accept = DFA.unpackEncodedString(DFA36_acceptS);
    static final short[] DFA36_special = DFA.unpackEncodedString(DFA36_specialS);
    static final short[][] DFA36_transition;

    static {
        int numStates = DFA36_transitionS.length;
        DFA36_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA36_transition[i] = DFA.unpackEncodedString(DFA36_transitionS[i]);
        }
    }

    class DFA36 extends DFA {

        public DFA36(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 36;
            this.eot = DFA36_eot;
            this.eof = DFA36_eof;
            this.min = DFA36_min;
            this.max = DFA36_max;
            this.accept = DFA36_accept;
            this.special = DFA36_special;
            this.transition = DFA36_transition;
        }
        public String getDescription() {
            return "2331:1: (this_XConstructorCall_0= ruleXConstructorCall | this_XBlockExpression_1= ruleXBlockExpression | this_XSwitchExpression_2= ruleXSwitchExpression | this_XFeatureCall_3= ruleXFeatureCall | this_XLiteral_4= ruleXLiteral | this_XIfExpression_5= ruleXIfExpression | this_XForLoopExpression_6= ruleXForLoopExpression | this_XWhileExpression_7= ruleXWhileExpression | this_XDoWhileExpression_8= ruleXDoWhileExpression | this_XThrowExpression_9= ruleXThrowExpression | this_XReturnExpression_10= ruleXReturnExpression | this_XTryCatchFinallyExpression_11= ruleXTryCatchFinallyExpression | this_XParenthesizedExpression_12= ruleXParenthesizedExpression )";
        }
    }
    static final String DFA40_eotS =
        "\40\uffff";
    static final String DFA40_eofS =
        "\40\uffff";
    static final String DFA40_minS =
        "\1\4\2\0\35\uffff";
    static final String DFA40_maxS =
        "\1\111\2\0\35\uffff";
    static final String DFA40_acceptS =
        "\3\uffff\2\1\1\2\32\uffff";
    static final String DFA40_specialS =
        "\1\0\1\1\1\2\35\uffff}>";
    static final String[] DFA40_transitionS = {
            "\1\1\4\5\7\uffff\1\5\1\uffff\1\5\13\uffff\1\5\2\uffff\1\3\3"+
            "\uffff\1\5\4\uffff\1\5\4\uffff\1\2\1\uffff\1\5\1\4\1\5\1\uffff"+
            "\1\5\1\uffff\1\5\3\uffff\6\5\1\uffff\10\5",
            "\1\uffff",
            "\1\uffff",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA40_eot = DFA.unpackEncodedString(DFA40_eotS);
    static final short[] DFA40_eof = DFA.unpackEncodedString(DFA40_eofS);
    static final char[] DFA40_min = DFA.unpackEncodedStringToUnsignedChars(DFA40_minS);
    static final char[] DFA40_max = DFA.unpackEncodedStringToUnsignedChars(DFA40_maxS);
    static final short[] DFA40_accept = DFA.unpackEncodedString(DFA40_acceptS);
    static final short[] DFA40_special = DFA.unpackEncodedString(DFA40_specialS);
    static final short[][] DFA40_transition;

    static {
        int numStates = DFA40_transitionS.length;
        DFA40_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA40_transition[i] = DFA.unpackEncodedString(DFA40_transitionS[i]);
        }
    }

    class DFA40 extends DFA {

        public DFA40(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 40;
            this.eot = DFA40_eot;
            this.eof = DFA40_eof;
            this.min = DFA40_min;
            this.max = DFA40_max;
            this.accept = DFA40_accept;
            this.special = DFA40_special;
            this.transition = DFA40_transition;
        }
        public String getDescription() {
            return "2575:3: ( ( ( ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> ( ( ( (lv_declaredFormalParameters_2_0= ruleJvmFormalParameter ) ) (otherlv_3= ',' ( (lv_declaredFormalParameters_4_0= ruleJvmFormalParameter ) ) )* )? ( (lv_explicitSyntax_5_0= '|' ) ) ) )?";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA40_0 = input.LA(1);

                         
                        int index40_0 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (LA40_0==RULE_ID) ) {s = 1;}

                        else if ( (LA40_0==47) ) {s = 2;}

                        else if ( (LA40_0==33) && (synpred20_InternalDeepClone())) {s = 3;}

                        else if ( (LA40_0==50) && (synpred20_InternalDeepClone())) {s = 4;}

                        else if ( ((LA40_0>=RULE_STRING && LA40_0<=RULE_DECIMAL)||LA40_0==16||LA40_0==18||LA40_0==30||LA40_0==37||LA40_0==42||LA40_0==49||LA40_0==51||LA40_0==53||LA40_0==55||(LA40_0>=59 && LA40_0<=64)||(LA40_0>=66 && LA40_0<=73)) ) {s = 5;}

                         
                        input.seek(index40_0);
                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA40_1 = input.LA(1);

                         
                        int index40_1 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred20_InternalDeepClone()) ) {s = 4;}

                        else if ( (true) ) {s = 5;}

                         
                        input.seek(index40_1);
                        if ( s>=0 ) return s;
                        break;
                    case 2 : 
                        int LA40_2 = input.LA(1);

                         
                        int index40_2 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred20_InternalDeepClone()) ) {s = 4;}

                        else if ( (true) ) {s = 5;}

                         
                        input.seek(index40_2);
                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 40, _s, input);
            error(nvae);
            throw nvae;
        }
    }
    static final String DFA64_eotS =
        "\101\uffff";
    static final String DFA64_eofS =
        "\1\2\100\uffff";
    static final String DFA64_minS =
        "\1\4\1\0\77\uffff";
    static final String DFA64_maxS =
        "\1\113\1\0\77\uffff";
    static final String DFA64_acceptS =
        "\2\uffff\1\2\75\uffff\1\1";
    static final String DFA64_specialS =
        "\1\uffff\1\0\77\uffff}>";
    static final String[] DFA64_transitionS = {
            "\5\2\6\uffff\4\2\2\uffff\32\2\1\1\2\2\1\uffff\16\2\1\uffff"+
            "\12\2",
            "\1\uffff",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA64_eot = DFA.unpackEncodedString(DFA64_eotS);
    static final short[] DFA64_eof = DFA.unpackEncodedString(DFA64_eofS);
    static final char[] DFA64_min = DFA.unpackEncodedStringToUnsignedChars(DFA64_minS);
    static final char[] DFA64_max = DFA.unpackEncodedStringToUnsignedChars(DFA64_maxS);
    static final short[] DFA64_accept = DFA.unpackEncodedString(DFA64_acceptS);
    static final short[] DFA64_special = DFA.unpackEncodedString(DFA64_specialS);
    static final short[][] DFA64_transition;

    static {
        int numStates = DFA64_transitionS.length;
        DFA64_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA64_transition[i] = DFA.unpackEncodedString(DFA64_transitionS[i]);
        }
    }

    class DFA64 extends DFA {

        public DFA64(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 64;
            this.eot = DFA64_eot;
            this.eof = DFA64_eof;
            this.min = DFA64_min;
            this.max = DFA64_max;
            this.accept = DFA64_accept;
            this.special = DFA64_special;
            this.transition = DFA64_transition;
        }
        public String getDescription() {
            return "3933:2: ( ( ( ( '(' ) )=> (lv_explicitOperationCall_8_0= '(' ) ) ( ( ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> (lv_featureCallArguments_9_0= ruleXShortClosure ) ) | ( ( (lv_featureCallArguments_10_0= ruleXExpression ) ) (otherlv_11= ',' ( (lv_featureCallArguments_12_0= ruleXExpression ) ) )* ) )? otherlv_13= ')' )?";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA64_1 = input.LA(1);

                         
                        int index64_1 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred26_InternalDeepClone()) ) {s = 64;}

                        else if ( (true) ) {s = 2;}

                         
                        input.seek(index64_1);
                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 64, _s, input);
            error(nvae);
            throw nvae;
        }
    }
    static final String DFA63_eotS =
        "\36\uffff";
    static final String DFA63_eofS =
        "\36\uffff";
    static final String DFA63_minS =
        "\1\4\2\0\33\uffff";
    static final String DFA63_maxS =
        "\1\111\2\0\33\uffff";
    static final String DFA63_acceptS =
        "\3\uffff\2\1\1\2\27\uffff\1\3";
    static final String DFA63_specialS =
        "\1\0\1\1\1\2\33\uffff}>";
    static final String[] DFA63_transitionS = {
            "\1\1\4\5\7\uffff\1\5\1\uffff\1\5\13\uffff\1\5\2\uffff\1\3\3"+
            "\uffff\1\5\4\uffff\1\5\4\uffff\1\2\1\35\1\5\1\4\2\uffff\1\5"+
            "\1\uffff\1\5\3\uffff\3\5\2\uffff\1\5\1\uffff\10\5",
            "\1\uffff",
            "\1\uffff",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA63_eot = DFA.unpackEncodedString(DFA63_eotS);
    static final short[] DFA63_eof = DFA.unpackEncodedString(DFA63_eofS);
    static final char[] DFA63_min = DFA.unpackEncodedStringToUnsignedChars(DFA63_minS);
    static final char[] DFA63_max = DFA.unpackEncodedStringToUnsignedChars(DFA63_maxS);
    static final short[] DFA63_accept = DFA.unpackEncodedString(DFA63_acceptS);
    static final short[] DFA63_special = DFA.unpackEncodedString(DFA63_specialS);
    static final short[][] DFA63_transition;

    static {
        int numStates = DFA63_transitionS.length;
        DFA63_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA63_transition[i] = DFA.unpackEncodedString(DFA63_transitionS[i]);
        }
    }

    class DFA63 extends DFA {

        public DFA63(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 63;
            this.eot = DFA63_eot;
            this.eof = DFA63_eof;
            this.min = DFA63_min;
            this.max = DFA63_max;
            this.accept = DFA63_accept;
            this.special = DFA63_special;
            this.transition = DFA63_transition;
        }
        public String getDescription() {
            return "3954:2: ( ( ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> (lv_featureCallArguments_9_0= ruleXShortClosure ) ) | ( ( (lv_featureCallArguments_10_0= ruleXExpression ) ) (otherlv_11= ',' ( (lv_featureCallArguments_12_0= ruleXExpression ) ) )* ) )?";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA63_0 = input.LA(1);

                         
                        int index63_0 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (LA63_0==RULE_ID) ) {s = 1;}

                        else if ( (LA63_0==47) ) {s = 2;}

                        else if ( (LA63_0==33) && (synpred27_InternalDeepClone())) {s = 3;}

                        else if ( (LA63_0==50) && (synpred27_InternalDeepClone())) {s = 4;}

                        else if ( ((LA63_0>=RULE_STRING && LA63_0<=RULE_DECIMAL)||LA63_0==16||LA63_0==18||LA63_0==30||LA63_0==37||LA63_0==42||LA63_0==49||LA63_0==53||LA63_0==55||(LA63_0>=59 && LA63_0<=61)||LA63_0==64||(LA63_0>=66 && LA63_0<=73)) ) {s = 5;}

                        else if ( (LA63_0==48) ) {s = 29;}

                         
                        input.seek(index63_0);
                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA63_1 = input.LA(1);

                         
                        int index63_1 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred27_InternalDeepClone()) ) {s = 4;}

                        else if ( (true) ) {s = 5;}

                         
                        input.seek(index63_1);
                        if ( s>=0 ) return s;
                        break;
                    case 2 : 
                        int LA63_2 = input.LA(1);

                         
                        int index63_2 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred27_InternalDeepClone()) ) {s = 4;}

                        else if ( (true) ) {s = 5;}

                         
                        input.seek(index63_2);
                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 63, _s, input);
            error(nvae);
            throw nvae;
        }
    }
    static final String DFA65_eotS =
        "\101\uffff";
    static final String DFA65_eofS =
        "\1\2\100\uffff";
    static final String DFA65_minS =
        "\1\4\1\0\77\uffff";
    static final String DFA65_maxS =
        "\1\113\1\0\77\uffff";
    static final String DFA65_acceptS =
        "\2\uffff\1\2\75\uffff\1\1";
    static final String DFA65_specialS =
        "\1\uffff\1\0\77\uffff}>";
    static final String[] DFA65_transitionS = {
            "\5\2\6\uffff\4\2\2\uffff\34\2\1\1\1\uffff\16\2\1\uffff\12\2",
            "\1\uffff",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA65_eot = DFA.unpackEncodedString(DFA65_eotS);
    static final short[] DFA65_eof = DFA.unpackEncodedString(DFA65_eofS);
    static final char[] DFA65_min = DFA.unpackEncodedStringToUnsignedChars(DFA65_minS);
    static final char[] DFA65_max = DFA.unpackEncodedStringToUnsignedChars(DFA65_maxS);
    static final short[] DFA65_accept = DFA.unpackEncodedString(DFA65_acceptS);
    static final short[] DFA65_special = DFA.unpackEncodedString(DFA65_specialS);
    static final short[][] DFA65_transition;

    static {
        int numStates = DFA65_transitionS.length;
        DFA65_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA65_transition[i] = DFA.unpackEncodedString(DFA65_transitionS[i]);
        }
    }

    class DFA65 extends DFA {

        public DFA65(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 65;
            this.eot = DFA65_eot;
            this.eof = DFA65_eof;
            this.min = DFA65_min;
            this.max = DFA65_max;
            this.accept = DFA65_accept;
            this.special = DFA65_special;
            this.transition = DFA65_transition;
        }
        public String getDescription() {
            return "4033:3: ( ( ( () '[' ) )=> (lv_featureCallArguments_14_0= ruleXClosure ) )?";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA65_1 = input.LA(1);

                         
                        int index65_1 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred28_InternalDeepClone()) ) {s = 64;}

                        else if ( (true) ) {s = 2;}

                         
                        input.seek(index65_1);
                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 65, _s, input);
            error(nvae);
            throw nvae;
        }
    }
    static final String DFA69_eotS =
        "\101\uffff";
    static final String DFA69_eofS =
        "\1\2\100\uffff";
    static final String DFA69_minS =
        "\1\4\1\0\77\uffff";
    static final String DFA69_maxS =
        "\1\113\1\0\77\uffff";
    static final String DFA69_acceptS =
        "\2\uffff\1\2\75\uffff\1\1";
    static final String DFA69_specialS =
        "\1\uffff\1\0\77\uffff}>";
    static final String[] DFA69_transitionS = {
            "\5\2\6\uffff\4\2\2\uffff\11\2\1\1\23\2\1\uffff\16\2\1\uffff"+
            "\12\2",
            "\1\uffff",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA69_eot = DFA.unpackEncodedString(DFA69_eotS);
    static final short[] DFA69_eof = DFA.unpackEncodedString(DFA69_eofS);
    static final char[] DFA69_min = DFA.unpackEncodedStringToUnsignedChars(DFA69_minS);
    static final char[] DFA69_max = DFA.unpackEncodedStringToUnsignedChars(DFA69_maxS);
    static final short[] DFA69_accept = DFA.unpackEncodedString(DFA69_acceptS);
    static final short[] DFA69_special = DFA.unpackEncodedString(DFA69_specialS);
    static final short[][] DFA69_transition;

    static {
        int numStates = DFA69_transitionS.length;
        DFA69_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA69_transition[i] = DFA.unpackEncodedString(DFA69_transitionS[i]);
        }
    }

    class DFA69 extends DFA {

        public DFA69(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 69;
            this.eot = DFA69_eot;
            this.eof = DFA69_eof;
            this.min = DFA69_min;
            this.max = DFA69_max;
            this.accept = DFA69_accept;
            this.special = DFA69_special;
            this.transition = DFA69_transition;
        }
        public String getDescription() {
            return "4176:2: ( ( ( '<' )=>otherlv_3= '<' ) ( (lv_typeArguments_4_0= ruleJvmArgumentTypeReference ) ) (otherlv_5= ',' ( (lv_typeArguments_6_0= ruleJvmArgumentTypeReference ) ) )* otherlv_7= '>' )?";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA69_1 = input.LA(1);

                         
                        int index69_1 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred29_InternalDeepClone()) ) {s = 64;}

                        else if ( (true) ) {s = 2;}

                         
                        input.seek(index69_1);
                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 69, _s, input);
            error(nvae);
            throw nvae;
        }
    }
    static final String DFA72_eotS =
        "\101\uffff";
    static final String DFA72_eofS =
        "\1\2\100\uffff";
    static final String DFA72_minS =
        "\1\4\1\0\77\uffff";
    static final String DFA72_maxS =
        "\1\113\1\0\77\uffff";
    static final String DFA72_acceptS =
        "\2\uffff\1\2\75\uffff\1\1";
    static final String DFA72_specialS =
        "\1\uffff\1\0\77\uffff}>";
    static final String[] DFA72_transitionS = {
            "\5\2\6\uffff\4\2\2\uffff\32\2\1\1\2\2\1\uffff\16\2\1\uffff"+
            "\12\2",
            "\1\uffff",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA72_eot = DFA.unpackEncodedString(DFA72_eotS);
    static final short[] DFA72_eof = DFA.unpackEncodedString(DFA72_eofS);
    static final char[] DFA72_min = DFA.unpackEncodedStringToUnsignedChars(DFA72_minS);
    static final char[] DFA72_max = DFA.unpackEncodedStringToUnsignedChars(DFA72_maxS);
    static final short[] DFA72_accept = DFA.unpackEncodedString(DFA72_acceptS);
    static final short[] DFA72_special = DFA.unpackEncodedString(DFA72_specialS);
    static final short[][] DFA72_transition;

    static {
        int numStates = DFA72_transitionS.length;
        DFA72_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA72_transition[i] = DFA.unpackEncodedString(DFA72_transitionS[i]);
        }
    }

    class DFA72 extends DFA {

        public DFA72(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 72;
            this.eot = DFA72_eot;
            this.eof = DFA72_eof;
            this.min = DFA72_min;
            this.max = DFA72_max;
            this.accept = DFA72_accept;
            this.special = DFA72_special;
            this.transition = DFA72_transition;
        }
        public String getDescription() {
            return "4225:3: ( ( ( '(' )=>otherlv_8= '(' ) ( ( ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> (lv_arguments_9_0= ruleXShortClosure ) ) | ( ( (lv_arguments_10_0= ruleXExpression ) ) (otherlv_11= ',' ( (lv_arguments_12_0= ruleXExpression ) ) )* ) )? otherlv_13= ')' )?";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA72_1 = input.LA(1);

                         
                        int index72_1 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred30_InternalDeepClone()) ) {s = 64;}

                        else if ( (true) ) {s = 2;}

                         
                        input.seek(index72_1);
                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 72, _s, input);
            error(nvae);
            throw nvae;
        }
    }
    static final String DFA71_eotS =
        "\36\uffff";
    static final String DFA71_eofS =
        "\36\uffff";
    static final String DFA71_minS =
        "\1\4\2\0\33\uffff";
    static final String DFA71_maxS =
        "\1\111\2\0\33\uffff";
    static final String DFA71_acceptS =
        "\3\uffff\2\1\1\2\27\uffff\1\3";
    static final String DFA71_specialS =
        "\1\0\1\1\1\2\33\uffff}>";
    static final String[] DFA71_transitionS = {
            "\1\1\4\5\7\uffff\1\5\1\uffff\1\5\13\uffff\1\5\2\uffff\1\3\3"+
            "\uffff\1\5\4\uffff\1\5\4\uffff\1\2\1\35\1\5\1\4\2\uffff\1\5"+
            "\1\uffff\1\5\3\uffff\3\5\2\uffff\1\5\1\uffff\10\5",
            "\1\uffff",
            "\1\uffff",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA71_eot = DFA.unpackEncodedString(DFA71_eotS);
    static final short[] DFA71_eof = DFA.unpackEncodedString(DFA71_eofS);
    static final char[] DFA71_min = DFA.unpackEncodedStringToUnsignedChars(DFA71_minS);
    static final char[] DFA71_max = DFA.unpackEncodedStringToUnsignedChars(DFA71_maxS);
    static final short[] DFA71_accept = DFA.unpackEncodedString(DFA71_acceptS);
    static final short[] DFA71_special = DFA.unpackEncodedString(DFA71_specialS);
    static final short[][] DFA71_transition;

    static {
        int numStates = DFA71_transitionS.length;
        DFA71_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA71_transition[i] = DFA.unpackEncodedString(DFA71_transitionS[i]);
        }
    }

    class DFA71 extends DFA {

        public DFA71(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 71;
            this.eot = DFA71_eot;
            this.eof = DFA71_eof;
            this.min = DFA71_min;
            this.max = DFA71_max;
            this.accept = DFA71_accept;
            this.special = DFA71_special;
            this.transition = DFA71_transition;
        }
        public String getDescription() {
            return "4230:2: ( ( ( ( () ( ( ( ruleJvmFormalParameter ) ) ( ',' ( ( ruleJvmFormalParameter ) ) )* )? ( ( '|' ) ) ) )=> (lv_arguments_9_0= ruleXShortClosure ) ) | ( ( (lv_arguments_10_0= ruleXExpression ) ) (otherlv_11= ',' ( (lv_arguments_12_0= ruleXExpression ) ) )* ) )?";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA71_0 = input.LA(1);

                         
                        int index71_0 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (LA71_0==RULE_ID) ) {s = 1;}

                        else if ( (LA71_0==47) ) {s = 2;}

                        else if ( (LA71_0==33) && (synpred31_InternalDeepClone())) {s = 3;}

                        else if ( (LA71_0==50) && (synpred31_InternalDeepClone())) {s = 4;}

                        else if ( ((LA71_0>=RULE_STRING && LA71_0<=RULE_DECIMAL)||LA71_0==16||LA71_0==18||LA71_0==30||LA71_0==37||LA71_0==42||LA71_0==49||LA71_0==53||LA71_0==55||(LA71_0>=59 && LA71_0<=61)||LA71_0==64||(LA71_0>=66 && LA71_0<=73)) ) {s = 5;}

                        else if ( (LA71_0==48) ) {s = 29;}

                         
                        input.seek(index71_0);
                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA71_1 = input.LA(1);

                         
                        int index71_1 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred31_InternalDeepClone()) ) {s = 4;}

                        else if ( (true) ) {s = 5;}

                         
                        input.seek(index71_1);
                        if ( s>=0 ) return s;
                        break;
                    case 2 : 
                        int LA71_2 = input.LA(1);

                         
                        int index71_2 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred31_InternalDeepClone()) ) {s = 4;}

                        else if ( (true) ) {s = 5;}

                         
                        input.seek(index71_2);
                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 71, _s, input);
            error(nvae);
            throw nvae;
        }
    }
    static final String DFA73_eotS =
        "\101\uffff";
    static final String DFA73_eofS =
        "\1\2\100\uffff";
    static final String DFA73_minS =
        "\1\4\1\0\77\uffff";
    static final String DFA73_maxS =
        "\1\113\1\0\77\uffff";
    static final String DFA73_acceptS =
        "\2\uffff\1\2\75\uffff\1\1";
    static final String DFA73_specialS =
        "\1\uffff\1\0\77\uffff}>";
    static final String[] DFA73_transitionS = {
            "\5\2\6\uffff\4\2\2\uffff\34\2\1\1\1\uffff\16\2\1\uffff\12\2",
            "\1\uffff",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA73_eot = DFA.unpackEncodedString(DFA73_eotS);
    static final short[] DFA73_eof = DFA.unpackEncodedString(DFA73_eofS);
    static final char[] DFA73_min = DFA.unpackEncodedStringToUnsignedChars(DFA73_minS);
    static final char[] DFA73_max = DFA.unpackEncodedStringToUnsignedChars(DFA73_maxS);
    static final short[] DFA73_accept = DFA.unpackEncodedString(DFA73_acceptS);
    static final short[] DFA73_special = DFA.unpackEncodedString(DFA73_specialS);
    static final short[][] DFA73_transition;

    static {
        int numStates = DFA73_transitionS.length;
        DFA73_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA73_transition[i] = DFA.unpackEncodedString(DFA73_transitionS[i]);
        }
    }

    class DFA73 extends DFA {

        public DFA73(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 73;
            this.eot = DFA73_eot;
            this.eof = DFA73_eof;
            this.min = DFA73_min;
            this.max = DFA73_max;
            this.accept = DFA73_accept;
            this.special = DFA73_special;
            this.transition = DFA73_transition;
        }
        public String getDescription() {
            return "4309:3: ( ( ( () '[' ) )=> (lv_arguments_14_0= ruleXClosure ) )?";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA73_1 = input.LA(1);

                         
                        int index73_1 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred32_InternalDeepClone()) ) {s = 64;}

                        else if ( (true) ) {s = 2;}

                         
                        input.seek(index73_1);
                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 73, _s, input);
            error(nvae);
            throw nvae;
        }
    }
    static final String DFA75_eotS =
        "\101\uffff";
    static final String DFA75_eofS =
        "\1\33\100\uffff";
    static final String DFA75_minS =
        "\1\4\32\0\46\uffff";
    static final String DFA75_maxS =
        "\1\113\32\0\46\uffff";
    static final String DFA75_acceptS =
        "\33\uffff\1\2\44\uffff\1\1";
    static final String DFA75_specialS =
        "\1\uffff\1\0\1\1\1\2\1\3\1\4\1\5\1\6\1\7\1\10\1\11\1\12\1\13\1"+
        "\14\1\15\1\16\1\17\1\20\1\21\1\22\1\23\1\24\1\25\1\26\1\27\1\30"+
        "\1\31\46\uffff}>";
    static final String[] DFA75_transitionS = {
            "\1\1\1\21\1\15\1\16\1\17\6\uffff\1\33\1\6\1\33\1\3\2\uffff"+
            "\11\33\1\10\6\33\1\4\4\33\1\2\4\33\1\32\1\33\1\12\1\uffff\2"+
            "\33\1\23\1\33\1\7\3\33\1\24\1\25\1\26\2\33\1\11\1\uffff\1\5"+
            "\1\13\1\14\1\20\1\22\1\27\1\30\1\31\2\33",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA75_eot = DFA.unpackEncodedString(DFA75_eotS);
    static final short[] DFA75_eof = DFA.unpackEncodedString(DFA75_eofS);
    static final char[] DFA75_min = DFA.unpackEncodedStringToUnsignedChars(DFA75_minS);
    static final char[] DFA75_max = DFA.unpackEncodedStringToUnsignedChars(DFA75_maxS);
    static final short[] DFA75_accept = DFA.unpackEncodedString(DFA75_acceptS);
    static final short[] DFA75_special = DFA.unpackEncodedString(DFA75_specialS);
    static final short[][] DFA75_transition;

    static {
        int numStates = DFA75_transitionS.length;
        DFA75_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA75_transition[i] = DFA.unpackEncodedString(DFA75_transitionS[i]);
        }
    }

    class DFA75 extends DFA {

        public DFA75(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 75;
            this.eot = DFA75_eot;
            this.eof = DFA75_eof;
            this.min = DFA75_min;
            this.max = DFA75_max;
            this.accept = DFA75_accept;
            this.special = DFA75_special;
            this.transition = DFA75_transition;
        }
        public String getDescription() {
            return "4631:1: ( ( ( ruleXExpression ) )=> (lv_expression_2_0= ruleXExpression ) )?";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA75_1 = input.LA(1);

                         
                        int index75_1 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred33_InternalDeepClone()) ) {s = 64;}

                        else if ( (true) ) {s = 27;}

                         
                        input.seek(index75_1);
                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA75_2 = input.LA(1);

                         
                        int index75_2 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred33_InternalDeepClone()) ) {s = 64;}

                        else if ( (true) ) {s = 27;}

                         
                        input.seek(index75_2);
                        if ( s>=0 ) return s;
                        break;
                    case 2 : 
                        int LA75_3 = input.LA(1);

                         
                        int index75_3 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred33_InternalDeepClone()) ) {s = 64;}

                        else if ( (true) ) {s = 27;}

                         
                        input.seek(index75_3);
                        if ( s>=0 ) return s;
                        break;
                    case 3 : 
                        int LA75_4 = input.LA(1);

                         
                        int index75_4 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred33_InternalDeepClone()) ) {s = 64;}

                        else if ( (true) ) {s = 27;}

                         
                        input.seek(index75_4);
                        if ( s>=0 ) return s;
                        break;
                    case 4 : 
                        int LA75_5 = input.LA(1);

                         
                        int index75_5 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred33_InternalDeepClone()) ) {s = 64;}

                        else if ( (true) ) {s = 27;}

                         
                        input.seek(index75_5);
                        if ( s>=0 ) return s;
                        break;
                    case 5 : 
                        int LA75_6 = input.LA(1);

                         
                        int index75_6 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred33_InternalDeepClone()) ) {s = 64;}

                        else if ( (true) ) {s = 27;}

                         
                        input.seek(index75_6);
                        if ( s>=0 ) return s;
                        break;
                    case 6 : 
                        int LA75_7 = input.LA(1);

                         
                        int index75_7 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred33_InternalDeepClone()) ) {s = 64;}

                        else if ( (true) ) {s = 27;}

                         
                        input.seek(index75_7);
                        if ( s>=0 ) return s;
                        break;
                    case 7 : 
                        int LA75_8 = input.LA(1);

                         
                        int index75_8 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred33_InternalDeepClone()) ) {s = 64;}

                        else if ( (true) ) {s = 27;}

                         
                        input.seek(index75_8);
                        if ( s>=0 ) return s;
                        break;
                    case 8 : 
                        int LA75_9 = input.LA(1);

                         
                        int index75_9 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred33_InternalDeepClone()) ) {s = 64;}

                        else if ( (true) ) {s = 27;}

                         
                        input.seek(index75_9);
                        if ( s>=0 ) return s;
                        break;
                    case 9 : 
                        int LA75_10 = input.LA(1);

                         
                        int index75_10 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred33_InternalDeepClone()) ) {s = 64;}

                        else if ( (true) ) {s = 27;}

                         
                        input.seek(index75_10);
                        if ( s>=0 ) return s;
                        break;
                    case 10 : 
                        int LA75_11 = input.LA(1);

                         
                        int index75_11 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred33_InternalDeepClone()) ) {s = 64;}

                        else if ( (true) ) {s = 27;}

                         
                        input.seek(index75_11);
                        if ( s>=0 ) return s;
                        break;
                    case 11 : 
                        int LA75_12 = input.LA(1);

                         
                        int index75_12 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred33_InternalDeepClone()) ) {s = 64;}

                        else if ( (true) ) {s = 27;}

                         
                        input.seek(index75_12);
                        if ( s>=0 ) return s;
                        break;
                    case 12 : 
                        int LA75_13 = input.LA(1);

                         
                        int index75_13 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred33_InternalDeepClone()) ) {s = 64;}

                        else if ( (true) ) {s = 27;}

                         
                        input.seek(index75_13);
                        if ( s>=0 ) return s;
                        break;
                    case 13 : 
                        int LA75_14 = input.LA(1);

                         
                        int index75_14 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred33_InternalDeepClone()) ) {s = 64;}

                        else if ( (true) ) {s = 27;}

                         
                        input.seek(index75_14);
                        if ( s>=0 ) return s;
                        break;
                    case 14 : 
                        int LA75_15 = input.LA(1);

                         
                        int index75_15 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred33_InternalDeepClone()) ) {s = 64;}

                        else if ( (true) ) {s = 27;}

                         
                        input.seek(index75_15);
                        if ( s>=0 ) return s;
                        break;
                    case 15 : 
                        int LA75_16 = input.LA(1);

                         
                        int index75_16 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred33_InternalDeepClone()) ) {s = 64;}

                        else if ( (true) ) {s = 27;}

                         
                        input.seek(index75_16);
                        if ( s>=0 ) return s;
                        break;
                    case 16 : 
                        int LA75_17 = input.LA(1);

                         
                        int index75_17 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred33_InternalDeepClone()) ) {s = 64;}

                        else if ( (true) ) {s = 27;}

                         
                        input.seek(index75_17);
                        if ( s>=0 ) return s;
                        break;
                    case 17 : 
                        int LA75_18 = input.LA(1);

                         
                        int index75_18 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred33_InternalDeepClone()) ) {s = 64;}

                        else if ( (true) ) {s = 27;}

                         
                        input.seek(index75_18);
                        if ( s>=0 ) return s;
                        break;
                    case 18 : 
                        int LA75_19 = input.LA(1);

                         
                        int index75_19 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred33_InternalDeepClone()) ) {s = 64;}

                        else if ( (true) ) {s = 27;}

                         
                        input.seek(index75_19);
                        if ( s>=0 ) return s;
                        break;
                    case 19 : 
                        int LA75_20 = input.LA(1);

                         
                        int index75_20 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred33_InternalDeepClone()) ) {s = 64;}

                        else if ( (true) ) {s = 27;}

                         
                        input.seek(index75_20);
                        if ( s>=0 ) return s;
                        break;
                    case 20 : 
                        int LA75_21 = input.LA(1);

                         
                        int index75_21 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred33_InternalDeepClone()) ) {s = 64;}

                        else if ( (true) ) {s = 27;}

                         
                        input.seek(index75_21);
                        if ( s>=0 ) return s;
                        break;
                    case 21 : 
                        int LA75_22 = input.LA(1);

                         
                        int index75_22 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred33_InternalDeepClone()) ) {s = 64;}

                        else if ( (true) ) {s = 27;}

                         
                        input.seek(index75_22);
                        if ( s>=0 ) return s;
                        break;
                    case 22 : 
                        int LA75_23 = input.LA(1);

                         
                        int index75_23 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred33_InternalDeepClone()) ) {s = 64;}

                        else if ( (true) ) {s = 27;}

                         
                        input.seek(index75_23);
                        if ( s>=0 ) return s;
                        break;
                    case 23 : 
                        int LA75_24 = input.LA(1);

                         
                        int index75_24 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred33_InternalDeepClone()) ) {s = 64;}

                        else if ( (true) ) {s = 27;}

                         
                        input.seek(index75_24);
                        if ( s>=0 ) return s;
                        break;
                    case 24 : 
                        int LA75_25 = input.LA(1);

                         
                        int index75_25 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred33_InternalDeepClone()) ) {s = 64;}

                        else if ( (true) ) {s = 27;}

                         
                        input.seek(index75_25);
                        if ( s>=0 ) return s;
                        break;
                    case 25 : 
                        int LA75_26 = input.LA(1);

                         
                        int index75_26 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred33_InternalDeepClone()) ) {s = 64;}

                        else if ( (true) ) {s = 27;}

                         
                        input.seek(index75_26);
                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 75, _s, input);
            error(nvae);
            throw nvae;
        }
    }
    static final String DFA90_eotS =
        "\101\uffff";
    static final String DFA90_eofS =
        "\1\2\100\uffff";
    static final String DFA90_minS =
        "\1\4\1\0\77\uffff";
    static final String DFA90_maxS =
        "\1\113\1\0\77\uffff";
    static final String DFA90_acceptS =
        "\2\uffff\1\2\75\uffff\1\1";
    static final String DFA90_specialS =
        "\1\uffff\1\0\77\uffff}>";
    static final String[] DFA90_transitionS = {
            "\5\2\6\uffff\4\2\2\uffff\11\2\1\1\23\2\1\uffff\16\2\1\uffff"+
            "\12\2",
            "\1\uffff",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA90_eot = DFA.unpackEncodedString(DFA90_eotS);
    static final short[] DFA90_eof = DFA.unpackEncodedString(DFA90_eofS);
    static final char[] DFA90_min = DFA.unpackEncodedStringToUnsignedChars(DFA90_minS);
    static final char[] DFA90_max = DFA.unpackEncodedStringToUnsignedChars(DFA90_maxS);
    static final short[] DFA90_accept = DFA.unpackEncodedString(DFA90_acceptS);
    static final short[] DFA90_special = DFA.unpackEncodedString(DFA90_specialS);
    static final short[][] DFA90_transition;

    static {
        int numStates = DFA90_transitionS.length;
        DFA90_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA90_transition[i] = DFA.unpackEncodedString(DFA90_transitionS[i]);
        }
    }

    class DFA90 extends DFA {

        public DFA90(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 90;
            this.eot = DFA90_eot;
            this.eof = DFA90_eof;
            this.min = DFA90_min;
            this.max = DFA90_max;
            this.accept = DFA90_accept;
            this.special = DFA90_special;
            this.transition = DFA90_transition;
        }
        public String getDescription() {
            return "5147:2: ( ( ( '<' )=>otherlv_1= '<' ) ( (lv_arguments_2_0= ruleJvmArgumentTypeReference ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleJvmArgumentTypeReference ) ) )* otherlv_5= '>' )?";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA90_1 = input.LA(1);

                         
                        int index90_1 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred39_InternalDeepClone()) ) {s = 64;}

                        else if ( (true) ) {s = 2;}

                         
                        input.seek(index90_1);
                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 90, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

    public static final BitSet FOLLOW_ruleModel_in_entryRuleModel75 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleModel85 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleHeader_in_ruleModel126 = new BitSet(new long[]{0x0000800200004010L});
    public static final BitSet FOLLOW_ruleBody_in_ruleModel147 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleHeader_in_entryRuleHeader183 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleHeader194 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_ruleHeader231 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBody_in_entryRuleBody270 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBody280 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePackageConfig_in_ruleBody335 = new BitSet(new long[]{0x0000800200000012L});
    public static final BitSet FOLLOW_ruleClassCloner_in_ruleBody357 = new BitSet(new long[]{0x0000800200000012L});
    public static final BitSet FOLLOW_rulePackageConfig_in_entryRulePackageConfig394 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePackageConfig404 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_rulePackageConfig441 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rulePackageNamespace_in_rulePackageConfig462 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePackageNamespace_in_entryRulePackageNamespace499 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePackageNamespace510 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rulePackageNamespace550 = new BitSet(new long[]{0x0000000000008002L});
    public static final BitSet FOLLOW_15_in_rulePackageNamespace569 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_rulePackageNamespace584 = new BitSet(new long[]{0x0000000000008002L});
    public static final BitSet FOLLOW_ruleClassCloner_in_entryRuleClassCloner631 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleClassCloner641 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleJvmTypeReference_in_ruleClassCloner687 = new BitSet(new long[]{0x0000000000010010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleClassCloner704 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_ruleClassCloner722 = new BitSet(new long[]{0x00000000000E0010L});
    public static final BitSet FOLLOW_ruleFieldCloner_in_ruleClassCloner743 = new BitSet(new long[]{0x00000000000E0010L});
    public static final BitSet FOLLOW_17_in_ruleClassCloner756 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFieldCloner_in_entryRuleFieldCloner792 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleFieldCloner802 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSimpleField_in_ruleFieldCloner849 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSimpleExcludedField_in_ruleFieldCloner876 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleComplexField_in_ruleFieldCloner903 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleReferenceField_in_ruleFieldCloner930 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSimpleField_in_entryRuleSimpleField965 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSimpleField975 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleSimpleField1016 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSimpleExcludedField_in_entryRuleSimpleExcludedField1056 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSimpleExcludedField1066 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_ruleSimpleExcludedField1103 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleSimpleExcludedField1120 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleComplexField_in_entryRuleComplexField1161 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleComplexField1171 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleComplexField1213 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_ruleComplexField1230 = new BitSet(new long[]{0x00000000000E0010L});
    public static final BitSet FOLLOW_ruleFieldCloner_in_ruleComplexField1251 = new BitSet(new long[]{0x00000000000E0010L});
    public static final BitSet FOLLOW_17_in_ruleComplexField1264 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleReferenceField_in_entryRuleReferenceField1300 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleReferenceField1310 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_ruleReferenceField1347 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleReferenceField1364 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleReferenceField1389 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleContainerType_in_entryRuleContainerType1429 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleContainerType1439 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFieldCloner_in_ruleContainerType1494 = new BitSet(new long[]{0x00000000000C0012L});
    public static final BitSet FOLLOW_ruleFieldClonerType_in_entryRuleFieldClonerType1533 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleFieldClonerType1543 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleFieldClonerType1594 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXExpression_in_entryRuleXExpression1637 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleXExpression1647 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXAssignment_in_ruleXExpression1693 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXAssignment_in_entryRuleXAssignment1727 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleXAssignment1737 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleValidID_in_ruleXAssignment1795 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_ruleOpSingleAssign_in_ruleXAssignment1811 = new BitSet(new long[]{0x38A28420400501F0L,0x00000000000003FDL});
    public static final BitSet FOLLOW_ruleXAssignment_in_ruleXAssignment1831 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXOrExpression_in_ruleXAssignment1861 = new BitSet(new long[]{0x0000000000200002L});
    public static final BitSet FOLLOW_ruleOpMultiAssign_in_ruleXAssignment1914 = new BitSet(new long[]{0x38A28420400501F0L,0x00000000000003FDL});
    public static final BitSet FOLLOW_ruleXAssignment_in_ruleXAssignment1937 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOpSingleAssign_in_entryRuleOpSingleAssign1977 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOpSingleAssign1988 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_ruleOpSingleAssign2025 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOpMultiAssign_in_entryRuleOpMultiAssign2065 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOpMultiAssign2076 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_ruleOpMultiAssign2113 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXOrExpression_in_entryRuleXOrExpression2152 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleXOrExpression2162 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXAndExpression_in_ruleXOrExpression2209 = new BitSet(new long[]{0x0000000000400002L});
    public static final BitSet FOLLOW_ruleOpOr_in_ruleXOrExpression2262 = new BitSet(new long[]{0x38A28420400501F0L,0x00000000000003FDL});
    public static final BitSet FOLLOW_ruleXAndExpression_in_ruleXOrExpression2285 = new BitSet(new long[]{0x0000000000400002L});
    public static final BitSet FOLLOW_ruleOpOr_in_entryRuleOpOr2324 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOpOr2335 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_ruleOpOr2372 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXAndExpression_in_entryRuleXAndExpression2411 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleXAndExpression2421 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXEqualityExpression_in_ruleXAndExpression2468 = new BitSet(new long[]{0x0000000000800002L});
    public static final BitSet FOLLOW_ruleOpAnd_in_ruleXAndExpression2521 = new BitSet(new long[]{0x38A28420400501F0L,0x00000000000003FDL});
    public static final BitSet FOLLOW_ruleXEqualityExpression_in_ruleXAndExpression2544 = new BitSet(new long[]{0x0000000000800002L});
    public static final BitSet FOLLOW_ruleOpAnd_in_entryRuleOpAnd2583 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOpAnd2594 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_ruleOpAnd2631 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXEqualityExpression_in_entryRuleXEqualityExpression2670 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleXEqualityExpression2680 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXRelationalExpression_in_ruleXEqualityExpression2727 = new BitSet(new long[]{0x0000000003000002L});
    public static final BitSet FOLLOW_ruleOpEquality_in_ruleXEqualityExpression2780 = new BitSet(new long[]{0x38A28420400501F0L,0x00000000000003FDL});
    public static final BitSet FOLLOW_ruleXRelationalExpression_in_ruleXEqualityExpression2803 = new BitSet(new long[]{0x0000000003000002L});
    public static final BitSet FOLLOW_ruleOpEquality_in_entryRuleOpEquality2842 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOpEquality2853 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_ruleOpEquality2891 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_25_in_ruleOpEquality2910 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXRelationalExpression_in_entryRuleXRelationalExpression2950 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleXRelationalExpression2960 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXOtherOperatorExpression_in_ruleXRelationalExpression3007 = new BitSet(new long[]{0x000000007C000002L});
    public static final BitSet FOLLOW_26_in_ruleXRelationalExpression3043 = new BitSet(new long[]{0x0000800200000010L});
    public static final BitSet FOLLOW_ruleJvmTypeReference_in_ruleXRelationalExpression3066 = new BitSet(new long[]{0x000000007C000002L});
    public static final BitSet FOLLOW_ruleOpCompare_in_ruleXRelationalExpression3127 = new BitSet(new long[]{0x38A28420400501F0L,0x00000000000003FDL});
    public static final BitSet FOLLOW_ruleXOtherOperatorExpression_in_ruleXRelationalExpression3150 = new BitSet(new long[]{0x000000007C000002L});
    public static final BitSet FOLLOW_ruleOpCompare_in_entryRuleOpCompare3190 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOpCompare3201 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_27_in_ruleOpCompare3239 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_28_in_ruleOpCompare3258 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_29_in_ruleOpCompare3277 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_ruleOpCompare3296 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXOtherOperatorExpression_in_entryRuleXOtherOperatorExpression3336 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleXOtherOperatorExpression3346 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXAdditiveExpression_in_ruleXOtherOperatorExpression3393 = new BitSet(new long[]{0x0000001FE0000002L});
    public static final BitSet FOLLOW_ruleOpOther_in_ruleXOtherOperatorExpression3446 = new BitSet(new long[]{0x38A28420400501F0L,0x00000000000003FDL});
    public static final BitSet FOLLOW_ruleXAdditiveExpression_in_ruleXOtherOperatorExpression3469 = new BitSet(new long[]{0x0000001FE0000002L});
    public static final BitSet FOLLOW_ruleOpOther_in_entryRuleOpOther3508 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOpOther3519 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_31_in_ruleOpOther3557 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_32_in_ruleOpOther3576 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_33_in_ruleOpOther3595 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_29_in_ruleOpOther3615 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_29_in_ruleOpOther3646 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_29_in_ruleOpOther3659 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_29_in_ruleOpOther3680 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_ruleOpOther3702 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_30_in_ruleOpOther3733 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_30_in_ruleOpOther3746 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_ruleOpOther3767 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_34_in_ruleOpOther3788 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_35_in_ruleOpOther3807 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_36_in_ruleOpOther3826 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXAdditiveExpression_in_entryRuleXAdditiveExpression3866 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleXAdditiveExpression3876 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXMultiplicativeExpression_in_ruleXAdditiveExpression3923 = new BitSet(new long[]{0x0000002000040002L});
    public static final BitSet FOLLOW_ruleOpAdd_in_ruleXAdditiveExpression3976 = new BitSet(new long[]{0x38A28420400501F0L,0x00000000000003FDL});
    public static final BitSet FOLLOW_ruleXMultiplicativeExpression_in_ruleXAdditiveExpression3999 = new BitSet(new long[]{0x0000002000040002L});
    public static final BitSet FOLLOW_ruleOpAdd_in_entryRuleOpAdd4038 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOpAdd4049 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_37_in_ruleOpAdd4087 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_ruleOpAdd4106 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXMultiplicativeExpression_in_entryRuleXMultiplicativeExpression4146 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleXMultiplicativeExpression4156 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXUnaryOperation_in_ruleXMultiplicativeExpression4203 = new BitSet(new long[]{0x000003C000000002L});
    public static final BitSet FOLLOW_ruleOpMulti_in_ruleXMultiplicativeExpression4256 = new BitSet(new long[]{0x38A28420400501F0L,0x00000000000003FDL});
    public static final BitSet FOLLOW_ruleXUnaryOperation_in_ruleXMultiplicativeExpression4279 = new BitSet(new long[]{0x000003C000000002L});
    public static final BitSet FOLLOW_ruleOpMulti_in_entryRuleOpMulti4318 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOpMulti4329 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_38_in_ruleOpMulti4367 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_39_in_ruleOpMulti4386 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_40_in_ruleOpMulti4405 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_41_in_ruleOpMulti4424 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXUnaryOperation_in_entryRuleXUnaryOperation4464 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleXUnaryOperation4474 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOpUnary_in_ruleXUnaryOperation4532 = new BitSet(new long[]{0x38A28420400501F0L,0x00000000000003FDL});
    public static final BitSet FOLLOW_ruleXCastedExpression_in_ruleXUnaryOperation4553 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXCastedExpression_in_ruleXUnaryOperation4582 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOpUnary_in_entryRuleOpUnary4618 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOpUnary4629 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_42_in_ruleOpUnary4667 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_ruleOpUnary4686 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_37_in_ruleOpUnary4705 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXCastedExpression_in_entryRuleXCastedExpression4745 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleXCastedExpression4755 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXMemberFeatureCall_in_ruleXCastedExpression4802 = new BitSet(new long[]{0x0000080000000002L});
    public static final BitSet FOLLOW_43_in_ruleXCastedExpression4837 = new BitSet(new long[]{0x0000800200000010L});
    public static final BitSet FOLLOW_ruleJvmTypeReference_in_ruleXCastedExpression4860 = new BitSet(new long[]{0x0000080000000002L});
    public static final BitSet FOLLOW_ruleXMemberFeatureCall_in_entryRuleXMemberFeatureCall4898 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleXMemberFeatureCall4908 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXPrimaryExpression_in_ruleXMemberFeatureCall4955 = new BitSet(new long[]{0x0000300000008002L});
    public static final BitSet FOLLOW_15_in_ruleXMemberFeatureCall5004 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleValidID_in_ruleXMemberFeatureCall5027 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_ruleOpSingleAssign_in_ruleXMemberFeatureCall5043 = new BitSet(new long[]{0x38A28420400501F0L,0x00000000000003FDL});
    public static final BitSet FOLLOW_ruleXAssignment_in_ruleXMemberFeatureCall5065 = new BitSet(new long[]{0x0000300000008002L});
    public static final BitSet FOLLOW_15_in_ruleXMemberFeatureCall5151 = new BitSet(new long[]{0x0000000040000010L});
    public static final BitSet FOLLOW_44_in_ruleXMemberFeatureCall5175 = new BitSet(new long[]{0x0000000040000010L});
    public static final BitSet FOLLOW_45_in_ruleXMemberFeatureCall5212 = new BitSet(new long[]{0x0000000040000010L});
    public static final BitSet FOLLOW_30_in_ruleXMemberFeatureCall5241 = new BitSet(new long[]{0x0000800200000010L,0x0000000000001000L});
    public static final BitSet FOLLOW_ruleJvmArgumentTypeReference_in_ruleXMemberFeatureCall5262 = new BitSet(new long[]{0x0000400020000000L});
    public static final BitSet FOLLOW_46_in_ruleXMemberFeatureCall5275 = new BitSet(new long[]{0x0000800200000010L,0x0000000000001000L});
    public static final BitSet FOLLOW_ruleJvmArgumentTypeReference_in_ruleXMemberFeatureCall5296 = new BitSet(new long[]{0x0000400020000000L});
    public static final BitSet FOLLOW_29_in_ruleXMemberFeatureCall5310 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleValidID_in_ruleXMemberFeatureCall5335 = new BitSet(new long[]{0x0002B00000008002L});
    public static final BitSet FOLLOW_47_in_ruleXMemberFeatureCall5369 = new BitSet(new long[]{0x38A78422400501F0L,0x00000000000003FDL});
    public static final BitSet FOLLOW_ruleXShortClosure_in_ruleXMemberFeatureCall5454 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_ruleXExpression_in_ruleXMemberFeatureCall5482 = new BitSet(new long[]{0x0001400000000000L});
    public static final BitSet FOLLOW_46_in_ruleXMemberFeatureCall5495 = new BitSet(new long[]{0x38A28420400501F0L,0x00000000000003FDL});
    public static final BitSet FOLLOW_ruleXExpression_in_ruleXMemberFeatureCall5516 = new BitSet(new long[]{0x0001400000000000L});
    public static final BitSet FOLLOW_48_in_ruleXMemberFeatureCall5533 = new BitSet(new long[]{0x0002300000008002L});
    public static final BitSet FOLLOW_ruleXClosure_in_ruleXMemberFeatureCall5568 = new BitSet(new long[]{0x0000300000008002L});
    public static final BitSet FOLLOW_ruleXPrimaryExpression_in_entryRuleXPrimaryExpression5608 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleXPrimaryExpression5618 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXConstructorCall_in_ruleXPrimaryExpression5665 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXBlockExpression_in_ruleXPrimaryExpression5692 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXSwitchExpression_in_ruleXPrimaryExpression5719 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXFeatureCall_in_ruleXPrimaryExpression5746 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXLiteral_in_ruleXPrimaryExpression5773 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXIfExpression_in_ruleXPrimaryExpression5800 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXForLoopExpression_in_ruleXPrimaryExpression5827 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXWhileExpression_in_ruleXPrimaryExpression5854 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXDoWhileExpression_in_ruleXPrimaryExpression5881 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXThrowExpression_in_ruleXPrimaryExpression5908 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXReturnExpression_in_ruleXPrimaryExpression5935 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXTryCatchFinallyExpression_in_ruleXPrimaryExpression5962 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXParenthesizedExpression_in_ruleXPrimaryExpression5989 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXLiteral_in_entryRuleXLiteral6024 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleXLiteral6034 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXClosure_in_ruleXLiteral6094 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXBooleanLiteral_in_ruleXLiteral6122 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXNumberLiteral_in_ruleXLiteral6149 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXNullLiteral_in_ruleXLiteral6176 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXStringLiteral_in_ruleXLiteral6203 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXTypeLiteral_in_ruleXLiteral6230 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXClosure_in_entryRuleXClosure6265 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleXClosure6275 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_49_in_ruleXClosure6335 = new BitSet(new long[]{0xF8AE8422400501F0L,0x00000000000003FDL});
    public static final BitSet FOLLOW_ruleJvmFormalParameter_in_ruleXClosure6408 = new BitSet(new long[]{0x0004400000000000L});
    public static final BitSet FOLLOW_46_in_ruleXClosure6421 = new BitSet(new long[]{0x0000800200000010L});
    public static final BitSet FOLLOW_ruleJvmFormalParameter_in_ruleXClosure6442 = new BitSet(new long[]{0x0004400000000000L});
    public static final BitSet FOLLOW_50_in_ruleXClosure6464 = new BitSet(new long[]{0xF8AA8420400501F0L,0x00000000000003FDL});
    public static final BitSet FOLLOW_ruleXExpressionInClosure_in_ruleXClosure6501 = new BitSet(new long[]{0x0008000000000000L});
    public static final BitSet FOLLOW_51_in_ruleXClosure6513 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXExpressionInClosure_in_entryRuleXExpressionInClosure6549 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleXExpressionInClosure6559 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXExpressionInsideBlock_in_ruleXExpressionInClosure6615 = new BitSet(new long[]{0xF8B28420400501F2L,0x00000000000003FDL});
    public static final BitSet FOLLOW_52_in_ruleXExpressionInClosure6628 = new BitSet(new long[]{0xF8A28420400501F2L,0x00000000000003FDL});
    public static final BitSet FOLLOW_ruleXShortClosure_in_entryRuleXShortClosure6668 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleXShortClosure6678 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleJvmFormalParameter_in_ruleXShortClosure6786 = new BitSet(new long[]{0x0004400000000000L});
    public static final BitSet FOLLOW_46_in_ruleXShortClosure6799 = new BitSet(new long[]{0x0000800200000010L});
    public static final BitSet FOLLOW_ruleJvmFormalParameter_in_ruleXShortClosure6820 = new BitSet(new long[]{0x0004400000000000L});
    public static final BitSet FOLLOW_50_in_ruleXShortClosure6842 = new BitSet(new long[]{0x38A28420400501F0L,0x00000000000003FDL});
    public static final BitSet FOLLOW_ruleXExpression_in_ruleXShortClosure6878 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXParenthesizedExpression_in_entryRuleXParenthesizedExpression6914 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleXParenthesizedExpression6924 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_47_in_ruleXParenthesizedExpression6961 = new BitSet(new long[]{0x38A28420400501F0L,0x00000000000003FDL});
    public static final BitSet FOLLOW_ruleXExpression_in_ruleXParenthesizedExpression6983 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_48_in_ruleXParenthesizedExpression6994 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXIfExpression_in_entryRuleXIfExpression7030 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleXIfExpression7040 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_53_in_ruleXIfExpression7086 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_47_in_ruleXIfExpression7098 = new BitSet(new long[]{0x38A28420400501F0L,0x00000000000003FDL});
    public static final BitSet FOLLOW_ruleXExpression_in_ruleXIfExpression7119 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_48_in_ruleXIfExpression7131 = new BitSet(new long[]{0x38A28420400501F0L,0x00000000000003FDL});
    public static final BitSet FOLLOW_ruleXExpression_in_ruleXIfExpression7152 = new BitSet(new long[]{0x0040000000000002L});
    public static final BitSet FOLLOW_54_in_ruleXIfExpression7173 = new BitSet(new long[]{0x38A28420400501F0L,0x00000000000003FDL});
    public static final BitSet FOLLOW_ruleXExpression_in_ruleXIfExpression7195 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXSwitchExpression_in_entryRuleXSwitchExpression7233 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleXSwitchExpression7243 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_55_in_ruleXSwitchExpression7289 = new BitSet(new long[]{0x38A28420400501F0L,0x00000000000003FDL});
    public static final BitSet FOLLOW_ruleValidID_in_ruleXSwitchExpression7332 = new BitSet(new long[]{0x0100000000000000L});
    public static final BitSet FOLLOW_56_in_ruleXSwitchExpression7344 = new BitSet(new long[]{0x38A28420400501F0L,0x00000000000003FDL});
    public static final BitSet FOLLOW_ruleXExpression_in_ruleXSwitchExpression7368 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_47_in_ruleXSwitchExpression7412 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleValidID_in_ruleXSwitchExpression7433 = new BitSet(new long[]{0x0100000000000000L});
    public static final BitSet FOLLOW_56_in_ruleXSwitchExpression7445 = new BitSet(new long[]{0x38A28420400501F0L,0x00000000000003FDL});
    public static final BitSet FOLLOW_ruleXExpression_in_ruleXSwitchExpression7468 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_48_in_ruleXSwitchExpression7480 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_ruleXSwitchExpression7494 = new BitSet(new long[]{0x0500800200000010L});
    public static final BitSet FOLLOW_ruleXCasePart_in_ruleXSwitchExpression7515 = new BitSet(new long[]{0x0700800200020010L});
    public static final BitSet FOLLOW_57_in_ruleXSwitchExpression7529 = new BitSet(new long[]{0x0100000000000000L});
    public static final BitSet FOLLOW_56_in_ruleXSwitchExpression7541 = new BitSet(new long[]{0x38A28420400501F0L,0x00000000000003FDL});
    public static final BitSet FOLLOW_ruleXExpression_in_ruleXSwitchExpression7562 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_ruleXSwitchExpression7576 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXCasePart_in_entryRuleXCasePart7612 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleXCasePart7622 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleJvmTypeReference_in_ruleXCasePart7668 = new BitSet(new long[]{0x0500000000000000L});
    public static final BitSet FOLLOW_58_in_ruleXCasePart7682 = new BitSet(new long[]{0x38A28420400501F0L,0x00000000000003FDL});
    public static final BitSet FOLLOW_ruleXExpression_in_ruleXCasePart7703 = new BitSet(new long[]{0x0100000000000000L});
    public static final BitSet FOLLOW_56_in_ruleXCasePart7717 = new BitSet(new long[]{0x38A28420400501F0L,0x00000000000003FDL});
    public static final BitSet FOLLOW_ruleXExpression_in_ruleXCasePart7738 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXForLoopExpression_in_entryRuleXForLoopExpression7774 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleXForLoopExpression7784 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_59_in_ruleXForLoopExpression7830 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_47_in_ruleXForLoopExpression7842 = new BitSet(new long[]{0x0000800200000010L});
    public static final BitSet FOLLOW_ruleJvmFormalParameter_in_ruleXForLoopExpression7863 = new BitSet(new long[]{0x0100000000000000L});
    public static final BitSet FOLLOW_56_in_ruleXForLoopExpression7875 = new BitSet(new long[]{0x38A28420400501F0L,0x00000000000003FDL});
    public static final BitSet FOLLOW_ruleXExpression_in_ruleXForLoopExpression7896 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_48_in_ruleXForLoopExpression7908 = new BitSet(new long[]{0x38A28420400501F0L,0x00000000000003FDL});
    public static final BitSet FOLLOW_ruleXExpression_in_ruleXForLoopExpression7929 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXWhileExpression_in_entryRuleXWhileExpression7965 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleXWhileExpression7975 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_60_in_ruleXWhileExpression8021 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_47_in_ruleXWhileExpression8033 = new BitSet(new long[]{0x38A28420400501F0L,0x00000000000003FDL});
    public static final BitSet FOLLOW_ruleXExpression_in_ruleXWhileExpression8054 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_48_in_ruleXWhileExpression8066 = new BitSet(new long[]{0x38A28420400501F0L,0x00000000000003FDL});
    public static final BitSet FOLLOW_ruleXExpression_in_ruleXWhileExpression8087 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXDoWhileExpression_in_entryRuleXDoWhileExpression8123 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleXDoWhileExpression8133 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_61_in_ruleXDoWhileExpression8179 = new BitSet(new long[]{0x38A28420400501F0L,0x00000000000003FDL});
    public static final BitSet FOLLOW_ruleXExpression_in_ruleXDoWhileExpression8200 = new BitSet(new long[]{0x1000000000000000L});
    public static final BitSet FOLLOW_60_in_ruleXDoWhileExpression8212 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_47_in_ruleXDoWhileExpression8224 = new BitSet(new long[]{0x38A28420400501F0L,0x00000000000003FDL});
    public static final BitSet FOLLOW_ruleXExpression_in_ruleXDoWhileExpression8245 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_48_in_ruleXDoWhileExpression8257 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXBlockExpression_in_entryRuleXBlockExpression8293 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleXBlockExpression8303 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_ruleXBlockExpression8349 = new BitSet(new long[]{0xF8A28420400701F0L,0x00000000000003FDL});
    public static final BitSet FOLLOW_ruleXExpressionInsideBlock_in_ruleXBlockExpression8371 = new BitSet(new long[]{0xF8B28420400701F0L,0x00000000000003FDL});
    public static final BitSet FOLLOW_52_in_ruleXBlockExpression8384 = new BitSet(new long[]{0xF8A28420400701F0L,0x00000000000003FDL});
    public static final BitSet FOLLOW_17_in_ruleXBlockExpression8400 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXExpressionInsideBlock_in_entryRuleXExpressionInsideBlock8436 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleXExpressionInsideBlock8446 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXVariableDeclaration_in_ruleXExpressionInsideBlock8493 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXExpression_in_ruleXExpressionInsideBlock8520 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXVariableDeclaration_in_entryRuleXVariableDeclaration8555 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleXVariableDeclaration8565 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_62_in_ruleXVariableDeclaration8618 = new BitSet(new long[]{0x0000800200000010L});
    public static final BitSet FOLLOW_63_in_ruleXVariableDeclaration8649 = new BitSet(new long[]{0x0000800200000010L});
    public static final BitSet FOLLOW_ruleJvmTypeReference_in_ruleXVariableDeclaration8697 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleValidID_in_ruleXVariableDeclaration8718 = new BitSet(new long[]{0x0000000000100002L});
    public static final BitSet FOLLOW_ruleValidID_in_ruleXVariableDeclaration8747 = new BitSet(new long[]{0x0000000000100002L});
    public static final BitSet FOLLOW_20_in_ruleXVariableDeclaration8761 = new BitSet(new long[]{0x38A28420400501F0L,0x00000000000003FDL});
    public static final BitSet FOLLOW_ruleXExpression_in_ruleXVariableDeclaration8782 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleJvmFormalParameter_in_entryRuleJvmFormalParameter8820 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleJvmFormalParameter8830 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleJvmTypeReference_in_ruleJvmFormalParameter8876 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleValidID_in_ruleJvmFormalParameter8898 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFullJvmFormalParameter_in_entryRuleFullJvmFormalParameter8934 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleFullJvmFormalParameter8944 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleJvmTypeReference_in_ruleFullJvmFormalParameter8990 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleValidID_in_ruleFullJvmFormalParameter9011 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXFeatureCall_in_entryRuleXFeatureCall9047 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleXFeatureCall9057 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleStaticQualifier_in_ruleXFeatureCall9114 = new BitSet(new long[]{0x0000000040000010L,0x0000000000000001L});
    public static final BitSet FOLLOW_30_in_ruleXFeatureCall9128 = new BitSet(new long[]{0x0000800200000010L,0x0000000000001000L});
    public static final BitSet FOLLOW_ruleJvmArgumentTypeReference_in_ruleXFeatureCall9149 = new BitSet(new long[]{0x0000400020000000L});
    public static final BitSet FOLLOW_46_in_ruleXFeatureCall9162 = new BitSet(new long[]{0x0000800200000010L,0x0000000000001000L});
    public static final BitSet FOLLOW_ruleJvmArgumentTypeReference_in_ruleXFeatureCall9183 = new BitSet(new long[]{0x0000400020000000L});
    public static final BitSet FOLLOW_29_in_ruleXFeatureCall9197 = new BitSet(new long[]{0x0000000040000010L,0x0000000000000001L});
    public static final BitSet FOLLOW_ruleIdOrSuper_in_ruleXFeatureCall9222 = new BitSet(new long[]{0x0002800000000002L});
    public static final BitSet FOLLOW_47_in_ruleXFeatureCall9256 = new BitSet(new long[]{0x38A78422400501F0L,0x00000000000003FDL});
    public static final BitSet FOLLOW_ruleXShortClosure_in_ruleXFeatureCall9341 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_ruleXExpression_in_ruleXFeatureCall9369 = new BitSet(new long[]{0x0001400000000000L});
    public static final BitSet FOLLOW_46_in_ruleXFeatureCall9382 = new BitSet(new long[]{0x38A28420400501F0L,0x00000000000003FDL});
    public static final BitSet FOLLOW_ruleXExpression_in_ruleXFeatureCall9403 = new BitSet(new long[]{0x0001400000000000L});
    public static final BitSet FOLLOW_48_in_ruleXFeatureCall9420 = new BitSet(new long[]{0x0002000000000002L});
    public static final BitSet FOLLOW_ruleXClosure_in_ruleXFeatureCall9455 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIdOrSuper_in_entryRuleIdOrSuper9493 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIdOrSuper9504 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleValidID_in_ruleIdOrSuper9551 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_64_in_ruleIdOrSuper9575 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleStaticQualifier_in_entryRuleStaticQualifier9616 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleStaticQualifier9627 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleValidID_in_ruleStaticQualifier9674 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000002L});
    public static final BitSet FOLLOW_65_in_ruleStaticQualifier9692 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_ruleXConstructorCall_in_entryRuleXConstructorCall9733 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleXConstructorCall9743 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_66_in_ruleXConstructorCall9789 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleQualifiedName_in_ruleXConstructorCall9812 = new BitSet(new long[]{0x0002800040000002L});
    public static final BitSet FOLLOW_30_in_ruleXConstructorCall9833 = new BitSet(new long[]{0x0000800200000010L,0x0000000000001000L});
    public static final BitSet FOLLOW_ruleJvmArgumentTypeReference_in_ruleXConstructorCall9855 = new BitSet(new long[]{0x0000400020000000L});
    public static final BitSet FOLLOW_46_in_ruleXConstructorCall9868 = new BitSet(new long[]{0x0000800200000010L,0x0000000000001000L});
    public static final BitSet FOLLOW_ruleJvmArgumentTypeReference_in_ruleXConstructorCall9889 = new BitSet(new long[]{0x0000400020000000L});
    public static final BitSet FOLLOW_29_in_ruleXConstructorCall9903 = new BitSet(new long[]{0x0002800000000002L});
    public static final BitSet FOLLOW_47_in_ruleXConstructorCall9926 = new BitSet(new long[]{0x38A78422400501F0L,0x00000000000003FDL});
    public static final BitSet FOLLOW_ruleXShortClosure_in_ruleXConstructorCall9999 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_ruleXExpression_in_ruleXConstructorCall10027 = new BitSet(new long[]{0x0001400000000000L});
    public static final BitSet FOLLOW_46_in_ruleXConstructorCall10040 = new BitSet(new long[]{0x38A28420400501F0L,0x00000000000003FDL});
    public static final BitSet FOLLOW_ruleXExpression_in_ruleXConstructorCall10061 = new BitSet(new long[]{0x0001400000000000L});
    public static final BitSet FOLLOW_48_in_ruleXConstructorCall10078 = new BitSet(new long[]{0x0002000000000002L});
    public static final BitSet FOLLOW_ruleXClosure_in_ruleXConstructorCall10113 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXBooleanLiteral_in_entryRuleXBooleanLiteral10150 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleXBooleanLiteral10160 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_67_in_ruleXBooleanLiteral10207 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_68_in_ruleXBooleanLiteral10231 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXNullLiteral_in_entryRuleXNullLiteral10281 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleXNullLiteral10291 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_69_in_ruleXNullLiteral10337 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXNumberLiteral_in_entryRuleXNumberLiteral10373 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleXNumberLiteral10383 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumber_in_ruleXNumberLiteral10438 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXStringLiteral_in_entryRuleXStringLiteral10474 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleXStringLiteral10484 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleXStringLiteral10535 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXTypeLiteral_in_entryRuleXTypeLiteral10576 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleXTypeLiteral10586 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_70_in_ruleXTypeLiteral10632 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_47_in_ruleXTypeLiteral10644 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleQualifiedName_in_ruleXTypeLiteral10667 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_48_in_ruleXTypeLiteral10679 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXThrowExpression_in_entryRuleXThrowExpression10715 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleXThrowExpression10725 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_71_in_ruleXThrowExpression10771 = new BitSet(new long[]{0x38A28420400501F0L,0x00000000000003FDL});
    public static final BitSet FOLLOW_ruleXExpression_in_ruleXThrowExpression10792 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXReturnExpression_in_entryRuleXReturnExpression10828 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleXReturnExpression10838 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_72_in_ruleXReturnExpression10884 = new BitSet(new long[]{0x38A28420400501F2L,0x00000000000003FDL});
    public static final BitSet FOLLOW_ruleXExpression_in_ruleXReturnExpression10915 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXTryCatchFinallyExpression_in_entryRuleXTryCatchFinallyExpression10952 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleXTryCatchFinallyExpression10962 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_73_in_ruleXTryCatchFinallyExpression11008 = new BitSet(new long[]{0x38A28420400501F0L,0x00000000000003FDL});
    public static final BitSet FOLLOW_ruleXExpression_in_ruleXTryCatchFinallyExpression11029 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000C00L});
    public static final BitSet FOLLOW_ruleXCatchClause_in_ruleXTryCatchFinallyExpression11059 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000C00L});
    public static final BitSet FOLLOW_74_in_ruleXTryCatchFinallyExpression11081 = new BitSet(new long[]{0x38A28420400501F0L,0x00000000000003FDL});
    public static final BitSet FOLLOW_ruleXExpression_in_ruleXTryCatchFinallyExpression11103 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_74_in_ruleXTryCatchFinallyExpression11125 = new BitSet(new long[]{0x38A28420400501F0L,0x00000000000003FDL});
    public static final BitSet FOLLOW_ruleXExpression_in_ruleXTryCatchFinallyExpression11146 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXCatchClause_in_entryRuleXCatchClause11184 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleXCatchClause11194 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_75_in_ruleXCatchClause11239 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_47_in_ruleXCatchClause11252 = new BitSet(new long[]{0x0000800200000010L});
    public static final BitSet FOLLOW_ruleFullJvmFormalParameter_in_ruleXCatchClause11273 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_48_in_ruleXCatchClause11285 = new BitSet(new long[]{0x38A28420400501F0L,0x00000000000003FDL});
    public static final BitSet FOLLOW_ruleXExpression_in_ruleXCatchClause11306 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualifiedName_in_entryRuleQualifiedName11343 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleQualifiedName11354 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleValidID_in_ruleQualifiedName11401 = new BitSet(new long[]{0x0000000000008002L});
    public static final BitSet FOLLOW_15_in_ruleQualifiedName11429 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleValidID_in_ruleQualifiedName11452 = new BitSet(new long[]{0x0000000000008002L});
    public static final BitSet FOLLOW_ruleNumber_in_entryRuleNumber11506 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleNumber11517 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_HEX_in_ruleNumber11561 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleNumber11589 = new BitSet(new long[]{0x0000000000008002L});
    public static final BitSet FOLLOW_RULE_DECIMAL_in_ruleNumber11615 = new BitSet(new long[]{0x0000000000008002L});
    public static final BitSet FOLLOW_15_in_ruleNumber11635 = new BitSet(new long[]{0x0000000000000180L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleNumber11651 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_DECIMAL_in_ruleNumber11677 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleJvmTypeReference_in_entryRuleJvmTypeReference11730 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleJvmTypeReference11740 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleJvmParameterizedTypeReference_in_ruleJvmTypeReference11788 = new BitSet(new long[]{0x0002000000000002L});
    public static final BitSet FOLLOW_49_in_ruleJvmTypeReference11826 = new BitSet(new long[]{0x0008000000000000L});
    public static final BitSet FOLLOW_51_in_ruleJvmTypeReference11838 = new BitSet(new long[]{0x0002000000000002L});
    public static final BitSet FOLLOW_ruleXFunctionTypeRef_in_ruleJvmTypeReference11870 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXFunctionTypeRef_in_entryRuleXFunctionTypeRef11905 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleXFunctionTypeRef11915 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_47_in_ruleXFunctionTypeRef11953 = new BitSet(new long[]{0x0001800200000010L});
    public static final BitSet FOLLOW_ruleJvmTypeReference_in_ruleXFunctionTypeRef11975 = new BitSet(new long[]{0x0001400000000000L});
    public static final BitSet FOLLOW_46_in_ruleXFunctionTypeRef11988 = new BitSet(new long[]{0x0000800200000010L});
    public static final BitSet FOLLOW_ruleJvmTypeReference_in_ruleXFunctionTypeRef12009 = new BitSet(new long[]{0x0001400000000000L});
    public static final BitSet FOLLOW_48_in_ruleXFunctionTypeRef12025 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_33_in_ruleXFunctionTypeRef12039 = new BitSet(new long[]{0x0000800200000010L});
    public static final BitSet FOLLOW_ruleJvmTypeReference_in_ruleXFunctionTypeRef12060 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleJvmParameterizedTypeReference_in_entryRuleJvmParameterizedTypeReference12096 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleJvmParameterizedTypeReference12106 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualifiedName_in_ruleJvmParameterizedTypeReference12154 = new BitSet(new long[]{0x0000000040000002L});
    public static final BitSet FOLLOW_30_in_ruleJvmParameterizedTypeReference12175 = new BitSet(new long[]{0x0000800200000010L,0x0000000000001000L});
    public static final BitSet FOLLOW_ruleJvmArgumentTypeReference_in_ruleJvmParameterizedTypeReference12197 = new BitSet(new long[]{0x0000400020000000L});
    public static final BitSet FOLLOW_46_in_ruleJvmParameterizedTypeReference12210 = new BitSet(new long[]{0x0000800200000010L,0x0000000000001000L});
    public static final BitSet FOLLOW_ruleJvmArgumentTypeReference_in_ruleJvmParameterizedTypeReference12231 = new BitSet(new long[]{0x0000400020000000L});
    public static final BitSet FOLLOW_29_in_ruleJvmParameterizedTypeReference12245 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleJvmArgumentTypeReference_in_entryRuleJvmArgumentTypeReference12283 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleJvmArgumentTypeReference12293 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleJvmTypeReference_in_ruleJvmArgumentTypeReference12340 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleJvmWildcardTypeReference_in_ruleJvmArgumentTypeReference12367 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleJvmWildcardTypeReference_in_entryRuleJvmWildcardTypeReference12402 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleJvmWildcardTypeReference12412 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_76_in_ruleJvmWildcardTypeReference12458 = new BitSet(new long[]{0x0000000000000002L,0x0000000000002001L});
    public static final BitSet FOLLOW_ruleJvmUpperBound_in_ruleJvmWildcardTypeReference12480 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleJvmLowerBound_in_ruleJvmWildcardTypeReference12507 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleJvmUpperBound_in_entryRuleJvmUpperBound12545 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleJvmUpperBound12555 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_77_in_ruleJvmUpperBound12592 = new BitSet(new long[]{0x0000800200000010L});
    public static final BitSet FOLLOW_ruleJvmTypeReference_in_ruleJvmUpperBound12613 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleJvmUpperBoundAnded_in_entryRuleJvmUpperBoundAnded12649 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleJvmUpperBoundAnded12659 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_ruleJvmUpperBoundAnded12696 = new BitSet(new long[]{0x0000800200000010L});
    public static final BitSet FOLLOW_ruleJvmTypeReference_in_ruleJvmUpperBoundAnded12717 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleJvmLowerBound_in_entryRuleJvmLowerBound12753 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleJvmLowerBound12763 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_64_in_ruleJvmLowerBound12800 = new BitSet(new long[]{0x0000800200000010L});
    public static final BitSet FOLLOW_ruleJvmTypeReference_in_ruleJvmLowerBound12821 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleValidID_in_entryRuleValidID12860 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleValidID12871 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleValidID12910 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOpMultiAssign_in_synpred1_InternalDeepClone1882 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOpOr_in_synpred2_InternalDeepClone2230 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOpAnd_in_synpred3_InternalDeepClone2489 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOpEquality_in_synpred4_InternalDeepClone2748 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_26_in_synpred5_InternalDeepClone3024 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOpCompare_in_synpred6_InternalDeepClone3095 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOpOther_in_synpred7_InternalDeepClone3414 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_29_in_synpred8_InternalDeepClone3630 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_29_in_synpred8_InternalDeepClone3635 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_synpred9_InternalDeepClone3717 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_30_in_synpred9_InternalDeepClone3722 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOpAdd_in_synpred10_InternalDeepClone3944 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOpMulti_in_synpred11_InternalDeepClone4224 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_43_in_synpred12_InternalDeepClone4818 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_synpred13_InternalDeepClone4972 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleValidID_in_synpred13_InternalDeepClone4981 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_ruleOpSingleAssign_in_synpred13_InternalDeepClone4987 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_synpred14_InternalDeepClone5090 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_44_in_synpred14_InternalDeepClone5104 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_45_in_synpred14_InternalDeepClone5124 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_47_in_synpred15_InternalDeepClone5351 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleJvmFormalParameter_in_synpred16_InternalDeepClone5403 = new BitSet(new long[]{0x0004400000000000L});
    public static final BitSet FOLLOW_46_in_synpred16_InternalDeepClone5410 = new BitSet(new long[]{0x0000800200000010L});
    public static final BitSet FOLLOW_ruleJvmFormalParameter_in_synpred16_InternalDeepClone5417 = new BitSet(new long[]{0x0004400000000000L});
    public static final BitSet FOLLOW_50_in_synpred16_InternalDeepClone5431 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_49_in_synpred17_InternalDeepClone5551 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_49_in_synpred18_InternalDeepClone6075 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleJvmFormalParameter_in_synpred20_InternalDeepClone6354 = new BitSet(new long[]{0x0004400000000000L});
    public static final BitSet FOLLOW_46_in_synpred20_InternalDeepClone6361 = new BitSet(new long[]{0x0000800200000010L});
    public static final BitSet FOLLOW_ruleJvmFormalParameter_in_synpred20_InternalDeepClone6368 = new BitSet(new long[]{0x0004400000000000L});
    public static final BitSet FOLLOW_50_in_synpred20_InternalDeepClone6382 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_54_in_synpred22_InternalDeepClone7165 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleValidID_in_synpred23_InternalDeepClone7307 = new BitSet(new long[]{0x0100000000000000L});
    public static final BitSet FOLLOW_56_in_synpred23_InternalDeepClone7313 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_47_in_synpred24_InternalDeepClone7389 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleValidID_in_synpred24_InternalDeepClone7396 = new BitSet(new long[]{0x0100000000000000L});
    public static final BitSet FOLLOW_56_in_synpred24_InternalDeepClone7402 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleJvmTypeReference_in_synpred25_InternalDeepClone8667 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleValidID_in_synpred25_InternalDeepClone8676 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_47_in_synpred26_InternalDeepClone9238 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleJvmFormalParameter_in_synpred27_InternalDeepClone9290 = new BitSet(new long[]{0x0004400000000000L});
    public static final BitSet FOLLOW_46_in_synpred27_InternalDeepClone9297 = new BitSet(new long[]{0x0000800200000010L});
    public static final BitSet FOLLOW_ruleJvmFormalParameter_in_synpred27_InternalDeepClone9304 = new BitSet(new long[]{0x0004400000000000L});
    public static final BitSet FOLLOW_50_in_synpred27_InternalDeepClone9318 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_49_in_synpred28_InternalDeepClone9438 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_synpred29_InternalDeepClone9825 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_47_in_synpred30_InternalDeepClone9918 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleJvmFormalParameter_in_synpred31_InternalDeepClone9948 = new BitSet(new long[]{0x0004400000000000L});
    public static final BitSet FOLLOW_46_in_synpred31_InternalDeepClone9955 = new BitSet(new long[]{0x0000800200000010L});
    public static final BitSet FOLLOW_ruleJvmFormalParameter_in_synpred31_InternalDeepClone9962 = new BitSet(new long[]{0x0004400000000000L});
    public static final BitSet FOLLOW_50_in_synpred31_InternalDeepClone9976 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_49_in_synpred32_InternalDeepClone10096 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXExpression_in_synpred33_InternalDeepClone10898 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_75_in_synpred34_InternalDeepClone11043 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_74_in_synpred35_InternalDeepClone11073 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_synpred37_InternalDeepClone11420 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_49_in_synpred38_InternalDeepClone11803 = new BitSet(new long[]{0x0008000000000000L});
    public static final BitSet FOLLOW_51_in_synpred38_InternalDeepClone11807 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_synpred39_InternalDeepClone12167 = new BitSet(new long[]{0x0000000000000002L});

}