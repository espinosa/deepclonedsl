/**
 */
package my.home.dsl.deepClone.impl;

import my.home.dsl.deepClone.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DeepCloneFactoryImpl extends EFactoryImpl implements DeepCloneFactory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static DeepCloneFactory init()
  {
    try
    {
      DeepCloneFactory theDeepCloneFactory = (DeepCloneFactory)EPackage.Registry.INSTANCE.getEFactory("http://www.home.my/dsl/DeepClone"); 
      if (theDeepCloneFactory != null)
      {
        return theDeepCloneFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new DeepCloneFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DeepCloneFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case DeepClonePackage.MODEL: return createModel();
      case DeepClonePackage.BODY: return createBody();
      case DeepClonePackage.PACKAGE_CONFIG: return createPackageConfig();
      case DeepClonePackage.CLASS_CLONER: return createClassCloner();
      case DeepClonePackage.FIELD_CLONER_TYPE: return createFieldClonerType();
      case DeepClonePackage.SIMPLE_FIELD: return createSimpleField();
      case DeepClonePackage.SIMPLE_EXCLUDED_FIELD: return createSimpleExcludedField();
      case DeepClonePackage.COMPLEX_FIELD: return createComplexField();
      case DeepClonePackage.REFERENCE_FIELD: return createReferenceField();
      case DeepClonePackage.BASE_TYPE: return createBaseType();
      case DeepClonePackage.CONTAINER_TYPE: return createContainerType();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Model createModel()
  {
    ModelImpl model = new ModelImpl();
    return model;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Body createBody()
  {
    BodyImpl body = new BodyImpl();
    return body;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PackageConfig createPackageConfig()
  {
    PackageConfigImpl packageConfig = new PackageConfigImpl();
    return packageConfig;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ClassCloner createClassCloner()
  {
    ClassClonerImpl classCloner = new ClassClonerImpl();
    return classCloner;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FieldClonerType createFieldClonerType()
  {
    FieldClonerTypeImpl fieldClonerType = new FieldClonerTypeImpl();
    return fieldClonerType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SimpleField createSimpleField()
  {
    SimpleFieldImpl simpleField = new SimpleFieldImpl();
    return simpleField;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SimpleExcludedField createSimpleExcludedField()
  {
    SimpleExcludedFieldImpl simpleExcludedField = new SimpleExcludedFieldImpl();
    return simpleExcludedField;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ComplexField createComplexField()
  {
    ComplexFieldImpl complexField = new ComplexFieldImpl();
    return complexField;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ReferenceField createReferenceField()
  {
    ReferenceFieldImpl referenceField = new ReferenceFieldImpl();
    return referenceField;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BaseType createBaseType()
  {
    BaseTypeImpl baseType = new BaseTypeImpl();
    return baseType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ContainerType createContainerType()
  {
    ContainerTypeImpl containerType = new ContainerTypeImpl();
    return containerType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DeepClonePackage getDeepClonePackage()
  {
    return (DeepClonePackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static DeepClonePackage getPackage()
  {
    return DeepClonePackage.eINSTANCE;
  }

} //DeepCloneFactoryImpl
