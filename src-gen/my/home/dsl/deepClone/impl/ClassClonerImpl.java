/**
 */
package my.home.dsl.deepClone.impl;

import my.home.dsl.deepClone.ClassCloner;
import my.home.dsl.deepClone.DeepClonePackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.xtext.common.types.JvmTypeReference;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Class Cloner</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link my.home.dsl.deepClone.impl.ClassClonerImpl#getClassToClone <em>Class To Clone</em>}</li>
 *   <li>{@link my.home.dsl.deepClone.impl.ClassClonerImpl#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ClassClonerImpl extends ContainerTypeImpl implements ClassCloner
{
  /**
   * The cached value of the '{@link #getClassToClone() <em>Class To Clone</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getClassToClone()
   * @generated
   * @ordered
   */
  protected JvmTypeReference classToClone;

  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ClassClonerImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return DeepClonePackage.Literals.CLASS_CLONER;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public JvmTypeReference getClassToClone()
  {
    return classToClone;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetClassToClone(JvmTypeReference newClassToClone, NotificationChain msgs)
  {
    JvmTypeReference oldClassToClone = classToClone;
    classToClone = newClassToClone;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DeepClonePackage.CLASS_CLONER__CLASS_TO_CLONE, oldClassToClone, newClassToClone);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setClassToClone(JvmTypeReference newClassToClone)
  {
    if (newClassToClone != classToClone)
    {
      NotificationChain msgs = null;
      if (classToClone != null)
        msgs = ((InternalEObject)classToClone).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DeepClonePackage.CLASS_CLONER__CLASS_TO_CLONE, null, msgs);
      if (newClassToClone != null)
        msgs = ((InternalEObject)newClassToClone).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DeepClonePackage.CLASS_CLONER__CLASS_TO_CLONE, null, msgs);
      msgs = basicSetClassToClone(newClassToClone, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DeepClonePackage.CLASS_CLONER__CLASS_TO_CLONE, newClassToClone, newClassToClone));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName)
  {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DeepClonePackage.CLASS_CLONER__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case DeepClonePackage.CLASS_CLONER__CLASS_TO_CLONE:
        return basicSetClassToClone(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case DeepClonePackage.CLASS_CLONER__CLASS_TO_CLONE:
        return getClassToClone();
      case DeepClonePackage.CLASS_CLONER__NAME:
        return getName();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case DeepClonePackage.CLASS_CLONER__CLASS_TO_CLONE:
        setClassToClone((JvmTypeReference)newValue);
        return;
      case DeepClonePackage.CLASS_CLONER__NAME:
        setName((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case DeepClonePackage.CLASS_CLONER__CLASS_TO_CLONE:
        setClassToClone((JvmTypeReference)null);
        return;
      case DeepClonePackage.CLASS_CLONER__NAME:
        setName(NAME_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case DeepClonePackage.CLASS_CLONER__CLASS_TO_CLONE:
        return classToClone != null;
      case DeepClonePackage.CLASS_CLONER__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (name: ");
    result.append(name);
    result.append(')');
    return result.toString();
  }

} //ClassClonerImpl
