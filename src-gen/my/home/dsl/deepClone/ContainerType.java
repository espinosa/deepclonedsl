/**
 */
package my.home.dsl.deepClone;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Container Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link my.home.dsl.deepClone.ContainerType#getFields <em>Fields</em>}</li>
 * </ul>
 * </p>
 *
 * @see my.home.dsl.deepClone.DeepClonePackage#getContainerType()
 * @model
 * @generated
 */
public interface ContainerType extends BaseType
{
  /**
   * Returns the value of the '<em><b>Fields</b></em>' containment reference list.
   * The list contents are of type {@link my.home.dsl.deepClone.FieldClonerType}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Fields</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Fields</em>' containment reference list.
   * @see my.home.dsl.deepClone.DeepClonePackage#getContainerType_Fields()
   * @model containment="true"
   * @generated
   */
  EList<FieldClonerType> getFields();

} // ContainerType
