package my.home.dsl.utils;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import my.home.dsl.deepClone.ClassCloner;
import my.home.dsl.deepClone.ContainerType;
import my.home.dsl.deepClone.FieldClonerType;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.common.types.JvmTypeReference;
import org.eclipse.xtext.common.types.util.TypeReferences;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.inject.Inject;

@SuppressWarnings("restriction")
public class DeepCloneUtils {
	
	@Inject ReflectionUtils reflectionUtils;
	
	@Inject TypeReferences typeReferences;
	
	/**
	 * Infer Java types for whole model AST. Variant with model root auto finder.
	 * @param someElement root any node from the model AST
	 */
	public void inferrJavaTypesFromAnywhere(EObject someElement) {
		inferrJavaTypes(findModelRoot(someElement));
	}
	
	/**
	 * Infer Java types for whole model AST  
	 * @param modelRoot root node of the model AST
	 */
	public void inferrJavaTypes(EObject modelRoot) {
		for (EObject child : modelRoot.eContents()) {
			if (child instanceof ClassCloner) {
				ClassCloner rootCloner = (ClassCloner)child;
				rootCloner.setJavaType(reflectionUtils.createDefensiveCopyOfJvmTypeReference(rootCloner.getClassToClone()));
			} else if (child instanceof FieldClonerType) {
				FieldClonerType fieldCloner = (FieldClonerType)child;
				JvmTypeReference parentType = ((ContainerType)fieldCloner.eContainer()).getJavaType();
				JvmTypeReference fieldType  = reflectionUtils.getFieldType(parentType.getType(), fieldCloner.getFieldName());
				fieldCloner.setJavaType(reflectionUtils.createDefensiveCopyOfJvmTypeReference(fieldType));
			}
			if (child.eContents()!=null && child.eContents().size()>0) {
				inferrJavaTypes(child);
			}
		}
	}
	
	/**
	 * Find model root from any model AST node
	 * @param element any node from model AST
	 * @return root node
	 */
	public EObject findModelRoot(EObject element) {
		EObject parent;
		while ((parent = element.eContainer())!=null) {
			element = parent;
		}
		return element;
	}
	
	/**
	 * Predicate for Google Collections filtering
	 */
	public Function<Field, String> reflFieldNameFunc = new Function<Field, String>() {
		@Override public String apply(Field f) {
			return f.getName();
		}
	};

	/**
	 * Predicate for Google Collections filtering
	 */
	public Function<FieldClonerType, String> fieldClonerNameFunc = new Function<FieldClonerType, String>() {
		@Override public String apply(FieldClonerType f) {
			return f.getFieldName();
		}
	};
	
	/**
	 * @return true if given field is a collection type, like List, Set, .. 
	 * Used to check 1:N relations between beans.
	 */
	public boolean isCollection(FieldClonerType field) {
		return typeReferences.isInstanceOf(field.getJavaType(), Collection.class);
	}

	/**
	 * Get full name for given Model element.
	 * Intended for use in toString() style methods and error messages.
	 * Skip parts with no name or unrecognised name.
	 */
	public String getNodeQualifiedName(EObject element) {
		List<String> result = new LinkedList<String>();
		do {
			String name = getSuitableNameForElement(element);
			if (name != null) {
				result.add(name);
			}
		} while ((element = element.eContainer()) != null);
		return Joiner.on(".").join(Lists.reverse(result));
	}
	
	/**
	 * Get suitable name for given Model element.
	 * Intended for use in toString() style methods and error messages.
	 * Get the name for known types from the DSL only.
	 */
	public String getSuitableNameForElement(EObject element) {
		String result = null;
		if (element instanceof FieldClonerType) {
			result = ((FieldClonerType)element).getFieldName();
		} else if (element instanceof ClassCloner) {
			ClassCloner cc = (ClassCloner)element;
			if (cc.getName() != null) {
				result = cc.getName();
			} else if (cc.getClassToClone() != null && cc.getClassToClone().getType() != null) {
				result = cc.getClassToClone().getType().getSimpleName();
			}
		}
		return result;
	}
}
